function [md, mdM, mnM] = data_processing3(maskArea, filtSize)

DEBUG = 1;

if nargin == 0
    maskArea = 0.9;
    filtSize = [21 21];
end
if nargin == 1
    filtSize = [21 21];
end

% =========================================================================
%
% CHOOSE ALL IMAGES
%
% =========================================================================

% Get first mage for segmentation
[filenames, pathname] = uigetfile('*.tif', 'Pick other images', ...
    'MultiSelect', 'on');
if isequal(filenames, 0)
    return
end
if ~iscell(filenames)
    error('You must choose at least two images.');
end
nFiles = numel(filenames);

% =========================================================================
%
% ALLOCATE MEMORY FOR RESULTS
%
% =========================================================================

% Allocate memory for results
md  = zeros(1, nFiles);
mdM = zeros(1, nFiles);
mnM = zeros(1, nFiles);

% =========================================================================
%
% SEGMENT AND ANALYZE FIRST IMAGE
%
% =========================================================================

% Read the image
img = imread(fullfile(pathname, filenames{1}));

% Low-pas filter
h = fspecial('gaussian', filtSize, 5);
I = double(imfilter(img, h, 'symmetric', 'same'));

% Normalize
mn = min(I(:));
mx = max(I(:));
J = (I - mn) ./ (mx - mn);

% Find the optimal threshold
thresh = 0.25;
thresh = fminsearch(@(thresh) findOptThresh(thresh, J, maskArea), thresh);
fprintf(1, 'Optimal threshold: %6.4f\n', thresh);

% Threshold
J = J > thresh;

% Display masked image for quality control
if DEBUG == 1
    figure;
    img3 = zeros([size(img), 3], class(img));
    img3(:, :, 1) = cast(double(max(img(:))) .* J, class(img));
    img3(:, :, 2) = img;
    imshow(img3, []);
end

% Get the pixels inside the mask
maskedInd = find(J);

% Store the first image statistics
md(1)  = median(img(:));
mdM(1) = median(img(maskedInd));
mnM(1) = mean(img(maskedInd));

% =========================================================================
%
% APPLY MASK TO OTHER IMAGES
%
% =========================================================================

for i = 2 : nFiles
    
    % Read current image
    img = imread(fullfile(pathname, filenames{i}));

    % Store the first image statistics
    md(i)  = median(img(:));
    mdM(i) = median(img(maskedInd));
    mnM(i) = mean(img(maskedInd));

end

% =========================================================================
%
% PLOT THE CURVES
%
% =========================================================================

if DEBUG == 1
    figure;
    plot(md,  'r-');
    hold on;
    plot(mdM, 'k-');
    plot(mnM, 'b-');
    legend('Median (all)', 'Median', 'Mean', 'Location', 'Best');
end
