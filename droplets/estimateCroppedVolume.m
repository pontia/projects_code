function [vOutside, V, f] = estimateCroppedVolume(radius, xC, yC, imSizeX, imSizeY, step)

DEBUG = 0;

areaFactor = (1 / step) ^ 2;

% =========================================================================
%
% GET ALL RELEVANT BOUNDARIES
%
% =========================================================================

xT = imSizeX / 2 - xC;
yT = imSizeY / 2 - yC;

% Circle coordinates
t = -pi : 0.0001 : pi;
xCircle = radius .* cos(t);
yCircle = radius .* sin(t);

% Image boundaries
x0Img = -imSizeX / 2 + xT;
xImg  =  imSizeX / 2 + xT;
y0Img = -imSizeY / 2 + yT;
yImg  =  imSizeY / 2 + yT;

% All range
x0Global = min(x0Img, min(xCircle));
y0Global = min(y0Img, min(yCircle));
xGlobal  = max(xImg,  max(xCircle));
yGlobal  = max(yImg,  max(yCircle));

% Quality control figure
if DEBUG == 1

    figure; plot(xCircle, yCircle, 'r-'); hold on
    axis equal
    plot(xT, yT, 'ko'); text(xT + 10, yT + 10, '(xT, yT)');
    plot(0, 0, 'r*'); text(10, 10, '(xC, yC)');
    
    plot([x0Img x0Img], [y0Img yImg ], 'b-');
    plot([xImg  xImg ], [y0Img yImg ], 'b-');
    plot([x0Img xImg ], [y0Img y0Img], 'b-');
    plot([x0Img xImg ], [yImg  yImg ], 'b-');

    plot([x0Global x0Global], [y0Global yGlobal ], 'g-');
    plot([xGlobal  xGlobal ], [y0Global yGlobal ], 'g-');
    plot([x0Global xGlobal ], [y0Global y0Global], 'g-');
    plot([x0Global xGlobal ], [yGlobal  yGlobal ], 'g-');
    
    % Plot with y reversed to match display of images
    axis ij

    title('z = 0');

end

% =========================================================================
%
% FIND ALL (X,Y)-COORDS IN THE CENTRAL PLANE OF THE SPHERE THAT
% ARE OUTSIDE THE IMAGE
%
% =========================================================================

% Go over Z planes and cound the pixels that fall within the surface. Stop
% as soon as we find a plane without pixels.

% First without "corners"

% North
volumeN = 0;
for z = 0 : radius
    [X, Y, Z] = meshgrid(step / 2 + x0Img : step : xImg, ...
        step / 2 + yImg : step : yGlobal, z);
    P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;

    n = numel(find(P > 0));
    if n == 0
        break;
    end
    
    % Add to the estimate of the volume
    volumeN = volumeN + (n / areaFactor);
    
    if DEBUG == 1 %&& z == 0
        % Plot them in the right place
        [y, x] = find(P);
        y = y + step / 2 + yImg - 1;
        x = x + step / 2 + x0Img - 1;
        plot(x, y, 'ro');
    end
    
end

% West
volumeW = 0;
for z = 0 : radius
    [X, Y, Z] = meshgrid(step / 2 + x0Global : step : x0Img, ...
        step / 2 + y0Img : step : yImg, z);
    P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;

    n = numel(find(P > 0));
    if n == 0
        break;
    end
    
    % Add to the estimate of the volume
    volumeW = volumeW + (n / areaFactor);
    
    if DEBUG == 1 && z == 0
        % Plot them in the right place
        [y, x] = find(P);
        y = y + step / 2 + y0Img - 1;
        x = x + step / 2 + x0Global - 1;
        plot(x, y, 'r.');
    end
    
end

% South
volumeS = 0;
for z = 0 : radius
    [X, Y, Z] = meshgrid(step / 2 + x0Img : step : xImg, ...
        step / 2 + y0Global : step : y0Img, z);
    P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;

    n = numel(find(P > 0));
    if n == 0
        break;
    end
    
    % Add to the estimate of the volume
    volumeS = volumeS + (n / areaFactor);
    
    if DEBUG == 1 && z == 0
        % Plot them in the right place
        [y, x] = find(P);
        y = y + step / 2 + y0Global - 1;
        x = x + step / 2 + x0Img - 1;
        plot(x, y, 'r.');
    end
    
end

% East
volumeE = 0;
for z = 0 : radius
    [X, Y, Z] = meshgrid(step / 2 + xImg : step : xGlobal, ...
        step / 2 + y0Img : step : yImg, z);
    P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;

    n = numel(find(P > 0));
    if n == 0
        break;
    end
    
    % Add to the estimate of the volume
    volumeE = volumeE + (n / areaFactor);
    
    if DEBUG == 1 && z == 0
        % Plot them in the right place
        [y, x] = find(P);
        y = y + step / 2 + y0Img - 1;
        x = x + step / 2 + xImg - 1;
        plot(x, y, 'r.');
    end
    
end

% Are there corners to consider?

% Between North and East
volumeNE = 0;
if inpolygon(xImg, yImg, xCircle, yCircle) > 0
    for z = 0 : radius
        [X, Y, Z] = meshgrid(step / 2 + xImg : step : xGlobal, ...
            step / 2 + yImg : step : yGlobal, z);
        P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;
        
        n = numel(find(P > 0));
        if n == 0
            break;
        end
        
        % Add to the estimate of the volume
        volumeNE = volumeNE + (n / areaFactor);
        
        if DEBUG == 1 && z == 0
            % Plot them in the right place
            [y, x] = find(P);
            y = y + step / 2 + yImg - 1;
            x = x + step / 2 + xImg - 1;
            plot(x, y, 'md');
        end
        
    end
end

% Between East and South
volumeES = 0;
if inpolygon(xImg, y0Img, xCircle, yCircle) > 0
    for z = 0 : radius
        [X, Y, Z] = meshgrid(step / 2 + xImg : step : xGlobal, ...
            step / 2 + y0Global : step : y0Img, z);
        P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;
        
        n = numel(find(P > 0));
        if n == 0
            break;
        end
        
        % Add to the estimate of the volume
        volumeES = volumeES + (n / areaFactor);
        
        if DEBUG == 1 && z == 0
            % Plot them in the right place
            [y, x] = find(P);
            y = y + step / 2 + y0Global - 1;
            x = x + step / 2 + xImg - 1;
            plot(x, y, 'go');
        end
        
    end
end

% Between South and West
volumeSW = 0;
if inpolygon(x0Img, y0Img, xCircle, yCircle) > 0
    for z = 0 : radius
        [X, Y, Z] = meshgrid(step / 2 + x0Global : step : x0Img, ...
            step / 2 + y0Global : step : y0Img, z);
        P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;
        
        n = numel(find(P > 0));
        if n == 0
            break;
        end
        
        % Add to the estimate of the volume
        volumeSW = volumeSW + (n / areaFactor);
        
        if DEBUG == 1 && z == 0
            % Plot them in the right place
            [y, x] = find(P);
            y = y + step / 2 + y0Global - 1;
            x = x + step / 2 + x0Global - 1;
            plot(x, y, 'cs');
        end
        
    end
end

% Between West and North
volumeWN = 0;
if inpolygon(x0Img, yImg, xCircle, yCircle) > 0
    for z = 0 : radius
        [X, Y, Z] = meshgrid(step / 2 + x0Global : step : x0Img, ...
            step / 2 + yImg : step : yGlobal, z);
        P = sqrt(X.^2 + Y.^2 + Z.^2) <= radius;
        
        n = numel(find(P > 0));
        if n == 0
            break;
        end
        
        % Add to the estimate of the volume
        volumeWN = volumeWN + (n / areaFactor);
        
        if DEBUG == 1 && z == 0
            % Plot them in the right place
            [y, x] = find(P);
            y = y + step / 2 + yImg - 1;
            x = x + step / 2 + x0Global - 1;
            plot(x, y, 'kp');
        end
        
    end
end

% Total volume INSIDE the circle but OUTSIDE the image
vOutside = volumeN + volumeW + volumeS + volumeE + ...
    volumeNE + volumeES + + volumeSW + volumeWN;

% Volume of the total hemisphere
V = 2 / 3 * radius^3 * pi;

% Fraction of the volume outside the image
f = vOutside / V;
