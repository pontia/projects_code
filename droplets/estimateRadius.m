function [radius, xC, yC] = estimateRadius(img, maskArea, filtSize)

% =========================================================================
%
% PREPROCESSING
%
% =========================================================================

% Low-pas filter
h = fspecial('gaussian', filtSize, 5);
I = double(imfilter(img, h, 'symmetric', 'same'));

% Normalize
mn = min(I(:));
mx = max(I(:));
J = (I - mn) ./ (mx - mn);

% Find the optimal threshold
thresh = 0.25;
thresh = fminsearch(@(thresh) findOptThresh(thresh, J, maskArea), thresh);
fprintf(1, 'Optimal threshold: %6.4f\n', thresh);

% Threshold
J = J > thresh;

% =========================================================================
%
% Find the center
%
% =========================================================================

M = locmax2d(I, filtSize);
[y, x] = find(M > 0);
n = 0;
v = 0;
for i = 1 : numel(y)
    if I(y(i), x(i)) > v
        v = I(y(i), x(i));
        n = i;
    end
end
yC = y(n);
xC = x(n);

% =========================================================================
%
% Find the radius
%
% =========================================================================

% Find the boundary pixels
[yB, xB] = find(J == 0);
indx = getPixelsAtBoundary(yB, xB, J);
yB = yB(indx);
xB = xB(indx);

% Calculate mean distance as radius
radius = mean(sqrt((yB - yC).^2 + (xB - xC).^2));

% Area of perfect circle
areaC = radius ^ 2 * pi;

% Area of mask
areaM = sum(J(:));

% Aprroximate droplet area in the field of view
areaF = areaM / areaC;

% =========================================================================
%
% Find the pixels at the mask boundary
%
% =========================================================================

function indx = getPixelsAtBoundary(yB, xB, J)

[sY, sX] = size(J);

indx = false(size(yB));

for i = 1 : numel(yB)
    
    y0 = yB(i) - 1; if y0 < 1,  y0 = 1;  end
    y  = yB(i) + 1; if y  > sY, y  = sY; end
    x0 = xB(i) - 1; if x0 < 1,  x0 = 1;  end
    x  = xB(i) + 1; if x  > sX, x  = sX; end
    
    if any(any(J(y0 : y, x0 : x) > 0))
        indx(i) = true;
    end
    
end
