function f = findOptThresh(x, J, maskArea)
% Cost function to determine the optimal threshold that segments the
% droplet
J = J > x;
N = numel(J);
n = numel(find(J));
f = abs(n/N - maskArea);