function metadata = mergeImarisChannels(inFileName, outDirName, mode)
% Merge selected Imaris channels into a new one by one of two methods.
%
% SYNOPSIS
%
%   mergeImarisChannels(inFileName, outDirName, mode)
%
% INPUT
%
%   inFileName: input file name, omit or set to [] to pick via a dialog.
%   outDirName: folder where the output file will be saved; omit or set 
%               to [] to pick via a dialog.
%   mode      : (optional, default = 'max'). One of 'max' or 'sum'.
%                  max: the merged channels is the max of the selected ones
%                  sum: the merged channels is the sum of the selected ones
%                       normalized to fix in the same range as the input
%                       channels.
%
% OUTPUT
%
%   None
%
%   The resulting channel is saved a a series of TIFFs into outDirName.
%
%       merged_channel_t0001_z0001.tif
%       merged_channel_t0001_z0002.tif
%       merged_channel_t0001_z0003.tif
%       ...
%       merged_channel_t0002_z0001.tif
%       ...
%
% Copyright, Aaron Ponti 2016

switch nargin
    case 0, 
        inFileName = [];
        outDirName = [];
        mode = 'max';
    case 1, 
        outDirName = [];
        mode = 'max';
    case 2, 
        mode = 'max';
    case 3,
        % All set
    otherwise,
        error('0, 1, or 2 input parameters expected.');
end

if strcmpi(mode, 'max') == 0 && strcmpi(mode, 'sum') == 0
    error('Bad value for mode. Please pick one of ''max'' or ''sum''.');
end
mode = lower(mode);

% Input file
if isempty(inFileName)
    [fName, dirName] = uigetfile('*.ims', 'Please pick an Imaris file');
    if isequal(fName, 0)
        return
    end

    inFileName = fullfile(dirName, fName);
end

% Output dir
if isempty(outDirName)
     outDirName = uigetdir(pwd(), ...
         'Please pick a directory to save the merged channel');
     if isequal(outDirName, 0)
         return
     end
end

if exist(outDirName, 'dir') == 0
    mkdir(outDirName);
end

% First read the metadata only
disp('Extracting metadata...');
metadata = imarisinfo(inFileName);
if isempty(metadata)
    error(['Could not open file ', inFileName, '.']);
end

% Check that we have at least two channels
if metadata.nChannels <= 2
    disp('At least two channels needed.');
    return;
end

% If more than two channels, ask to pick
if metadata.nChannels > 2
    [s, v] = listdlg( ...
        'PromptString', 'Please pick the channels to merge:',...
        'SelectionMode', 'multi', ...
        'ListString', metadata.channelNames);
    if v == 0
        return
    end

    if numel(s) < 2
        disp('Please select at least two files.');
        return;
    end

else

    % There are two channels in the file, we select them.
    s = [1 2];

end

% The indices in the Imaris file are 0-based
s = s - 1;

% Now process
hWaitbar = waitbar(0, 'Processing channels...');

for i = 1 : numel(s)

    % Read the dataset
    dataset = imarisread(inFileName, s(i));

    % Store the original data type
    if i == 1
        datatype = class(dataset{1});
    end

    switch mode

        case 'max',

            if i == 1
                out = dataset;
            else
                for t = 1 : metadata.nTimepoints
                    out{t} = max( ...
                        cat(4, out{t}, dataset{t}), ...
                        [], 4);
                end
            end

        case 'sum',

            if i == 1
                out = cell(metadata.nTimepoints, 1);
                for t = 1 : metadata.nTimepoints
                    out{t} = single(dataset{i});
                end
            else
                for t = 1 : metadata.nTimepoints
                    out{t} = out{t} + single(dataset{t});
                end
            end

        otherwise,

            error('Bad value for ''mode''.');
    end

    % Update waitbar
    waitbar(i / numel(s), hWaitbar);
    
end

% Close waitbar
close(hWaitbar);

% Normalize
if strcmp(mode, 'sum') == 1

    globalMax = 0;
    for i = 1 : size(out, 1)   
        currMax = max(out{t}(:));
        if currMax > globalMax
            globalMax = currMax;
        end
    end

    % Max value for given dat type
    maxValue = single(intmax(datatype));

    % Normalize between 0 and globalMax and cast to the original datatype
    for t = 1 : size(out, 1)
        out{t} =  cast(maxValue .* (out{t} ./ currMax), datatype);
    end

end

% Now process
hWaitbar = waitbar(0, 'Saving new channel...');

% Save the result
nFilesToSave = size(out, 1) * size(out{1}, 3);

% Format string
strg = sprintf('%%.%dd', 4);

n = 0;
for t = 1 : size(out, 1)

    for z = 1 : size(out{t}, 3)

        % File name
        indxStrT = sprintf(strg, t);
        indxStrZ = sprintf(strg, z);
        fName = fullfile(outDirName, ...
            ['merged_channel_t', indxStrT, '_z', indxStrZ, '.tif']);

        % Save
        imwrite(out{t}(:, :, z), fName);

        % Update waitbar
        n = n + 1;
        waitbar(n / nFilesToSave, hWaitbar);

    end

end

% Close waitbar
close(hWaitbar);

