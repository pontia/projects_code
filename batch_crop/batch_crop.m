function batch_crop()
% From X 225 (128 um) To 1610 (921 um) Size 1386 (793 um)  
% From Y 188 (107 um) To 1678 (960 um) Size 1491 (853 um)
% From Z 135 (172 um) To 379 (485 um) Size 245 (314 um)

% Choose files to crop
[fNames, dirName] = uigetfile( ...
    {'*.*',  'All Files (*.*)'}, ...
    'Pick one or more files to crop', ...
    'MultiSelect', 'on');

if isequal(fNames, 0)
    return
end

% Define ranges
prompt = {'x0:', 'x', 'y0:', 'y', 'z0:', 'z'};
dlg_title = 'Please define crop ranges (in pixels)';
num_lines = 1;
defaultans = {'', '', '', '', '', ''};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

if isempty(answer)
    return
end

% Get the extends
x0 = str2double(answer{1});
x  = str2double(answer{2});
y0 = str2double(answer{3});
y  = str2double(answer{4});
z0 = str2double(answer{5});
z  = str2double(answer{6});

if any(isnan([x0 x y0 y z0 z]))
    error('Invalid range provided,');
end

if x0 < 1 || x0 > x || y0 < 1 || y0 > y || z0 < 1 || z0 > z
    error('Invalid range provided,');
end

% Process the files
nFiles = numel(fNames);

tic
%parfor i = 1 : nFiles
for i = 1 : nFiles
  
    cropped = [];

    % Get the body of the file name
    [~, bodyName, ~] = fileparts(fNames{i});
    
    fprintf(1, 'Loading file %s...\n', fNames{i});

    % Read the file
    dataset = loadGeneric(fullfile(dirName, fNames{i}));
    
    for t = 1 : numel(dataset, 1)
        
        for c = 1 : numel(dataset, 2)

            try
                cropped = dataset{t, c}(y0:y, x0:x, z0:z);
            catch ME
                disp(['There was a problem cropping the dataset (', ...
                    ME.identifier, '). Aborting.']);
                continue
            end
        end
        
        % Build file name
        outFileName = fullfile(dirName, [bodyName, '_t_', ...
            sprintf('%03d', t), '_c_', sprintf('%03d', c), '.tif']);
        
        fprintf(1, 'Writing file %s...\n', outFileName);
         
        % Save as a 3D tiff
        writeToTIFF(cropped, outFileName);
        
    end
  
end
toc

% == 
function writeToTIFF(cropped, outFileName)

for p = 1 : size(cropped, 3)
    
    if p == 1
        imwrite(cropped(:, :, p), outFileName);
    else
        imwrite(cropped(:, :, p), outFileName, 'WriteMode','append');
    end
    
end

