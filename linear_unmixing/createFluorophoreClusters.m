function fluorophoreImages = createFluorophoreClusters(imgSize, ...
    nFluorophores, maxClusterSize, nClustersPerImage, ...
    randomNumClusters, randomIntensityClusters, allowOverlap, maxIntensity)
% Creates images with randomly distributed fluorophore clusters.
%
% Each image contains several clustes of ONE fluorophore. The images can
% then be composed (added with specific weights defined by the fraction of
% spectral overlap between emission spectra) to simulate images acquired on
% the microscope for a given (excitation, emission filter)-pair.
%
% SEE ALSO
%
%   createSynthSpectralOverlappingImages.m
%
% INPUT
%
%    imgSize          : [h w], image size.
%    nFluorophores    : number of fluorophores in the sample (which
%                       corresponds to the number of images generated).
%    maxClusterSize   : [h w], maximum size of a cluster. Since clusters 
%                       are represented by rectangles or ellipses, 
%                       maxClusterSize defines the height and width of a 
%                       rectangle or the major and minor axis length of
%                       an eclipse.
%    nClustersPerImage: number of fluorophore clusters generated per image.
%    randomNumClusters: [0 | 1], if 1, nClustersPerImage the number of
%                       clusters is random with nClustersPerImage as upper
%                       bound.
%    randomIntensityClusters: [0 | 1], if 1, the intensity of the cluster
%                       is a random value between 0 and 1, if 0 the 
%                       instensity is 1.
%    allowOverlap     : [0 | 1], if 1 clusters are allowed to overlap, both
%                       within one image or over images. If 0, no pixel
%                       position can be populated more than once over the
%                       whole set of images.
%    maxIntensity     : maximum intensity of the fluorescence
%
% OUTPUT
%
%    fluorophoreImages: cell array of images with fluorophore clusters.
%
% Aaron Ponti, 2012/11/15

% =========================================================================
%
% INPUT PARAMETER CHECK
%
% =========================================================================

if nargin ~= 8
    error('8 input parameters expected.');
end

% Quick parameter check
if numel(imgSize) ~= 2
    error('imgSize must be in the form [h w].');
end
if ~isscalar(nFluorophores) || nFluorophores < 1
    error('nFluorophores must be a positive scalar.');
end
if numel(maxClusterSize) ~= 2
    error('maxClusterSize must be in the form [h w].');
end    
if ~isscalar(nClustersPerImage) || nClustersPerImage < 1
    error('nClustersPerImage must be a positive scalar.');
end
if ~isscalar(randomNumClusters) || randomNumClusters < 0 || ...
        randomNumClusters > 1
    error('randomNumClusters must be either 0 or 1.');
end
if ~isscalar(randomIntensityClusters) || randomIntensityClusters < 0 || ...
        randomIntensityClusters > 1
    error('randomIntensityClusters must be either 0 or 1.');
end
if ~isscalar(allowOverlap) || allowOverlap < 0 || ...
        allowOverlap > 1
    error('allowOverlap must be either 0 or 1.');
end
if numel(maxIntensity) ~= nFluorophores
    error('maxIntensity must be a vector with one entry per fluorophore.');
end    
if any(maxIntensity < 1)
    error('maxIntensity must contain positive values.');
end

% Make sure the cluster size is not too low or there might be problems
% filling in the shapes.
if any(maxClusterSize < 5)
    disp('MaxShapeSize must be at least [5 5].');
end

% =========================================================================
%
% GETERATE THE FLUOROPHORE IMAGES
%
% =========================================================================

% Allocate space to store the fluorophore images
fluorophoreImages = cell(1, nFluorophores);

% Generate the images
for imageNumber = 1 : nFluorophores
    
    % Inform the user
    disp(['Generating fluorophore distribution ', ...
        num2str(imageNumber), '.']);
    
    % Create the image
    img = zeros(imgSize, 'double');
    
    % Number of clusters in this image
    if randomNumClusters == 1
        % Random between 1 and nClustersPerImage
        nClustersInThisImage = randi(nClustersPerImage, 1);
    else
        % Constant nClustersPerImage
        nClustersInThisImage = nClustersPerImage;
    end
    
    % Add the clusters
    for shapeNumber = 1 : nClustersInThisImage
        
        if rand(1) > 0.5
            
            % Add an elliptic cluster
            img = addEllipse(img, imgSize, maxClusterSize, ...
                allowOverlap, randomIntensityClusters, ...
                fluorophoreImages, imageNumber, maxIntensity(imageNumber));
            
        else
            
            % Add a rectangular cluster
            img = addRectangle(img, imgSize, maxClusterSize, ...
                allowOverlap, randomIntensityClusters, ...
                fluorophoreImages, imageNumber, maxIntensity(imageNumber));
            
        end
        
    end
    
    % Store the image
    fluorophoreImages{imageNumber} = img;
    
    
end

% =========================================================================
%
% Functions
%
% =========================================================================

% Add an elliptic cluster
% =========================================================================

function img = addEllipse(img, imgSize, maxClusterSize, ...
    allowOverlap, randomIntensityClusters, images, imageNumber, ...
    maxIntensity)

success = 0;

while success == 0
    
    % Get the outline coordinates of an ellipse
    [X, Y] = getEllipseOutlineCoords(imgSize, maxClusterSize);
    if isempty(X) || isempty(Y)
        continue;
    end
    
    % Burn in
    tmp = burnIn(X, Y, randomIntensityClusters, imgSize, maxIntensity);
    
    % Check for overlap
    [success, img] = combine(allowOverlap, imageNumber, ...
        tmp, img, images);
    
end

% Add a rectangular cluster
% =========================================================================
function img = addRectangle(img, imgSize, maxClusterSize, ...
    allowOverlap, randomIntensityClusters, images, imageNumber, ...
    maxIntensity)

success = 0;

while success == 0
    
    % Get the outline coordinates of a rectangle
    [X, Y] = getRectangleOutlineCoords(imgSize, maxClusterSize);
    if isempty(X) || isempty(Y)
        continue;
    end
    
    % Burn in
    tmp = burnIn(X, Y, randomIntensityClusters, imgSize, maxIntensity);
    
    % Check for overlap
    [success, img] = combine(allowOverlap, imageNumber, ...
        tmp, img, images);
    
end

% Get the ellipse outline coordinates
% =========================================================================
function [X, Y] = getEllipseOutlineCoords(imgSize, maxClusterSize)

% Initialize output
X = [];
Y = [];

% Get a random center coordinate
x0 = randi(imgSize(2), 1);
y0 = randi(imgSize(1), 1);

% Get random minor and major axes
a  = randi(maxClusterSize(2), 1);
b  = randi(maxClusterSize(1), 1);
if a < 3 || b < 3
    return;
end

% Get angle
phi = rand(1) * pi;

% Get ellipse coordinates
[ X, Y ] = ellipsedraw(a, b, x0, y0, phi);
if any(X < 1) || any(X > imgSize(2)) || ...
        any(Y < 1) || any(Y > imgSize(1))
    X = []; 
    Y = [];
end

% Get the rectangle outline coordinates
% =========================================================================
function [X, Y] = getRectangleOutlineCoords(imgSize, maxClusterSize)

% Initialize output
X = [];
Y = [];

% Get a random center coordinate
x0 = randi(imgSize(2), 1);
y0 = randi(imgSize(1), 1);

% Get random minor and major axes
a  = randi(maxClusterSize(2), 1);
b  = randi(maxClusterSize(1), 1);
if a < 3 || b < 3
    return;
end

% Get angle
phi = rand(1) * pi;

% Get outline
xLeft   = round(x0 - a/2);
xRight  = round(x0 + a/2);
yTop    = round(y0 - b/2);
yBottom = round(y0 + b/2);

if xLeft < 1 || xRight > imgSize(2) || ...
        yTop < 1 || yBottom > imgSize(1)
    return;
end

% Allocate space for the coordinates
coords = zeros(2 * (1 + xRight - xLeft) + ...
    2 * (1 + yBottom - yTop) - 3, 2);
c = 0;
% Top row
x = xLeft;
while x <= xRight
    c = c + 1;
    coords(c, : ) = [yTop, x];
    x = x + 1;
end
% Right column
y = yTop + 1;
while y <= yBottom
    c = c + 1;
    coords(c, : ) = [y, xRight];
    y = y + 1;
end
x = xRight - 1;
% Bottom row
while x >= xLeft
    c = c + 1;
    coords(c, : ) = [yBottom, x];
    x = x - 1;
end
% Left column
y = yBottom - 1;
while y >= yTop
    c = c + 1;
    coords(c, : ) = [y, xLeft];
    y = y - 1;
end

% Rotate
XData = coords(:, 2) - x0;
YData = coords(:, 1) - y0;
X = x0 + cos( phi ) * XData - sin( phi ) * YData;
Y = y0 + sin( phi ) * XData + cos( phi ) * YData;

if any(X < 1) || any(X > imgSize(2)) || ...
        any(Y < 1) || any(Y > imgSize(1))
    X = []; 
    Y = [];
end


% Burn in the cluster shape in the image
% =========================================================================
function tmp = burnIn(X, Y, randomIntensityClusters, imgSize, ...
        maxIntensity)
tmp = false(imgSize);
for i = 1 : numel(X)
    tmp(round(Y(i)), round(X(i))) = 1;
end
tmp = double(imfill(tmp, round([mean(Y) mean(X)]), 6));
% Apply the intensity
if randomIntensityClusters == 1
    value = rand(1);
    if value < 0.1
        value = 0.1;
    end
    tmp = value .* tmp;
end
tmp = maxIntensity .* tmp;

% Check whether current cluster is overlapping to any other one
% =========================================================================
function [success, img] = combine(allowOverlap, ...
    imageNumber, tmp, img, images)
% Make sure we did not fill in all of the image
if numel(unique(tmp(:))) == 1
    success = 0;
    return;
end
if allowOverlap == 1
    img = tmp + img;
    success = 1;
else
    % Since clusters in both tmp and img can have varying intensity levels,
    % we check the sum for binarized versions of them for double exposure
    overlap = double(tmp > 0) + double(img > 0);
    if numel(find(overlap>1)) == 0
        if imageNumber == 1
            img = tmp + img; % Original intensities
            success = 1;
        else
            % Now check also against the images already created
            for j = 1 : imageNumber - 1
                overlapFull = double(tmp > 0) + double(images{j} > 0);
                if numel(find(overlapFull>1)) > 0
                    disp('   Overlap found: skipping');
                    success = 0;
                    return;
                end
            end
            % No overlap even over images: we can add the images and return
            img = tmp + img;  % Original intensities
            success = 1;
        end
    else
        disp('   Overlap found: skipping');
        success = 0;
        return;
    end
end



