function unmixedImages = performLinearUnmixing(mixedImages, settings)
% TODO DESCRIBE
%
% INPUT
%    mixedImages
%    settings
%
% OUTPUT
%    unmixedImages
%
% Aaron Ponti, 2012/11/16

% Number of images
nImages = numel(mixedImages);

% Allocate space for the solutions
unmixedImages = cell(1, nImages);

% Combine the images into a 3d stack to get faster pixel access
mixedStack = zeros([settings.imgSize settings.nFluorophores]);
for i = 1 : settings.nFluorophores
    mixedStack(:, :, i) = mixedImages{i};
end

% Initialize a stack for the solution
unmixedStack = zeros([settings.imgSize settings.nFluorophores]);

% Perform the unmixing
for i = 1 : settings.imgSize(1)

    for j = 1 : settings.imgSize(2)
        
        % Get all intensity values for pixel (i, j) into a column vector
        v = squeeze(mixedStack(i, j, :));
        
        % Solve
        u = settings.spectralContributions \ v;

        % Put the pixels back
        unmixedStack(i, j, : ) = reshape(u, [1 1 settings.nFluorophores]);
    end
    
end

% Separate the images
for i = 1 : settings.nFluorophores
    unmixedImages{i} = unmixedStack(:, :, i);  
end
