function noiseAnalysis

% Ask the user to choose where to save the results
outDir = uigetdir();
if isequal(outDir, 0)
    return
end

resultsFile = [ outDir, filesep, 'report.txt' ];
fid = fopen(resultsFile, 'w');
if fid == -1
    uiwait(errordlg('Could not create results file.', 'Error', 'modal'));
    return;
end

fprintf(fid, '\n\n*****\n\n');

for nReps = 1 : 5
    
    % 2 channels, gaussian noise added
    settings = initSettings();
    settings.gaussianNoise = 1;
    settings.gaussianSigma = 10;
    settings.saveImages = 1;
    settings.saveFolder = outDir;
    
    fprintf(fid, 'Input: 2 channels, noise std: %8.4f\n', settings.gaussianSigma);
    
    [unmixedImages, ~, ~, fluorophoreImages] = simulateAndSolve(settings);
    
    for i = 1 : numel(unmixedImages)
        noiseImage = unmixedImages{i} - fluorophoreImages{i};
        n = std(noiseImage(:));
        
        fprintf(fid, 'Results: 2 channels, image %d, noise std: %8.4f\n', i, n);
        
    end
    
end

fprintf(fid, '\n\n*****\n\n');

for nReps = 1 : 5
    
    % 3 channels, gaussian noise added
    settings = initSettings();
    settings.nFluorophores = 3;
    settings.spectralContributions = [...
        0.6 0.3 0.1;
        0.1 0.6 0.2;
        0.0 0.3 0.7];
    settings.maxIntensity = [100 100 100];
    settings.offset = [0 0 0];
    settings.gaussianNoise = 1;
    settings.gaussianSigma = 10;
    settings.saveImages = 1;
    settings.saveFolder = outDir;
    
    fprintf(fid, 'Input: 3 channels, noise std: %8.4f\n', settings.gaussianSigma);
    
    [unmixedImages, ~, ~, fluorophoreImages] = simulateAndSolve(settings);
    
    for i = 1 : numel(unmixedImages)
        noiseImage = unmixedImages{i} - fluorophoreImages{i};
        n = std(noiseImage(:));
        
        fprintf(fid, 'Results: 3 channels, image %d, noise std: %8.4f\n', i, n);
        
    end

end

fprintf(fid, '\n\n*****\n\n');

for nReps = 1 : 5
    
    % 5 channels, gaussian noise added
    settings = initSettings();
    settings.nFluorophores = 5;
    settings.offset = [0 0 0 0 0];
    settings.spectralContributions = [...
        0.5000    0.2500    0.1500    0.0700    0.0300
        0.2500    0.6000    0.1000    0.0500         0
        0.0500    0.1500    0.6000    0.1500    0.0500
        0         0    0.2041    0.7143    0.0816
        0         0    0.1111    0.6667    0.2222
        ];
    settings.maxIntensity = [100 100 100 100 100];
    settings.gaussianNoise = 1;
    settings.gaussianSigma = 10;
    settings.saveImages = 1;
    settings.saveFolder = outDir;
    
    fprintf(fid, 'Input: 5 channels, noise std: %8.4f\n', settings.gaussianSigma);
    
    [unmixedImages, ~, ~, fluorophoreImages] = simulateAndSolve(settings);
    
    for i = 1 : numel(unmixedImages)
        noiseImage = unmixedImages{i} - fluorophoreImages{i};
        n = std(noiseImage(:));
        
        fprintf(fid, 'Results: 5 channels, image %d, noise std: %8.4f\n', i, n);
        
    end
    
end

fprintf(fid, '\n\n*****\n\n');

for nReps = 1 : 5
    
    settings = initSettings();
    settings.nFluorophores = 6;
    settings.offset = [0 0 0 0 0 0];
    settings.spectralContributions = [ ...
        0.50 0.25 0.15 0.07 0.03 0.00;...
        0.25 0.60 0.10 0.05 0.00 0.00; ...
        0.05 0.15 0.60 0.15 0.05 0.00; ...
        0.00 0.00 0.20 0.70 0.08 0.02; ...
        0.00 0.00 0.10 0.60 0.20 0.10; ...
        0.00 0.00 0.10 0.70 0.18 0.02];
    settings.maxIntensity = 100 .* ones(1, 6);
    settings.gaussianNoise = 1;
    settings.gaussianSigma = 10;
    settings.saveImages = 1;
    settings.saveFolder = outDir;

    [unmixedImages, ~, ~, fluorophoreImages] = simulateAndSolve(settings);
    
    for i = 1 : numel(unmixedImages)
        noiseImage = unmixedImages{i} - fluorophoreImages{i};
        n = std(noiseImage(:));
        
        fprintf(fid, 'Results: 6 channels, image %d, noise std: %8.4f\n', i, n);
        
    end
    
end

fclose(fid);

edit(resultsFile)
