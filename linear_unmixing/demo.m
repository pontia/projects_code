%% DETERMINED SYSTEM
% 2 FLUOROPHORES, 2 CHANNELS, PER-PIXEL FLUOROPHORE CONTRIBUTION

% Unknown: Pixel-wise contribution of each fluorophore
% A1 = ?
% A2 = ?

%% Normalized spectral contributions to each channel
% Derived from lambda stacks
F11 = 0.8;    % Fluorophore 1 contribution in channel 1
F12 = 0.3;    % Fluorophore 1 contribution in channel 2
F21 = 0.2;    % Fluorophore 2 contribution in channel 1
F22 = 0.7;    % Fluorophore 2 contribution in channel 2

S = [ F11 F21;
    F12 F22 ]

%% Assertion (the total, normalized contribution per channel must be 1)
assert(all(sum(S, 2)) == 1);

%% Measured data
% In this case, we simulate by calculating the pixel value
% from the 'unknown' fluorophore contributions, here set to A1 = 0.6, 
% A2 = 0.4)
C = S * [0.6; 0.4];

%% Solve for the unknown fluorophore contributions
A = S \ C;

%% Display results
A1 = A(1)
A2 = A(2)

%% OVER-DETERMINED SYSTEM
% 2 FLUOROPHORES, 3 CHANNELS, PER-PIXEL FLUOROPHORE CONTRIBUTION

% Unknown: Pixel-wise contribution of each fluorophore
% A1 = ?
% A2 = ?

%% Normalized spectral contributions to each channel
% Derived from lambda stacks)
F11 = 0.7;    % Fluorophore 1 contribution in channel 1
F12 = 0.3;    % Fluorophore 1 contribution in channel 2
F13 = 0.6;    % Fluorophore 1 contribution in channel 3
F21 = 0.3;    % Fluorophore 2 contribution in channel 1
F22 = 0.7;    % Fluorophore 2 contribution in channel 2
F23 = 0.4;    % Fluorophore 2 contribution in channel 3

S = [ F11 F21;
    F12 F22;
    F13 F23 ]

%% Assertion (the total, normalized contribution per channel must be 1)
assert(all(sum(S, 2)) == 1);

%% Measured data
% In this case, we simulate by calculating the pixel value
% from the 'unknown' fluorophore contributions, here set to A1 = 0.6, 
% A2 = 0.4)
C = S * [0.6; 0.4];

%% Solve for the unknown fluorophore contributions
A = S \ C;

%% Display results
A1 = A(1)
A2 = A(2)
