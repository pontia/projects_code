function [mixedImages, fluorophoreImages] = createSynthSpectralOverlappingImages(settings)
% TODO DESCRIBE
%
% INPUT
%    settings: (optional) settings structure as initialized by initSettings
%              if not passed, initSettings() is called to generate defaults.
%
% OUTPUT
%    mixedImages: settings.nFluorophores images with overlapping spectra
%                 defined by settings.spectralContributions
%    fluorophoreImages: ground-truth fluorophore clusters
%
% Aaron Ponti, 2012/11/07

% Chck input
if nargin == 0
    settings = initSettings();
end

% Assertion (the total, normalized contribution per channel must be 1)
assert(all(sum(settings.spectralContributions, 2)) == 1, ...
    'The total, normalized contribution per channel must be 1');

% Create the ground-truth fluorophore images
fluorophoreImages = createFluorophoreClusters(...
    settings.imgSize, ...
    settings.nFluorophores, ...
    settings.maxClusterSize, ...
    settings.nClustersPerImage, ...
    settings.randomNumClusters, ...
    settings.randomIntensityClusters, ...
    settings.allowOverlap, ...
    settings.maxIntensity);

% Add offset
for i = 1 : settings.nFluorophores

    fluorophoreImages{i} = settings.offset(i) + fluorophoreImages{i};

end

% Combine the images spectrally
mixedImages = cell(1, settings.nFluorophores);

for i = 1 : settings.nFluorophores

    mixedImages{i} = zeros(settings.imgSize);
    
    for j = 1 : settings.nFluorophores
        
        mixedImages{i} = mixedImages{i} + ...
            settings.spectralContributions(i, j) .* fluorophoreImages{j};

    end
    
end

% Add Poisson noise
if settings.poissonNoise == 1
    
    for i = 1 : settings.nFluorophores

        % imnoise does not take double as input parameter
        if settings.maxIntensity <= 65535
            mixedImages{i} = double(...
                imnoise(uint16(mixedImages{i}), 'poisson'));
        else
            error('Max intensity must be < 65535.');
        end

    end
    
end

% Add gaussian noise
if settings.gaussianNoise == 1
    for i = 1 : settings.nFluorophores
        mixedImages{i} = mixedImages{i} + ...
            settings.gaussianSigma .* randn(settings.imgSize);
    end
end


