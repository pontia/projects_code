function [unmixedImages, n, mixedImages, fluorophoreImages] = ...
    simulateAndSolve(option)

% The function takes either a scalar for a series of presets, or a setting
% structure as returned by initSettings().
if isnumeric(option)
    
    switch option
        
        case 1,
            
            % 2 channels, pure signal
            settings = initSettings();
            
        case 2,
            
            % 2 channels, pure signal, offset
            settings = initSettings();
            settings.maxIntensity = [100 50];
            
        case 3,
            
            % 2 channels, pure signal, with overlap
            settings = initSettings();
            settings.allowOverlap = 1;
            settings.maxClusterSize = [200 200];
            
        case 4,
            
            % 4 channels, pure signal
            settings = initSettings();
            settings.nFluorophores = 4;
            settings.offset = [0 0 0 0];
            settings.spectralContributions = [ ...
                0.50 0.30 0.15 0.05;...
                0.25 0.60 0.10 0.05; ...
                0.05 0.20 0.70 0.05; ...
                0.00 0.10 0.20 0.70];
            
        case 5,
            
            % 2 channels, poisson noise added
            settings = initSettings();
            settings.poissonNoise = 1;
            
        case 6,
            
            % 2 channels, gaussian noise added
            settings = initSettings();
            settings.gaussianNoise = 1;
            settings.gaussianSigma = 10;
            
        case 7,
            
            % 2 channels, poisson and gaussian noise added
            settings = initSettings();
            settings.poissonNoise = 1;
            settings.gaussianNoise = 1;
            settings.gaussianSigma = 10;
            
        case 8,
            
            % 4 channels, pure signal
            settings = initSettings();
            settings.nFluorophores = 6;
            settings.offset = [0 0 0 0 0 0];
            settings.spectralContributions = [ ...
                0.50 0.25 0.15 0.07 0.03 0.00;...
                0.25 0.60 0.10 0.05 0.00 0.00; ...
                0.05 0.15 0.60 0.15 0.05 0.00; ...
                0.00 0.00 0.20 0.70 0.08 0.02; ...
                0.00 0.00 0.10 0.60 0.20 0.10; ...
                0.00 0.00 0.10 0.70 0.18 0.02];
            settings.maxIntensity = 100 .* ones(1, 6);
            
        otherwise,
            
            error('Unknown option.');
            
    end
else
    if ~isstruct(option)
        error('Expected a scalar or a settings structure.');
    end
    settings = option;
end

% Create the ground-truth fluorophore clusters and the mixed images
disp('Creating synthetic spectrally-overlapping images...');
[mixedImages, fluorophoreImages] = ...
    createSynthSpectralOverlappingImages(settings);

% Perform linear unmixing
tic;
disp('Performing linear unmixing...');
unmixedImages = performLinearUnmixing(mixedImages, settings);
toc

% Compare the unmixed images with the gorund-truth fluorophore images
n = zeros(1, settings.nFluorophores);
for i = 1 : settings.nFluorophores
    
    % Calculate the element-wise difference between the solution and the
    % ground truth
    r = unmixedImages{i} - fluorophoreImages{i};
    
    % Store the norm ot the residuals
    n(i) = norm(r, 2);
    
end

% Pick a proper max value for intensity stretching in the display
if settings.allowOverlap == 1
    maxI = 0;
    for i = 1 : settings.nFluorophores
        m = max(fluorophoreImages{i}(:));
        if m > maxI
            maxI = m;
        end
    end
else
    maxI = max(settings.maxIntensity(:));
end

% Display result images
hFigure = figure;

if settings.nFluorophores <= 4
    numCols = settings.nFluorophores;
else
    numCols = 4;
end

for i = 1 : settings.nFluorophores
    
    % We display four fluorophores per figure
    col = mod(i, 4);
    
    if col == 0
        col = 4;
    end
    
    subplot(4, numCols, col);
    imshow(fluorophoreImages{i}, [0 maxI]);
    xlabel(['Fluorophore ', num2str(i)]);
    subplot(4, numCols, numCols + col);
    imshow(mixedImages{i}, [0 maxI]);
    xlabel(['Spectral overlap ', num2str(i)]);
    subplot(4, numCols, 2 * numCols + col);
    imshow(unmixedImages{i}, [0 maxI]);
    xlabel(['Unmixed fluorophore ', num2str(i)]);
    subplot(4, numCols, 3 * numCols + col);
    imshow(unmixedImages{i} - fluorophoreImages{i}, [0 maxI]);
    xlabel({['Residuals ', num2str(i)], ['norm = ', num2str(n(i))]});
    
    if col == 4
        % If needed, save current figure before we open the next
        nRound = fix(i / 4);
        if settings.saveImages
            name = fullfile(settings.saveFolder, ...
                [num2str(settings.nFluorophores), '_channels_', ...
                sprintf('%6.0f', 1e6 * rem(now,1)), ...
                '_', num2str((nRound - 1) * 4 + 1), '-', ...
                num2str(nRound * 4), '.png']);
            %saveas(hFigure, name, 'png')
            print(['-f', num2str(hFigure)], '-dpng','-r300', name);
        end

        % Open new figure
        hFigure = figure;
    end
end

% Save the image if needed 
if settings.saveImages
    name = fullfile(settings.saveFolder, ...
        [num2str(settings.nFluorophores), '_channels_', ...
        sprintf('%6.0f', 1e6 * rem(now,1))]);
    if settings.nFluorophores < 4
        name = [name, '.png'];
    else
        nRound = fix(i / 4);
        name = [name, '_', num2str((nRound) * 4 + 1), '-', ...
                num2str((nRound + 1) * 4), '.png'];
    end
    % saveas(hFigure, name, 'png')
    print(['-f', num2str(hFigure)], '-dpng','-r300', name);
end
