function saveEventProfilesToDisk(eventProfiles, outFileName)
% Export intensity profiles of all events to disk
%
% SYNOPSIS
%
%   eventProfiles = extractAndSaveEventProfilesToDisk(eventProfiles, outFileName)    
%
% INPUT
%
%   eventProfiles: nTimepoints x nEvents matrix of intensity profiles.
%   outFileName  : name with full path for the output file.
%
% OUTPUT
%
%   None
%
% Aaron POnti, 2015/06/23

% Open the file
fid = fopen(outFileName, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Create format string
frmt = '';
for i = 1 : size(eventProfiles, 2)
    frmt = cat(2, frmt, '%8.4f\t');
end
frmt = cat(2, frmt, '\n');

% Write to file
fprintf(fid, frmt, eventProfiles');

% Close and display results file
if fid ~= 1
    fclose(fid);
    edit(outFileName);
end
