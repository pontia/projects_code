function displayMapsInteractively(extDataset, locMaxima, side, displayMaxima, mn, mx)
% Display image interactively

if nargin == 4
    [mn, mx] = getGlobalBounds(extDataset);
end

current = 1;
nTimepoints = numel(extDataset);

hF = figure('Name', num2str(current), 'NumberTitle', 'off');
hImg = imshow(extDataset{current});
hA = get(hF, 'CurrentAxes');
set(hA, 'CLim', [mn mx]);
colorbar()
colormap('jet');
xlabel('Use (shift) arrow keys to navigate.');

% Display the maxima
hold on;
d = fix(side / 2);

nPlots = 0;

if displayMaxima == 1
    nPlots = size(locMaxima, 1);
    hPlots = zeros(1, nPlots);

    for i = 1 : nPlots
        
        x0 = locMaxima(i, 2) - d;
        x  = locMaxima(i, 2) + d;
        y0 = locMaxima(i, 3) - d;
        y  = locMaxima(i, 3) + d;
        
        XData = [x0 x x  x0 x0];
        YData = [y  y y0 y0 y ];
        
        hPlots(i) = plot(XData, YData, 'w-', 'LineWidth', 2);
        
        text(x0 + 1, y - 1, num2str(i), 'Color', 'w');
        
    end
end

impixelinfo();

set(hF, 'WindowKeyPressFcn', @keyPressFcn);

    function keyPressFcn(~, e)
        step = 1;
        if ~isempty(e.Modifier)
            switch e.Modifier{1}
                case 'shift'
                    step = 50;
                otherwise
                    step = 1;
            end
        end
        switch e.EventName
            case 'WindowKeyPress'
                switch e.Key
                    case 'leftarrow'
                        if current > step
                            current = current - step;
                            updateImage(current);
                            updateMaxima(current)
                        end
                    case 'rightarrow'
                        if current < (nTimepoints - step)
                            current = current + step;
                            updateImage(current);
                            updateMaxima(current)
                        end
                    case 'uparrow',
                        current = nTimepoints;
                        updateImage(current);
                        updateMaxima(current)
                    case 'downarrow',
                        current = 1;
                        updateImage(current);
                        updateMaxima(current)
                    otherwise,
                        % Nothing
                end
            otherwise,
                % Nothing
        end
    end

    function updateImage(i)

        set(hF, 'Name', num2str(i));
        set(hImg, 'CData', extDataset{i});

    end

    function updateMaxima(i)
       
        for j = 1 : nPlots
            
           if locMaxima(j, 1) == i
               set(hPlots(j), 'Color', 'k');
           else
               set(hPlots(j), 'Color', 'w');
           end

        end
        
    end

end

% ====

function [gMn, gMx ] = getGlobalBounds(cellArray)

gBounds = cellfun(@(x) [min(x(:)), max(x(:))], cellArray, 'UniformOutput', false);
gMn = +Inf;
gMx = -Inf;
for counter = 1 : numel(gBounds);
    if gBounds{counter}(1) < gMn
        gMn = gBounds{counter}(1);
    end
    if gBounds{counter}(2) > gMx
        gMx = gBounds{counter}(2);
    end
end

end
    