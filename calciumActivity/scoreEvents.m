function [candidateEvents, allEvents] = scoreEvents(dataset, N, minEventDistance)
% Finds and scores significant events in images.
%
% SYNOPSIS
%
%   [candidateEvents, allEvents] = scoreEvents(dataset, N, radius) 
%
% INPUT
%
%   dataset         : cell array of 2D images (time series).
%   N               : number of strongest events (signal local maxima) to
%                     consider. Set to Inf to retun all events that satisfy
%                     the minEventDistance criterion.
%   minEventDistance: weaker events within a given radius in pixels from a
%                     stronger one are ignored.
%
% OUTPUT
%
% candidateEvents   : N accepted candidate events.
% allEvents         : all events.
%
% Aaron Ponti, 2015/06/23


% Number of images
nImages = numel(dataset);

hWaitbar = waitbar(0, 'Scoring and filtering events...');

% Find all signal maxima
allEvents = [];
for i = 1 : nImages
    
    % Get current image
    tmp = dataset{i};

    % Run a local max detector
    BW = imregionalmax(tmp);
    
    % Now get coordinates and intensities of the extracted localmaxima
    [y, x, v] = find(BW .* tmp);

    % Discard all negative local maxima
    indx = find(v < 0);
    y(indx) = [];
    x(indx) = [];
    v(indx) = [];
    
    % Sort the maxima by intensity
    currentEvents = [repmat(i, numel(x), 1), x, y, v];
    currentEvents = sortrows(currentEvents, -4);
    
    % Discard all maxima that do not satisfy the minEventDistance criterion
    % within current timepoint
    candidateEvents = [];
    p = 1;
    while size(candidateEvents, 1) < N && p < size(currentEvents, 1)
        
        % Get current max
        currentMax = double(currentEvents(p, :));

        if p == 1
        
            % Store the first maximum without additional checks
            candidateEvents = cat(1, candidateEvents, currentMax);
        
            % Move on
            p = p + 1;
            continue;

        end
    
        % If the next max is too close to any of the accepted ones, we skip it
        D = createDistanceMatrix(currentMax(1, 2 : 3), ...
            candidateEvents(:, 2 : 3));

        if all(D > minEventDistance)

            % Store current maximum
            candidateEvents = cat(1, candidateEvents, currentMax);

        end
    
        % Next
        p = p + 1;

    end
    
    % Store the positions
    allEvents = cat(1, allEvents, candidateEvents);

    waitbar(i / (nImages + 1), hWaitbar);
    
end

% Change the waitbar message
waitbar(i / (nImages + 1), hWaitbar, 'Sorting events...');

% Now inspect the maxima for the N stronger ones. They have to be spatially
% and temporally unrelated.
allEvents = sortrows(allEvents, -4);

waitbar(1.0, hWaitbar);

% Make sure not to extract more local maxima than there actually exists
if size(allEvents, 1) < N
    N = size(allEvents, 1);
end

% Reset waitbar
waitbar(0, hWaitbar, 'Filtering events in time...');

candidateEvents = [];
p = 1;
maxIter = size(allEvents, 1);
while size(candidateEvents, 1) < N && p < maxIter
    
    % Get current max
    currentMax = double(allEvents(p, :));

    if p == 1
        
        % Store the first maximum without additional checks
        candidateEvents = cat(1, candidateEvents, currentMax);
        
        % Move on
        p = p + 1;
        continue;

    end
    
    % If the next max is too close to any of the accepted ones, we skip it
    D = createDistanceMatrix(currentMax(:, 2 : 3), ...
        candidateEvents(:, 2 : 3));

    if all(D > minEventDistance)

        % Store current maximum
        candidateEvents = cat(1, candidateEvents, currentMax);

    end
    
    % Next
    p = p + 1;
    
    % Update waitbar
    waitbar(p / maxIter, hWaitbar);
    
end

% Close waitbar
close(hWaitbar);





