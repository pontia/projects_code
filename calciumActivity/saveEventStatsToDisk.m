function saveEventStatsToDisk(results, timeInterval, outFileName)
% Saves the results extracted by analizeIntensityProfiles() to a tsv-file.
%
% SYNOPSIS
%
%   saveEventStatsToDisk(results, timeInterval, outFileName)
%
% INPUT
%
%   results:      cell array of event results as returned by
%                 analizeIntensityProfiles().
%   timeInterval: time interval of the time series.
%   outFileName:  name with full path for the output file.
%
% OUTPUT
%
%   None
%
% Aaron Ponti, 2015/06/23

if isempty(timeInterval)
    disp('Time interval is set to 1.');
    timeInterval = 1;
end

% Make sure the output folder exists
dirName = fileparts(outFileName);
if ~exist(dirName, 'dir')
    mkdir(dirName);
end

% Open the file
fid = fopen(outFileName, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write header
fprintf(fid, '%14s\t%14s\t%14s\t%14s\n', ...
    'Start', 'Peak', 'Duration (s)', 'Amplitude');

% Write results to file
for i = 1 : numel(results)

    nEvents = numel(results{i}.amplitude);
    
    if nEvents == 0
        fprintf(fid, '%14s\t%14s\t%14s\t%14s', ...
            'NaN', 'NaN', 'NaN', 'NaN');
        
    else
        for j = 1 : nEvents
            fprintf(fid, '%14d\t%14d\t%14.4f\t%14.4f\t', ...
                results{i}.start(j), ...
                results{i}.peak(j), ...
                results{i}.duration(j) * timeInterval, ...
                results{i}.amplitude(j));
        end
    end
    fprintf(fid, '\n');
    
end

% Close and display results file
if fid ~= 1
    fclose(fid);
    edit(outFileName);
end
