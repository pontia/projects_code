function [results, significantEventProfiles, significantEvents, filterDataset] = calciumActivityAnalysis(fileName, outDirName, Ncandidates, N, eventRadius, minEventDistance, maxThreshold, minThreshold, displayMaxima)
% Performs calcium activity analysis on time series.
%
% SYNOPSIS
%
%   [results, significantEventProfiles, significantEvents, filterDataset] = ...
%      calciumActivityAnalysis(fileName, outDirName, Ncandidates, N, ...
%        eventRadius, minEventDistance, maxThreshold, minThreshold)
%
% INPUT
%
%   fileName   : file name to be analyzed with full path.
%   outDirName : full path to directory where to save analysis results and
%                quality control figures. It will be created if it does not
%                exist.
%   Ncandidates: number of calcium influx events to consider. Set to N to
%                Inf to test all profiles (even the weakest).
%   N          : number of significant calcium influx events to consider.
%                This will be a subset of Ncandidates.
%   eventRadius: expected size of a calcium influx event. Used to estimate
%                the kernel size of a low-pass filter.
%   minEventDistance: minimum distance from the locally strongest event 
%                for weaker events to be considered.
%   maxThresh  : number of median absolute deviations above the intensity
%                profile median for an event (local maximum in calcium 
%                signal) to be considered significant.
%   minThresh  : number of median absolute deviations above the intensity
%                profile median that defines the background level. The
%                duration of an event is defined as the time it takes for
%                the signal to go from background to peak and back into
%                background again.
%   displayMaxima: [0|1, default = 1]. Display the location of the event
%                  maxima  on the quality control figures.
%
% OUTPUT
%
%   results                 : cell array containing a set of results per 
%                             original event. Each position in the array 
%                             contains a structure of form:
%
%                                 results.amplitude
%                                 results.duration
%                                 results.peak  
%                                 results.start
%
%                             with each property being an array containing
%                             a value for each significant event found in 
%                             the time series.
%   significantEventProfiles: nTimepoints x N matrix of intensity profiles.
%                             If not enough significant profiles were 
%                             found, the number of columns might be lower 
%                             than N (see input).
%   significantEvents       : significant events. Might be less than N 
%                             (See input).
%   filterDataset           : filtered dataset (time series).
%
% REMARK
%
%   'results', 'significantEventProfiles', and 'significantEvents' are also
%   saved to a MATLAB file 'all_results.mat' in outDirName. Quality control
%   figures are saved in outDirName/qc.
% 
% Aaron Ponti, 2015/06/23

% Check input arguments
switch nargin
    case 2, 
        Ncandidates = 100;
        N = 10;
        eventRadius = 5;
        minEventDistance = 5;
        maxThreshold = 7;
        minThreshold = 1;
        displayMaxima = 1;
    case 3, 
        N = 10;
        eventRadius = 5;
        minEventDistance = 5;
        maxThreshold = 7;
        minThreshold = 1;
        displayMaxima = 1;
    case 4,
        eventRadius = 5;
        minEventDistance = 5;
        maxThreshold = 7;
        minThreshold = 1;
        displayMaxima = 1;
    case 5,
        minEventDistance = 5;
        maxThreshold = 7;
        minThreshold = 1;
        displayMaxima = 1;
    case 6,
        maxThreshold = 7;
        minThreshold = 1;
        displayMaxima = 1;
    case 7,
        minThreshold = 1;
        displayMaxima = 1;
    case 8,
        displayMaxima = 1;
    case 9,
        % All set
    otherwise,
        error('Wrong number of input arguments.');
end

% Load and filter the file
[filterDataset, metadata] = loadAndFilter(fileName, eventRadius);

% Find significant events
acceptedEvents = scoreEvents(filterDataset, Ncandidates, minEventDistance);

% Extract and correct the significant events
eventProfiles = extractEventProfiles(acceptedEvents, filterDataset);

% Analyize profiles to extract amplitude and duration
[results, indices] = analizeIntensityProfiles(eventProfiles, N, maxThreshold, ...
    minThreshold, 1, fullfile(outDirName, 'qc'));

% Filter the results
significantEvents = acceptedEvents(indices, :);
significantEventProfiles = eventProfiles(:, indices);

% Display image interactively
displayMapsInteractively(filterDataset, significantEvents, eventRadius, displayMaxima);

% Save event profiles to disk
saveEventProfilesToDisk(significantEventProfiles, ...
    fullfile(outDirName, 'profiles.txt'));

% Save extracted statistics to disk
saveEventStatsToDisk(results, metadata.timeInterval, ...
    fullfile(outDirName, 'stats.txt'));

% Save results to disk ask well
save(fullfile(outDirName, 'all_results.mat'), ...
    'results', 'significantEventProfiles', 'significantEvents');

% Inform
fprintf(1, '\nResults saved into %s.\n', outDirName);
