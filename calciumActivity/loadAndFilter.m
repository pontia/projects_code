function [filterDataset, metadata, dataset] = loadAndFilter(fileName, eventRadius)
% Load and filter given file (time series)
%
% SYNOPSIS
%
%   [filterDataset, metadata, dataset] = loadAndFilter(fileName, eventRadius)
%
% INPUT
%
%    fileName:    full path to file to read.
%    eventRadius: support radius for the Gaussian filter: corresponds to the
%                 expected biological event size.
%
% OUTPUT
%
%    filterDataset: filtered dataset (time series). Further analysis should
%                   be performed on this.
%    metadata     : file metadata
%    dataset      : original dataset.
%
% Aaron Ponti, 2015/06/23

if nargin == 0
    fileName = [];
    eventRadius = 3;
elseif nargin == 1
    eventRadius = 3;
elseif nargin == 2
    % All set
else
    error('Wrong number of input parameters.');
end

if isempty(fileName)
    [fName, dirName] = uigetfile('*.*', 'Please pick file to process...');
    if isequal(fName, 0)
        return
    end
    fileName = fullfile(dirName, fName);
end
    
% Some small optimization...
[~, ~, e] = fileparts(fileName);
if strcmpi(e, '.ims')
    % Load the dataset
    [dataset, metadata] = imarisread(fileName);
else
    % Load the dataset
    [dataset, metadata] = loadGeneric(fileName);
end

if isempty(dataset)
    error(['Could not open file ', fileName]);
end

[nTimepoints, ~] = size(dataset);

% Create delta images
nSteps = nTimepoints;
filterDataset = cell(nSteps, 1);

hWaitbar = waitbar(0, 'Filtering images...');

% Filter
sigma = eventRadius / 2;
sz = round(3 .* sigma);
if mod(sz, 2) == 0
    sz = sz + 1;
end

k = fspecial('gaussian', [sz sz], sigma);

% The channel to be analyzed is currently expected to be the first!
signalChannel = 1;

for i = 1 : nTimepoints
    
    filterDataset{i} = imfilter(double(dataset{i, signalChannel}), k);
    waitbar(i/nTimepoints, hWaitbar);

end

close(hWaitbar);
