function eventProfiles = extractEventProfiles(acceptedEvents, dataset)
% Extract intensity profiles for all events.
%
% SYNOPSIS
%
%   eventProfiles = extractAndSaveEventProfilesToDisk(acceptedEvents, dataset)
%
% INPUT
%
%   acceptedEvents: accepted events as returned by scoreEvents()
%   dataset       : cell array of 2D images (time series)
%
% OUTPUT
%
%   eventProfiles    : nTimepoints x nEvents matrix of intensity profiles.
%
% Aaron POnti, 2015/06/23

% Number of images
nImages = numel(dataset);

% Now extract the intensitities
nAcceptedLocalMaxima = size(acceptedEvents, 1);
eventProfiles = zeros(nImages, nAcceptedLocalMaxima);

hWaitbar = waitbar(0, 'Exctracting and baselining intensity profiles...');

for i = 1 : nAcceptedLocalMaxima
    
    x = acceptedEvents(i, 2);
    y = acceptedEvents(i, 3);

    for j = 1 : nImages
        
        eventProfiles(j, i) = dataset{j}(y, x);
        
    end

    % Subtract baseline
    eventProfiles(:, i) = subtractBaseline(eventProfiles(:, i));


    waitbar(i/nAcceptedLocalMaxima, hWaitbar);
    
    
end

close(hWaitbar);



% =========================================================================

function profile = subtractBaseline(profile)

n = round(numel(profile) / 3);
if mod(n, 2) == 0
    n = n + 1;
end

e = padarray(profile(:)', [0 n], 'symmetric', 'both');
filtE = medfilt1(e, n);
fittedBaseline = filtE(n + 1: end - n);

profile = profile - fittedBaseline';
