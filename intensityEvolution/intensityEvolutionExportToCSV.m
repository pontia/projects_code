function intensityEvolutionExportToCSV(intensityProfile, outFileName, timeInterval)
% Saves the intensity profiles to CSV file
%
% SYNOPSIS
%
%   intensityEvolutionExportToCSV(intensityProfile, outFileName, timeInterval)
%
% INPUT
%
%   intensityProfile: nChannels x nTimepoints matrix of mean intensity
%                     values for the segmented signal.
%   outFileName     : (optional) Output file name with full path. Omit or
%                     set it to [] to pick the file name with a save
%                     dialog.
%   timeInterval    : time interval between two timepoints in the file.
%                     Used for plotting.
%
% OUTPUT
%
%   None
%
% Aaron Ponti, 2015/05/18
if nargin == 1
    outFileName = [];
    timeInterval = 1;
elseif nargin == 2
    timeInterval = 1;
elseif nargin == 3
    % All set
else
    error('Wrong number of input parameters.');
end

% Ask the user to pick a file
if isempty(outFileName)
    [fName, dirName] = uiputfile('*.tsv', 'Save intensity profiles as...');
    if isequal(fName, 0)
        return
    end
    outFileName = fullfile(dirName, fName);
end

% Number of channels 
[nChannels, nTimepoints] = size(intensityProfile);

timepoints = 0 : timeInterval : timeInterval * (nTimepoints - 1);

% Open the file
fid = fopen(outFileName, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write header
fprintf(fid, '%12s\t', 'Time');
for i = 1 : nChannels
    fprintf(fid, '%12s\t', ['Channel ', num2str(i)]);
end
fprintf(fid, '\n');

% Wrtie the values
for i = 1 : nTimepoints

    % Store the timepoint
    fprintf(fid, '%12d\t', timepoints(i));
    
    for j = 1 : nChannels
        
        % Store intensities for all channels and current timepoint
        fprintf(fid, '%12.4f\t', intensityProfile(j, i));

    end
    
    % New line
    fprintf(fid, '\n');

end

if fid ~= 1
    fclose( fid );
end
