function intensities = intensityEvolution(fileName, createQCFigures)
% Segments and extracts intensity profile from time series.
%
% SYNOPSIS
%
%   [1] intensities = intensityEvolution()
%   [2] intensities = intensityEvolution(fileName)
%   [3] intensities = intensityEvolution(fileName, createQCFigures)
%
% INPUT
%
%   fileName       : (optional, default = []) Name with full path of the file to 
%                    be analyzed. Set to [] to pick a file via a file dialog.
%   createQCFigures: (optional, default = 1) Toggles creation of control 
%                    figures. Set to 0 to disable.
%
% OUTPUT
%
%   intensities    : nChannels x nTimepoints matrix of mean intensity
%                    values for the segmented signal.
%
% Aaron Ponti, 2015/04/30

if nargin == 0
    fileName = [];
    createQCFigures = 1;
elseif nargin == 1
    createQCFigures = 1;
elseif nargin == 2
    % All set
else
    error('Wrong number of input arguments.');
end

if nargin == 0 || isempty(fileName)
    [fName, dirName] = uigetfile('*.*', 'Please pick file to process...');
    if isequal(fName, 0)
        return
    end
    fileName = fullfile(dirName, fName);
end
    
% Some small optimization...
[~, ~, e] = fileparts(fileName);
if strcmpi(e, '.ims')
    % Load the dataset
    dataset = imarisread(fileName);
else
    % Load the dataset
    dataset = loadGeneric(fileName);
end

if isempty(dataset)
    error(['Could not open file ', fileName]);
end

% Alias
[nTimepoints, nChannels] = size(dataset);

% Allocate space to store the intensity information
intensities = zeros(nChannels, nTimepoints);

% Prepare the output name for the quality control figures
[d, f, e] = fileparts(fileName);
strg = sprintf('%%.%dd', 3);

% Create a subfolder for this run
tmpDirName = fullfile(d, ['QC_', strrep(f, ' ', '_'), strrep(e, '.', '_')]);
mkdir(tmpDirName);
disp([fileName, ': saving quality control figures in ', tmpDirName]);

% Now process in parallel
parfor i = 1 : nTimepoints
    
    if createQCFigures == 1
        meanFgd = segmentSignal(dataset(i, :), ...
            fullfile(tmpDirName, [f, '_t_', sprintf(strg, i)]));
    else
        meanFgd = segmentSignal(dataset(i, :), []);
    end
    
    intensities(:, i) = meanFgd;
    
end
