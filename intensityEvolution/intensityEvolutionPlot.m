function hFig = intensityEvolutionPlot(intensityProfile, titleStr, timeInterval, maxY, hFigure)
% If figure handle is passed, try appending to the existing plot
%
% SYNOPSIS
%
%   hFig = intensityEvolutionPlot(intensityProfile, titleStr, timeInterval, maxY, hFigure)
%
% INPUT
%
%   intensityProfile: nChannels x nTimepoints matrix of mean intensity
%                     values for the segmented signal.
%   titleStr        : plot title
%   timeInterval    : time interval between two timepoints in the file.
%                     Used for plotting.
%   maxY            : (optional, default = []). Max value for the Y axis 
%                     (set to [] to automatic scaling to maximum intensity
%                     in the matrix)
%   hFigure         : (optional, default = []). Existing figure handle to
%                     append current plot to. Set to [] to plot to a new
%                     figure.
%
% OUTPUT
%
%   hFig            : figure handle of current plot (to be optionally reused).
%
% Aaron Ponti, 2015/05/18

if nargin == 2
    maxY = [];
    hFig = figure;
    timeInterval = 1;
elseif nargin == 3
    maxY = [];
    hFig = figure;
elseif nargin == 4
    hFig = figure;
elseif nargin == 5
    if ishandle(hFigure)
        hFig = hFigure;
    else
        hFig = figure;
    end
else
    error('Wrong number of input parameters.');
end

hold on;

colors = {'b', 'r', 'g', 'k', 'm', 'y'};

[nChannels, nTimepoints] = size(intensityProfile);

xTicks = 0 : timeInterval : timeInterval * (nTimepoints - 1);

hPlotHandles = zeros(1, nChannels);
for i = 1 : nChannels

    if i < numel(colors)
        clr = colors{i};
    else
        clr = 'b';
    end
    
    hPlotHandles(i) = plot(xTicks, intensityProfile(i, :), 'Color', clr, 'DisplayName', ['Ch ', num2str(i)]);
        
end

if ~isempty(maxY)
    set(gca, 'YLim', [0 maxY])
end

set(get(hFig, 'CurrentAxes'), 'XTick', xTicks);

xlabel('Timepoint');
ylabel('Mean intensity');
title(titleStr);

legend(hPlotHandles);
