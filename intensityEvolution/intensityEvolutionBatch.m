% ===
%
% USE THE FOLLOWING AS AN EXAMPLE TO ANALYZE GROUPS OF DATASETS
%
% ===

% % CONTROLS
%
% controlsFileName = 'F:\Data\Projects\Bram\control.mat';
% 
% intensitiesControl1 = intensityEvolution('F:\Data\Projects\Bram\control1.ims');
% intensitiesControl2 = intensityEvolution('F:\Data\Projects\Bram\control2.ims');
% intensitiesControl3 = intensityEvolution('F:\Data\Projects\Bram\control3.ims');
% intensitiesControl4 = intensityEvolution('F:\Data\Projects\Bram\control4.ims');
% 
% save(controlsFileName, 'intensitiesControl1', 'intensitiesControl2', 'intensitiesControl3', 'intensitiesControl4');

% % TREATED
%
% treatedFileName = 'F:\Data\Projects\Bram\treated.mat';
% 
% intensitiesTreated1 = intensityEvolution('F:\Data\Projects\Bram\treat1.ims');
% intensitiesTreated2 = intensityEvolution('F:\Data\Projects\Bram\treat2.ims');
% intensitiesTreated3 = intensityEvolution('F:\Data\Projects\Bram\treat3.ims');
% intensitiesTreated4 = intensityEvolution('F:\Data\Projects\Bram\treat4.ims');
% 
% save(treatedFileName, 'intensitiesTreated1', 'intensitiesTreated2', 'intensitiesTreated3', 'intensitiesTreated4');

% ===
%
% EXAMPLE PLOT
%
% ===
% 
% nChannels = size(intensitiesControl, 1);
% 
% hF = figure;
% hold on
% hPlots = zeros(1, nChannels);
% for i = 1 : size(intensitiesControl, 1) % Number of channels
%     
%     hPlots(i) = plot(intensitiesControl(i, :), '-o', 'DisplayName', ['Channel ', num2str(i)]);
%     
% end
% 
% ylabel('Mean signal intensity');
% xlabel('Timepoint');
% title('Intensity evolution');
% legend(hPlots);
% 
% savefig(hF, 'F:\Data\Projects\Bram\control1.fig');

timeInterval = 1;

%cd('F:\Data\Projects\Bram\');
cd('/media/pontia/My Book/Data/Projects/Bram/');

intensities_E_cadherin_GFP_cyclosA_complete = intensityEvolution('3D HKC-8 E-cadherin GFP_cyclosA_complete.ims');
save('intensitiesAll.mat', 'intensities_E_cadherin_GFP_cyclosA_complete');

intensities_E_cadherin_GFP_AA_complete = intensityEvolution('3D HKC-8 E-cadherin GFP_AA_complete.ims');
save('intensitiesAll.mat', 'intensities_E_cadherin_GFP_AA_complete', '-append');

intensities_E_cadherin_GFP_no_drug_complete = intensityEvolution('3D HKC-8 E-cadherin GFP_no drug_complete.ims');
save('intensitiesAll.mat', 'intensities_E_cadherin_GFP_no_drug_complete', '-append');

intensities_CycleTrak_AA_complete = intensityEvolution('3D HKC-8 CycleTrak_AA_complete.ims');
save('intensitiesAll.mat', 'intensities_CycleTrak_AA_complete', '-append');

intensities_CycleTrak_cyclosA_complete = intensityEvolution('3D HKC-8 CycleTrak_cyclosA_complete.czi');
save('intensitiesAll.mat', 'intensities_CycleTrak_cyclosA_complete', '-append');

intensities_CycleTrak_control_no_drug_complete = intensityEvolution('3D HKC-8 CycleTrak_control no drug_complete.czi');
save('intensitiesAll.mat', 'intensities_CycleTrak_control_no_drug_complete', '-append');

% Create plots
hF = intensityEvolutionPlot(intensities_E_cadherin_GFP_cyclosA_complete, 'E cadherin GFP cyclosA complete', timeInterval, 2600);
savefig(hF, 'E_cadherin_GFP_cyclosA_complete.fig');
hF = intensityEvolutionPlot(intensities_E_cadherin_GFP_AA_complete,  'E cadherin GFP GFP AA complete', timeInterval, 2600);
savefig(hF, 'E_cadherin_GFP_AA_complete.fig');
hF = intensityEvolutionPlot(intensities_E_cadherin_GFP_no_drug_complete, 'E cadherin GFP no drug complete', timeInterval, 2600);
savefig(hF, 'E_cadherin_GFP_no_drug_complete.fig');
hF = intensityEvolutionPlot(intensities_CycleTrak_AA_complete, 'CycleTrak AA complete', timeInterval, 2600);
savefig(hF, 'CycleTrak_AA_complete.fig');
hF = intensityEvolutionPlot(intensities_CycleTrak_cyclosA_complete, 'CycleTrak cyclosA complete', timeInterval, 2600);
savefig(hF, 'CycleTrak_cyclosA_complete.fig');

% Save intensity profiles to tab-separated-value files
intensityEvolutionExportToCSV(intensities_E_cadherin_GFP_cyclosA_complete, 'E_cadherin_GFP_cyclosA_complete.tsv', timeInterval)
intensityEvolutionExportToCSV(intensities_E_cadherin_GFP_AA_complete, 'E_cadherin_GFP_AA_complete.tsv', timeInterval)
intensityEvolutionExportToCSV(intensities_E_cadherin_GFP_no_drug_complete, 'E_cadherin_GFP_no_drug_complete.tsv', timeInterval)
intensityEvolutionExportToCSV(intensities_CycleTrak_AA_complete, 'CycleTrak_AA_complete.tsv', timeInterval)
intensityEvolutionExportToCSV(intensities_CycleTrak_cyclosA_complete, 'CycleTrak_cyclosA_complete.tsv', timeInterval)
intensityEvolutionExportToCSV(intensities_CycleTrak_control_no_drug_complete, 'CycleTrak_control_no_drug_complete.tsv', timeInterval)


