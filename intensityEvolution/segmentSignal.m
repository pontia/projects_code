function [meanFgd, BWs] = segmentSignal(dataset, outBaseFileName)
% Segment the signal and returns a BW mask
% 
% SYNOPSIS
%
%   [meanFgd, BWs] = segmentSignal(dataset, outBaseFileName)
%
%
% INPUT
%
%   stack          : cell array of size [1 x nChannels]. Each position in
%                    the cell array is the 3D stack for the corresponding
%                    channel.
%   outBaseFileName: optional. Set it to a univocal name for the dataset
%                    and timepoint to create and save quality control
%                    images of the segmentation.
%
% OUTPUT  
%
%   meanFg         : nChannels x nTimepoints matrix of mean intensity
%                    values for the segmented foreground signal.
%   BW             : black and white mask per channel
%
% Aaron Ponti, 2015/04/30

if nargin == 1
    outBaseFileName = [];
elseif nargin == 2
    % All set
else
    error('1 or 2 input parameters expected.');
end

if size(dataset, 1) > 1
    error('The dataset must have only one time point!');
end

nChannels = size(dataset, 2);

% Initialize output
meanFgd = zeros(nChannels, 1);

if nargout == 2
    BWs = cell(1, nChannels);
end

for c = 1 : nChannels

    % Get current stack
    stack = dataset{c};
    
    % Threshold
    T = isoDataThreshold(stack);
    if isnan(T)
        T = 0;
    end

    % Create BW mask and clean it
    BW = false(size(stack));
    BW(stack >= T) = 1;
    BW = filterSmallComponents(BW, 21);
    
    if ~isempty(outBaseFileName)
        
        % Maximum value in stack for normalization
        mx = prctile(stack(:), 99);
        
        strg    = sprintf('%%.%dd', 3);
        for i = 1 : size(stack, 3)
            %             imgRGB = overlaySegmentation(stack(:, :, i), BW(:, :, i), mx);
            %             fName = [outBaseFileName, ...
            %                 '_c_', sprintf(strg, c), '_z_', sprintf(strg, i), '.tif'];
            %             imwrite(imgRGB, fName, 'Compression', 'lzw');
            maskedImg = maskImage(stack(:, :, i), BW(:, :, i), mx);
            fName = [outBaseFileName, ...
                '_c_', sprintf(strg, c), '_z_', sprintf(strg, i), '.tif'];
            imwrite(maskedImg, fName, 'Compression', 'lzw');

        end
        
    end

    % Store current mean intensity
    meanFgd(c) = mean(stack(BW == 1));
    
    if nargout == 2
    
        BWs{c} = BW;

    end
    
end

% =========================================================================

function T = isoDataThreshold(img)

img = single(img(:));

mn = min(img);
mx = max(img);

Tlast = Inf;
T = 0.5 * (mn + mx);
tol = 5e-1;
while (abs(T - Tlast) > tol)
    
    bkg = img(img < T);
    frg = img(img >= T);
    
    mnB = mean(bkg);
    mnF = mean(frg);
    
    Tlast = T;
    T = 0.5 * (mnB + mnF);
    
    
end

% =========================================================================

function fBW = filterSmallComponents(BW, szThresh)

L = bwconncomp(BW);

fBW = false(size(BW));

for i = 1 : numel(L.PixelIdxList)
    
    if numel(L.PixelIdxList{i}) >= szThresh
        fBW(L.PixelIdxList{i}) = 1;
    end
    
end

% =========================================================================

function imgRGB = overlaySegmentation(img, mask, mx)

imgRGB = zeros([size(img), 3], 'like', img);
m = single(intmax(class(img)));
maskedImg = m .* single(single(img) .* mask) ./ single(mx);
imgRGB(:, :, 1) = cast(0.5 * m * (~mask), 'like', img);
imgRGB(:, :, 2) = cast(maskedImg, 'like', img);

% =========================================================================

function maskedImg = maskImage(img, mask, mx)

m = single(intmax(class(img)));
maskedImg = m .* single(single(img) .* mask) ./ single(mx);
maskedImg = cast(maskedImg, 'like', img);


