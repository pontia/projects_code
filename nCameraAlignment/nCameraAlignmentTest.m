function nCameraAlignmentTest(outDirName)
% Create some synthetic images to test nCameraAlignment.
%
% SYNOPSIS
%
%   nCameraAlignmentTest(outDirName)
%
% INPUT
%
%   outDirName: full path to the folder where to save the synthetic images.
%               Set to [] to pick via a file dialog.
%
% OUTPUT
% 
%   None
%
% Aaron Ponti, 2015/04/28

if nargin == 0
    outDirName = [];
end

% Choose folder where to store the results
if isempty(outDirName)
    outDirName = uigetdir('Pick folder to store the aligned images');
    if isequal(outDirName, 0)
        return
    end
end

pos1Dir = fullfile(outDirName, 'Pos1');
if exist(pos1Dir, 'dir') == 0
    mkdir(pos1Dir);
end
pos2Dir = fullfile(outDirName, 'Pos2');
if exist(pos2Dir, 'dir') == 0
    mkdir(pos2Dir);
end

t = [7 11]; % [x, y]
img = imread('cameraman.tif');
img2 = imtranslate(img, t);

imwrite(img, fullfile(pos1Dir, 'img01.tif'), 'Compression', 'none');
imwrite(img2, fullfile(pos2Dir, 'img01.tif'), 'Compression', 'none');

fprintf(1, 'Pos2 is translated with respect to Pos1 by (x = %d, y = %d).\n', t(1), t(2));
