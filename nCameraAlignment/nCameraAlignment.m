function T = nCameraAlignment(inDirName, outDirName, algorithm, nIter, transform)
% Aligns images taken from n cameras over time. Currently, n is fixed to 2.
%
% SYNOPSIS
%
%   T = nCameraAlignment(inDirName, outDirName, algorithm, nIter, transform)
%
% INPUT
%
%   outDirName: full path to the folder where the raw images are.
%               Images from each position are expected to be in
%               subfolders 'Pos1', 'Pos2', ... 'Pos{n}'.
%               Set to [] to pick via a file dialog.
%   outDirName: full path to the folder where to save the aligned images.
%               Set to [] to pick via a file dialog.
%   algorithm : registration algorithm (optional, default = 'lk'). One of
%                  'lk': Lukas-Kanade
%                  'surf': Speeded Up Robust Features
%                  'phase': phase-only correlation
%   nIter     : number of iterations (optional, default = 20). For 
%               algorithm == 'surf' it is meant to be 'maximum number of
%               iterations. Ignored for algorithm == 'phase'.
%   transform : allowed transformation. (optional, default = 'translation').
%                  algorithm == 'lk', one of:
%                     'translation'
%                     'euclidean' (i.e. rigid body)
%                     'affine'
%                     'homography'
%                  algorithm == 'surf', one of:
%                     'translation'
%                     'euclidean' (i.e. rigid body)
%                     'affine'
%                     'homography',
%                     'similarity'
%                  algorithm == 'phase', only:
%                     'translation'
%
% OUTPUT
% 
%   T         : cell array containing all transformation matrices. Please
%               notice that a translation matrix for 'lk' and 'surf' 
%               contains the transformation of image1 to bring it on top
%               of image2; while the translation vector returned by the
%               'phase' algorithm contains the necessary step to bring
%               image2 back on top of image1. The end result of the
%               transformation is the same. The correction in the first two
%               cases applies the inverse of the estimated transformation
%               matrix. If you compare the translation vector of 'lk' and
%               'surf' with the one from 'phase', remember that:
%
%                    t = [x, y]   for 'lk' and 'surf'
%                    t = [-y, -x] for 'phase'
%
% Aaron Ponti, 2015/04/28

T = {};

DEBUG = 1;

if nargin == 0
    inDirName = [];
    outDirName = [];
    algorithm = 'lk';
    nIter = 10;
    transform = 'translation';
elseif nargin == 1
    outDirName = [];
    algorithm = 'lk';
    nIter = 10;
    transform = 'translation';
elseif nargin == 2
    algorithm = 'lk';
    nIter = 10;
    transform = 'translation';
elseif nargin == 3
    nIter = 10;
    transform = 'translation';
elseif nargin == 4
    transform = 'translation';
elseif nargin == 5
    % All set
else
    error('Wrong number of input parameters.');
end

% We start with just one z level - later we will pool all relevant
% positions. Moreover, now we only align for 2 positions - later we can
% relax this.

% Choose folder containing positions 1 and 2
if isempty(inDirName)
    inDirName = uigetdir('Pick folder containing the positions to be aligned');
    if isequal(inDirName, 0)
        return
    end
end

% Choose folder where to store the results
if isempty(outDirName)
    outDirName = uigetdir('Pick folder to store the aligned images');
    if isequal(outDirName, 0)
        return
    end
end

% Build the full path to the position folders
pos1Dir = fullfile(inDirName, 'Pos1');
if ~exist(pos1Dir, 'dir')
    error(['Expected folder ', pos1Dir, ' doe not exist.']);
end
pos2Dir = fullfile(inDirName, 'Pos2');
if ~exist(pos2Dir, 'dir')
    error(['Expected folder ', pos2Dir, ' doe not exist.']);
end

% Read in the sequences of files
pos1Files = getFilteredFileSequence(pos1Dir);
pos2Files = getFilteredFileSequence(pos2Dir);

nFiles = numel(pos1Files);
if nFiles ~= numel(pos2Files)
    error('Mismatch in the number of files in the two folders.');
end

% Parameters for the various algorithms
parameters = struct();

switch algorithm
        
    case 'lk',
        
        parameters.levels = 1;
        parameters.iterations = nIter;
        parameters.transform = transform;

    case 'surf',
        
    case 'phase',

    otherwise, error('Bad value for ''algorithm''.');
end
   
% Allocate memory to store the transformations
T = cell(1, nFiles);

% Some constants
[sizeY, sizeX] = size(imread(pos1Files{1}));
sY = 1 : sizeY;
sX = 1 : sizeX;

tic;

% Process all planes in the stack
parfor i = 1 : nFiles
%for i = 1 : nFiles
   
    % Initialize the temporary variables to prevent a warning from parfor.
    t = []; %#ok<NASGU>
    aligned = []; %#ok<NASGU>
    
    % Read the two images
    image1 = imread(pos1Files{i});
    image2 = imread(pos2Files{i});

    % Make sure they are 1-channel, gray-valued
    image1 = prepareImage(image1);
    image2 = prepareImage(image2);

    switch algorithm
        
        case 'lk',
        
            % Find the transformation
            t = iat_LucasKanadeIC(image2, image1, parameters);
        
            % Apply the transformation
            aligned = iat_inverse_warping(image2, t, parameters.transform, sX, sY);
            
        case 'surf',

            % Extract features
            [d1, l1] = iat_surf(image2, 128);
            [d2, l2] = iat_surf(image1, 128);
        
            % Match them
            [~, ~, imgInd, tmpInd] = iat_match_features(d1, d2, 0.95);
        
            % Get rid of outliers in the matches
            X1 = l1(imgInd, 1:2)';
            X2 = l2(tmpInd, 1:2)';
            X1h = iat_homogeneous_coords(X1);
            X2h = iat_homogeneous_coords(X2);
            [~, t] = iat_ransac(X2h, X1h, transform, 'maxIter', nIter, ...
                'maxInvalidCount', 10, 'tol', 0.1);
        
            % Now transform the image
            aligned = iat_inverse_warping(image2, t, transform, sX, sY);
    
        case 'phase'
            
            [aligned, t] = alignWithPhaseCorrelation(image1, image2);
            
        otherwise,
            
            error('Bad value for algorithm.');
            
    end

    % Cast appropriately
    aligned = cast(aligned, 'like', image1);

    % Write the aligned image
    [~, n, e] = fileparts(pos2Files{i});
    imwrite(aligned, fullfile(outDirName, [n, e]), 'Compression', 'none');

    % Save quality control image?
    if DEBUG == 1
        qcFileName = fullfile(outDirName, [n, '_qc', e]);
        saveQualityControl(image1, image2, aligned, qcFileName);
    end
  
    % Store the transform
    T{i} = t;

end

% Elapsed time
fprintf(1, 'Elapsed time: %.2fs\n', toc);


% =========================================================================
function filteredFileSequence = getFilteredFileSequence(dirName)

fileSequence = dir(dirName);
nFiles = numel(fileSequence);
filteredFileSequence = cell(1, nFiles);
c = 0;
for i = 1 : nFiles
   if fileSequence(i).isdir == 0
       c = c + 1;
       filteredFileSequence{c} = fullfile(dirName, fileSequence(i).name);
   end
end
if c > 0
    filteredFileSequence = filteredFileSequence(1:c);
end

% =========================================================================
function saveQualityControl(image1, image2, aligned, qcFileName)

[sy, sx] = size(image1);
[sy2, sx2] = size(image2);
[sy3, sx3] = size(aligned);
if sy ~= sy2 || sx ~= sx2 || sy ~= sy3 || sx ~= sx3
    disp(['Images do not have the same size. ', ...
        'Skipping generation of quality control figure.']);
    return
end

imgRGB = zeros([sy, 2 * sx, 3], 'like', image1);
imgRGB(:, 1 : sx, 1) = image1;
imgRGB(:, sx + 1 : end, 1) = image1;
imgRGB(:, 1 : sx, 2) = image2;
imgRGB(:, sx + 1 : end, 2) = aligned;
imwrite(imgRGB, qcFileName, 'Compression', 'none');

% =========================================================================
function [aligned, t] = alignWithPhaseCorrelation(template, image)

% Calculare phase correlation
fcorr = phaseCorr2D(image, template);

% Calculate offsets
offsets = fix(size(template) ./ 2) + 1;

% Calculate the shift
[ ~, ind ] = max(fcorr(:));
[ y, x ]   = ind2sub(size(fcorr), ind);
t = [ y x ] - offsets;

% Apply the transform
if any(t ~= 0)

    % Resampler
    resampler = makeresampler({'nearest', 'nearest'}, 'fill');
    
    % Shift
    TM = [eye(length(t)); t];
    TF = maketform('affine', TM);
    
    % Apply translation
    aligned = tformarray( ...
        image, TF, resampler, 1 : ndims(image), ...
        1 : ndims(image), size(image), [], 0);
     
else
    
    aligned = image;
    
end

% =========================================================================
function img = prepareImage(img)

nChannels = size(img, 3);

if ~(nChannels == 1 || nChannels == 3)
    img = img(:, :, 1);
end

img = im2double(img);

    
            