# -*- coding: utf-8 -*-
import math
import pickle
import os
import cv2
import numpy as np
import scipy as sp
import sys
import matplotlib.pyplot as plt


def pad_image(img, target_size):
    """Pad the image to the requested size."""
    start_size = img.shape

    pad_h = target_size[0] - start_size[0]
    pad_v = target_size[1] - start_size[1]

    if pad_h < 0 or pad_v < 0:
        raise Exception("Target size must be larger than source size.")

    # Horizontal padding
    pad_left = pad_h / 2  # Integer division

    # Vertical padding
    pad_top = pad_v / 2  # Integer division

    # Now embed the starting image into the padded image
    padded = np.zeros(target_size, img.dtype)
    padded[pad_left:pad_left + start_size[0],
           pad_top:pad_top + start_size[1]] = img

    return padded


def simple_region_growing(img, seed, threshold=1):
    """
    A (very) simple implementation of region growing.
    Extracts a region of the input image depending on a start position and a
    stop condition.
    The input should be a single channel 8 bits image and the seed a pixel
    position (x, y).
    The threshold corresponds to the difference between outside pixel intensity
    and mean intensity of region.
    In case no new pixel is found, the growing stops.
    Outputs a single channel 8 bits binary (0 or 255) image. Extracted region
    is highlighted in white.
    """

    # Image size
    dims = img.shape

    # Check seed
    if (seed[0] or seed[1]) < 0:
        raise ValueError("(%s) Seed should have positive values!" %
                         (sys._getframe().f_code.co_name))
    elif ((seed[0] > dims[0]) or (seed[1] > dims[1])):
        raise ValueError("(%s) Seed values greater than img size!" %
                         (sys._getframe().f_code.co_name))

    reg = np.zeros(dims, dtype=np.uint8)

    # Parameters
    mean_reg = float(img[seed[1], seed[0]])
    size = 1
#    pix_area = dims[0]*dims[1]
    pix_area = 0.5 * dims[0] * 0.5 * dims[1]

    contour = []  # will be [ [[x1, y1], val1],..., [[xn, yn], valn] ]
    contour_val = []
    dist = 0
    # TODO: may be enhanced later with 8th connectivity
    orient = [(1, 0), (0, 1), (-1, 0), (0, -1)]  # 4 connectivity
    cur_pix = [seed[0], seed[1]]

    # Spreading
    while(dist < threshold and size < pix_area):
        # adding pixels
        for j in range(4):
            # select new candidate
            temp_pix = [cur_pix[0] + orient[j][0], cur_pix[1] + orient[j][1]]

            # check if it belongs to the image
            is_in_img = dims[0] > temp_pix[0] > 0 and dims[1] > temp_pix[1] > 0
            # candidate is taken if not already selected before
            if (is_in_img and (reg[temp_pix[1], temp_pix[0]] == 0)):
                contour.append(temp_pix)
                contour_val.append(img[temp_pix[1], temp_pix[0]])
                reg[temp_pix[1], temp_pix[0]] = 150
        # add the nearest pixel of the contour in it
        dist = abs(int(np.mean(contour_val)) - mean_reg)

        dist_list = [abs(i - mean_reg) for i in contour_val]
        dist = min(dist_list)  # get min distance
        index = dist_list.index(min(dist_list))  # mean distance index
        size += 1  # updating region size
        reg[cur_pix[1], cur_pix[0]] = 255

        # updating mean MUST BE FLOAT
        mean_reg = (mean_reg*size + float(contour_val[index]))/(size+1)
        # updating seed
        cur_pix = contour[index]

        # removing pixel from neigborhood
        del contour[index]
        del contour_val[index]

    return reg


def sampleCircle(r, center=(0, 0), n=100):
    """Crude way of creating pixel coordinates along a circle circumference.

    This will be replaced. For now we need some coordinates that we can
    transform.

    Keyword arguments:
    r -- circumference radius
    center -- circumference center (x, y)
    n -- number of points along the circumference
    """

    return np.array([(center[0] + math.cos(2 * math.pi / n * x) * r,
                     center[1] + math.sin(2 * math.pi / n * x) * r)
                     for x in xrange(0, n + 1)])


def findCentralCircle(circles, imgSize):
    """Return the circle whose center is closest to the center of the image."""

    if (circles is None):

        return None

    elif (circles.shape[1] == 1):

        return circles[0][0]

    else:

        # Find the circle whose center is closest to the center of the image
        centerCoords = [imgSize[0]/2, imgSize[1]/2]

        d = np.zeros(circles.shape[1])
        for i in range(circles.shape[1]):
            d[i] = np.linalg.norm(circles[0][i][0:2] - centerCoords)

        sel = np.where(d == min(d))

    return circles[0][sel[0]][0]


def flipImage(img, direction='y'):
    """Flips the image in the given direction.

    direction is: 'y', 'x', or 'both'.
    """

    # Copy the image
    timg = img.copy()

    # Flip
    if direction == "y":

        timg = cv2.flip(img, 0)

    elif direction == "x":

        timg = cv2.flip(img, 1)

    elif direction == "both":

        timg = cv2.flip(img, -1)

    else:
        raise Exception("Invalid flipping direction.")

    return timg


def invertImage(img):
    """Invert a copy of the image."""

    # Copy image
    timg = img.copy()

    # Invert
    if img1.dtype == np.uint8:
        timg = 255 - timg
    elif img1.dtype == np.uint16:
        timg = 65535 - timg
    else:
        raise Exception("Bad data type.")

    return timg


def findCircles(img, cannyThresh=100, accThresh=30, med_filter=True,
                invert=True, minRadius=0, maxRadius=0):
    """Finds all circles in the image.

    Goal: set the parameters such that only one circle is found.

    Keyword arguments:
    img1 -- target image (must be grayscale)
    cannyThresh -- threshold for Canny edge detector
    accThresh -- threshold for accumulator
    med_filt -- fist median filter to reduce the number of false circles found
    invert -- invert the image first
    minRadius -- min circle radius
    maxRadius -- max circle radius
    """

    if invert:
        timg = invertImage(img)
    else:
        timg = img.copy()

    if med_filter:
        timg = cv2.medianBlur(timg, 5)

    # Find circles with the HoughCircles function
    # Notice the fix: cv2.cv.CV_HOUGH_GRADIENT
    circles = cv2.HoughCircles(timg, cv2.cv.CV_HOUGH_GRADIENT,
                               1, 20, param1=cannyThresh, param2=accThresh,
                               minRadius=minRadius, maxRadius=maxRadius)

    if circles is None:
        return None

    # Round coordinates
    circles = np.uint16(np.around(circles))

    return circles


def drawCircles(circles, img):
    """Draws all circles on image and returns the image."""

    # Create the control figure
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    for i in circles[0, :]:

        # Draw the circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)

        # Draw the circle center
        cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)

    # Return all
    return cimg


def displayMatches(img1, img2, sel_matches, k1, k2, maxMatches=None):
    """Displays the matches on a control image and returns it."""

    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]
    view = sp.zeros((max(h1, h2), w1 + w2, 3), sp.uint8)
    view[:h1, :w1, 0] = img1
    view[:h2, w1:, 0] = img2
    view[:, :, 1] = view[:, :, 0]
    view[:, :, 2] = view[:, :, 0]

    n = 0
    for m in sel_matches:
        # draw the keypoints
        # print m.queryIdx, m.trainIdx, m.distance
        color = tuple([sp.random.randint(0, 255) for _ in xrange(3)])
        try:
            cv2.line(view,
                     (int(k1[m.queryIdx].pt[0]),
                      int(k1[m.queryIdx].pt[1])),
                     (int(k2[m.trainIdx].pt[0] + w1),
                      int(k2[m.trainIdx].pt[1])),
                     color)
            n = n + 1
            if maxMatches is not None:
                if n >= maxMatches:
                    break
        except:
            print(m.queryIdx, m.trainIdx)

    return view


def drawSingleCircle(circle, img):
    """Draws a single circle on image and returns the image."""

    # Create the control figure
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    if circle is not None:

        # Draw the circle
        cv2.circle(cimg, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)

        # Draw the circle center
        cv2.circle(cimg, (circle[0], circle[1]), 2, (0, 255, 0), 3)

    return cimg


def register_images_orb_alt(img1, img2, control_image=False):
    """Register 2 images from FAST/BRIEF key points (alternative
    implementation)."""

    detector = cv2.FeatureDetector_create("ORB")

    print(detector.getParams())

    descriptor = cv2.DescriptorExtractor_create("BRISK")
    matcher = cv2.DescriptorMatcher_create("BruteForce-Hamming")

    # detect keypoints
    kp1 = detector.detect(img1)
    kp2 = detector.detect(img2)

    print '#keypoints in image1: %d, image2: %d' % (len(kp1), len(kp2))

    # descriptors
    k1, d1 = descriptor.compute(img1, kp1)
    k2, d2 = descriptor.compute(img2, kp2)

    print '#keypoints in image1: %d, image2: %d' % (len(d1), len(d2))

    # match the keypoints
    matches = matcher.match(d1, d2)

    # visualize the matches
    print '#matches:', len(matches)
    dist = [m.distance for m in matches]

    print 'distance: min: %.3f' % min(dist)
    print 'distance: mean: %.3f' % (sum(dist) / len(dist))
    print 'distance: max: %.3f' % max(dist)

    # threshold: half the mean
    thres_dist = (sum(dist) / len(dist)) * 0.5

    # keep only the reasonable matches
    sel_matches = [m for m in matches if m.distance < thres_dist]

    print '#selected matches:', len(sel_matches)

    view = None
    if control_image:
        # Create quality contol image
        view = displayMatches(img1, img2, sel_matches, k1, k2, 10)

    # Reshape points to be used for transformation estimation
    s_pts = np.float32([kp1[m.queryIdx].pt for m in sel_matches]).reshape(-1, 1, 2)
    d_pts = np.float32([kp2[m.trainIdx].pt for m in sel_matches]).reshape(-1, 1, 2)

    # Estimate the image transformation
    M, mask = cv2.findHomography(s_pts, d_pts, cv2.RANSAC, 5.0)

    # Warp img1 based on the extracted transformation
    aligned = cv2.warpPerspective(img1, M, img1.shape)

    # Return
    return (aligned, M, mask, view)


def register_images_brief(img1, img2, control_image=False):
    """Register 2 images from FAST/BRIEF key points (alternative
    implementation)."""

    detector = cv2.FeatureDetector_create("SURF")
    descriptor = cv2.DescriptorExtractor_create("BRIEF")
    matcher = cv2.DescriptorMatcher_create("BruteForce-Hamming")

    # detect keypoints
    kp1 = detector.detect(img1)
    kp2 = detector.detect(img2)

    print '#keypoints in image1: %d, image2: %d' % (len(kp1), len(kp2))

    # descriptors
    k1, d1 = descriptor.compute(img1, kp1)
    k2, d2 = descriptor.compute(img2, kp2)

    print '#keypoints in image1: %d, image2: %d' % (len(d1), len(d2))

    # match the keypoints
    matches = matcher.match(d1, d2)

    # visualize the matches
    print '#matches:', len(matches)
    dist = [m.distance for m in matches]

    print 'distance: min: %.3f' % min(dist)
    print 'distance: mean: %.3f' % (sum(dist) / len(dist))
    print 'distance: max: %.3f' % max(dist)

    # threshold: half the mean
    thres_dist = (sum(dist) / len(dist)) * 0.5

    # keep only the reasonable matches
    sel_matches = [m for m in matches if m.distance < thres_dist]

    print '#selected matches:', len(sel_matches)

    view = None
    if control_image:
        # Create quality contol image
        view = displayMatches(img1, img2, sel_matches, k1, k2, 10)

    # Reshape points to be used for transformation estimation
    s_pts = np.float32([kp1[m.queryIdx].pt for m in sel_matches]).reshape(-1, 1, 2)
    d_pts = np.float32([kp2[m.trainIdx].pt for m in sel_matches]).reshape(-1, 1, 2)

    # Estimate the image transformation
    M, mask = cv2.findHomography(s_pts, d_pts, cv2.RANSAC, 5.0)

    # Warp img1 based on the extracted transformation
    aligned = cv2.warpPerspective(img1, M, img1.shape)

    # Return
    return (aligned, M, mask, view)


def register_images_orb(img1, img2, control_image=False):
    """Register 2 images from FAST/BRIEF key points.

    Keyword arguments:
    img1 -- target image (must be grayscale)
    img2 -- image to be registered (must be grayscale)
    """

    # Detect FAST/BRIEF points in both images
    orb = cv2.ORB(nfeatures=500)
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)


    img1_descr = cv2.drawKeypoints(img1, kp1, color=(0,255,0), flags=0)
    img2_descr = cv2.drawKeypoints(img2, kp2, color=(0,255,0), flags=0)
    cv2.imshow("Image 1 descriptors", img1_descr)
    cv2.imshow("Image 2 descriptors", img2_descr)


    # Create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1, des2)

    # Sort them by distance.
    matches = sorted(matches, key = lambda x : x.distance)

    view = None
    if control_image:
        # Create quality contol image
        view = displayMatches(img1, img2, matches, kp1, kp2, 10)

    # Reshape points to be used for transformation estimation
    s_pts = np.float32([kp1[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
    d_pts = np.float32([kp2[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)

    # Estimate the image transformation
    M, mask = cv2.findHomography(s_pts, d_pts, cv2.RANSAC, 5.0)

    # Warp img1 based on the extracted transformation
    aligned = cv2.warpPerspective(img1, M, img1.shape)

    # Return
    return (aligned, M, mask, view)


def register_images_flann(img1, img2, control_image=False):
    """Register 2 images from SURF key points.

    H. Bay, A. Ess, T. Tuytelaars, and L. Van Gool.
    “Speeded-up robust features (SURF)”
    Comput. Vis. image.
    September, 2008.

    Keyword arguments:
    img1 -- target image (must be grayscale)
    img2 -- image to be registered (must be grayscale)
    """

    # Detect SURF points in both images
    surf = cv2.SURF(400)
    surf.extended = True
    kp1, des1 = surf.detectAndCompute(img1, None)
    kp2, des2 = surf.detectAndCompute(img2, None)

    # Match the points
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=100)   # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)

    # Store all the good matches as per Lowe's ratio test.
    good = []
    for m, n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    view = None
    if control_image:
        # Create quality contol image
        sel_kp1 = [kp1[m.queryIdx] for m in good]
        sel_kp2 = [kp2[m.trainIdx] for m in good]
        view = displayMatches(img1, img2, good, sel_kp1, sel_kp2)

    # Extract the good points and reshape to use for transformation estimation
    s_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    d_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

    # Estimate the image transformation
    M, mask = cv2.findHomography(s_pts, d_pts, cv2.RANSAC, 5.0)

    # Warp img1 based on the extracted transformation
    aligned = cv2.warpPerspective(img1, M, img1.shape)

    return (aligned, M, mask, view)


def register_images_ecc(img1, img2):

    # Find size of image1
    sz = img1.shape

    # Define the motion model
    warp_mode = cv2.MOTION_HOMOGRAPHY

    # Define 2x3 or 3x3 matrices and initialize the matrix to identity
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        warp_matrix = np.eye(3, 3, dtype=np.float32)
    else:
        warp_matrix = np.eye(2, 3, dtype=np.float32)

    # Specify the number of iterations.
    number_of_iterations = 5000

    # Specify the threshold of the increment
    # in the correlation coefficient between two iterations
    termination_eps = 1e-10

    # Define termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
                number_of_iterations,  termination_eps)

    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix) = cv2.findTransformECC(img1, img2, warp_matrix,
                                             warp_mode, criteria)

    if warp_mode == cv2.MOTION_HOMOGRAPHY:

        # Use warpAffine for Translation, Euclidean and Affine
        img2_aligned = cv2.warpPerspective(img2, warp_matrix, (sz[1], sz[0]),
                                           flags=cv2.INTER_LINEAR +
                                           cv2.WARP_INVERSE_MAP)
    else:

        # Use warpPerspective for Homography
        img2_aligned = cv2.warpAffine(img1, warp_matrix, (sz[1], sz[0]),
                                      flags=cv2.INTER_LINEAR +
                                      cv2.WARP_INVERSE_MAP)

    # Show final results
    cv2.imshow("Image 1", img1)
    cv2.imshow("Image 2", img2)
    cv2.imshow("Aligned Image 2", img2_aligned)
    cv2.waitKey(0)

    # Return
    return (img2_aligned, warp_matrix)

# === Entry point ===

# Root path
# root = 'F:/Data/Projects/moritz/'
# root = 'F:/Data/Projects/moritz/simulation/'
# root = 'F:/Data/Projects/moritz/simulation2/'
# root = 'F:/Data/Projects/moritz/simulation3/'
#root = 'F:/Data/Projects/moritz/cameraman/calc/'
#root = 'F:/Data/Projects/moritz/cameraman/calc2/'
root = 'F:/Data/Projects/moritz/calibration'

# Get files
d1 = os.listdir(os.path.join(root, 'Pos2'))
d2 = os.listdir(os.path.join(root, 'Pos1'))

# Read images
img1 = cv2.imread(os.path.join(root, 'Pos2', d1[0]))
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.imread(os.path.join(root, 'Pos1', d2[0]))
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

cv2.imshow("img1", img1)
cv2.imshow("img2", img2)

# Invert image1
#img2 = invertImage(img2)

# Flip image1 (in y direction)
img2 = flipImage(img2, 'y')

# Make sure the output directory exists
if not os.path.exists(os.path.join(root, 'out')):
    os.mkdir(os.path.join(root, 'out'))

# Pad smaller image
img1 = pad_image(img1, img2.shape)

# Find the circle
circles = findCircles(img2, cannyThresh=50, accThresh=30,
                      med_filter=True, invert=False,
                      minRadius=0, maxRadius=0)

# Find the central circle
circle = findCentralCircle(circles, img2.shape)

# Draw the cricle
cimg = drawSingleCircle(circle, img2)

# Align the images
(aligned, M, mask, view) = register_images_orb(img1, img2, True)
# (aligned, M) = register_images_ecc(img1, img2)
# view = None

# Save the feature matching quality control image
if view is not None:
    cv2.imshow("Feature alignment", view)
    cv2.imwrite(os.path.join(root, 'out/feature_alignment.tif'), view)

# Save the aligned image
cv2.imwrite(os.path.join(root, 'out/aligned.tif'), aligned)

# Save the transformation matrix
out = open(os.path.join(root, 'out/transform.pkl'), 'wb')
pickle.dump(M, out)
out.close()

# Save the circle control image
if cimg is not None:
    cv2.imwrite(os.path.join(root, 'out/circle.tif'), cimg)
    cv2.imshow("Circle", cimg)

# Find the pixels belonging to the circle
if circle is not None:
    x_seed = circle[0] + circle[2] + 1
    y_seed = circle[1]
    extracted_circle = simple_region_growing(img2, (x_seed, y_seed))

    # Now transform the extracted circle
    warped_extracted_circle = cv2.warpPerspective(extracted_circle,
                                                  np.linalg.inv(M),
                                                  extracted_circle.shape)

    # And extract the pixels
    circle_pixels = np.array(np.where(warped_extracted_circle))

    # Create also a control image for the circle transformations
    circle_transformed = img1.copy()
    for i in range(circle_pixels.shape[1]):
        circle_transformed[circle_pixels[0, i], circle_pixels[1, i]] = 255

    # Show img1 with the transformed circle
    cv2.imshow("Region-growing: circle", extracted_circle)
    cv2.imshow("Transformed circle", circle_transformed)

    cv2.imwrite(os.path.join(root, 'out/extracted_circle.tif'),
                extracted_circle)
    cv2.imwrite(os.path.join(root, 'out/transformed_circle.tif'),
                circle_transformed)

# Create, save and show a quality control image
if img2.shape == aligned.shape:

    imgRGB = np.dstack((0 * img2, img2, aligned))
    cv2.imwrite(os.path.join(root, 'out/control.tif'),
                imgRGB)

    # Swow result
    cv2.imshow("Aligned", imgRGB)

else:

    # Swow result
    cv2.imshow("Aligned", aligned)

cv2.waitKey(0)
cv2.destroyAllWindows()
