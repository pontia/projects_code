function applyTransformToImages(firstImage, outDirName, transformFile)
% Apply a transform to a series of images.
% 
% SYNOPSIS
%
%   applyTransformToStack(firstImage, outDirName, transformFile)
%
% INPUT
%
%   firstImage   : full path to the first image in the sequence to align.
%                  Set to [] to pick via a file dialog.
%   outDirName   : full path to the folder where to save the aligned images.
%                  Set to [] to pick via a file dialog.
%   transformFile: full path to the transform.mat file stored by 
%                  alignImagesAndStoreTransformations().
%                  Set to [] to pick via a file dialog.
%
% OUTPUT
%
%   None
%
% Aaron Ponti, 07.02.2015

if nargin ~= 3
    error('3 input parameters expected,');
end

if isempty(firstImage)

    % Get the first image in the sequence
    [fName, dirName] = uigetfile('*.tif', ...
        'Select first image from sequence');
    if isequal(dirName, 0)
        return;
    end
    
else
    
    % Decompose referenceImage
    [dirName, n, e] = fileparts(firstImage);
    fName = [n, e];
    
end

% Get the folder where to store the aligned files
if isempty(outDirName)
    outDirName = uigetdir(pwd(), ...
        'Select folder where to store the aligned images');
    if isequal(outDirName, 0)
        return;
    end
end

% Pick the transformation file
if isempty(transformFile)
    [transformFName, transformDirName] = uigetfile('transform.mat', ...
        'Select transformation file');
    if isequal(transformDirName, 0)
        return;
    end
    
    transformFile = fullfile(transformDirName, transformFName);
end

% Get the list of files to be processed
filesToProcess = getFileSequence(dirName, fName);
if isempty(filesToProcess)
    disp('Sorry, could not find a file sequence to process.')
    return;
end

% Number of files to preocess
nFiles = numel(filesToProcess);

% Load the transformation file
s = load(transformFile, 'T', 'parameters');
if isempty(s)
    disp('The transform file does not contain the expected cariable.');
    return;
end
T = s.T;
if numel(T) ~= nFiles
    disp('The transform is incompatible with the selected image sequence.');
    return;
end
parameters = s.parameters;

% Inform
disp(['Apply transform to ', num2str(nFiles), ' images.']);

% Store some constants (we need to do this because of limitations of parfor
tmp = imread(filesToProcess{1});
[sizeY, sizeX] = size(tmp); sY = 1 : sizeY; sX = 1 : sizeX;
transform = parameters.transform;

% Instantiate waitbar
ppm = ParforProgressStarter2( ...
    'Please wait while applying the transformation...', nFiles, ...
    0.1, 0, 0, 0);

tic;

% Process all planes in the stack
parfor i = 1 : nFiles

    % Image
    current = imread(filesToProcess{i});
    
    % Get current transformation
    t = T{i};
   
    % Apply it
    aligned = iat_inverse_warping(current, t, transform, sX, sY);
    
    % Cast appropriately
    aligned = cast(aligned, 'like', current);
   
    % Write the aligned image
    [~, n, e] = fileparts(filesToProcess{i});
    imwrite(aligned, fullfile(outDirName, [n, e]), 'Compression', 'none');

    % Update the waitbar
    ppm.increment(i);

end

% Elapsed time
fprintf(1, 'Elapsed time: %.2fs\n', toc);

% Close the waitbar
try
    delete(ppm); % Old MATLAB versions
catch me %#ok<NASGU>
end
