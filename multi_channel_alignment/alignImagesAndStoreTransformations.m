function alignImagesAndStoreTransformations(referenceImage, outDirName, nIter, transform)
% Batch align images against a reference using the iat_LucasKanadeIC function.
%
% All images are registered against the one selected in the folder!
% 
% SYNOPSIS
%
%   alignImagesAndStoreTransformations(referenceImage, outDirName, nIter, transform)
%
% INPUT
%
%   referenceImage: full path to the reference image to align against.
%                   All other images in the same sequence will be aligned
%                   against it. Set to [] to pick via a file dialog.
%   outDirName    : full path to the folder where to save the aligned images.
%                   Set to [] to pick via a file dialog.
%   nIter         : (optional, default = 50) Number of iterations for 
%                   the iat_LucasKanadeIC function
%   transform     : (optional, default = 'translation') Transform for
%                   the iat_LucasKanadeIC function
%
% OUTPUT
%
%   None
%
% Aaron Ponti, 07.02.2015

if nargin < 2 || nargin > 4
    error('Wrong number of input arguments.');
end

% Default number of iterations
if nargin == 2, nIter = 50; transform = 'translation';
elseif nargin == 3, transform = 'translation'; end

% Please notice that these parameters are NOT the defaults of
% the iat_LucasKanadeIC() function!
parameters = struct();
parameters.levels = 1;
parameters.iterations = nIter;
parameters.transform = transform;

% Reference image
if isempty(referenceImage)

    % Get the reference image
    [fName, dirName] = uigetfile('*.tif', ...
        'Select REFERENCE file from sequence');
    if isequal(dirName, 0)
        return;
    end
    
else
    
    % Decompose referenceImage
    [dirName, n, e] = fileparts(referenceImage);
    fName = [n, e];
    
end

% Get the folder where to store the aligned files
if isempty(outDirName)
    outDirName = uigetdir(pwd(), ...
        'Select folder where to store the aligned images');
    if isequal(outDirName, 0)
        return;
    end
end

% Get the list of files to be processed
[filesToProcess, index] = getFileSequence(dirName, fName);
if isempty(filesToProcess)
    disp('Sorry, could not find a file sequence to process.')
    return;
end

% Total number of files (template included)
nFiles = numel(filesToProcess);

% Template file
templateFile = filesToProcess{index};

% Inform
disp(['Registering ', num2str(nFiles - 1), ' images against ', fName, '.']);

% Allocate memory to store the transformations (also the one for the
% reference file).
T = cell(1, nFiles);

% Read the reference image
template = imread(templateFile);

% Since the reference is not transformed, we save as is in the output
% folder
imwrite(template, fullfile(outDirName, fName), 'Compression', 'none');

% Some constants
[sizeY, sizeX] = size(template);
sY = 1 : sizeY;
sX = 1 : sizeX;

% Instantiate waitbar
ppm = ParforProgressStarter2( ...
    'Please wait while registering your images...', nFiles, ...
    0.1, 0, 0, 0);

tic;

% Process all planes in the stack
parfor i = 1 : nFiles

    % We do not need to align the template
    if i == index
        
        % Update the waitbar
        ppm.increment(i);
        
        % Continue to the next file
        continue;
        
    end
        
    % Image
    current = imread(filesToProcess{i});
   
    % Find the transformation
    t = iat_LucasKanadeIC(current, template, parameters);
   
    % Apply it
    aligned = iat_inverse_warping(current, t, parameters.transform, sX, sY);
   
    % Cast appropriately
    aligned = cast(aligned, 'like', template);
    
    % Write the aligned image
    [~, n, e] = fileparts(filesToProcess{i});
    imwrite(aligned, fullfile(outDirName, [n, e]), 'Compression', 'none');
   
    % Store the transform
    T{i} = t;

    % Update the waitbar
    ppm.increment(i);

end

% Elapsed time
fprintf(1, 'Elapsed time: %.2fs\n', toc);

% Close the waitbar
try
    delete(ppm); % Old MATLAB versions
catch me %#ok<NASGU>
end

% We store the identity transform for the template as well
if index ~= nFiles
    t = T{nFiles};
else
    t = T{nFiles - 1};
end

if all(size(t) == [2 1])
    T{index} = [0; 0];
elseif all(size(t) == [2 3])
    T{index} = [eye(2) zeros(2,1)];
elseif all(size(t) == [3 3])
    T{index} = eye(3);
else
    error('Unexpected transform matrix size!');
end

% Save the transformation in the output folder
transformOutFileName = fullfile(outDirName, 'transform.mat');
save(transformOutFileName, 'T', 'parameters');
disp(['Transformations saved as ', transformOutFileName]);


