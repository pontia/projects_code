function [outStack, T] = alignStackAndStoreTransformations(stack, nIter, transform)
% Align a stack and returns the transformations (translations only!).
%
% All images are registered against the first in the stack.
% 
% SYNOPSIS
%
%   [outStack, T] = alignStackAndStoreTransformations(stack)
%
% INPUT
%
%   stack    : 3D stack of images to be aligned, size(T) := [2 1 nPlanes]
%   nIter    : (optional, default = 50) Number of iterations for the 
%              inverse-compositional version of Lucas-Kanade image alignment
%              alignment.
%   transform: (optional, default = 'translation') Transform for
%              the iat_LucasKanadeIC function
%
% OUTPUT
%
%   outStack: 3D stack of aligned images.
%   T       : stack of translation transforms.
%
% Aaron Ponti, 06.02.2015

% Default parameters
if nargin == 1, nIter = 50; transform = 'translation';
elseif nargin == 2, transform = 'translation';
elseif nargin == 3, % Ok
else error('Wrong number of input arguments!');
end

% Set parameters (LucasKanadeIC)
parameters = struct();
parameters.levels = 1;
parameters.iterations = nIter;
parameters.transform = transform;

% Initialize output
outStack = zeros(size(stack), 'like', stack);
outStack(:, :, 1) = stack(:, :, 1);

% Number of planes
nPlanes = size(stack, 3);

% Allocate memory to store the transformations
T = cell(1, nPlanes);

% Align against first plane
template = stack(:, :, 1);
[sizeY, sizeX] = size(template);
sY = 1 : sizeY;
sX = 1 : sizeX;

% Instantiate waitbar
hWaitbar = waitbar(0, 'Please wait while registering your stack...');

% Process all planes in the stack
for z = 2 : nPlanes
    
   % Image
   current = stack(:, :, z);  
   
   % Find the transformation
   t = iat_LucasKanadeIC(current, template, parameters);
   
   % Apply it
   aligned = iat_inverse_warping(current, t, parameters.transform, sX, sY);
   
   % Push back the aligned image into the stack
   outStack(:, :, z) = aligned;
   
   % Store the transform
   T{z} = t;

   % Update the waitbar
   waitbar((z - 1) / (nPlanes - 1));

end

% Close the waitbar
close(hWaitbar);

% We store the identity transform for the template as well
if all(size(t) == [2 1])
    T{1} = [0; 0];
elseif all(size(t) == [2 3])
    T{1} = [eye(2) zeros(2,1)];
elseif all(size(t) == [3 3])
    T{1} = eye(3);
else
    error('Unexpected transform matrix size!');
end

