function outStack = applyTransformToStack(stack, T, transform)
% Apply a transform to a stack.
% 
% SYNOPSIS
%
%   outStack = applyTransformToStack(stack, T, transform)
%
% INPUT
%
%   stack    : 3D stack of images to be aligned.
%   T        : cell array of transform matrices, numel(T) := nPlanes
%   transform: (optioanl, default = 'translation') one of the transforms
%              accepted by the iat_inverse_warping function (must be 
%              compatible with the transformation matrices stored in T!)
%
% OUTPUT
%
%   outStack: 3D stack of aligned images.
%
% Aaron Ponti, 06.02.2015

if nargin < 2 || nargin > 3
    error('Wrong number of input parameters!');
end

if nargin == 2
    transform = 'translation';
end

% Initialize output
outStack = zeros(size(stack), 'like', stack);
outStack(:, :, 1) = stack(:, :, 1);

% Get some constants
[sizeY, sizeX, nPlanes] = size(stack);
sY = 1 : sizeY;
sX = 1 : sizeX;

% Instantiate waitbar
hWaitbar = waitbar(0, 'Please wait will applying the transformation...');

% Process all planes in the stack
for z = 2 : nPlanes
    
   % Image
   current = stack(:, :, z);  
   
   % Get current transformation
   t = T{z};
   
   % Apply it
   aligned = iat_inverse_warping(current, t, transform, sX, sY);
   
   % Push back the aligned image into the stack
   outStack(:, :, z) = aligned;

   % Update the waitbar
   waitbar((z - 1) / (nPlanes - 1));

end

% Close the waitbar
close(hWaitbar);

