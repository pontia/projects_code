function [sequence, index] = getFileSequence(dirName, fName)
% getFileSequence returns the list of files that belong to the same sequence as fName
%
% A sequence is defined as a series of file belonging to the same directory
% that share the same body and differ only by a numerical suffix, e.g.
%
%    image01.tif
%    image02.tif
%    image03.tif
%    image04.tif
%    image05.tif
%
% SYNOPSIS   [sequence, index] = getFileSequence(dirName, fName)
%
% INPUT      dirName  : directory containing the sequence to be sought for.
%            fName    : name of one file in the sequence
%
% OUTPUT     sequence : cell array containing the list of full file names
%            index    : position of the passed fName in the sequence
%
% Original code: Aaron Ponti, 2006/12/28

if exist(dirName, 'dir') ~= 7
    error('The passed directory does not exist. ');
end

% Get all file names
ll = dir(dirName);

% Discard directories
ll([ll.isdir] == 1) = [];

% Get all file names
[~, commonFileName] = getFilenameBody([dirName, filesep, fName]);
sequence = {ll( ~ [ll.isdir] ).name};
toBeKept = strncmp(sequence, commonFileName, length(commonFileName));
sequence = sequence(toBeKept);
index = find(strncmp(sequence, fName, length(fName))); % Store which file was actually selected

% Check that the numerical indices are consequent (and discard those with
% no numerical index)
counter1 = 0; counter2 = 0; toBeDel = zeros(1, length(sequence));
nIndices = zeros(1, length(sequence));
for i = 1 : length(sequence)
    [~, ~, n] = getFilenameBody(char(sequence(i)));
    if ~isempty(n)
        counter1 = counter1 + 1;
        nIndices(counter1) = str2double(n);
    else
        counter2 = counter2 + 1;
        toBeDel(counter2) = i;
    end
end
% Remove non valid entries in the file list if needed
if counter1 < numel(nIndices)
    nIndices(counter1 + 1 : end) = [];
    toBeDel(counter1 + 1 : end) = [];
    sequence(toBeDel) =[];
    % Update the index of the file which was selected
    index = find(strncmp(sequence, fName, length(fName)));
end
% Now sort the files NUMERICALLY
[nIndices, origSequence] = sort(nIndices);
sequence = sequence(origSequence);

% Check for discontinuities in the numerical indices
nIndices = [1 diff(nIndices)];
posJumps = find(nIndices ~= 1);

if isempty(posJumps)
    before = [];
    after  = [];
else
    before = posJumps(posJumps <= index);
    after  = posJumps(posJumps >  index);
end
if isempty(before)
    start = 1;
else
    start = before(end);
end
if isempty(after)
    stop  = length(sequence);
else
    stop  = after(1) - 1;
end

sequence = sequence(start : stop);
for i = 1 : length(sequence)
    sequence(i) = {[dirName, filesep, char(sequence(i) )]};
end

% Update index (since the first file name of the current stack might not be
% the first file name in the directory listing)
index = index - start + 1;
