function saveAlignedStackAsTIFSequence(stack, baseName, dirName)
% Save all planes of a stack as TIFF file.
%
% SYNOPSIS
%
%   saveAlignedStackAsTIFSequence(stack, baseName, dirName)
%
% INPUT
%
%   stack   : 3D stack of images to be saved
%   baseName: base name of the file, to which a numeric extension will
%             be appended
%   dirName : full path of the directory where the files are to be saved
%
% OUTPUT
%
%   None
%
% Aaron Ponti, 06.02.2015

% Constant
nPlanes = size(stack, 3);

% Numeric format
strg = sprintf('%%.%dd', length(num2str(nPlanes)));

% Instantiate waitbar
hWaitbar = waitbar(0, 'Please wait while saving images...');

for i = 1 : nPlanes 

    % Write current plane
    imwrite(stack(:, :, i), ...
        fullfile(dirName, [baseName, sprintf(strg, i), '.tif']), ...
        'Compression', 'none');
    
   % Update the waitbar
   waitbar(i / nPlanes);

end

% Close the waitbar
close(hWaitbar);
