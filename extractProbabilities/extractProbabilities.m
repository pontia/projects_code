function extractProbabilities(rawDirName, probDirName, qcDirName)
% Extract probabilities from ilastik classifications.
%
% SYNOPSIS
%
%    extractProbabilities(rawDirName, probDirName, debugDirName)
%
% INPUT
%
%   rawDirName : folder where all raw (TIFF) images are stored. Set to []
%                to pick via a dialog.
%   probDirName: folder where all ilastik probability images are stored.
%                Set to [] to pick via a dialog.
%                The results of the analysis (results.txt) will be saved in
%                this folder.
%   qcDirName  : folder where to store all quality control images.
%                Omit to disable the generation of quality control images.
%                Set to [] to pick via a dialog.
%
% OUTPUT
%
%    None
%
%    The result of the analysis (results.txt) will be saved in probDirName.
%
% Copyright Aaron Ponti, 2016

if nargin < 2 || nargin > 3
    error('2 or 3 input parameters expected.');
end

if nargin == 2
    CREATE_QC = 0;
else
    CREATE_QC = 1;
end

if isempty(rawDirName)
    rawDirName = uigetdir(pwd(), ...
        'Please pick the directory containing the raw images:');
    if isequal(rawDirName, 0)
        return
    end
end

if isempty(probDirName)
    probDirName = uigetdir(pwd(), ...
        'Please pick the directory containing the exported ilastik probabilities:');
    if isequal(probDirName, 0)
        return
    end
end

if CREATE_QC == 1
    if isempty(qcDirName)
        qcDirName = uigetdir(pwd(), ...
            'Please pick the directory where to save the quality control figures:');
    end

    if exist(qcDirName, 'dir') == 0
        mkdir(qcDirName);
    end 
end

d = dir(rawDirName);
nEntries = numel(d);

n = 0;
fileNames = cell(1, nEntries);
adheshionSegmentation = cell(1, nEntries);
adheshionProbabilities = cell(1, nEntries);
cellSurfaceSegmentation = cell(1, nEntries);
cellSurfaceProbabilities = cell(1, nEntries);
cytoplasmSegmentation = cell(1, nEntries);
cytoplasmProbabilities = cell(1, nEntries);

% Open waitbar
hWaitbar = waitbar(0, 'Please wait while processing...');

for i = 1 : nEntries

    if d(i).isdir == 1
        continue;
    end
    
    [~, b, e] = fileparts(d(i).name);
    
    if strcmpi(e, '.tif') == 0
        continue;
    end
    
    % Build the names of the probability map files
    backgroundName = fullfile(probDirName, [b, '_Probabilities___0.tiff']);
    cytoplasmName = fullfile(probDirName, [b, '_Probabilities___1.tiff']);
    nucleusName = fullfile(probDirName, [b, '_Probabilities___2.tiff']);
    adhesionsName = fullfile(probDirName, [b, '_Probabilities___3.tiff']);
    
    % Load probability maps
    background = loadMultiPageTIFF(backgroundName);
    cytoplasm = loadMultiPageTIFF(cytoplasmName);
    nucleus = loadMultiPageTIFF(nucleusName);
    adhesions = loadMultiPageTIFF(adhesionsName);
    
    % Number of timepoints
    nTimepoints = size(background, 3);
    
    % Allocate space to store the results
    adheshionSegmentationPerTimepoint = zeros(1, nTimepoints);
    adheshionProbabilitiesPerTimepoint = zeros(1, nTimepoints);
    cellSurfaceSegmentationPerTimepoint = zeros(1, nTimepoints);
    cellSurfaceProbabilitiesPerTimepoint = zeros(1, nTimepoints);
    cytoplasmSegmentationPerTimepoint = zeros(1, nTimepoints);
    cytoplasmProbabilitiesPerTimepoint = zeros(1, nTimepoints);
    
    % Process all time points
    for t = 1 : nTimepoints 

        % Prepare quality control stacks
        if CREATE_QC == 1
            
            if t == 1
                
                adhesions_qc = zeros(size(adhesions), 'uint8');
                adhesions_prob_qc = adhesions_qc;
                
                cytoplasm_qc = adhesions_qc;
                cytoplasm_prob_qc = adhesions_qc;
                
                cellSurface_qc = adhesions_qc;
                cellSurface_prob_qc = adhesions_qc;

            end
            
        end

        % Extract current timepoint
        curr_background = background(:, :, t);
        curr_cytoplasm = cytoplasm(:, :, t);
        curr_nucleus = nucleus(:, :, t);
        curr_adhesions = adhesions(:, :, t);
        
        % Create max propability map
        maxProbMap = max(...
            cat(3, curr_background, curr_cytoplasm, ...
            curr_nucleus, curr_adhesions), ...
            [], 3);
        
        % Extract segmentation masks
        backgroundMask = curr_background == maxProbMap;
        cytoplasmMask = curr_cytoplasm == maxProbMap;
        %     nucleusMask = curr_nucleus == maxProbMap;
        adhesionsMask = curr_adhesions == maxProbMap;
        cellSurfaceMask = (~backgroundMask);
        
        % Extract masked probabilities
        adhesionsProb = adhesionsMask .* (double(curr_adhesions) ./ 255.0);
        cytoplasmProb = cytoplasmMask .* (double(curr_cytoplasm) ./ 255.0);
        cellSurfaceProb = cellSurfaceMask .* ((255.0 - backgroundMask) ./ 255.0);
        
        % Store the results
        adheshionSegmentationPerTimepoint(t) = sum(adhesionsMask(:));
        adheshionProbabilitiesPerTimepoint(t) = sum(adhesionsProb(:));
        cellSurfaceSegmentationPerTimepoint(t) = sum(cellSurfaceMask(:));
        cellSurfaceProbabilitiesPerTimepoint(t) = sum(cellSurfaceProb(:));
        cytoplasmSegmentationPerTimepoint(t) = sum(cytoplasmMask(:));
        cytoplasmProbabilitiesPerTimepoint(t) = sum(cytoplasmProb(:));

        % Store the quality control images for current time point
        if CREATE_QC == 1
            
            adhesions_qc(:, :, t) = uint8(255 .* adhesionsMask);
            adhesions_prob_qc(:, :, t) = uint8(255 .* adhesionsProb);
            
            cytoplasm_qc(:, :, t) = uint8(255 .* cytoplasmMask);
            cytoplasm_prob_qc(:, :, t) = uint8(255 .* cytoplasmProb);
            
            cellSurface_qc(:, :, t) = uint8(255 .* cellSurfaceMask);
            cellSurface_prob_qc(:, :, t) = uint8(255 .* cellSurfaceProb);
            
        end
    
    end

    % Save quality control images
    if CREATE_QC == 1

        % RGB quality control figures
        segmRGB = cat(4, adhesions_qc, cytoplasm_qc, cellSurface_qc);
        probRGB = cat(4, adhesions_prob_qc, cytoplasm_prob_qc, cellSurface_prob_qc);

        % Save
        saveMultiPageTIFF(segmRGB, fullfile(qcDirName, [b, '_segmentation.tif']));
        saveMultiPageTIFF(probRGB, fullfile(qcDirName, [b, '_probabilities.tif']));

    end

    % Extract and store statistics
    n = n + 1;
    fileNames{n} = d(i).name;
    adheshionSegmentation{n} = adheshionSegmentationPerTimepoint;
    adheshionProbabilities{n} = adheshionProbabilitiesPerTimepoint;
    cellSurfaceSegmentation{n} = cellSurfaceSegmentationPerTimepoint;
    cellSurfaceProbabilities{n} = cellSurfaceProbabilitiesPerTimepoint;
    cytoplasmSegmentation{n} = cytoplasmSegmentationPerTimepoint;
    cytoplasmProbabilities{n} = cytoplasmProbabilitiesPerTimepoint;

    % Update waitbar
    waitbar(i / nEntries, hWaitbar);
end

% Close waitbar
close(hWaitbar);

% Reset the number of (valid) entries
nEntries = n;

% Keep only the first nEntries.
fileNames = fileNames(1 : nEntries);
adheshionSegmentation = adheshionSegmentation(1 : nEntries);
adheshionProbabilities = adheshionProbabilities(1 : nEntries);
cellSurfaceSegmentation = cellSurfaceSegmentation(1 : nEntries);
cellSurfaceProbabilities = cellSurfaceProbabilities(1 : nEntries);
cytoplasmSegmentation = cytoplasmSegmentation(1 : nEntries);
cytoplasmProbabilities = cytoplasmProbabilities(1 : nEntries);

% Now save the results to file
resultFile = fullfile(probDirName, 'results.txt');
fid = fopen(resultFile, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write the header
fprintf(fid, ...
    [ ...
    '%28s\t', ...                  % File name
    '%28s\t', ...                  % Timepoint
    '%28s\t', ...                  % Adhesion
    '%28s\t', ...                  % Adhesion (weighted)
    '%28s\t', ...                  % Cell surface
    '%28s\t', ...                  % Cell surface (weighted)
    '%28s\t', ...                  % Cytoplasm
    '%28s\n' ], ...                % Cytoplasm (weighted)
    'File name', ...               % File name
    'Time point', ...              % Time point
    'Adhesion', ...                % Adhesion
    'Adhesion (weighted)', ...     % Adhesion (weighted)
    'Cell surface', ...            % Cell surface
    'Cell surface (weighted)', ... % Cell surface (weighted)
    'Cytoplasm', ...               % Cytoplasm
    'Cytoplasm (weighted)');       % Cytoplasm (weighted)

% Write the results
for i = 1 : nEntries

    for t = 1 : numel(adheshionSegmentation{i})
    
        if t == 1
            fName = fileNames{i};
        else
            fName = '';
        end

        fprintf(fid, ...
            [ ...
            '%28s\t', ...                       % File name
            '%28d\t', ...                       % Time point
            '%28d\t', ...                       % Adhesion
            '%28.4f\t', ...                     % Adhesion (weighted)
            '%28d\t', ...                       % Cell surface
            '%28.4f\t', ...                     % Cell surface (weighted)
            '%28d\t', ...                       % Cytoplasm
            '%28.4f\n' ], ...                   % Cytoplasm (weighted)
            fName, ...                          % File name
            t, ...                              % Time point
            adheshionSegmentation{i}(t), ...    % Adhesion
            adheshionProbabilities{i}(t), ...   % Adhesion (weighted)
            cellSurfaceSegmentation{i}(t), ...  % Cell surface
            cellSurfaceProbabilities{i}(t), ... % Cell surface (weighted)
            cytoplasmSegmentation{i}(t), ...    % Cytoplasm
            cytoplasmProbabilities{i}(t));      % Cytoplasm (weighted)
        
    end

end

% Close the file and open it in the editor
if fid ~= 1
    fclose(fid);
    edit(resultFile);
end

% Load a multi page TIFF file
function stack = loadMultiPageTIFF(fName)

info = imfinfo(fName);
nImages = numel(info);

img = imread(fName, 1);
stack = zeros([info(1).Height, info(1).Width, nImages], 'like', img);
stack(:, :, 1) = img;

for i = 2 : nImages
    stack(:, :, i) = imread(fName, i);
end

% Save a multi pahe TIFF file
function saveMultiPageTIFF(stack, fName)

imwrite(squeeze(stack(:, :, 1, 1 : end)), fName, 'WriteMode', 'overwrite');
for i = 2 : size(stack, 3)
    imwrite(squeeze(stack(:, :, i, 1 : end)), fName, 'WriteMode', 'append');
end
