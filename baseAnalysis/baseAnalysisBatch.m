folder = 'C:\Temp';
%pathImageA = fullfile(folder, 'ptp-1.jpg');
%pathImageB = fullfile(folder, 'ptp-reference.jpg');
pathImageA = fullfile(folder, 'test243 sequencing 1.nd2');
pathImageB = fullfile(folder, 'test246 sequencing 2.nd2');
maskIndex = 5;
signalIndices = 1:4;
pathResultsDir = fullfile(folder, 'results');
range = 10000;
bases = {'A', 'C', 'T', 'G'};
minRatioDiff = 0.2;
QC = 1;

[assignedBasesA, assignedBasesB] = baseAnalysis( ...
    pathImageA, pathImageB, maskIndex, signalIndices, pathResultsDir, ...
    range, bases, minRatioDiff, QC);
