function [assignedBasesA, assignedBasesB, Mindices] = baseAnalysis( ...
    pathImageA, pathImageB, maskIndex, signalIndices, pathResultsDir, ...
    range, bases, minRatioDiff, QC)
% Match positions between two images and assign bases.
%
% SYNOPSIS
%
% [assignedBasesA, assignedBasesB, Mindices] = baseAnalysis( ...
%   pathImageA, pathImageB, maskIndex, signalIndices, pathResultsDir, ...
%   range, bases, minRatioDiff, QC)
%
% INPUT
%
%    pathImageA    : full path to image A
%    pathImageB    : full path to image B
%    maskIndex     : index of the channel to use for segmentation of the
%                    positions
%    signalIndices : indices of the image channels that contain the signal
%                    to be extracted, e.g. [1 2 3 4]
%    pathResultsDir: full path to the directory where the to save the
%                    results and optionally the quality control figures. 
%    range         : intensity range within a position used to segment
%                    from the local background
%    bases         : cell array of bases, e.g. {'A', 'C', 'T', 'G'}
%    minRatioDiff  : min intensity difference between the strongest and the
%                    second strongest base for a univocal hit.
%    QC            : (optional, default 0) Set to 1 to create quality
%                    control figures.
%
% OUTPUT
%
%    assignedBasesA
%    assignedBasesB
%    Mindices
%
% Aaron POnti, 2016/04/14

if nargin < 8 || nargin > 9
    error('8 or 9 input parameters expected.');
end

if nargin == 8
    QC = 1;
end

% Make sure the output path extists
if ~exist(pathResultsDir, 'dir')
    mkdir(pathResultsDir);
end
pathQCDir = fullfile(pathResultsDir, 'QC');
if ~exist(pathQCDir, 'dir')
    mkdir(pathQCDir);
end


% First image
datasetA = loadGeneric(pathImageA);
[coordsA, intensitiesA] = getCoordsAndIntensities(datasetA, maskIndex, ...
    signalIndices, range);
assignedBasesA = assignBases(intensitiesA, bases, minRatioDiff);

% Second image
datasetB = loadGeneric(pathImageB);
[coordsB, intensitiesB] = getCoordsAndIntensities(datasetB, maskIndex, ...
    signalIndices, range);
assignedBasesB = assignBases(intensitiesB, bases, minRatioDiff);

% Track the coordinates
[~, Mindices] = trackerBMTNN(coordsA, coordsB, [], [], 25, 105);

% Filenames
[~, bodyA, extA] = fileparts(pathImageA);
fileNameNoPathA = [bodyA, extA];
[~, bodyB, extB] = fileparts(pathImageB);
fileNameNoPathB = [bodyB, extB];

% Store the info in a text file
storeResults(...
    fullfile(pathResultsDir, [bodyA, '_', bodyB, '_results.txt']), ...
    Mindices, assignedBasesA, assignedBasesB);

% Store the results as MAT file
save(fullfile(pathResultsDir, [bodyA, '_', bodyB, '_results.mat']), ...
    'assignedBasesA', 'assignedBasesB', 'Mindices', ...
    'intensitiesA', 'intensitiesB');

% If requested, generate some quality control figures
if QC == 1
    generateAndSaveQualityControlFigures(datasetA, datasetB, maskIndex, ...
        signalIndices, Mindices, coordsA, coordsB, fileNameNoPathA, ...
        fileNameNoPathB, bodyA, bodyB, pathQCDir, intensitiesA, ...
        intensitiesB)
end

% =========================================================================
%
% Functions
%
% =========================================================================

function [coords, intensities] = getCoordsAndIntensities(dataset, ...
    maskIndex, signalIndices, range)
% img: image to be processed
% range: range of intensity variation allowed within one region
if nargin < 2
    range = 10000;
end
img = dataset{maskIndex};
if ~isa(img, 'double')
    img = double(img);
end
I = medfilt2(img, [5 5]);
BW = imextendedmax(I, range, 8);
rprops = regionprops(BW, {'Centroid'});
coords = reshape([rprops.Centroid], 2, numel(rprops))';

% Now extract the intensities
intensities = cell(1, numel(signalIndices));
for i = 1 : numel(signalIndices)
    rprops = regionprops(BW, dataset{signalIndices(i)}, {'MeanIntensity'});
    intensities{i} = [rprops.MeanIntensity];
end

% -------------------------------------------------------------------------

function hFig = plotTracks(IA, IB, Mindices, coordsA, coordsB)

tstep = 10;

dx = size(IA, 2);

cImg = cat(2, IA, IB);
hFig = figure; imshow(cImg, [])

hold on

for i = 1 : size(Mindices, 1)

    if Mindices(i, 1) == 0

        x = coordsB(Mindices(i, 2), 1) + dx;
        y = coordsB(Mindices(i, 2), 2);
        plot(x, y, 'wo'); 
        text(x, y + tstep, num2str(Mindices(i, 2)), ...
            'Color', 'r', 'FontSize', 12);
        
    elseif Mindices(i, 2) == 0
        
        x0 = coordsA(Mindices(i, 1), 1);
        y0 = coordsA(Mindices(i, 1), 2);
        plot(x0, y0, 'wo');
        text(x0, y0 + tstep, num2str(Mindices(i, 1)), ...
            'Color', 'r', 'FontSize', 12);

    else
        
        x0 = coordsA(Mindices(i, 1), 1);
        y0 = coordsA(Mindices(i, 1), 2);
        x = coordsB(Mindices(i, 2), 1) + dx;
        y = coordsB(Mindices(i, 2), 2);
        clr = rand(1, 3);
        plot([x0, x], [y0, y], '-', 'Color', clr);
        plot(x0, y0, '*', 'Color', clr);
        plot(x, y, '*', 'Color', clr);
        text(x0, y0 + tstep, num2str(Mindices(i, 1)), ...
            'Color', 'r', 'FontSize', 12);
        text(x, y + tstep, num2str(Mindices(i, 2)), ...
            'Color', 'r', 'FontSize', 12);
        
    end
    
end

% -------------------------------------------------------------------------

function assignedBases = assignBases(intensities, bases, minRatioDiff)

% Pack the intensities in a matrix
intensityMatrix = [];
for i = 1 : numel(intensities)
    intensityMatrix = cat(1, intensityMatrix, intensities{i});
end

% Normalize them
maxIntensities = max(intensityMatrix, [], 1);

for i = 1 : size(intensityMatrix, 1)
   intensityMatrix(i, :) = intensityMatrix(i, :) ./ maxIntensities; 
end

% Assign the bases
assignedBases = cell(1, size(intensityMatrix, 2));
for i = 1 : size(intensityMatrix, 2)

    current = intensityMatrix(:, i);
    
    % Find the strongest base
    mx = find(current == max(current), 1);
    
    % Take all those that are within minRatioDiff from the max base
    delta = current(mx) - current;
    delta(mx) = Inf;
    others = find(delta < minRatioDiff);
    
    base = bases(mx);
    otherBases = '';
    if ~isempty(others)
        % Sort from strongest to weakest
        [~, indices] = sort(delta(others));
        otherBases = bases(others(indices));
    end
    
    assignedBases{i} = [base, otherBases];

end

% -------------------------------------------------------------------------

function storeResults(resultFile, Mindices, assignedBasesA, assignedBasesB)

sortedMindices = sortrows(Mindices);
notPairedIndices = sortedMindices(:, 1) == 0 | sortedMindices(:, 2) == 0;
notPaired = sortedMindices(notPairedIndices, :);
paired = sortedMindices(~notPairedIndices, :);

fid = fopen(resultFile, 'w');
if fid == -1
    disp( 'Could not create report file. Writing to console.' );
    fid = 1;
end

fprintf( fid, ...
    [ ...
    '%16s\t', ...      % Position number from image A
    '%16s\t', ...      % Position number from image B
    '%16s\t', ...      % Base A
    '%16s\n'], ...     % Base B 
    'Pos # (A)', ...  
    'Pos # (B)', ...  
    'Base A', ...
    'Base B');

% Add all paired positions
for i = 1 : size(paired, 1)

    fprintf(fid, ...
        [ ...
        '%16d\t', ...      % Position number from image A
        '%16d\t', ...      % Position number from image B
        '%16s\t', ...      % Base A
        '%16s\n'], ...     % Base B
        paired(i, 1), ...
        paired(i, 2), ...
        implode(assignedBasesA{paired(i, 1)}), ...
        implode(assignedBasesB{paired(i, 2)}));
    
end

% Add all unpaired positions
notPairedA = notPaired(notPaired(:, 2) == 0, :);
for i = 1 : size(notPairedA, 1)

    fprintf(fid, ...
        [ ...
        '%16d\t', ...      % Position number from image A
        '%16d\t', ...      % Position number from image B
        '%16s\t', ...      % Base A
        '%16s\n'], ...     % Base B
        notPairedA(i, 1), ...
        notPairedA(i, 2), ...
        implode(assignedBasesA{notPairedA(i, 1)}), ...
        '--');
    
end

notPairedB = notPaired(notPaired(:, 1) == 0, :);
for i = 1 : size(notPairedB, 1)

    fprintf(fid, ...
        [ ...
        '%16d\t', ...      % Position number from image A
        '%16d\t', ...      % Position number from image B
        '%16s\t', ...      % Base A
        '%16s\n'], ...     % Base B
        notPairedB(i, 1), ...
        notPairedB(i, 2), ...
        '--', ...
        implode(assignedBasesB{notPairedB(i, 2)}));
    
end

% Add a final note on how many unpaired positions there were
fprintf(1, '\n');
fprintf(fid, ...
    ['Number of positions from image A not paired to image B: ', ...
    '%4d (%5.2f%%)\n'], size(notPairedA, 1), ...
    100 * (size(notPairedA, 1) / numel(assignedBasesA)));
fprintf(fid, ...
    ['Number of positions from image B not paired to image A: ',  ...
    '%4d (%5.2f%%)\n'], size(notPairedB, 1), ...
    100 * (size(notPairedB, 1) / numel(assignedBasesB)));

% Close file
if fid ~= -1
    fclose(fid);
end

% -------------------------------------------------------------------------

function generateAndSaveQualityControlFigures(datasetA, datasetB, ...
    maskIndex, signalIndices, Mindices, ...
    coordsA, coordsB, fileNameNoPathA, fileNameNoPathB, bodyA, bodyB, ...
    pathQCDir, intensitiesA, intensitiesB)

imgA = datasetA{maskIndex};
imgB = datasetB{maskIndex};

% Plot and save the segmentation for file A
hFig = figure;
hFig.InvertHardcopy = 'off';
subplot(1, 2, 1);
imshow(imgA);
hold on;
plot(coordsA(:, 1), coordsA(:, 2), 'w*');
xlabel(fileNameNoPathA);

% Plot and save the segmentation for file B
subplot(1, 2, 2);
imshow(imgB);
hold on;
plot(coordsB(:, 1), coordsB(:, 2), 'w*');
xlabel(fileNameNoPathB);
print(fullfile(pathQCDir, ...
    [bodyA, '_', bodyB, '_segmentation.tif']), '-dtiff', '-r300');
close(hFig);

% Plot the tracks
hFig = plotTracks(imgA, imgB, Mindices, coordsA, coordsB);
print(fullfile(pathQCDir, [bodyA, '_', bodyB, '_tracks.tif']), ...
    '-dtiff', '-r300');
close(hFig);

% Show the relative signal intensities in a stacked bar plot
signalA = [];
signalB = [];
for i = 1 : numel(signalIndices)
    signalA = cat(2, signalA, intensitiesA{signalIndices(i)}');
    signalB = cat(2, signalB, intensitiesB{signalIndices(i)}');
end

hFig = figure;
subplot(1, 2, 1); bar(signalA, 'stacked'); xlabel(fileNameNoPathA);
subplot(1, 2, 2); bar(signalB, 'stacked'); xlabel(fileNameNoPathB);
print(fullfile(pathQCDir, ...
    [bodyA, '_', bodyB, '_rel_intensities.tif']), '-dtiff', '-r300');
close(hFig);

% -------------------------------------------------------------------------

function csChar = implode(cellArray)
csChar = cellArray{1};
if numel(cellArray) == 1
    return;
end
for i = 2 : numel(cellArray)
    csChar = cat(2, csChar, ', ', cellArray{i});
end
