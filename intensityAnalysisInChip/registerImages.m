function registerImages(filename, outDir)

% Disable quality control images
qualityControl = 0;

[~, metadata] = loadBioFormats(filename, 1, 1);

% Format string for the number of series
strgS = sprintf('%%.%dd', length(num2str(metadata.totalNumSeries)));

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing series 1...');

for i = 1 : metadata.totalNumSeries
   
    % Load current series
    dataset = loadBioFormats(filename, i, 0);

    % Format string for the number of timepoints and channels
    strgT = sprintf('%%.%dd', length(num2str(size(dataset, 1))));
    strgC = sprintf('%%.%dd', length(num2str(size(dataset, 2))));
    
    % Register images
    for j = 1 : size(dataset, 1) - 1
       
        % Update waitbar
        waitbar((i - 1) / metadata.totalNumSeries, hWaitbar, ...
            ['Estimating transformation for timepoint ', ...
            num2str(j)]);
        
        % Estimate the transform
        tform = estimateTransform(dataset{j, 1}, dataset{j + 1, 1}, ...
            qualityControl);
        
        % Now correct all channels
        for k = 1 : size(dataset, 2)

            % Update waitbar
            waitbar((i - 1) / metadata.totalNumSeries, hWaitbar, ...
                ['Applying transformation for timepoint ', ...
                num2str(j), ' and channel ', num2str(k)]);

            dataset{j + 1, k} = applyTransform(tform, ...
                dataset{j, k}, dataset{j + 1, k}, ...
                qualityControl);
        end

    end
    
    % Save the images
    for t = 1 : size(dataset, 1)
        for c = 1 : size(dataset, 2)
            
            imwrite(dataset{t, c}, ...
                fullfile(outDir, ...
                ['series', sprintf(strgS, i), ...
                '_t', sprintf(strgT, t), ...
                '_c', sprintf(strgC, c), ...
                '.tif']));
        end
    end
    
    % Update waitbar
    waitbar(i / metadata.totalNumSeries, hWaitbar, ...
        ['Processing series ', num2str(i + 1), '1...']);
    
end

% Close waitbar
close(hWaitbar);

% === Functions ===========================================================

function tform = estimateTransform(original, distorted, qualityControl)
% Estimate the transform between two images using SURF features
if nargin == 2
    qualityControl = 0;
end

% Detect SURF features
ptsOriginal = detectSURFFeatures(original);
ptsDistorted = detectSURFFeatures(distorted);

% Extract SURF features
[featuresOriginal, validPtsOriginal] = extractFeatures(original,  ...
    ptsOriginal);
[featuresDistorted, validPtsDistorted] = extractFeatures(distorted, ...
    ptsDistorted);

% Match SURF features
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal  = validPtsOriginal(indexPairs(:, 1));
matchedDistorted = validPtsDistorted(indexPairs(:, 2));

% Estimate the geometric transform
[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchedDistorted, matchedOriginal, 'similarity');

if qualityControl == 1
    figure;
    subplot(1, 2, 1);
    showMatchedFeatures(original, distorted, matchedOriginal, ...
        matchedDistorted);
    title('Putatively matched points (including outliers)');
    subplot(1, 2, 2);
    showMatchedFeatures(original, distorted, inlierOriginal, ...
        inlierDistorted);
    title('Matching points (inliers only)');
    legend('ptsOriginal', 'ptsDistorted');
end

% -------------------------------------------------------------------------

function recovered = applyTransform(tform, original, distorted, ...
    qualityControl)
% Apply the estimated transformation

if nargin == 0
    qualityControl = 0;
end

% Transform the image
outputView = imref2d(size(original));
recovered = imwarp(distorted, tform, 'OutputView', outputView);

if qualityControl == 1
    figure;
    imshowpair(original, recovered, 'montage')
end
