function [dataset, bgsbDataset, cellPos, cellPosTimepointMax, cells] = ...
    pistonExperimentAnalysis(dataset, outDirName, controlCellPos, ...
    controlCells, cellDiameter, threshFactor, cmpThresholdFactor, ...
    firstIndex, lastIndex)
% Segment cells and extracts intensity over time
%
% SYNOPSIS
%
%   [dataset, bgsbDataset, cellPos, cellPosTimepointMax] = ...
%       pistonExperimentAnalysis(dataset, outDirName, controlCellPos, ...
%       controlCells, cellDiameter, thresholdFactor, ...
%       cmpThresholdFactor, firstIndex, lastIndex)
%
% INPUT
%
%   dataset      : a dataset as returned by loadBioFormats; set to [] to
%                  pick a file with a system dialog.
%   outDirName   : full path to folder where to save the results; set to []
%                  to pick a folder with a system dialog.
%   controlCellPos: list of cell coordinates; optional, default = []. If set
%                  to [], cells will be searched in current dataset; if
%                  set to [y x]_n, they will be used to extract the
%                  information from current dataset (notice that they might
%                  be corrected for global movement, e.g. stage shifts).
%   controlCells : (optional) complete result of a previous analysis for 
%                  comparison, i.e. the 'cells' output argument of a 
%                  previous run.
%                  Set to [] to omit the comparison. Otherwise, at every
%                  timepoint of current dataset, the cell intensities are
%                  compared to those for the corresponding timepoint in the
%                  passed control cells matrix. Cells that have
%                  intensities larger than:
%                     mean(control) + comparisonThreshod * std(control)
%                  are considered positive (see 'comparisonThreshod' below).
%   cellDiameter : approximate diameter of a cell; optional, default = 20
%   threshFactor : factor that multiplies the median absolute deviation of
%                  all candidate cells in the time point with strongest
%                  signal (from the candidate cells themselves) to
%                  distinguish real cells from noisy false positives;
%                  optional, default = 11. Pass a vector of values to 
%                  create a series of quality control figures that can help
%                  defining the threshold to use. Please notice, when
%                  passing a vector of values, the analysis will stop
%                  after segmentation!
%   cmpThresholdFactor: factor that multiplies std('control') to test
%                  for positive response in a cell of current dataset
%                  (see 'cells' above).
%   firstIndex   : first time point to consider; optional, default = 1
%   lastIndex    : last time point to consider; optional, default = Inf.
%                  Omit or set to Inf to process all time points.
%
% OUTPUT
%
%   dataset      : loaded dataset
%   bgsbDataset  : background-subtracted dataset
%   cellPos      : list of cell coordinates found in current dataset (or
%                  passed as input argument controlCellPos).
%   cellPosTimepointMax: list of cell coordinates found in current dataset
%                  (or transformed to fit current dataset, if a non-empty
%                  controlCellPos matrix was passed as input argument).
%   cells        : complete list of segmented cells in an Nx4 matrix:
%                  [X Y Intensity Timepoint]_N. The intensity reported is
%                  the intensity from the background-subtracted images.
%
%
% 'CONTROL' vs. 'TREATMENT' COMPARISONS
%
% Assuming that the cells between control and treatment do not move (with
% the exception of a global stage movement that is estimated ONCE and
% applied to all timepoints in the 'TREATMENT' set), a comparison is
% performed by running this function twice. Part of the results of the 
% first run are used as input argument in the second as follows.
%
% First run: control
%
% [dataset, bgsbDataset, *cellPos*, cellPosTimepointMax, *cells*] = ...
%    pistonExperimentAnalysis([], [], [], [], 20, 10);
%
% Second run: treatment
%
% The results 'cellPos' and 'cells' from the first run are used as input
% arguments in the second run. In addition, a new input parameter 
% 'cmpThresholdFactor' is passed that is used to test for cells that show
% a significant increase or decrease of intensity in the treatment. In the
% example below, cmpThresholdFactor is set to 1.96.
%
% [dataset, bgsbDataset, cellPos, cellPosTimepointMax, cells] = ...
%    pistonExperimentAnalysis([], [], *cellPos*, *cells*, 20, 10, *1.96*);
%
% SEARCH FOR BEST threshFactor VALUE 
%
% You can pass a range of values as 'threshFactor' to create a series of 
% quality control images that will help in finding the best threshFactor
% for segmentation. Screenshots with names:
%
%    qc_threshold_both_{threshold}.png
%    qc_threshold_pos_{threshold}.png
%
% will be saved in the 'outDirName' folder with BOTH negative and positive 
% objects (background noise vs. cells) and POSitive only objects (cells).
%
% No analysis will be performed. After you have picked the best value for
% the threshold, run this function again passing just the selected number 
% as 'threshFactor'.
%
% OTHER RESULTS
%
% A results.csv file with all intensities per cell and timepoint is
% stored in outDirName. Also, two quality control images are stored in
% the same flder, with the positions of the extracted cells overlaid on
% the original and the background-subtracted image with highest mean
% cell intensity. Quality control images for picking the best threshold 
% are also saved there, if a range of values is passed for 'threshFactor'.
%
% Aaron Ponti, 2018-11 - 2019-02

% Default parameter values
defaultControlCellPos = [];
defaultControlCells = [];
defaultCellDiameter = 20;
defaultThreshFactor = 11;
defaultCmpThresholdFactor = [];
defaultFirstIndex = 1;
defaultLastIndex = Inf;

switch nargin
    case 2
        controlCellPos = defaultControlCellPos;
        controlCells = defaultControlCells;
        cellDiameter = defaultCellDiameter;
        threshFactor = defaultThreshFactor;
        cmpThresholdFactor = defaultCmpThresholdFactor;
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 3
        controlCells = defaultControlCells;
        cellDiameter = defaultCellDiameter;
        threshFactor = defaultThreshFactor;
        cmpThresholdFactor = defaultCmpThresholdFactor;
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 4
        cellDiameter = defaultCellDiameter;
        threshFactor = defaultThreshFactor;
        cmpThresholdFactor = defaultCmpThresholdFactor;
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 5
        threshFactor = defaultThreshFactor;
        cmpThresholdFactor = defaultCmpThresholdFactor;
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 6
        cmpThresholdFactor = defaultCmpThresholdFactor;
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 7
        firstIndex = defaultFirstIndex;
        lastIndex = defaultLastIndex;
    case 8
        lastIndex = defaultLastIndex;
    case 9
        % All set
    otherwise
        error('2 to 7 input arguments expected!');
end

% ------------------------------------------------------------------------%
%                                                                         %
%   PREPARE SOME CONSTANTS                                                %
%                                                                         %
% ------------------------------------------------------------------------%

% Cell radius
radius = fix(cellDiameter/2);
if mod(radius, 2) == 0
    radius = radius + 1;
end

% Sigma for the Laplacian of Gaussian filter
sigma = fix(radius/2);
if mod(sigma, 2) == 0
    sigma = sigma + 1;
end

% ------------------------------------------------------------------------%
%                                                                         %
%   PICK A DATASET TO OPEN, IF NEEDED                                     %
%                                                                         %
% ------------------------------------------------------------------------%

needToLoad = false;

% Ask to pick a file if dataset is empty
if isempty(dataset)
    [fileName, dirName] = uigetfile('*.*', 'Please pick file to open.');
    if isequal(dirName, 0)
        bgsbDataset = {};
        cellPos = [];
        cellPosTimepointMax = [];
        cells = [];
        return;
    end
    needToLoad = true;
end

% ------------------------------------------------------------------------%
%                                                                         %
%   PICK RESULT DIRECTORY, IF NEEDED                                      %
%                                                                         %
% ------------------------------------------------------------------------%

if isempty(outDirName)
    outDirName = uigetdir('', 'Please pick a folder to store the results.');
    if isequal(outDirName, 0)
        bgsbDataset = {};
        cellPos = [];
        cellPosTimepointMax = [];
        cells = [];        
        return;
    end
end

% ------------------------------------------------------------------------%
%                                                                         %
%   LOAD DATASET, IF NEEDED                                               %
%                                                                         %
% ------------------------------------------------------------------------%

if needToLoad == true
    dataset = loadBioFormats(fullfile(dirName, fileName));
end

% Check the indices
if isinf(lastIndex)
    lastIndex = size(dataset, 1);
end
if lastIndex > size(dataset, 1)
    error('lastIndex is out of bounds.');
end

% Time points to process
timePointsToProcess = firstIndex : lastIndex;

% Store image size for future use
[imageHeight, imageWidth] = size(dataset{1, 1});

% ------------------------------------------------------------------------%
%                                                                         %
%   FIND CANDIDATE CELLS                                                  %
%                                                                         %
% ------------------------------------------------------------------------%

% Prepare a waitbar
hWaitbar = waitbar(0, 'Processing');

% Initialize space for background-subtracted dataset
bgsbDataset = cell(size(dataset));

% Process the dataset
filteredCells = [];
for t = timePointsToProcess

    % Background-subtract the image (keep for later)
    bgsbDataset{t, 1} = backgroundSubtractImage(dataset{t, 1}, sigma);

    % Extract candidate cells from current time point
    currentCells = segmentCells(dataset{t, 1}, sigma);

    % Add the timepoint column
    currentCells = [currentCells, t .* ones(size(currentCells, 1), 1)];

    % Append the results to the global cells
    filteredCells = cat(1, filteredCells, currentCells);

    % Update the waitbar
    waitbar((t + 1 - firstIndex) / numel(timePointsToProcess), hWaitbar, ...
        'Processing time points...');

end

% Find the time point with the most intense cell signal

% Extract median intensities per time point
mInts = zeros(size(dataset, 1), 1);
for t = timePointsToProcess
    values = filteredCells(filteredCells(:, 4) == t, 3);
    if isempty(values)
        mInts(t) = 0;
    else
        mInts(t) = median(values);
    end

    % Update the waitbar
    waitbar(t / size(dataset, 1), hWaitbar, ...
        'Calculating mean cell intensities...');
end

% Find the timepoint with the strongest signal
timepointMax = find(mInts == max(mInts), 1);

% Analyze the cells for timepointMax
cellsMax = filteredCells(filteredCells(:, 4) == timepointMax, :);

% Calculate median and mmad of the intensities
md = median(cellsMax(:, 3));
mm = mmad(cellsMax(:, 3));

% Extract the cells to be used for further analysis
if isscalar(threshFactor)
    cellPosTimepointMax = cellsMax(cellsMax(:, 3) > md + threshFactor * mm, 1:2);
else
    % Find the optimal threshFactor from the vector of values
    testSeriesOfThresholdFactors(cellsMax, threshFactor, ...
        md, mm, dataset{timepointMax}, outDirName);
    
    fprintf(1, ['\nUse the generated QC figures in\n   "%s"\n', ...
        'to select a value for threshFactor.\n\nExiting now.\n'], ...
        outDirName);

    cellPos = [];
    cellPosTimepointMax = [];
    cells = [];

    return
end

% Create quality control figure
h = figure('NumberTitle', 'off', 'Name', ...
    'Median cell intensity profile');
plot(timePointsToProcess, mInts(timePointsToProcess), 'r-');
xlabel('Time points');
ylabel('Median candidate cell intensity');
fName = fullfile(outDirName, ...
    ['median_int_profile_cd_', num2str(cellDiameter), ...
    '_tf_', num2str(threshFactor), ...
    '_from_', num2str(firstIndex), ...
    '_to_', num2str(lastIndex), '.png']);
print(h, fName,  '-r150' , '-dpng');

% If we got control cell positions, we find the transformation to map
% those positions on the selected time point (timepointMax) 
% and then transform them to the new reference frame. We then 
% use these to extract the intensity information. The original
% controlCellPos is left unchanged!
if ~isempty(controlCellPos)

    % Calculate transformation
    M = cellPosTimepointMax';
    M(3, :) = zeros(1, size(M, 2));
    D = controlCellPos';
    D(3, :) = zeros(1, size(D, 2));    
    [Ricp, Ticp] = icp(M, D, 15, ...
        'Matching', 'bruteForce', ...
        'Extrapolation', true, ...
        'Iter', 100);

    % Apply transformation
    Dicp = Ricp * D + repmat(Ticp, 1, size(D, 2));
    Dicp(3, :) = [];
    cellPosTimepointMax = Dicp';

    % Round coordinates
    cellPosTimepointMax = round(cellPosTimepointMax);

end


% ------------------------------------------------------------------------%
%                                                                         %
%   EXTRACT INTENSITY INFORMATION FOR ALL CELLS OVER ALL TIME POINTS
%
%       The coordinates to be used are stored in cellPosTimepointMax.
%                                                                         %
% ------------------------------------------------------------------------%

% Open file for writing. If we are comparing to a previous run,
% we add '_comparison' to the file name.
if isempty(controlCellPos)
    comparisonSuffix = '';
else
    comparisonSuffix = '_comparison';
end
CSVOutFileName = fullfile(outDirName, ...
    ['results_cd_', num2str(cellDiameter), ...
    '_tf_', num2str(threshFactor), ...
    '_from_', num2str(firstIndex), ...
    '_to_', num2str(lastIndex), ...
    comparisonSuffix, '.csv']);
fid = fopen(CSVOutFileName, 'w');
if fid == -1
    fid = 1;
end

% Format string
strg = sprintf('%%.%dd', 4);

% Write header
fprintf(fid, 'Timepoint;');
for c = 1 : size(cellPosTimepointMax, 1)
    indxStrC = sprintf(strg, c);
    fprintf(fid, ['cell', indxStrC, ';']);
end

if ~isempty(controlCells) && ~isempty(cmpThresholdFactor)
    fprintf(fid, 'Total;Positive;Mean\n');
else
    fprintf(fid, 'Mean\n');
end

% Allocate space for the output argument cells
cells = zeros(size(dataset, 1) * size(cellPosTimepointMax, 1), 4);

% Now read all intensities from all timepoints for all cells
for t = timePointsToProcess

    % If we have control cell results, we use them to test which 
    % of current cells at the same time points are significantly
    % brighter. First, we calculate mean and standard deviation 
    % of the control cells.
    if ~isempty(controlCells)

        % Get control cells for current time point
        controlCellsCurrentTimepoint = ...
            controlCells(controlCells(:, 4) == t, :); 

        % Calculate intensity mean and standard deviation
        controlMean = mean(controlCellsCurrentTimepoint(:, 3));
        controlStd = std(controlCellsCurrentTimepoint(:, 3));
        if isempty(cmpThresholdFactor)
            comparisonIntensityThresholdLower = 0;
            comparisonIntensityThresholdUpper = 0;
        else
            comparisonIntensityThresholdLower = controlMean - ...
                cmpThresholdFactor * controlStd;
            comparisonIntensityThresholdUpper = controlMean + ...
                cmpThresholdFactor * controlStd;
        end
    else
        comparisonIntensityThresholdLower = 0;
        comparisonIntensityThresholdUpper = 0;
    end

    % Keep track of the total intensity for all
    % cells in the image
    totalIntValue = 0;

    % Print timepoint
    fprintf(fid, '%d;', t);

    % Print all cell intensities
    numPositiveCells = 0;
    for i = 1 : size(cellPosTimepointMax, 1)

        % Average over a 3x3 area
        y0 = cellPosTimepointMax(i, 1) - 1;
        if (y0 < 1), y0 = 1; end
        y  = cellPosTimepointMax(i, 1) + 1;
        if (y > imageHeight), y = imageHeight; end
        x0 = cellPosTimepointMax(i, 2) - 1;
        if (x0 < 1), x0 = 1; end
        x  = cellPosTimepointMax(i, 2) + 1;
        if (x > imageWidth), x = imageWidth; end
        value = mean(mean(bgsbDataset{t, 1}(y0:y, x0:x)));

        fprintf(fid, '%.2f;', value);

        % Store the value in 'cells'
        rowNumber = (t - 1) * size(cellPosTimepointMax, 1) + i;
        if ~all(cells(rowNumber, :) == 0)
            error('Wrong row index!');
        end
        cells(rowNumber, :) = [ ...
            cellPosTimepointMax(i, 1), ...
            cellPosTimepointMax(i, 2), ...
            value, ...
            t];

        % Should we test for positive?
        if ~isempty(controlCells) && ~isempty(cmpThresholdFactor)
            if ...
                    value >= comparisonIntensityThresholdUpper || ...
                    value <= comparisonIntensityThresholdLower 
                numPositiveCells = numPositiveCells + 1;
            end
        end

        % Keep track of the mean value
        totalIntValue = totalIntValue + value;
    end

    % Print mean intensity
    meanIntValue = totalIntValue / size(cellPosTimepointMax, 1);

    if ~isempty(controlCells) && ~isempty(cmpThresholdFactor)
        % Add number of cells, number of positive cells and mean intensity
        fprintf(fid, '%d;%d;%.2f\n', ...
            size(cellPosTimepointMax, 1), ...
            numPositiveCells, ...
            meanIntValue);
    else
        % Just add the mean intensity
        fprintf(fid, '%.2f\n', meanIntValue);
    end

    % Update the waitbar
    waitbar(t / size(dataset, 1), hWaitbar, ...
        'Writing cell intensities to file...');
end

% Close file
fclose(fid);

% Close waitbar
close(hWaitbar);

% ------------------------------------------------------------------------%
%                                                                         %
%   CREATE QUALITY CONTROL IMAGES                                         %
%                                                                         %
% ------------------------------------------------------------------------%

% Format string
strg = sprintf('%%.%dd', 4);

% Prepare the background-subtracted image
img = single(bgsbDataset{timepointMax, 1});
img = img ./ max(img(:));
img = uint8(255.0 .* img);

h = figure('NumberTitle', 'off', 'Name', 'Background-subtracted image');
imshow(img, []);
hold on
plot(cellPosTimepointMax(:, 2), cellPosTimepointMax(:, 1), 'ro');

if ~isempty(controlCellPos)
    plot(controlCellPos(:, 2), controlCellPos(:, 1), 'yo');
    suffix = '_red_transformed_coords';
else
    suffix = '';
end

% File name
indxStrT = sprintf(strg, timepointMax);
fName = fullfile(outDirName, ...
    ['qc_bkg_cd_', num2str(cellDiameter), ...
    '_tf_', num2str(threshFactor), '_timepoint_' ...
    indxStrT, suffix, '.png']);

% Save
print(h, fName,  '-r150' , '-dpng');

% Prepare the original image
img = single(dataset{timepointMax, 1});
img = img ./ max(img(:));
img = uint8(255.0 .* img);

h = figure('NumberTitle', 'off', 'Name', 'Original image');
imshow(img, []);
hold on
plot(cellPosTimepointMax(:, 2), cellPosTimepointMax(:, 1), 'ro');

if ~isempty(controlCellPos)
    plot(controlCellPos(:, 2), controlCellPos(:, 1), 'yo');
    suffix = '_red_transformed_coords';
else
    suffix = '';
end

% File name
indxStrT = sprintf(strg, timepointMax);
fName = fullfile(outDirName, ...
    ['qc_orig_cd_', num2str(cellDiameter), ...
    '_tf_', num2str(threshFactor), '_timepoint_' ...
    indxStrT, suffix, '.png']);

% Save
print(h, fName,  '-r150' , '-dpng');

% Notice, if controlCellPos was not passed, at this stage it is empty. 
% We therefore set cellPos to be equal to cellPosTimepointMax and
% return both
if isempty(controlCellPos)
    cellPos = cellPosTimepointMax;
else
    cellPos = controlCellPos;
end

% Completed
fprintf(1, 'Results saved in %s.\n', outDirName);
fprintf(1, 'Result CSV file:\n > %s\n', CSVOutFileName);
fprintf(1, 'All done.\n');

% =========================================================================

function bkgsbtrImg = backgroundSubtractImage(img, sigma)

% Apply background subtraction
se = strel('disk', 2 * sigma + 1);
background = imopen(img, se);
bkgsbtrImg = img - background;

% =========================================================================

function cells = segmentCells(img, sigma)

% Is the image RGB?
if size(img, 3) == 3
    img = rgb2gray(img);
end

% Keep a copy of the original image
% origImg = img;

% Cast the image to single
img = single(img);

% Laplacian of Gaussian filter
sz = round([5 * sigma, 5 * sigma]);
if mod(size(sz, 1), 2) == 0
    sz = sz + [1 1];
end
H = -1 .* fspecial('log', sz, sigma);
I = imfilter(img, H, 'symmetric');

% Find local maxima
sz2 = round([2 .* sigma + 1, 2 .* sigma + 1, 3]);
if mod(size(sz2, 1), 2) == 0
    sz2 = sz2 + [1 1 0];
end
S = zeros([size(I) 3], class(I));
S(:, :, 2) = I;
pos = locmax3d(S, sz2);

% Extract the intensities from the LoG-filtered image
locMaxInd = sub2ind(size(I), pos(:, 1), pos(:, 2));
intensities = I(locMaxInd);

% Return
cells = [pos(:, 1:2), intensities];

% =========================================================================

function testSeriesOfThresholdFactors(cellsMax, threshFactor, ...
    md, mm, img, outDirName)

for i = 1 : numel(threshFactor)

    % Extract cell candidates
    bIndices = cellsMax(:, 3) > md + threshFactor(i) * mm;   

    % Both positive and negative
    h = figure('NumberTitle', 'off', 'Name', ...
        ['Threshold = ', num2str(threshFactor(i))]);

    imshow(img, []);
    hold on
    plot(cellsMax(~bIndices, 2), cellsMax(~bIndices, 1), 'yo');
    plot(cellsMax(bIndices, 2), cellsMax(bIndices, 1), 'ro');

    % File name
    fName = fullfile(outDirName, ...
        ['qc_threshold_both_', num2str(threshFactor(i)), '.png']);

    % Save
    print(h, fName,  '-r150' , '-dpng');

    % Only positive
    h = figure('NumberTitle', 'off', 'Name', ...
        ['Threshold = ', num2str(threshFactor(i))]);

    imshow(img, []);
    hold on
    plot(cellsMax(bIndices, 2), cellsMax(bIndices, 1), 'ro');

    % File name
    fName = fullfile(outDirName, ...
        ['qc_threshold_pos_', num2str(threshFactor(i)), '.png']);

    % Save
    print(h, fName,  '-r150' , '-dpng');

end

% =========================================================================

function k = locmax3d(image,mask)
%LOCMAX3D finds the local (intensity) maxima in a 3D image fast
%
% SYNOPSIS  locMaxCoord = locmax3d(image,mask)
%
% INPUT     image: any 3D-matrix of at least size [3,3,3]
%           mask (opt): size of the patch in which the local maximum has to
%                       be a maximum. Has to be odd size. Default: [3,3,3]
%
% OUTPUT    locMaxCoord: [y,x,z]-coordinates of the local maxima
%           function ignores border maxima.
%
% created by Dominik Thomann
% modified by Jonas Dorn, Aaron Ponti
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
%
%   The contents of this file are subject to the Mozilla Public License
%   Version 1.1 (the "License"); you may not use this file except in
%   compliance with the License. You may obtain a copy of the License at
%   http://www.mozilla.org/MPL/
%
%   Software distributed under the License is distributed on an "AS IS"
%   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
%   License for the specific language governing rights and limitations
%   under the License.
%
%   The Original Code is "Qu for MATLAB".
%
%   The Initial Developer of the Original Code is Aaron Ponti.
%   All Rights Reserved.
%
%--------------------------------------------------------------------------

%---------------test input----------------------------

%look for image
if nargin < 1 || isempty(image)
    error('we need at least an image as input argument!')
end

imgSize = size(image);

%image size
if length(imgSize)~=3 || any(imgSize<3)
    error('we need a 3D image of at least size 3x3x3!')
end

%mask
if nargin < 2 || isempty(mask)
    mask = [3 3 3];
else
    %check that we have a valid mask size (3D, not larger than image)
    
    if length(mask)~=3 || any(mask>imgSize)
        error(['we need a 3D-mask that is smaller than the input image (maskSize: ',num2str(mask),', imgSize: ',num2str(imgSize),')!']);
    end
    if any(floor(mask/2)==mask/2)
        error(['we need a mask of odd size (maskSize: ',num2str(mask),')!']);
    end
end

%-----------end test input-------------------------------


%-----------do doms fast but crazy routine

s = imgSize;
ci=zeros(prod(imgSize),1);
ct = 0;
for p=2:(s(3)-1)
    tmp =  (find((image(2:(s(1)-1),1:(s(2)-2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),3:s(2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),1:(s(2)-2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),3:s(2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),1:(s(2)-2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),3:s(2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),2:(s(2)-1),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),2:(s(2)-1),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),1:(s(2)-2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),1:(s(2)-2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),3:s(2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),3:s(2),p) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),2:(s(2)-1),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),2:(s(2)-1),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),2:(s(2)-1),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),2:(s(2)-1),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),2:(s(2)-1),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),2:(s(2)-1),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),1:(s(2)-2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),1:(s(2)-2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),3:s(2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(2:(s(1)-1),3:s(2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),1:(s(2)-2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),3:s(2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),3:s(2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),1:(s(2)-2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),1:(s(2)-2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),3:s(2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(1:(s(1)-2),1:(s(2)-2),p+1) < image(2:(s(1)-1),2:(s(2)-1),p))...
        & (image(3:s(1),3:s(2),p-1) < image(2:(s(1)-1),2:(s(2)-1),p)))...
        +(p-2)*(s(2)-2)*(s(1)-2));
    step = length(tmp);
    ci(ct+1:ct+step) = tmp;
    ct = ct + step;
end

% convert coords of maxima
[l, m, n]=ind2sub(s-2,ci(1:ct));

% add 1, because we ignored border pixels
k=[l+1 m+1 n+1];
% preassign ot
ot = zeros(size(k));

d=floor(mask/2);
ct=1;
for i=1:size(k,1)
    if(all((k(i,:)-d)>0) && all((k(i,:)+d)<=[size(image,1) size(image,2) size(image,3)]))
        patch=image(k(i,1)-d(1):k(i,1)+d(1),k(i,2)-d(2):k(i,2)+d(2),k(i,3)-d(3):k(i,3)+d(3));
        % only the values which are max in mask survive
        if all(image(k(i,1),k(i,2),k(i,3))>=patch)
            ot(ct,:)=k(i,:);
            ct=ct+1;
        end
    end
end
k=ot(1:ct-1,:);

% =========================================================================

function M=mmad(varargin)
% Median absolute deviation
%
% A widely used estimator for the scale of a sample is the standard
% deviation, which, however, is not robust. By definition, the median
% absolute deviation is a robust estimation of the 0.75 quantile of the
% standard normal distribution. A robust estimate of the scale can be
% therefore calculated as a function of the median absolute deviation,
% like this: s=mmad/0.6745.
%
% REMARK
% ------
% The mad returned by this function is s=median|xi-median(xi)|/0.6745,
% which is the robust alternative to the standard deviation. To obtain the
% estimator of the 0.75 quantile, which is defined as median|xi-median(xi)|,
% multiply the value returned by this function by the factor 0.6745.
%
% SYNOPSIS      s=mmad(A,mask)
%
% INPUT  A    : (1-3)D array
%        mask : (optional): none, or [y x], or '3d'
%               If no mask is passed, the array is considered to be 1D
%               This can be used to calculate the global mad for an N-D
%               array. If a mask is passed, the mad per mask is calculated
%               and an array with the same size as the input is returned.
%               REMARK: in 3D, mmad uses 26 neighbors (this cannot be
%                       changed, if another mask [y x z] is passed, mmad
%                       will still calculate the 3D mad but with 26
%                       neighbors).
% OUTPUT S    : mad of the input array A. It is either a scalar or an N-D
%               array, depending on the input mask.
%
% Aaron Ponti, 2005/06/27


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Check input parameter
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==1
    mask=1;
end

if nargin==2
    
    % Get mask
    mask=varargin{2};
    
    % Check wheteher the user passed a string -> 3D
    if ischar(mask)
        mask=[1 1 1];
    end
    
end

% Get input array
A=varargin{1};

% Array dimensions
nArrayDim=numel(size(A));

% Mask dimensions
nMaskDim=numel(mask);

% Check that dimensions match
if nMaskDim>1 && nArrayDim~=nMaskDim
    error(['The number of dimension of the input array A', ...
        ' should match the number of dimentions of the mask.']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 1D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==1
    
    % Absolute deviatons from the median
    D=abs(A-median(A(:)));
    
    % Median of the absolute deviations
    M=1.4826*median(D(:));
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 2D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==2
    
    % Absolute deviatons from the median
    d=medfilt2(A,mask);
    D=abs(A-d);
    
    % Median of the absolute deviations
    M=1.4826*medfilt2(D,mask);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 3D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==3
    
    % Absolute deviatons from the median
    d=ordfilt3D(A,14);
    D=abs(A-d);
    
    % Median of the absolute deviations
    M=1.4826*ordfilt3D(D,14);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% More than 3D is not supported
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)>3
    error('More than 3D is not supported.');
end

