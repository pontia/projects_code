function [singleFileDisplacementTables, folderName] = analysis(folderName)

% Initialize output
singleFileDisplacementTables = {};

CREATE_QUALITY_CONTROL_IMAGES = 0;

if nargin == 0
    
    % Select results folder
    folderName = uigetdir(pwd(), 'Select the folder with the .csv results');
    
    if isequal(folderName, 0)
        return
    end
    
end

%
% STEP 0: build the file names
%
displacementFileName = fullfile(folderName, 'Displacement.csv');
timeIndicesFileName = fullfile(folderName, 'Time_Index.csv');

%
% STEP 1: read the files
%
try
    displacementTable = importDisplacements(displacementFileName);
    fprintf(1, 'Imported file %s.\n', displacementFileName);
catch
    disp('File ''Displacement.csv'' not found in folder. Aborting.');
    return;
end

% If we opened a single file export, we won't have an ID
if all(isnan(displacementTable.ID))
    try
        displacementTable = importDisplacementsSingleFile(displacementFileName);
        fprintf(1, 'Imported single file %s.\n', displacementFileName);
    catch
        disp('File ''Displacement.csv'' not found in folder. Aborting.');
        return;
    end
end

try
    timeIndexTable = importTimeIndices(timeIndicesFileName);
    fprintf(1, 'Imported file %s.\n', timeIndicesFileName);
catch
    disp('File ''Time_Index.csv'' not found in folder. Aborting.');
    return;
end

% If we opened a single file export, we won't have an ID
if all(isnan(timeIndexTable.ID))
    try
        timeIndexTable = importTimeIndicesSingleFile(timeIndicesFileName);
        fprintf(1, 'Imported single file %s.\n', displacementFileName);
    catch
        disp('File ''Time_Index.csv'' not found in folder. Aborting.');
        return;
    end
end

% If the files are from a single export, there won't be an 
% "OriginalImageID" field.
if ~ismember('OriginalImageID', displacementTable.Properties.VariableNames) || ...
        ~ismember('OriginalImageID', timeIndexTable.Properties.VariableNames)
    
    displacementTable.OriginalImageID = ...
        categorical(repmat({'File01'}, size(displacementTable, 1), 1));
    timeIndexTable.OriginalImageID = ...
        categorical(repmat({'File01'}, size(timeIndexTable, 1), 1));
end
 
%
% STEP 2: split the results in tables per file
%

% Extract individual files
imarisFiles = unique(displacementTable.OriginalImageID);

% Allocate space
singleFileDisplacementTables = cell(1, numel(imarisFiles));
singleFileTimeIndexTables = cell(1, numel(imarisFiles));

% Split
for i = 1 : numel(imarisFiles)
    
    % Extract displacements for current file
    singleFileDisplacementTables{i} = ...
        displacementTable(...
        displacementTable.OriginalImageID == imarisFiles(i), :);
    
    % Extract time indices for current file
    singleFileTimeIndexTables{i} = ...
        timeIndexTable(...
        timeIndexTable.OriginalImageID == imarisFiles(i), :);
    
    % Check that the sizes match!
    if size(singleFileDisplacementTables{i}, 1) ~= ...
            size(singleFileTimeIndexTables{i}, 1)
        error(['Mismatch in the spot number for file ', imarisFiles(i)]);
    end
    
end

%
% STEP 3: Merge the tables (add a TimeIndex column to Displacement tables)
%
for i = 1 : numel(imarisFiles)
    
    % Add column
    singleFileDisplacementTables{i}.TimeIndex = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    
    % Process all spots
    for j = 1 : size(singleFileDisplacementTables{i}, 1)
        
        % Spot ID
        spotID = singleFileDisplacementTables{i}.ID(j);
        
        % Get the corresponding time point in timeIndexTable
        index = find(singleFileTimeIndexTables{i}.ID == spotID);
        
        % Check
        if numel(index) ~= 1
            error(['Could not find a univocal match for spot ', ...
                num2str(spotID), ' in time index table for file ', ...
                imarisFiles(i), '.']);
        end
        
        % Update the singleFileDisplacementTable
        singleFileDisplacementTables{i}.TimeIndex(j) = ...
            singleFileTimeIndexTables{i}.TimeIndex(index);
        
    end
    
end

%
% STEP 4: correct for stage movement
%
for i = 1 : numel(imarisFiles)
    
    % Add columns for corrected displacements
    singleFileDisplacementTables{i}.CorrectedDisplacementX = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.CorrectedDisplacementY = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.CorrectionTermX = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.CorrectionTermY = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    
    % Get all unique timepoints
    timeIndices = unique(singleFileDisplacementTables{i}.TimeIndex);
    
    % Extract all displacements for each time point
    for j = 1 : numel(timeIndices)
        
        indices = ...
            find(singleFileDisplacementTables{i}.TimeIndex == ...
            timeIndices(j));
        
        displX = singleFileDisplacementTables{i}.DisplacementX(indices);
        displY = singleFileDisplacementTables{i}.DisplacementY(indices);
        
        % Calculate the median displacement
        corrX = median(displX);
        corrY = median(displY);
        
        % Correct the displacements
        singleFileDisplacementTables{i}.CorrectedDisplacementX(indices) = ...
            singleFileDisplacementTables{i}.DisplacementX(indices) - corrX;
        singleFileDisplacementTables{i}.CorrectedDisplacementY(indices) = ...
            singleFileDisplacementTables{i}.DisplacementY(indices) - corrY;
        
        % Store the correction term
        singleFileDisplacementTables{i}.CorrectionTermX(indices) = corrX;
        singleFileDisplacementTables{i}.CorrectionTermY(indices) = corrY;
        
    end
end

%
% STEP 5: build relative positions
%
for i = 1 : numel(imarisFiles)
    
    % Add four more columns to the result table
    singleFileDisplacementTables{i}.RelativePositionX = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.RelativePositionY = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.CorrectedRelativePositionX = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    singleFileDisplacementTables{i}.CorrectedRelativePositionY = ...
        zeros(size(singleFileDisplacementTables{i}, 1), 1);
    
    % Make sure the rows are sorted by trackID and timepoint
    singleFileDisplacementTables{i} = ...
        sortrows(singleFileDisplacementTables{i}, ...
        {'TrackID', 'TimeIndex'}, ...
        'ascend');
    
    % Extract unique track IDs
    trackIDs = unique(singleFileDisplacementTables{i}.TrackID);
    
    for j = 1 : numel(unique(trackIDs))
        
        indices = find(...
            singleFileDisplacementTables{i}.TrackID == trackIDs(j));
        
        % Build relative positions and corrected relative positions
        singleFileDisplacementTables{i}.RelativePositionX(indices) = ...
            cumsum(singleFileDisplacementTables{i}.DisplacementX(indices));
        singleFileDisplacementTables{i}.RelativePositionY(indices) = ...
            cumsum(singleFileDisplacementTables{i}.DisplacementY(indices));
        singleFileDisplacementTables{i}.CorrectedRelativePositionX(indices) = ...
            cumsum(singleFileDisplacementTables{i}.CorrectedDisplacementX(indices));
        singleFileDisplacementTables{i}.CorrectedRelativePositionY(indices) = ...
            cumsum(singleFileDisplacementTables{i}.CorrectedDisplacementY(indices));
    end
    
end

%
% STEP 6: extract max displacement per track per file
%


%
% STEP 7 (optional): build control plots
%
if CREATE_QUALITY_CONTROL_IMAGES == 1

    mkdir(folderName, 'controls');
    controlDir = fullfile(folderName, 'controls');
    
    for i = 1 : numel(imarisFiles)
        
        % Extract unique track IDs
        trackIDs = unique(singleFileDisplacementTables{i}.TrackID);
        
        for j = 1 : numel(unique(trackIDs))
            
            indices = find(...
                singleFileDisplacementTables{i}.TrackID == trackIDs(j));
            
            maxX = max([ ...
                max(abs(singleFileDisplacementTables{i}.RelativePositionX(indices))), ...
                max(abs(singleFileDisplacementTables{i}.CorrectedRelativePositionX(indices)))
                ]);
            
            maxY = max([ ...
                max(abs(singleFileDisplacementTables{i}.RelativePositionY(indices))), ...
                max(abs(singleFileDisplacementTables{i}.CorrectedRelativePositionY(indices)))
                ]);
            
            rangeX = [-maxX - 1, maxX + 1];
            rangeY = [-maxY - 1, maxY + 1];
            
            h = figure();
            subplot(1, 2, 1);
            plot(...
                singleFileDisplacementTables{i}.RelativePositionX(indices), ...
                singleFileDisplacementTables{i}.RelativePositionY(indices), ...
                'r-x');
            axis([rangeX, rangeY]);
            xlabel('Relative position');
            
            subplot(1, 2, 2);
            plot(...
                singleFileDisplacementTables{i}.CorrectedRelativePositionX(indices), ...
                singleFileDisplacementTables{i}.CorrectedRelativePositionY(indices), ...
                'r-x');
            axis([rangeX, rangeY]);
            xlabel('Corrected relative position');
            
            fName = char(singleFileDisplacementTables{i}.OriginalImageName(indices(1), :));
            
            title(fName, 'Interpreter', 'none');
            
            print(h, fullfile(controlDir, ...
                [fName, '_track_', num2str(j, '%04d'), '_ctrl.png']), ...
                '-r150', '-dpng');
            
            close(h);
            
        end
        
    end
    
end

