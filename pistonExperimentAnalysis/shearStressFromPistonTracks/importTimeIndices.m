function TimeIndex = importTimeIndices(filename, dataLines)
%IMPORTFILE Import data from a text file
%  TIMEINDEX = IMPORTFILE(FILENAME) reads data from text file FILENAME
%  for the default selection.  Returns the data as a table.
%
%  TIMEINDEX = IMPORTFILE(FILE, DATALINES) reads data for the specified
%  row interval(s) of text file FILENAME. Specify DATALINES as a
%  positive scalar integer or a N-by-2 array of positive scalar integers
%  for dis-contiguous row intervals.
%
%  Example:
%  TimeIndex = importfile("E:\Data\Projects\Beads\results\Bead tracking.icsx_[ibrx_2019-10-11T11-26-21.113]_Statistics\Time_Index.csv", [5, Inf]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 11-Oct-2019 11:43:28

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [5, Inf];
end

%% Setup the Import Options
opts = delimitedTextImportOptions("NumVariables", 13);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["TimeIndex", "Unit", "Category", "Births", "Deaths", "TrackID", "ID", "OriginalID", "OriginalComponentName", "OriginalComponentID", "OriginalImageName", "OriginalImageID", "VarName13"];
opts.VariableTypes = ["double", "string", "categorical", "double", "double", "double", "double", "double", "categorical", "double", "categorical", "categorical", "string"];
opts = setvaropts(opts, [2, 13], "WhitespaceRule", "preserve");
opts = setvaropts(opts, [2, 3, 9, 11, 12, 13], "EmptyFieldRule", "auto");
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
TimeIndex = readtable(filename, opts);

end