function maskChannelSignal(filename, channels, zRange, wSize, stepSize, outDirName, nStds, fraction, saveFrgd)
% maskChannelSignal masks significant local signal over a moving window.
%
% SYNOPSIS
%
%   maskChannelSignal(filename, channels, zRange, wSize, stepSize, outDirName, nStds, fraction, saveBkg)
%
% INPUT
%
%   filename  : full path to the imaris file to open. Set it to [] to open
%               via a file dialog.
%   channels  : indices of channels to be considered. Examples: set
%                   channels = 1         to use only channel 1.
%                   channels = [1, 2, 4] to use only planes 1, 2 and 4.
%                   channels = 3:7       to use only planes 3 through 7.
%                   channels = []        to use all channels.
%   zRange    : array of planes to be considered. Examples: set
%                   zRange = 5           to use only plane 5.
%                   zRange = 1 : 10      to use only planes 1 through 10.
%                   zRange = []          to use the whole z extent
%   wSize     : [wX, wY, wZ]: size of the sliding window (voxels)
%   stepSize  : [sX, sY, sZ]: step size in X, Y, Z directions (voxels)
%   outDirName: full path to folder where to store the results. Set it to
%               [] to pick via a file dialog.
%   nStds     : (optional, default = 3). Number of standard deviations
%               above the mean background intensity for voxel intensities
%               to be considered signal.
%   fraction  : (optional, default 0.5). Fraction of pixels in a window
%               with signal intensities above threshold for the window to
%               be considered positive.
%   saveFrgd  : (optional, default 0). Set to 1 to save a mask with the
%               plain foreground segmentation.

% Check the number of parameters
if nargin == 6
    nStds = 3;
    fraction = 0.5;
    saveFrgd = 0;
elseif nargin == 7
    fraction = 0.5;
    saveFrgd = 0;
elseif nargin == 8
    saveFrgd = 0;
elseif nargin == 9
    % All set
else
    error('6, 7, 8, or 9 input parameters expected.');
end

% Check the parameter values
try
    wSizeX = wSize(1);
    wSizeY = wSize(2);
    wSizeZ = wSize(3);
catch
    error('Invalid parameter wSize.');
end

try
    stepX = stepSize(1);
    stepY = stepSize(2);
    stepZ = stepSize(3);
catch
    error('Invalid parameter wSize.');
end

% If no file was passed, ask the user to pick one
if isempty(filename)
    
    [fName, dirName] = uigetfile('*.ims', 'Select an Imaris file');
    if isequal(fName, 0)
        return;
    end
    filename = fullfile(dirName, fName);
end

if isempty(outDirName)
    outDirName = uigetdir('', 'Pick folder to store results...');
    if isequal(outDirName, 0)
        return;
    end
end

% Get file info
metadata = imarisinfo(filename);
if isempty(metadata)
    fprintf(1, 'Could not open file %s\n,', filename);
    return
end

% Make sure that the z range is compatible with the dataset
if numel(zRange) == 1
    if zRange < 1 || zRange > metadata.nPlanes
        error(['The selected plane ', num2str(zRange), ' is out of bounds!']);
    end
elseif isempty(zRange)
    zRange = 1 : metadata.nPlanes;
else
    zRange = sort(zRange);
    if any(zRange < 1 | zRange > metadata.nPlanes)
        error('At least one of the selected planes is out of bounds!');
    end
end

% In older MATLAB versions, the matlabpool must be opened explicitly
if verLessThan('matlab', '8.4')
    eval('if matlabpool(''size'') == 0, matlabpool open local; end');
end

% Check that the channels argument is compatible with the dataset
if numel(channels) == 1
    if channels < 1 || channels > metadata.nChannels
        error(['The selected channel ', num2str(channels), ' is out of bounds!']);
    end
elseif isempty(channels)
    channels = 1 : metadata.nChannels;
else
    channels = sort(channels);
    if any(channels < 1 | channels > metadata.nChannels)
        error('At least one of the selected channels is out of bounds!');
    end
end
    
% Initialize waitbar
hWaitbar = waitbar(0, 'Processing channels...');

curr = 0;
for c = channels
    
    % Keep track of the iteration number 
    curr = curr + 1;
    
    % Load volume
    data = imarisread(filename, c - 1);
    
    % Store original datatype
    datatype = class(data{1, 1});

    % Keep only the selected z range
    volume = data{1, 1};
    volume = volume(:, :, zRange);

    % Cast to single
    volume = single(volume);

    % Estimate background level
    nLevels = 8;
    n = zeros(1, nLevels);
    
    tic;
    parfor i = 1 : nLevels
        
        % Estimate background level
        BW = imextendedmin(volume, i, 26);
        
        % Get the background pixels
        bkg = volume(BW > 0);
        
        % Background estimation
        mn = mean(bkg);
        sd = std(single(bkg));
        
        n(i) = mn + nStds * sd;
        
        fprintf(1, '.');
        
    end
    fprintf(1, '\n');
    fprintf(1, 'Background estimation: %6.4f s\n', toc);
   
    % Calculate the optimal threshold
    minN = find(n == min(n), 1);
    optimalBkgLevel = n(minN);

    % Get volume size
    [sizeY, sizeX, sizeZ] = size(volume);

    % Store foreground if requested
    if saveFrgd == 1

        frgdFName = fullfile(outDirName, ['foreground_', ...
            metadata.channelNames{c}, '.tiff']);

        for i = 1 : sizeZ
    
            % Get curret plane
            current = volume(:, :, i);
            current(current <  optimalBkgLevel) = 0;
            current(current >= optimalBkgLevel) = single(intmax(datatype));
            current = cast(current, datatype);

            if i == 1
                mode = 'overwrite';
            else
                mode = 'append';
            end
    
            % Save the masked volume
            try
                imwrite(current, frgdFName, ...
                    'Compression', 'none', 'WriteMode', mode);
            catch
                disp(['Retrying appending plane ', num2str(i), ...
                    ' to file ', frgdFName, '...']);
                pause(0.1);
                imwrite(current, frgdFName, ...
                    'Compression', 'none', 'WriteMode', mode);
            end

        end
        
    end
    
    % Prepare the mask
    M = false([sizeY, sizeX, sizeZ]);
    
    % Number of pixels in a window
    nPixelsPerWindow = wSizeX * wSizeY * wSizeZ;

    tic;
    % TODO: Parallel!
    for y0 = 1 : stepY: sizeY
        
        for x0 = 1 : stepX : sizeX
            
            for z0 = 1 : stepZ : sizeZ
                
                winSmaller = 0;
                
                y = y0 + wSizeY - 1; if y > sizeY, y = sizeY; winSmaller = 1; end;
                x = x0 + wSizeX - 1; if x > sizeX, x = sizeX; winSmaller = 1; end;
                z = z0 + wSizeZ - 1; if z > sizeZ, z = sizeZ; winSmaller = 1; end;
                    
                % Extract the blocks
                B = volume(y0 : y, x0 : x, z0 : z);
                
                if winSmaller == 0
                    currNPixelsPerWindow = nPixelsPerWindow;
                else
                    currNPixelsPerWindow = numel(B(:));
                end
                
                nSignalAboveThresh = numel(find(B(:) > optimalBkgLevel));
                
                if nSignalAboveThresh / currNPixelsPerWindow >= fraction
                    
                    % Mark the window as positive
                    M(y0:y, x0:x, z0:z) = 1;
                    
                end
                
            end
            
        end
    end
    fprintf(1, 'Masking: %6.4f s\n', toc);
    
    % Now mask the volume and type cast the map
    maskedVolume = cast(single(volume) .* M, datatype);
    M = cast(single(intmax(datatype)) .* single(M), datatype);
    
    % Build the file names
    maskedFName = fullfile(outDirName, ['masked_', ...
        metadata.channelNames{c}, '.tiff']);
    binaryFName = fullfile(outDirName, ['binary_', ...
        metadata.channelNames{c}, '.tiff']);
    
    for i = 1 : sizeZ
    
        if i == 1
            mode = 'overwrite';
        else
            mode = 'append';
        end
        
        try
            % Save the masked volume
            imwrite(maskedVolume(:, :, i), maskedFName, ...
                'Compression', 'none', 'WriteMode', mode);
        catch
            disp(['Retrying appending plane ', num2str(i), ...
                ' to file ', maskedFName, '...']);
            pause(0.1);
            imwrite(maskedVolume(:, :, i), maskedFName, ...
                'Compression', 'none', 'WriteMode', mode);
        end
        
        try
            % Save the binary map
            imwrite(M(:, :, i), binaryFName, ...
                'Compression', 'none', 'WriteMode', mode);
        catch
            disp(['Retrying appending plane ', num2str(i), ...
                ' to file ', binaryFName, '...']);
            pause(0.1);
            imwrite(M(:, :, i), binaryFName, ...
                'Compression', 'none', 'WriteMode', mode);
        end
            
    end
    
    % Update waitbar
    waitbar(curr / numel(channels), hWaitbar);

end

% Close waitbar
close(hWaitbar);

% Save settings to file
settingsFName = fullfile(outDirName, 'settings.txt');
fid = fopen(settingsFName, 'w');
if fid == -1
    disp(['Could not open file ', settingsFName, ': ', ...
        'outputting to console.']);
    fid = 1;
end

% Write settings

% Format string for the channels
strgC = '%20s: ';
for i = 1 : numel(channels)
    strgC = cat(2, strgC, '%d ');
end
strgC = cat(2, strgC, '\n');
fprintf(fid, '%20s: %s\n', 'File name', filename);
fprintf(fid, strgC, 'Channels', channels);
fprintf(fid, '%20s: %d - %d\n', 'z range', min(zRange), max(zRange));
fprintf(fid, '%20s: [%d, %d, %d]\n', 'Window size', wSize(1), ...
    wSize(2), wSize(3));
fprintf(fid, '%20s: [%d, %d, %d]\n', 'Step size', stepSize(1), ...
    stepSize(2), stepSize(3));
fprintf(fid, '%20s: %s\n', 'Output dir name', outDirName);
fprintf(fid, '%20s: %d\n', 'Number of stds', nStds);
fprintf(fid, '%20s: %2.2f\n', 'Fraction pos voxels', fraction);
fprintf(fid, '%20s: %d\n', 'Save foreground', saveFrgd);

if fid > 1
    fclose(fid);
    edit(settingsFName);
end

fprintf(1, 'All done!\n');