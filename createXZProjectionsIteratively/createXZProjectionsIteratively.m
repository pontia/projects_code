function createXZProjectionIteratively(varargin)
% createXZProjectionIteratively creates XZ projections of TIFF files within a
% defined folder structure.
%
% The directory structure must be as follows: a root 'Experiment' folder
% contains any number of 'Condition' subfolders, each of which contains
% any number of 'Cell' subfolders. Each 'Cell' subfolder contains any
% number of single-channel, multi-plane TIFF files. Folder and file names
% are irrelevant, provided the hierarchy is maintained. The folder
% structure is recreated in a user-specified target directory.
%
% Experiment
% |_ Condition
%    |_ Cell
%       |_ timepoint1.tif
%       |_ timepoint2.tif
%       |_ ...
%       |_ timepointN.tif
%
% SYNOPSIS   createXZProjectionIteratively(expDir, outDir, voxelSz)
%
% INPUT      expDir : (optional) full path to experiment dir.
%                     If not passed or [], the user must pick it via a dialog.
%
%            outDir : (optional) full path to output dir. If it does not
%                     exist, it will be created.
%                     If not passed or [], the user must pick it via a dialog
%                     (in which case it must already exist).
%
%            voxelSz: (optional) array with [XY and Z] voxel sizes. If not
%                     passed or [], the user must specify it via a dialog.
%                     This is used to calculate the aspect ratio of the
%                     dimensions to interpolate the projection to maintain
%                     the correct spatial dimensions.
%
% OUTPUT     None
%
% Aaron Ponti, 2014/04/23

% =========================================================================
%
% Parse input
%
% =========================================================================

expDir = []; outDir = []; voxelSz = [];
switch nargin
    case 0, % Nothing
    case 1, expDir = varargin{1};
    case 2, expDir = varargin{1}; outDir = varargin{2};
    case 3, expDir = varargin{1}; outDir = varargin{2}; voxelSz = varargin{3};
    otherwise, error('Wrong number of input parameters.');
end

% Get experiment dir
if isempty(expDir)
    expDir = getExpDir();
    if isequal(expDir, 0)
        disp('Interrupted by the user.');
        return
    end
end

% Get output dir
if isempty(outDir)
    outDir = getOutDir();
    if isequal(outDir, 0)
        disp('Interrupted by the user.');
        return
    end
end

% Get voxel sizes
if isempty(voxelSz)
    voxelSz = getVoxelSz();
    if isequal(voxelSz, 0)
        disp('Interrupted by the user.');
        return
    end
end

% Make sure the output dir exists
if exist(outDir, 'dir') == 0
    mkdir(outDir);
end

% =========================================================================
%
% Process experiment
%
% =========================================================================

% Get all condition subfolders of expDir
condDirs = dir(expDir);

% Get rid of files
condDirs([condDirs.isdir] == 0) = [];

% Process all condition subdirectories
for i = 1 : numel(condDirs)
    
    % Skip '.' and '..'
    if strcmp(condDirs(i).name, '.') == 1 || ...
            strcmp(condDirs(i).name, '..') == 1
        continue;
    end
    
    % Create the corresponding output condition subfloder
    outCondFullDir = fullfile(outDir, condDirs(i).name);
    if exist(outCondFullDir, 'dir') ~= 7
        if mkdir(outDir,condDirs(i).name) == 0
            error(['Could not create folder ', outCondFullDir, '. Aborting.']);
        end
    end
    
    
    % Current condition folder (full path)
    condFullDir = fullfile(expDir, condDirs(i).name);
    
    % Extract the Cell subfolders
    cellDirs = dir(condFullDir);
    
    % Get rid of files
    cellDirs([cellDirs.isdir] == 0) = [];
    
    % Process all cell subdirectories
    for j = 1 : numel(cellDirs)
        
        % Skip '.' and '..'
        if strcmp(cellDirs(j).name, '.') == 1 || ...
                strcmp(cellDirs(j).name, '..') == 1
            continue;
        end
        
        % Create the corresponding output cell subfloder
        outCellFullDir = fullfile(outCondFullDir, cellDirs(j).name);
        if exist(outCellFullDir, 'dir') ~= 7
            if mkdir(outCondFullDir,cellDirs(j).name) == 0
                error(['Could not create folder ', ...
                    outCellFullDir, '. Aborting.']);
            end
        end
        
        % Current cell folder (full path)
        cellFullDir = fullfile(condFullDir, cellDirs(j).name);
        
        % Extract the tiff files
        tiffFiles = dir(cellFullDir);
        tiffFiles([tiffFiles.isdir] == 1) = [];
        
        % Process
        for k = 1 : numel(tiffFiles)
            
            % File name
            inFileName = fullfile(cellFullDir, tiffFiles(k).name);
            
            if strcmpi(inFileName(end - 3 : end), '.tif') == 0 &&  ...
                    strcmpi(inFileName(end - 4 : end), '.tiff') == 0
                continue;
            end
            
            % Inform
            disp(['Processing file ', inFileName, '...']);
            
            % Load the stack
            try
                stack = loadTIFFStack(inFileName);
            catch
                stack = [];
            end
            if isempty(stack)
                disp('* * * Not a valid multi-plane TIFF. Skipping...');
                continue;
            end
            
            % Calculate the projection
            projXZ = calcXZProjection(stack, voxelSz);
            
            % Build file name
            fName = fullfile(outCellFullDir, ['xz_', tiffFiles(k).name]);
            
            % Save the projection
            imwrite(projXZ, fName, 'Compression', 'none');
            
        end
        
    end
    
end

disp('Done.');


% === FUNCTIONS ===========================================================

function expDir = getExpDir()
expDir = uigetdir(pwd(), 'Please pick the experiment directory');

% -------------------------------------------------------------------------

function outDir = getOutDir()
outDir = uigetdir(pwd(), 'Please pick the output directory');

% -------------------------------------------------------------------------

function voxelSz = getVoxelSz()
voxelSz = 0;
answer = inputdlg({'XY voxel size:', 'Z voxel size:'}, ...
    'Input required', 1, {'1', '1'});
if isempty(answer)
    return;
end
xy = str2double(answer{1});
if isempty(xy)
    return;
end
z = str2double(answer{2});
if isempty(z)
    return;
end
voxelSz = [xy z];

% -------------------------------------------------------------------------

function stack = loadTIFFStack(fileName)

% Initialize output
stack = [];

% Do we have a tiff file?
if strcmp(fileName(end - 3 : end), '.tif') || ...
        strcmp(fileName(end - 4 : end), '.tiff')
    
    % Allocate memory
    info = imfinfo(fileName);
    nPlanes = numel(info);
    stackSize = [info(1).Height, info(1).Width, nPlanes];
    
    if info(1).BitDepth == 8
        stack = zeros(stackSize, 'uint8');
    elseif info(1).BitDepth == 16
        stack = zeros(stackSize, 'uint16');
    else
        stack = zeros(stackSize, 'single');
    end
    
    for n = 1 : nPlanes
        stack(:, :, n) = imread(fileName, n);
    end
end

% -------------------------------------------------------------------------

function stack_p_XZ = calcXZProjection(stack, voxelSz)

% Project along Y (to get an XZ projection)
stack_py = max(stack, [], 1);
stack_p_XZ = squeeze(stack_py)';

% Aspect ratios
aspect_XZ = voxelSz(2) / voxelSz(1);

% Calculate size of projections for interpolation
sizeP_XZ = [round(size(stack, 3) * aspect_XZ), size(stack, 2)];

% Is there a need to resize?
if all(size(stack_p_XZ) == sizeP_XZ)
    return;
end

% Interpolate to preserve the aspect ratio
stack_p_XZ = imresize(stack_p_XZ, sizeP_XZ, 'lanczos3');
