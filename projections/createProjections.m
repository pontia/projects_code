currDir = pwd();

DATASET = 2;

if DATASET == 1
    
    % ===== DATASET 1 =======
    
    cd('/work/projects/rajib/maximum projection image for aaron/')
    
    VX = 108; % vX = 0.108;
    VY = 108; % xY = 0.108;
    VZ = 420; % vZ = 0.420;
    
    % Channel 0: "DAPI"    Color: [0 0 1]
    % Channel 1: "EGFP"    Color: [0 1 0]
    % Channel 2: "dTomato" Color: [1 0 0]
    % Channel 3: "Alx647"  Color: [1 0.502 1]
    
else
    
    % ===== DATASET 2 =======
    
    cd('/work/projects/rajib/xz projection another one/')
    
    VX = 180; % vX = 0.180;
    VY = 180; % xY = 0.180;
    VZ = 294; % vZ = 0.294;
    
    % Channel 0: "Green"    Color: [0 0 1]
    % Channel 1: "Red"      Color: [0 1 0]
    % Channel 2: "Cyan"     Color: [0 1 1]
    
end

% ===== Common =======

% Using IceImarisConnector transfer all channels from Imaris to the MATLAB
% workspace as ch0, ch1, ... and and save them into channels.mat
%
% ch0 = conn.getDataVolume(0, 0);
% ...
% save('channels.mat', 'ch0', 'ch1', 'ch2', 'ch3')

load('channels.mat');

% Project along Y
ch0_py = max(ch0, [], 1);
ch1_py = max(ch1, [], 1);
ch2_py = max(ch2, [], 1);
if exist('ch3', 'var')
    ch3_py = max(ch3, [], 1);
end

% View from size
ch0_p_XZ = squeeze(ch0_py)';
ch1_p_XZ = squeeze(ch1_py)';
ch2_p_XZ = squeeze(ch2_py)';
if exist('ch3', 'var')
    ch3_p_XZ = squeeze(ch3_py)';
end

% Project along X
ch0_px = max(ch0, [], 2);
ch1_px = max(ch1, [], 2);
ch2_px = max(ch2, [], 2);
if exist('ch3', 'var')
    ch3_px = max(ch3, [], 2);
end

% View from size
ch0_p_YZ = squeeze(ch0_px);
ch1_p_YZ = squeeze(ch1_px);
ch2_p_YZ = squeeze(ch2_px);
if exist('ch3', 'var')
    ch3_p_YZ = squeeze(ch3_px);
end

if exist('ch3', 'var')
    save('projections.mat', ...
        'ch0_p_XZ', 'ch1_p_XZ', 'ch2_p_XZ', 'ch3_p_XZ', ...
        'ch0_p_YZ', 'ch1_p_YZ', 'ch2_p_YZ', 'ch3_p_YZ');
else
    save('projections.mat', ...
        'ch0_p_XZ', 'ch1_p_XZ', 'ch2_p_XZ', ...
        'ch0_p_YZ', 'ch1_p_YZ', 'ch2_p_YZ');
end

imwrite(ch0_p_XZ, 'ch0_p_XZ.tif', 'Compression', 'none', 'Resolution', [VZ VX]);
imwrite(ch1_p_XZ, 'ch1_p_XZ.tif', 'Compression', 'none', 'Resolution', [VZ VX]);
imwrite(ch2_p_XZ, 'ch2_p_XZ.tif', 'Compression', 'none', 'Resolution', [VZ VX]);
imwrite(ch0_p_YZ, 'ch0_p_YZ.tif', 'Compression', 'none', 'Resolution', [VZ VY]);
imwrite(ch1_p_YZ, 'ch1_p_YZ.tif', 'Compression', 'none', 'Resolution', [VZ VY]);
imwrite(ch2_p_YZ, 'ch2_p_YZ.tif', 'Compression', 'none', 'Resolution', [VZ VY]);
if exist('ch3', 'var')
    imwrite(ch3_p_XZ, 'ch3_p_XZ.tif', 'Compression', 'none', 'Resolution', [VZ VX]);
    imwrite(ch3_p_YZ, 'ch3_p_YZ.tif', 'Compression', 'none', 'Resolution', [VZ VY]);
end

cd(currDir)





