function [meanIntensities, stdIntensities, peakIntensities, ...
    deltaPeakIntervals, peakTimes, t, W, background] = estimateFiringIntervals(...
    TrackID, Value, Time, minTrajLength, deltaT, fct, fctWeight, ...
    outDir, calcStdDevs, qcFigure)
% Calculates firing interval of neurons from mean cell intensities.
%
% SYNOPSIS
%
%   [meanIntensities, stdIntensities, peakIntensities, ...
%    deltaPeakIntervals, peakTimes, t, W, background] = ...
%    estimateFiringIntervals(TrackID, Value, Time, minTrajLength, deltaT, 
%    fct, fctWeight, outDir, calcStdDevs, qcFigure)
%
% INPUT
%
%   TrackID:       vector of track IDs as imported from Imaris statistics
%                  in csv format.
%   Value:         vector of mean or median intensities as imported from
%                  Imaris statistics in csv format.
%   Time:          vector of time indices as imported from Imaris statistics
%                  in csv format.
%   minTrajLength: min length in time points for trajectories to be 
%                  considered for the analysis.
%   deltaT:        acquisition interval in seconds (e.g. 0.1s).
%   fct:           function of the peaks amplitudes to be used to calculate
%                  a threshold for discarding weak peaks.
%   fctWeight:     factor by which to multiply the result of 'fct' of the
%                  peak amplitudes.
%                  The threshold is fctWeight * fct(peak_amplitudes)  
%   outDir:        directory where the results are saved. Set to [] to 
%                  choose it via a file dialog.
%   calcStdDevs:   set to 1 to calculate and plot the standard deviations 
%                  of the intensities along the trajectories.
%                  WARNING: this  can be quite memory intensive!
%   qcFigure:      set to 1 to add quality control plots to the results 
%                  figure.
%
% OUTPUT
% 
%   meanIntensities:    mean intensities over all considered trajectories.
%   stdIntensities:     standard deviation of the intensities over all 
%                       considered trajectories.
%   peakIntensities:    mean intensities at the peaks only (all other
%                       values are set to NaN.
%   peakTimes:          times at which intensity peaks occur.
%   deltaPeakIntervals: intervals in seconds between peaks.
%   t:                  time vector up to the end of the longest trajectory.
%   W:                  matrix of all trajectory intensities (only filled if
%                       calcStdDevs == 1).
%   background:         fitted signal background (piece-wise linear).
%
% Aaron ponti, 2017/01/03 - 04

if nargin ~= 10
    error('10 input parameters expected!');
end

% Reconstruct trajectories
TrackID(isnan(TrackID)) = [];
uTrackID = unique(TrackID);

traj = cell(1, numel(uTrackID));
births = zeros(1, numel(uTrackID));

for i = 1 : numel(uTrackID)
    indices = TrackID == uTrackID(i);    
    traj{i} = Value(indices);
    births(i) = min(Time(indices));
end

% Calculate all trajectory lengths in time points
trajLengths = cellfun(@numel, traj);

% Keep only trajectories that are at least 'minTrajLength' time points
longBirths = births(trajLengths >= minTrajLength);
longTraj = traj(trajLengths >= minTrajLength);

% Time vector
t = 0 : deltaT : 2 * fix(max(births));

% Allocate space to store the (weighted) mean intensities
maxTrajLength = max(trajLengths);

% Change from index to time point
longBirths = t(longBirths);

% Build the trajectories
if calcStdDevs == 1

    W = nan(numel(longBirths), maxTrajLength);

    % Process all trajectories in parallel
    for i = 1 : numel(longBirths)

        b = longBirths(i);
        d = abs(t-b);
        indx = find(d == min(d), 1);

        last = indx + numel(longTraj{i}) - 1;
        if last > maxTrajLength
            last = maxTrajLength;
        end
        r = 1:(last - indx + 1);
        W(i, indx : last) = longTraj{i}(r)';
    end

    % Now calculate the mean trajectory intensities and their standard
    % deviations.
    meanIntensities = nanmean(W, 1);
    stdIntensities = nanstd(W, 1);

else

    % We do not store all trajectories
    W = [];

    % Allocate space for mean intensities and weights
    meanIntensities = zeros(1, maxTrajLength);
    w = zeros(1, maxTrajLength);

    % Process all trajectories incrementally
    for i = 1 : numel(longBirths)

        b = longBirths(i);
        d = abs(t-b);
        indx = find(d == min(d), 1);

        last = indx + numel(longTraj{i}) - 1;
        if last > maxTrajLength
            last = maxTrajLength;
        end
        r = 1:(last - indx + 1);
        meanIntensities(indx : last) = ...
            meanIntensities(indx : last) + longTraj{i}(r)';
        w(indx : last) = w(indx : last) + 1;

    end

    % Calculate the (weighted) mean intensity over all trajectories
    w(w == 0) = 1;
    meanIntensities = meanIntensities ./ w;
    stdIntensities = zeros(size(meanIntensities));

end

% Find the peak intensities
ind = locmax1d(meanIntensities);

% Find the local minima between peaks
indMin = locmin1d(meanIntensities);
minIntensities = nan(size(meanIntensities));
minIntensities(indMin) = 1;
minIntensities = minIntensities .* meanIntensities;

% Drop maxima that are before the fist min and after the last min
ind(ind < min(indMin)) = [];
ind(ind > max(indMin)) = [];

% Extract the maxima
peakIntensities = nan(size(meanIntensities));
peakIntensities(ind) = 1;
peakIntensities = peakIntensities .* meanIntensities;

% Find the signal background (approximated with a line)
background = zeros(size(meanIntensities));
for i = 1 : numel(indMin)
    if i == 1
        background(1:indMin(1)) = minIntensities(indMin(1));
    else   
        slope = (minIntensities(indMin(i)) - ...
            minIntensities(indMin(i - 1))) ./ ...
            (indMin(i) - indMin(i - 1));

        b = meanIntensities(indMin(i - 1));
        background(indMin(i - 1):indMin(i)) = ...
            b + slope .* (0:numel(indMin(i - 1):indMin(i)) - 1);
    end

    if i == numel(indMin)
        background(indMin(i):end) = minIntensities(indMin(i));
    end
end

% Get coordinates of background at peak positions
backgroundAtPeakPos = background(ind);

% Calculate the deltaFs
deltaF = peakIntensities(ind) - background(ind);

% Calculate threshold on deltaF
deltaFThresh = fctWeight * feval(fct, deltaF);

% Drop small peaks
ind(deltaF < deltaFThresh) = [];

% Update peak intensities
peakIntensities = nan(size(meanIntensities));
peakIntensities(ind) = meanIntensities(ind);

% Update background intensities
backgroundAtPeakPos(deltaF < deltaFThresh) = [];

% Calculate the distribution characteristics of deltaF
deltaFAtPos = meanIntensities(ind) - backgroundAtPeakPos;
meanDeltaF = mean(deltaFAtPos);
stdDeltaF = std(deltaFAtPos);

% Calculate dF / F
dFoverF = (meanIntensities - background) ./ background;

% Calculate the intervals between peaks
peakTimes = t(ind);
deltaPeakIntervals = diff(peakTimes);

t = t(1:numel(meanIntensities));

if isempty(outDir)
    outDir = uigetdir();
    if isequal(outDir, 0)
        outDir = pwd();
    end
end

% Save variables
save(fullfile(outDir, 'results.mat'), 'meanIntensities', 'stdIntensities', ...
    'peakIntensities', 'deltaPeakIntervals', 'peakTimes', 't', ...
    'W', 'background');

% Plot and save (quality control) figure

hFig = figure;
subplot(1, 2, 1);
plot(t, meanIntensities, '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
hold on;
if calcStdDevs == 1
    errorbar(t, meanIntensities, stdIntensities);
end
plot(t, background, '-', 'LineWidth', 1.0);
plot(peakTimes, backgroundAtPeakPos, 's');
if qcFigure == 1
    stem(t, peakIntensities);
    stem(t, minIntensities);
end
for i = 1 : numel(peakTimes)
    text(peakTimes(i), meanIntensities(ind(i)) + 25, ...
        [num2str(peakTimes(i)), 's']);
end
title({['Period (mean +/- std): ', num2str(mean(deltaPeakIntervals)), ...
    ' +/- ', num2str(std(deltaPeakIntervals)), 's'], ...
    ['Intensity (mean +/- std): ', num2str(meanDeltaF), ' +/- ', ...
    num2str(stdDeltaF)], ['Function used for peak intensity thresholding: ', ...
    num2str(fctWeight), ' * ', fct, '(dF)']});
xlabel(['Acquisition interval: ', num2str(deltaT), 's']);
ylabel('Mean/median signal intensity');
subplot(1, 2, 2);
plot(t, dFoverF);
ylabel('deltaF/F');

% Save figure
savefig(hFig, fullfile(outDir, 'plot.fig'))
