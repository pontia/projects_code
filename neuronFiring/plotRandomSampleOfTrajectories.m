function hFig = plotRandomSampleOfTrajectories(W, t, n, minLength, hFig)
% Simple function to plot a random subset of n trajectories of min length.
%
% Aaron Ponti, 2017/01/04

if nargin < 5
    hFig = [];
end

if n > size(W, 1)
    n = size(W, 1);
end

if isempty(hFig) || ~ishandle(hFig)
    hFig = figure;
else
    figure(hFig);
end

hold on;
    
nFound = 0; tested = 0; plotted = zeros(1, n);
while nFound < n && tested <= size(W, 1)
    
    index = randi(size(W, 1));
    if isempty(find(plotted == index, 1))
        traj = W(index, :);
        if numel(find(~isnan(traj))) > minLength
            plot(t, traj);
            nFound = nFound + 1;
            plotted(nFound) = index;
        end
    end
    tested = tested + 1;

end

fprintf(1, 'Tested %d trajectories to find %d with lifetime > %d.\n', ...
    tested, n, minLength);
