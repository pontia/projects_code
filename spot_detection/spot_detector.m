function [all_pos_x, all_pos_y, thresh] = spot_detector(folder, out_folder, spot_diameter, cell_diameter, thresh)
% Detect spots in a series of images.
%
% SYNOPSIS
%
%   [all_pos_y, all_pos_x, thresh] = spot_detector(folder, out_folder, spot_diameter, cell_diameter, thresh)
%
% INPUT
%
%   folder       : folder to be scanned for TIFF files to process
%   out_folder   : output folder where the quality control images are 
%                  stored; set to '' or [] to omit.
%   spot_diameter: (optional, default = 5) expected diameter of a spot
%   cell_diameter: (optional, default = 25) expected diameter of a cell
%                  (containing the spots); if there are no cells, it is 
%                  suggested to set this diameter to a large number 
%                  (several times larger than the spot diameter to correct 
%                  for the image background. Set to 0 to omit.
%  thresh        : (optional) threshold that distinguishes real spots from 
%                  noisy local maxima. Omit to estimate from spot intensity
%                  distribution.
%
% OUTPUT
%
% all_pos_x      : cell array of all x coordinates of spots for each image
% all_pos_y      : cell array of all y coordinates of spots for each image
% thresh         : estimated spot instensity threshold (see INPUT).
%
% Aaron Ponti, 04/2017

if nargin < 2 || nargin > 5
    error('2, 3, 4 or 5 input parameters expected.');
end

if nargin == 2
    spot_diameter = 5;
    cell_diameter = 25;
    thresh = -1;
elseif nargin ==3 
    cell_diameter = 25;
    thresh = -1;
elseif nargin == 4
    thresh = -1;
elseif nargin == 5
    % All set
else
    error('Wrong number of parameters!');
end
   
% Spot kernel
sigma = floor(spot_diameter / 4);
sz = [sigma * 9, sigma * 9];
D = DoGMask3D(sz, sigma, 0.9);

% Background subtraction kernel
if cell_diameter == 0
    H = [];
else
    sigma_cell = floor(cell_diameter / 4);
    sz_cell = [sigma_cell * 9, sigma_cell * 9];
    H = fspecial('gaussian', sz_cell, sigma_cell);
end

% Loc max operator
spot_radius = floor(spot_diameter / 2);
locmax_sz = [spot_radius spot_radius];

% Get files
d = dir(fullfile(folder, '*.tif'));

% Allocate space for results
all_k = cell(1, numel(d) - 2);
all_pos_x = cell(1, numel(d) - 2);
all_pos_y = cell(1, numel(d) - 2);

n = 0;
c = 0;
toBeDel = [];

% Open waitbar
hWaitbar = waitbar(0, 'Processing...');

% Process
for i = 1 : numel(d)

    if d(i).isdir == 1
        c = c + 1;
        toBeDel = cat(2, toBeDel, i);
        continue
    end

    try
        % Load and cast to single precision
        img = single(imread(fullfile(folder, d(i).name)));
    catch
        c = c + 1;
        toBeDel = cat(2, toBeDel, i);
        fprintf(1, 'Skipping file %s.\n', fullfile(folder, d(i).name));
        continue;
    end

    % Current image
    n = n + 1;

    % Background subtraction
    if ~isempty(H)
        img = img - imfilter(img, H, 'same');
        img(img < 0) = 0.0;
    end

    % Filter with DoG
    I = single(imfilter(img, D, 'same'));

    % Find local maxima and store them
    L = locmax2d(I, locmax_sz);
    all_k{n} = L(L > 0);
    [y, x] = find(L > 0);
    all_pos_y{n} = y;
    all_pos_x{n} = x;

    % Inform
    waitbar(i/numel(d), hWaitbar);

end
d(toBeDel) = [];

% Close waitbar
close(hWaitbar);

% Process the extracted candidates
all_k = all_k(1:n);
all_pos_x = all_pos_x(1:n);
all_pos_y = all_pos_y(1:n);

all_k_vector = zeros(1, sum(cellfun(@numel, all_k)));
pos = 1;
for i = 1 : n
    all_k_vector(pos : pos + numel(all_k{i}) - 1) = all_k{i};
    pos = pos + numel(all_k{i});
end

% Analyze the local maxima
if thresh == -1
    thresh = multithresh(all_k_vector, 1);
end

if ~isempty(out_folder)
    if ~exist(out_folder, 'dir')
        mkdir(out_folder);
    end
    storeResults = 1;
else
    storeResults = 0;
end

warning off
for i = 1 : n

    % Filter the results
    indx = find(all_k{i} < thresh);
    all_k{i}(indx) = [];
    all_pos_x{i}(indx) = [];
    all_pos_y{i}(indx) = [];

    % If requested, create quality control figures
    if storeResults == 1
        img = imread(fullfile(folder, d(i).name));
        h = figure; imshow(img, []); hold on;
        plot(all_pos_x{i}, all_pos_y{i}, 'ro');
        print(fullfile(out_folder, ['qc_', d(i).name]), '-dtiff', '-r125');
        close(h);
    end
end
warning on