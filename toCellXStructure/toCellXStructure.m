function [inFolders, outFolders] = toCellXStructure(inputFolder, outputFolder)

if nargin == 0

    inputFolder = uigetdir(pwd(), 'Pick input folder');
    if isequal(inputFolder, 0)
        return;
    end

    outputFolder = uigetdir(pwd(), 'Pick output folder');
    if isequal(outputFolder, 0)
        return;
    end

end

% Build structure of folders to process
inFolders = {};
outFolders = {};
[inFolders, outFolders] = rdir(inputFolder, outputFolder, inFolders, outFolders);


function [allInPath, allOutPath] = rdir(inDirName, outDirName, allInPath, allOutPath)
d = dir(inDirName);
processedFilesInThisDir = false;
for i = 1 : size(d)
    if ~d(i).isdir

        if processedFilesInThisDir == true
            % This folder was already processed; we keep scanning in the
            % case there are still folders
            continue;
        end

        % Construct the list of files to be processed for this folder
        sets = processFileList(d);
        if isempty(sets)
            continue;
        end
        
        % Process the set of files
        processFiles(inDirName, outDirName, sets);

        % Now inform that all files have been processed
        processedFilesInThisDir = true;

    else
        if strcmp(d(i).name, '.') || strcmp(d(i).name, '..')
            continue;
        else
            
            % Process the next level of directories
            nextInPath = fullfile(inDirName, d(i).name);
            nextOutPath = fullfile(outDirName, d(i).name);
            allInPath = cat(1, allInPath, nextInPath);
            allOutPath = cat(1, allOutPath, nextOutPath);
            [allInPath, allOutPath] = rdir(nextInPath, nextOutPath, allInPath, allOutPath);
        end
    end
end

function sets = processFileList(dirList)

sets = containers.Map();

for i = 1 : numel(dirList)

    if dirList(i).isdir
        continue;
    else
        
        if ~endsWith(dirList(i).name, '.tif', 'IgnoreCase', true)
            continue;
        end
        
        % Fast version - works only with current filename structure
        currCode = dirList(i).name(1:9);
        
        if sets.isKey(currCode)
            files = sets(currCode);
        else
            files = {};
        end
        
        % Append the file
        files = cat(1, files, dirList(i).name);
        
        % Add to the map
        sets(currCode) = files;
    end
end

function processFiles(inDirName, outDirName, fileSet)

max8bit = 150;
max16bit = 8000;

k = 1/25 .* ones(5, 5);

fprintf(1, 'Processing folder %s...', inDirName);

codes = fileSet.keys;
for i = 1 : numel(codes)
    
    % The key corresponds to the folder name to create
    outCodeDirName = fullfile(outDirName, codes{i});
    if ~exist(outCodeDirName, 'dir')
        mkdir(outCodeDirName);
    end
    
    % Open the images
    imageNames = fileSet(codes{i});
    for j = 1 : numel(imageNames)
       
        % Open the image
        imageName = fullfile(inDirName, imageNames{j});
        GFP = imread(imageName, 1);
        RFP = imread(imageName, 2);
        
        % Process the RFP channel
        if isa(RFP, 'uint8')
            pRFP = uint8(255.0 .* single(imfilter(RFP, k, 'symmetric', 'same')) ./ max8bit);
        elseif isa(RFP, 'uint16')
            pRFP = uint16(65535.0 .* single(imfilter(RFP, k, 'symmetric', 'same')) ./ max16bit);
        else
            pRFP = imadjust(imfilter(RFP, k, 'symmetric', 'same'));
        end

        % Save the images to the output folder
        imwrite(GFP, fullfile(outCodeDirName, ['GFP_p', imageNames{j}]));
        imwrite(RFP, fullfile(outCodeDirName, ['RFP_p', imageNames{j}]));
        imwrite(pRFP, fullfile(outCodeDirName, ['Segm_p', imageNames{j}]));
       
    end
    
end

fprintf(1, 'Done.\n');

