function phaseContrastSegmentation(imgOrFileName, sizeRange)
% Finds cells in phase contrast images.
%
% SYNPSIS
%
%   phaseContrastSegmentation(imgOrFileName, sizeRange)
%
% INPUT
%
%   imgOrFileName: either a full path to the image to be read or a 2D,
%                  1-channel image.
%
%   sizeRange    : [minSize maxSize] range of acceptable sizes (diameters)
%                  for cells (in pixels).
%
% Copyright: Aaron Ponti, BSSE, ETHZ, 2014

if nargin ~= 2
    error('Two input parameters expected.');
end
    
if isa(imgOrFileName, 'char')
    % Try loading the file
    dataset = loadGeneric(imgOrFileName);
    if isempty(dataset)
        error(['Could not load ', imgOrFileName]);
    end
    if any(size(dataset, 1) > 1)
        error('Only single-channel, single-timepoint datasets are supported.');
    end
    img = dataset{1, 1};
else
    % Todo: add checks
    img = imgOrFileName;
end

if size(img, 3) > 1
    error('2D images are supported.');
end

% Keep a copy of the images
imgOriginal = img;
img = im2double(imgOriginal);

% Low-pass filter
sigma = sizeRange(1) / 2; 
sigma = sigma + 1 - mod(sigma, 2);
sz = sigma * 3; 
K = fspecial('gaussian', [sz sz], sigma);
img = imfilter(img, K, 'same', 'symmetric');

% Background subtraction
% img = imclose(img, strel('disk', round(sizeRange(2) * 1.5))) - img;

% Use the circular Hough transform to find the cells
centers = imfindcircles(img, sizeRange, ...
    'ObjectPolarity', 'bright', 'Method', 'TwoStage', ...
    'Sensitivity', 0.85);
if isempty(centers)
    disp('No cells found.');
    return;
end

figure; imshow(imgOriginal, []);
hold on;
plot(centers(:, 1), centers(:, 2), 'r.', 'MarkerSize', 8);
