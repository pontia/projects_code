function extractSubImages(inDirName, outDirName)
% extractSubImages(inDirName, outDirName)
% Aaron Ponti, 2014/10/27

if nargin > 2
    error('1 or 2 input parameters expected.');
end

if nargin == 0
    inDirName = uigetdir('', 'Please select input folder');
    outDirName = uigetdir('', 'Please select output folder');
end

if isequal(inDirName, 0)
    return;
end

if nargin == 1
    outDirName = uigetdir('', 'Please select output folder');
end

if isequal(outDirName, 0)
    return;
end

if exist(inDirName, 'dir') ~= 7
    error('The input directory does not seem to exist.');
end

if exist(outDirName, 'dir') ~= 7
    % Try creating it
    [s, msg] = mkdir(outDirName);
    if s == 0
        error(msg);
    end
end

d = dir(inDirName);

for i = 1 : numel(d)

    if d(i).isdir
        continue;
    end
    
    % Open the image
    try
        [img, map] = imread(fullfile(inDirName, d(i).name));
        if ~isempty(map)
            img = ind2gray(img, map);
        end
    catch
        fprintf(1,['File ', d(i).name, ' is not an (valid) image. Skipping.\n']);
        continue;
    end
    
    % Inform
    fprintf(1, ['Processing file ', d(i).name, '... ']);
    
    % Get the relevant file name parts
    [~, body, ext] = fileparts(d(i).name);
    
    % Extract the subimages
    [A, B] = extractSubImagesFromCurrent(img);
    
    % Save them
    imwrite(A, map, fullfile(outDirName, [body, '_A', ext]));
    imwrite(B, map, fullfile(outDirName, [body, '_B', ext]));

    % Inform
    fprintf(1, 'Done!\n');
        
end

function [A, B] = extractSubImagesFromCurrent(img)

% Get the image size
[~, sX] = size(img);

if sX < 51
    A = [];
    B = [];
    return;
end    

firstRow = -1;
lastRow = Inf;

inBackground = 1;
beyondSignal = 0;

for r = 1 : size(img, 1)
   
    if beyondSignal 
        break;
    end
    
    currentRange = range(img(r, :));
    
    if inBackground == 1 && beyondSignal == 0
        
        if currentRange == 0
        
            continue;
            
        else
            
            % Here we enter the signal
            firstRow = r;
            inBackground = 0;
            continue;

        end
        
    end
    
        
    if inBackground == 0 && beyondSignal == 0
        
        if currentRange > 0
        
            continue;
            
        else
            
            % Here we leave the signal
            lastRow = r - 1;
            inBackground = 1;
            beyondSignal = 1;
            continue;

        end
        
    end
    
end

if firstRow == -1 || lastRow == Inf
    A = [];
    B = [];
    return
end

% Extract the signal found so far
img = img(firstRow : lastRow, :);

% Now divide in the middle
middleColumn = fix(sX / 2);

if range(img(:, middleColumn)) ~= 0
    A = [];
    B = [];
    return;
end
    
% Find the end of the signal
b = 0;
i = 1;
lastColumnA = Inf;
while b == 0
   if range(img(:, middleColumn - i)) == 0
       i = i + 1;
   else
       b = 1;
       lastColumnA = middleColumn - i;
   end
end

b = 0;
i = 1;
firstColumnB = Inf;
while b == 0
   if range(img(:, middleColumn + i)) == 0
       i = i + 1;
   else
       b = 1;
       firstColumnB = middleColumn + i;
   end
end

if lastColumnA == Inf || firstColumnB == Inf
    A = [];
    B = [];
    return;
end

% Final cut
A = img(:, 1 : lastColumnA);
B = img(:, firstColumnB :end);
