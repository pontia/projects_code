function backgroundSubtract(radius, stretch, inDirName, outDirName)
% backgroundSUbtract(radius, stretch, inDirName, outDirName)
% Aaron Ponti, 2014/10/28

% Check the input
if nargin < 2 || nargin > 4
    error('2 to 4 input parameters expected.');
end

if ~isscalar(radius) || radius < 1
    error('radius must be a positive integer.');
end

if mod(radius, 2) == 0
    radius = radius + 1;
end
hsize = 3 * radius;

if ~isscalar(stretch) || (stretch ~= 0 && stretch ~= 1)
    error('stretch must be either 0 or 1.');
end

if nargin < 3
    inDirName = uigetdir('', 'Please select input folder');
    outDirName = uigetdir('', 'Please select output folder');
end

if isequal(inDirName, 0)
    return;
end

if nargin < 4
    outDirName = uigetdir('', 'Please select output folder');
end

if isequal(outDirName, 0)
    return;
end

if exist(inDirName, 'dir') ~= 7
    error('The input directory does not seem to exist.');
end

if exist(outDirName, 'dir') ~= 7
    % Try creating it
    [s, msg] = mkdir(outDirName);
    if s == 0
        error(msg);
    end
end


% Create the morphological structuring element
% se = strel('disk', radius);
Ky = fspecial('gaussian', [hsize 1], radius);
Kx = Ky';

% Get the file list   
d = dir(inDirName);

% Process all files
for i = 1 : numel(d)

    if d(i).isdir
        continue;
    end
    
    % Open the image
    try
        [img, map] = imread(fullfile(inDirName, d(i).name));
        if ~isempty(map)
            img = ind2gray(img, map);
        end
    catch
        fprintf(1,['File ', d(i).name, ' is not an (valid) image. Skipping.\n']);
        continue;
    end
    
    % Inform
    fprintf(1, ['Processing file ', d(i).name, '... ']);
    
    % Get the relevant file name parts
    [~, body, ext] = fileparts(d(i).name);
    
    % Run background subtraction
    I = subtractBackgroundFromCurrent(img, Ky, Kx, stretch);
    
    % Save them
    imwrite(I, fullfile(outDirName, [body, '_sub', ext]));

    % Inform
    fprintf(1, 'Done!\n');
end

function I = subtractBackgroundFromCurrent(img, Ky, Kx, stretch)
  
O = double(img);
I = O;

I = imfilter(I, Ky, 'same', 'symmetric');
I = imfilter(I, Kx, 'same', 'symmetric');

I = O - I;

if stretch == 1

    mx = max(I(:));
    mn = min(I(:));
    
    I = (I - mn) ./ (mx - mn);
    
    I = double(intmax(class(img))) .* I;

end
I = cast(I, 'like', img);
