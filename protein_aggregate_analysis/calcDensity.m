function calcDensity(inDirName)
% calcDensity(inDirName)
% Aaron Ponti, 2014/10/29

% Check the input
if nargin > 1
    error('0 or 1 input parameters expected.');
end

if nargin == 0
    inDirName = uigetdir('', 'Please select input folder with probabilty images');
end

if exist(inDirName, 'dir') ~= 7
    error('The input directory does not seem to exist.');
end

% Get the file list   
d = dir(inDirName);

% Allocate space to store the results
res = cell(numel(d) - 2, 3);

% Process all files
n = 0;
for i = 1 : numel(d)

    if d(i).isdir
        continue;
    end
    
    n = n + 1;
    
    % Open the image
    try
        img = imread(fullfile(inDirName, d(i).name));
        if ~isa(img, 'single') || min(img(:)) < 0 || max(img(:)) > 1
            error('The image must be of class ''single'' and be in the 0..1 range.');
        end
    catch
        fprintf(1,['File ', d(i).name, ' is not an (valid) image. Skipping.\n']);
        continue;
    end
    
    % Inform
    fprintf(1, ['Processing file ', d(i).name, '... ']);
    
    % Calculate the area covered and the material covered
    den = numel(img);
    pBinary = sum(img(:) > 0) / den;
    pMaterial = sum(img(:)) / den;

    % Store
    res{n, 1} = d(i).name;
    res{n, 2} = pBinary;
    res{n, 3} = pMaterial;
    
    % Inform
    fprintf(1, 'Done!\n');
end

% Save the results to file

% Directory where to store the file
OUTPUTDIR=getenv('TMP');
if isempty(OUTPUTDIR)
    OUTPUTDIR=getenv('TEMP');
    if isempty(OUTPUTDIR)
        OUTPUTDIR=getenv('HOME');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=pwd;
        end
    end
end

tmpfile = fullfile(OUTPUTDIR, 'areas.txt');
fid = fopen(tmpfile, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

fprintf( fid, ...
    [ ...
    '%64s\t', ...   % File name
    '%20s\t', ...    % Area
    '%20s\n' ], ...  % Area (weighted)
    'File name', ...
    'Rel area', ...   
    'Rel area (weighted)' );

for i = 1 : size(res, 1)

    if length(res{i, 1}) < 64
        name = res{i, 1};
    else
        name = [res{i, 1}(1:30), '...', res{i, 1}(end - 30:end)];
    end
    
    fprintf( fid, ...
    [ ...
    '%64s\t', ...   % File name
    '%20.4f\t', ...    % Area
    '%20.4f\n' ], ...  % Area (weighted)
    name, ...
    res{i, 2}, ...   
    res{i, 3} );

end

if fid ~= 1
    fclose(fid);
    edit(tmpfile);
end
