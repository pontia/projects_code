function exportIlastikDatasetsFromHDF5ToTiff(inFileName, outDirName, outDataType)
% Exports an Ilastik dataset to TIFF file with optional data type conversion.
%
% This function assumes that there is just one dataset in the root of the
% file and will export only that.
%
% SYNOPSIS
%
%   exportIlastikDatasetsFromHDF5ToTiff(inFileName, outFileName, outDataType)
%
% INPUT
%
%   inFileName : full path to the input HDF5 file name. Set it to [] to pick
%                a file via a file dialog
%   outDirName:  full path to the folder where the (multi-page) TIFF files
%                will be saved. Each file contains all z planes for a given
%                class. Naming convention is:
% 
%                               inFileName_class0.tif
%                               inFileName_class1.tif
%                               inFileName_class2.tif
%                                      ...       
%                Set it to [] to pick a file via a file dialog
%   outDataType: (optional) omit for no conversion. Otherwise, one of
%                'uint8', 'uint16', 'single'.
%
%   Please note: if the input dataset datatype is single and all values are
%                between 0 and 1, they will be stretched to cover the full
%                output data range if outDataType is 'uint8' or 'uint16'.
%
% OUTPUT
%
%   None
%
% Copyright Aaron Ponti, 2015/02/10


if nargin < 1 || nargin > 3
    error('One or two input parameters expected.');
end

% Did the user specify an output data type?
if nargin < 3
    outDataType = [];
end

% If the file name is not passed, ask the user to pick it
if isempty(inFileName)
    [inFileName, inDirName] = uigetfile(...
        {'*.h5;*.hdf5', 'HDF5 files (*.h5;*.hdf5)';}, ...
        'Please pick hdf5 file');
    if isequal(inFileName, 0)
        return
    end
    
    % Full input file name
    fullInputFileName = fullfile(inDirName, inFileName);
    
else
    
    % Use the argument
    fullInputFileName = inFileName;
    
end

% If the file name is not passed, ask the user to pick it
if isempty(outDirName)
    
    if exist('inDirName', 'var')
        suggestedOutDirName = inDirName;
    else
        suggestedOutDirName = pwd();
    end
     
    outDirName = uigetdir(suggestedOutDirName, 'Select output directory');
    if isequal(outDirName, 0)
        return
    end
    
end

% Get info
fileinfo = h5info(fullInputFileName);

% Are there datasets in the root?
if numel(fileinfo.Datasets) == 0
    error('Expected one dataset at the root of the file. None found.');
end
if numel(fileinfo.Datasets) == 0
    disp(['Warning: Expected one dataset at the root of the file. ', ...
        'Only the first will be exported.']);
end

% Get name of the dataset
datasetName = fileinfo.Datasets(1).Name;

% Read the dataset
data = hdf5read(fullInputFileName, datasetName);

% Convert and export
nClasses = size(data, 1);
nPlanes = size(data, 4);

for c = 1 : nClasses
    
    % BUild output file name
    [~, b] = fileparts(fullInputFileName);
    currentOutputFileName = fullfile(outDirName, ...
        [b, '_class', num2str(c - 1), '.tif']);
    
    for z = 1 : nPlanes
        
        % Extract current plane
        currentPlane = squeeze(data(c, :, :, z));
        
        if nPlanes > 1
            currentPlane = currentPlane';
        end
        
        % Depending on the input and output datatype and range, difference
        % conversions might be needed.
        if isa(currentPlane, 'single')
            
            if isempty(outDataType)
                
                % Nothing to do - we will save as single precision data
                
            elseif strcmpi(outDataType, 'uint8')
                
                if min(currentPlane(:)) >=0 && max(currentPlane(:)) <= 1
                    
                    % Stretch to intmax 8
                    currentPlane = uint8(255.0 * currentPlane);
                    
                else
                    
                    % Just cast
                    currentPlane = uint8(currentPlane);
                    
                end
                
            elseif strcmpi(outDataType, 'uint16')
                
                if min(currentPlane(:)) >=0 && max(currentPlane(:)) <= 1
                    
                    % Stretch to intmax 16
                    currentPlane = uint16(65535.0 * currentPlane);
                    
                else
                    
                    % Just cast
                    currentPlane = uint16(curentPlane);
                    
                end
                
            else
                
                % Nothing to do - we will save as single precision data
                
            end
            
        elseif isa(currentPlane, 'uint8')
            
            % Just cast
            currentPlane = uint8(currentPlane);
            
        elseif isa(currentPlane, 'uint16')
            
            % Just cast
            currentPlane = uint16(curentPlane);
            
        else
            
            % We try to write as single
            disp('Writing single-precision (32-bit) data...');
            currentPlane = single(currentPlane);
            
        end
        
        % Write the file
        if z == 1
            
            try
                
                % Write first plane to file
                imwrite(currentPlane, currentOutputFileName, ...
                    'Compression', 'None');
                
            catch
                disp(['Retrying appending plane ', num2str(z), ...
                    ' to file ', currentOutputFileName, '...']);
                pause(0.1);
                imwrite(currentPlane, currentOutputFileName, ...
                    'Compression', 'None');
            end
            
        else
            
            try
                
                % Applend other planes
                imwrite(currentPlane, currentOutputFileName, ...
                    'Compression', 'None', ...
                    'WriteMode','append');
                
            catch
                disp(['Retrying appending plane ', num2str(z), ...
                    ' to file ', currentOutputFileName, '...']);
                pause(0.1);
                imwrite(currentPlane, currentOutputFileName, ...
                    'Compression', 'None', ...
                    'WriteMode','append');
            end
            
        end
    end
    
end
