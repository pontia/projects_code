function exportIlastikDatasetsFromHDF5ToRaw(inFileName, outDirName, outDataType)
% Exports an Ilastik dataset to RAW file with optional data type conversion.
%
% This function assumes that there is just one dataset in the root of the
% file and will export only that.
%
% SYNOPSIS
%
%   exportIlastikDatasetsFromHDF5ToTiff(inFileName, outFileName, outDataType)
%
% INPUT
%
%   inFileName : full path to the input HDF5 file name. Set it to [] to pick
%                a file via a file dialog
%   outDirName:  full path to the folder where the RAW files will be saved.
%                Each file contains all z planes for a given class. 
%                Naming convention is:
% 
%                               inFileName_class0.raw
%                               inFileName_class1.raw
%                               inFileName_class2.raw
%                                      ...       
%                Set it to [] to pick a file via a file dialog
%   outDataType: (optional) omit for no conversion. Otherwise, one of
%                'uint8', 'uint16', 'single'.
%
%   Please note: if the input dataset datatype is single and all values are
%                between 0 and 1, they will be stretched to cover the full
%                output data range if outDataType is 'uint8' or 'uint16'.
%
% OUTPUT
%
%   None
%
% Copyright Aaron Ponti, 2018/01/09


if nargin < 1 || nargin > 3
    error('One or two input parameters expected.');
end

% Did the user specify an output data type?
if nargin < 3
    outDataType = [];
end

% If the file name is not passed, ask the user to pick it
if isempty(inFileName)
    [inFileName, inDirName] = uigetfile(...
        {'*.h5;*.hdf5', 'HDF5 files (*.h5;*.hdf5)';}, ...
        'Please pick hdf5 file');
    if isequal(inFileName, 0)
        return
    end
    
    % Full input file name
    fullInputFileName = fullfile(inDirName, inFileName);
    
else
    
    % Use the argument
    fullInputFileName = inFileName;
    
end

% If the file name is not passed, ask the user to pick it
if isempty(outDirName)
    
    if exist('inDirName', 'var')
        suggestedOutDirName = inDirName;
    else
        suggestedOutDirName = pwd();
    end
     
    outDirName = uigetdir(suggestedOutDirName, 'Select output directory');
    if isequal(outDirName, 0)
        return
    end
    
end

% Get info
fileinfo = h5info(fullInputFileName);

% Are there datasets in the root?
if numel(fileinfo.Datasets) == 0
    error('Expected one dataset at the root of the file. None found.');
end
if numel(fileinfo.Datasets) == 0
    disp(['Warning: Expected one dataset at the root of the file. ', ...
        'Only the first will be exported.']);
end

% Get name of the dataset
datasetName = fileinfo.Datasets(1).Name;

% Read the dataset
data = hdf5read(fullInputFileName, datasetName);

% Convert and export
nClasses = size(data, 1);

for c = 1 : nClasses
    
    % Build output file name
    [~, b] = fileparts(fullInputFileName);
    currentOutputFileName = fullfile(outDirName, ...
        [b, '_class', num2str(c - 1), '.raw']);
    
    % Open the file
    fid = fopen(currentOutputFileName, 'w');
    
    % Extract current channel
    currentStack = squeeze(data(c, :, :, :));
    currentStack = permute(currentStack, [2 1 3]);

    % Depending on the input and output datatype and range, difference
    % conversions might be needed.
    if isa(currentStack, 'single')
            
        if isempty(outDataType)
                
            % Nothing to do - we will save as single precision data
                
        elseif strcmpi(outDataType, 'uint8')
                
            if min(currentStack(:)) >=0 && max(currentStack(:)) <= 1
                    
                % Stretch to intmax 8
                currentStack = uint8(255.0 * currentStack);
                    
            else
                    
                % Just cast
                currentStack = uint8(currentStack);
                    
            end
                
        elseif strcmpi(outDataType, 'uint16')
                
            if min(currentStack(:)) >=0 && max(currentStack(:)) <= 1
                
                % Stretch to intmax 16
                currentStack = uint16(65535.0 * currentStack);
                    
            else
                    
                % Just cast
                currentStack = uint16(curentPlane);
                    
            end
                
        else
                
            % Nothing to do - we will save as single precision data
            
        end
            
    elseif isa(currentStack, 'uint8')
            
        % Just cast
        currentStack = uint8(currentStack);
            
    elseif isa(currentStack, 'uint16')
            
        % Just cast
        currentStack = uint16(curentPlane);
            
    else
            
        % We try to write as single
        disp('Writing single-precision (32-bit) data...');
        currentStack = single(currentStack);
            
    end
        
    % Write the stack to the file
    fwrite(fid, currentStack(:), class(currentStack));

    % Close the file
    fclose(fid);
    
end
