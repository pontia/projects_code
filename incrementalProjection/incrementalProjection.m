function incrementalProjection(inputFile, outputFile, framerate, w1, ...
    w2, save_images, channel_order)
% Creates fly-through AVI movie from a z stack (of up to 3 channels)
%
% Should be used together with incrementalProjectionGUI.
%
% Each frame z in the movie M is calculate from the input stack I as
% follows (pseudo-code):
%
%   M(z) = w1 * I(z) + w2 * max(I(1:z))
% 
% Each frame in the movie is the weighted average of current plane and the
% maximum intensity projection of all planes between 1 and z - 1.
%
% Interesting combinations of w1 and w2:
%
%   Individual z planes      : w1 = 1.0; w2 = 0.0
%   Incremental z projections: w1 = 0.0; w2 = 1.0
% 
% SYNOPSIS
%
%    incrementalProjection(inputFile, outputFile, framerate, ...
%        w1, w2, save_images, channel_order)
%
% INPUT
%
%    inputFile    : input file with full path. Set to '' to pick with an 
%                   open dialog.
%    outputFile   : output AVI file with full path. Set to '' to pick with
%                   an open dialog.
%    framerate    : movie frame rate (optional, default 5).
%    w1           : (optional, default = 0.6) weigth for current plane z
%    w2           : (optional, default = 0.4) weight for the max 
%                   projection of 1 : (z - 1)
%    save_images  : (0|1, optional; default 0) whether to save the frames 
%                   as TIFF files as well.
%    channel_order: (optional, default = [1, 2, 3]) Use a permutation of 
%                   [1, 2, 3] to swap files in the output.
%
% OUTPUT
%
%    None
%
% Aaron Ponti, 2017

% Hard-coded
lastZ = Inf;

% Check the input argument number
if nargin < 2 || nargin > 7
    error('2, 3, 4, 5, 6 or 7 input parameters expected.');
end

% Default input parameters
if nargin == 2
    framerate = 5; w1 = 0.6; w2 = 0.4; save_images = 1; 
    channel_order = [1, 2, 3];
elseif nargin == 3
    w1 = 0.6; w2 = 0.4; save_images = 1; 
    channel_order = [1, 2, 3];
elseif nargin == 4
    w2 = 0.4; save_images = 1; channel_order = [1, 2, 3];
elseif nargin == 5
    save_images = 1; channel_order = [1, 2, 3];
elseif nargin == 6
    channel_order = [1, 2, 3];
else
    % Pass
end

% Pick an input file if none was given
if isempty(inputFile)
    [fName, dirName] = uigetfile('*.*', 'Pick file...');
    if isequal(fName, 0)
        return;
    end
    inputFile = fullfile(dirName, fName);
end

% Pick an output file if none was given
if isempty(outputFile)
    [d, b, ~] = fileparts(inputFile);
    proposedName = fullfile(d, [b, '.avi']);
    [outFName, outDirName] = uiputfile(proposedName, 'Save movie as...');
    if isequal(outFName, 0)
        return;
    end
    outputFile = fullfile(outDirName, outFName); 
end

% Load the input file
disp('Loading file...');
hWaitbar = waitbar(0.5, 'Loading file...');
[~, ~, ext] = fileparts(inputFile);
if strcmpi(ext, '.ims')
    dataset = imarisread(inputFile);
else
    dataset = loadGeneric(inputFile);
end
close(hWaitbar);

nImages = size(dataset{1, 1}, 3);
nChannels = size(dataset, 2);
if nChannels > 3
    disp('Only the first 3 channels will be considered.');
    nChannels = 3;
end
projC = cell(nChannels, nImages);

% Build projections
disp('Building movie frames...');

% Part 1
hWaitbar = waitbar(0, 'Building movie frames: first pass...');
for i = 1 : nImages
    for c = 1 : nChannels
        if w2 > 0
            % Running max projection
            if i == 1
                projC{c, 1} = single(dataset{1, c}(:, :, 1));
            else
                projC{c, i} = single(max( ...
                    cat(3, ...
                    projC{c, i - 1}, ...
                    dataset{1, c}(:, :, i)), ...
                    [], 3));
            end
        else
            projC{c, i} = single(dataset{1, c}(:, :, i));
        end
    end
    % Update waitbar
    waitbar(i/nImages, hWaitbar);
end

% Close waitbar
close(hWaitbar);

% Part 2
hWaitbar = waitbar(0, 'Building movie frames: second pass...');
blended = cell(nChannels, nImages);
for i = 1 : nImages
    for c = 1 : nChannels
        if i == 1
            blended{c, i} = w1 .* projC{c, i};
        else
            if w2 > 0
                blended{c, i} = w1 * single(dataset{1, c}(:, :, i)) + ...
                    w2 * projC{c, i - 1};
            else
                % Current plane only
                blended{c, i} = w1 .* projC{c, i};
            end
        end
    end
    % Update waitbar
    waitbar(i/nImages, hWaitbar);
end
projC = blended;

% Close waitbar
close(hWaitbar);

% Build RGB image
disp('Normalizing and converting to 8-bit RGB...');
y = size(dataset{1, 1}, 1);
x = size(dataset{1, 1}, 2);
comb = zeros([y, x, 3], 'uint16');
projComb = cell(1, nImages);
maxVal = -1;

hWaitbar = waitbar(0, 'Normalizing: first pass...');
for i = 1 : nImages
    current = comb;
    for c = 1 : nChannels
        current(:, :, c) = projC{channel_order(c), i}; % Swap channels?
        mx = max(projC{channel_order(c), i}(:));
        if mx > maxVal
            maxVal = mx;
        end   
    end
    projComb{i} = current;
        
    % Update waitbar
    waitbar(i/nImages, hWaitbar);
end
maxVal = single(maxVal);

% Close waitbar
close(hWaitbar);

% Normalize
hWaitbar = waitbar(0, 'Normalizing: second pass...');
for i = 1 : nImages
    tmp = single(projComb{i});
    tmp = uint8(tmp ./ maxVal .* (2^8 - 1));
    projComb{i} = tmp;

    % Update waitbar
    waitbar(i/nImages, hWaitbar);
end

% Close waitbar
close(hWaitbar);

% Save images
if save_images == 1
    hWaitbar = waitbar(0, 'Saving images...');
    w1_str = ['w1_', num2str(w1), '_'];
    w2_str = ['w2_', num2str(w2), '_'];
    outDir = fileparts(outputFile);
    disp('Saving images...');
    strg = sprintf('%%.%dd', 5);
    for i = 1 : nImages
        if i > lastZ
            break;
        end
        outFileName = fullfile(outDir, ...
            ['proj_', w1_str, w2_str, ...
            sprintf(strg, i), '.tif']);
        imwrite(projComb{i}, outFileName, 'Compression', 'None');
        
        % Update waitbar
        waitbar(i/nImages, hWaitbar);
    end

    % Close waitbar
    close(hWaitbar);
end

% Save movie
disp('Saving movie...');
hWaitbar = waitbar(0, 'Saving movie...');
outputVideo = VideoWriter(outputFile);
outputVideo.FrameRate = framerate;
open(outputVideo)
for i = 1 : nImages
    if i > lastZ
        break;
    end
   writeVideo(outputVideo, projComb{i});
           
   % Update waitbar
   waitbar(i/nImages, hWaitbar);
end
close(outputVideo)

% Close waitbar
close(hWaitbar);
disp('All done.');
