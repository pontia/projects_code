function varargout = incrementalProjectionGUI(varargin)
% INCREMENTALPROJECTIONGUI MATLAB code for incrementalProjectionGUI.fig
%      INCREMENTALPROJECTIONGUI, by itself, creates a new INCREMENTALPROJECTIONGUI or raises the existing
%      singleton*.
%
%      H = INCREMENTALPROJECTIONGUI returns the handle to a new INCREMENTALPROJECTIONGUI or the handle to
%      the existing singleton*.
%
%      INCREMENTALPROJECTIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INCREMENTALPROJECTIONGUI.M with the given input arguments.
%
%      INCREMENTALPROJECTIONGUI('Property','Value',...) creates a new INCREMENTALPROJECTIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before incrementalProjectionGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to incrementalProjectionGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help incrementalProjectionGUI

% Last Modified by GUIDE v2.5 15-Mar-2017 16:56:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @incrementalProjectionGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @incrementalProjectionGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function incrementalProjectionGUI_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for incrementalProjectionGUI
handles.output = hObject;

% Parameters
handles.inFileName = '';
handles.outFileName = '';

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes incrementalProjectionGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function varargout = incrementalProjectionGUI_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% =========================================================================
%
% CALLBACKs
%
% =========================================================================

function pushInputFile_Callback(hObject, eventdata, handles)
[fName, dirName] = uigetfile('*.*', 'Pick file...');
if isequal(fName, 0)
    return;
end
hObject.String = fName;
handles.inFileName = fullfile(dirName, fName);
guidata(gcf, handles);

function pushOutputFile_Callback(hObject, eventdata, handles)
if ~isempty(handles.inFileName)
    [d, b, ~] = fileparts(handles.inFileName);
    proposedName = fullfile(d, [b, '.avi']);
else
    proposedName = fullfile(pwd(), 'movie.avi');
end
[outFName, outDirName] = uiputfile(proposedName, 'Save movie as...');
if isequal(outFName, 0)
    return;
end
hObject.String = outFName;
handles.outFileName = fullfile(outDirName, outFName);
guidata(gcf, handles);

function editFrameRate_Callback(hObject, eventdata, handles)
val = str2double(hObject.String);
if isnan(val) || val < 1
    hObject.String = '5';
end

function editBlendW1_Callback(hObject, eventdata, handles)
val = str2double(hObject.String);
if isnan(val) || val < 0.0 || val > 1.0
    hObject.String = '0.6';
end
handles.popupPresets.Value = 1;

function editBlendW2_Callback(hObject, eventdata, handles)
val = str2double(hObject.String);
if isnan(val) || val < 0.0 || val > 1.0
    hObject.String = '0.4';
end
handles.popupPresets.Value = 1;

function editChannelOrder_Callback(hObject, eventdata, handles)
val = str2num(hObject.String);
if isempty(val) || numel(val) > 3 || any(val < 1) || any(val > 3)
    hObject.String = '1, 2, 3';
end

function pushHelp_Callback(hObject, eventdata, handles)
msg = { ...
    'Creates a fly-through AVI movie from a z stack (of up to 3 channels).', ...
    '', ...
    'Each frame z in the movie M is calculate from the input stack I as', ...
    'follows (pseudo-code):', ...
    '', ...
    'M(z) = w1 * I(z) + w2 * max(I(1:z))', ...
    '', ...
    'Each frame in the movie is the weighted average of current plane', ...
    'and the maximum intensity projection of all planes between 1 and', ...
    'z - 1.', ...
    '', ...
    'Interesting combinations of w1 and w2:', ...
    '', ...
    'Individual z planes: w1 = 1.0; w2 = 0.0', ...
    'Incremental z projections: w1 = 0.0; w2 = 1.0', ...
    '' };
uiwait(msgbox(msg , 'Help'));

function checkSaveImages_Callback(hObject, eventdata, handles)

function pushRun_Callback(hObject, eventdata, handles)
% Additional parameter checks will be performed by incrementalProjection
inputFile = handles.inFileName;
if isempty(inputFile)
    uiwait(errordlg('Please pick an input file!', ...
        'Error', 'modal'));
    return
end
outputFile = handles.outFileName;
if isempty(outputFile)
    uiwait(errordlg('Please pick an output movie file!', ...
        'Error', 'modal'));
    return
end
framerate = str2double(handles.editFrameRate.String);
w1 = str2double(handles.editBlendW1.String);
w2 = str2double(handles.editBlendW2.String);
save_images = handles.checkSaveImages.Value;
channel_order = str2num(handles.editChannelOrder.String);

incrementalProjection(...
    inputFile, ...
    outputFile, ...
    framerate, ...
    w1, ...
    w2, ...
    save_images, ...
    channel_order)

uiwait(msgbox('All done.', 'Info', 'modal'));

function popupPresets_Callback(hObject, eventdata, handles)
val = hObject.Value;
switch val
    case 1
        % Custom
        return 
    case 2
        % Individual z planes
        % blend = 0
        % w1 = 1.0
        % w2 = 0.0
        handles.editBlendW1.String = '1.0';
        handles.editBlendW2.String = '0.0';
    case 3
        % Incremental projections
        % blend = 0
        % w1 = 0.0
        % w2 = 1.0
        handles.editBlendW1.String = '0.0';
        handles.editBlendW2.String = '1.0';
    otherwise
        return
end



% =========================================================================
%
% CREATEFCNs
%
% =========================================================================

function editBlendW1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editFrameRate_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editBlendW2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editChannelOrder_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupPresets_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
