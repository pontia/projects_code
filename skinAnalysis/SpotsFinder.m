function SpotsFinder
% Copyright D-BSSE, ETHZ Basel, 2017
import java.awt.Color;
import javax.swing.border.LineBorder;
close all
%% ###########################################DRAW GUI######################
SS=get(0,'ScreenSize');
f=figure('outerposition',[0.1*SS(3) 0.1*SS(4) 0.8*SS(3) 0.9*SS(4)],'Name','Spot detection','numbertitle','off','toolbar','figure');
%---------------------- first panel - load images Control
panel1=uipanel('Units','normalized','Position',[0.01 0.01 0.33 0.99],'Title','Control Images','backgroundcolor',[0.95 0.87 0.73]);
h.FileBtn=uicontrol('parent',panel1,'Style','pushbutton','Units','normalized','Position',[0.08 0.95 0.3 0.04],'FontSize',9.9999,'String','Load control images','backgroundcolor',[0 0.75 0.75],'Callback',{@FileBtn_Callback});
h.showfilename=uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.4 0.96 0.4 0.02],'backgroundcolor',[0.95 0.87 0.73],'String','filename','fontsize',7);
h.Image=axes('Units','normalized','Position',[0.05 0.18 0.8 0.8],'FontSize',1,'parent',panel1); axis square
h.PrevImgBtn=uicontrol('parent',panel1,'Style','pushbutton','Units','normalized','Position',[0.15 0.12 0.25 0.05],'String','< Previous image','backgroundcolor',[0.7 0.8 0.6],'Callback',{@PrevImgBtn_Callback});
h.NextImgBtn=uicontrol('parent',panel1,'Style','pushbutton','Units','normalized','Position',[0.5 0.12 0.25 0.05],'String','Next image >','backgroundcolor',[0.7 0.8 0.6],'Callback',{@NextImgBtn_Callback});
uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.3 0.86 0.08 0.04],'BackgroundColor',[0.95 0.87 0.73],'String','R=','fontsize',12);
h.REdt=uicontrol('parent',panel1,'Style','edit','Units','normalized','Position',[0.38 0.875 0.08 0.03],'fontsize',13,'String','30','backgroundcolor','w');
h.SelectSpotBtn=uicontrol('parent',panel1,'Style','pushbutton','Units','normalized','Position',[0.05 0.87 0.2 0.04],'String','Select spot','backgroundcolor',[0.7 0.9 0.7],'Callback',{@SelectSpotBtn_Callback});
h.MeanSpotInt1=uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.48 0.872 0.2 0.03],'BackgroundColor',[0.95 0.87 0.73],'String','<Spot>=?','fontsize',8);
h.SelectPaperBtn=uicontrol('parent',panel1,'Style','pushbutton','Units','normalized','Position',[0.05 0.81 0.2 0.04],'String','Select paper','backgroundcolor',[0.6 0.9 0.7],'Callback',{@SelectPaperBtn_Callback});
h.MeanBlackInt1=uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.25 0.81 0.2 0.03],'BackgroundColor',[0.95 0.87 0.73],'String','<Black>=?','fontsize',8);
h.ShowImgNum1=uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.25 0.3 0.5 0.03],'BackgroundColor',[0.95 0.87 0.73],'String','(o_O)','fontsize',12,'Visible','off');
h.SkinKoefEdt=uicontrol('parent',panel1,'Style','edit','Units','normalized','Position',[0.9 0.875 0.06 0.03],'fontsize',13,'String','4','backgroundcolor',[0.6 0.6 0.6]);
h.ShowGroupControl=uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.23 0.03 0.9 0.08],'BackgroundColor',[0.95 0.87 0.73],'String','<Here will be group data>','fontsize',12);
uicontrol('parent',panel1,'Style','text','Units','normalized','Position',[0.74 0.87 0.15 0.028],'BackgroundColor',[0.95 0.87 0.73],'String','R_skin=','fontsize',12);
%---------------------------- second panel - load images Data-----------
panel2=uipanel('Units','normalized','Position',[0.34 0.01 0.33 0.99],'Title','Data Images','backgroundcolor',[0.9 0.8 0.73]);
h.FileBtnData=uicontrol('parent',panel2,'Style','pushbutton','Units','normalized','Position',[0.08 0.95 0.3 0.04],'FontSize',9.9999,'String','Load data images','backgroundcolor',[0 0.75 0.75],'Callback',{@FileBtnData_Callback});
h.showfilenameData=uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.4 0.96 0.4 0.02],'backgroundcolor',[0.9 0.8 0.73],'String','filename','fontsize',7);
h.ImageData=axes('Units','normalized','Position',[0.05 0.18 0.8 0.8],'FontSize',1,'parent',panel2); axis square
h.PrevImgBtnData=uicontrol('parent',panel2,'Style','pushbutton','Units','normalized','Position',[0.15 0.12 0.25 0.05],'String','< Previous image','backgroundcolor',[0.7 0.8 0.6],'Callback',{@PrevImgBtnData_Callback});
h.NextImgBtnData=uicontrol('parent',panel2,'Style','pushbutton','Units','normalized','Position',[0.5 0.12 0.25 0.05],'String','Next image >','backgroundcolor',[0.7 0.8 0.6],'Callback',{@NextImgBtnData_Callback});
uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.3 0.86 0.08 0.04],'BackgroundColor',[0.9 0.8 0.73],'String','R=','fontsize',12);
h.REdtData=uicontrol('parent',panel2,'Style','edit','Units','normalized','Position',[0.38 0.872 0.08 0.03],'fontsize',13,'String','30','backgroundcolor','w');
h.SelectSpotBtnData=uicontrol('parent',panel2,'Style','pushbutton','Units','normalized','Position',[0.05 0.87 0.2 0.04],'String','Select spot','backgroundcolor',[0.7 0.9 0.7],'Callback',{@SelectSpotBtnData_Callback});
h.MeanSpotInt2=uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.48 0.872 0.2 0.03],'BackgroundColor',[0.9 0.8 0.73],'String','<Spot>=?','fontsize',8);
h.SelectPaperBtnData=uicontrol('parent',panel2,'Style','pushbutton','Units','normalized','Position',[0.05 0.81 0.2 0.04],'String','Select paper','backgroundcolor',[0.6 0.9 0.7],'Callback',{@SelectPaperBtnData_Callback});
h.MeanBlackInt2=uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.25 0.81 0.2 0.03],'BackgroundColor',[0.9 0.8 0.73],'String','<Black>=?','fontsize',8);
h.ShowImgNum2=uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.45 0.3 0.5 0.03],'BackgroundColor',[0.9 0.8 0.73],'String','(o_O)','fontsize',12,'Visible','off');
h.ShowGroupData=uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.23 0.03 0.9 0.08],'BackgroundColor',[0.9 0.8 0.73],'String','<Here will be group data>','fontsize',12);
h.SkinKoefDataEdt=uicontrol('parent',panel2,'Style','edit','Units','normalized','Position',[0.9 0.875 0.06 0.03],'fontsize',13,'String','4','backgroundcolor',[0.6 0.6 0.6]);
uicontrol('parent',panel2,'Style','text','Units','normalized','Position',[0.74 0.87 0.15 0.028],'BackgroundColor',[0.9 0.8 0.73],'String','R_skin=','fontsize',12);
%---------------------------- third panel - manipulate images, get data-----------
% Initialize panel
panel3=uipanel('Units','normalized','Position',[0.67 0.01 0.33 0.99],'Title','Output Data','backgroundcolor',[0.73 0.83 0.96]);


h.CalculateAllBtn=uicontrol('parent',panel3,'Style','pushbutton','Units','normalized','Position',[0.25 0.9 0.3 0.05],'String','Calculate all','backgroundcolor',[0.7 0.6 0.7],'Callback',{@CalculateAllBtn_Callback});
h.StatisticResult=uicontrol('parent',panel3,'Style','text','Units','normalized','Position',[0.15 0.7 0.5 0.2],'String','','fontsize',12, 'HorizontalAlignment', 'left', 'backgroundcolor',[0.73 0.83 0.96]);
h.StatisticAxes=axes('parent',panel3,'Units','normalized','Position',[0.1 0.1 0.8 0.4],'FontSize',10);
h.SaveAllBtn=uicontrol('parent',panel3,'Style','pushbutton','Units','normalized','Position',[0.7 0.03 0.2 0.05],'String','Save all','backgroundcolor',[0.7 0.6 0.8],'Callback',{@SaveAllBtn_Callback});
h.BoxAxes=axes('parent',panel3,'Units','normalized','Position',[0.2 0.53 0.45 0.1],'FontSize',10);
h.HistChc=uicontrol('parent',panel3,'Style','checkbox','Units','normalized','Position',[0.1 0.03 0.45 0.035],'String','Plot hist for mouse','backgroundcolor',[0.73 0.83 0.96],'Value',0);
h.rect=[];h.circle= [];h.circleSkin=[];
h.rectData=[];h.circleData= [];



%% ####################################CALLBACKS######################
%% Control
    function []=FileBtn_Callback(source,eventdata)
        h.step=0;
        [h.filename, h.pathname]=uigetfile('*.tif', 'Select .TIF file','MultiSelect','on');
        if ~iscell(h.filename)
            h.filename = {h.filename};
        end
        set(h.showfilename, 'String', h.filename(1,1));
        h.ImgNum=1;
        h.imgMatrix=im2double(imread(fullfile(h.pathname, cell2mat(h.filename(1,h.ImgNum)))));
        imshow(h.imgMatrix, [], 'parent',h.Image);
        h.Norm=zeros(size(h.filename,2),1);
        set(h.ShowImgNum1,'String',['Showing ',num2str(h.ImgNum),' out of ',num2str(size(h.filename,2))])
        set(h.ShowImgNum1,'Visible','on')
        h.Spots=cell(1,size(h.filename,2));
        set(h.MeanSpotInt1,'String','<Spot>=?');
        set(h.MeanBlackInt1,'String',strcat('<Black>=?'));
    end
    function []=NextImgBtn_Callback(source,eventdata)
        if h.ImgNum>=size(h.filename,2)
            return;
        end
        h.ImgNum=h.ImgNum+1;
        
        if iscell(h.filename)==1
            h.imgMatrix=im2double(imread(fullfile(h.pathname, cell2mat(h.filename(1,h.ImgNum)))));
            imshow(h.imgMatrix, [],'parent',h.Image);
            set(h.showfilename, 'String', h.filename(1,h.ImgNum));
        end
        set(h.ShowImgNum1,'String',['Showing ',num2str(h.ImgNum),' out of ',num2str(size(h.filename,2))])
        if isnan(h.Spots{1,h.ImgNum})==0
            set(h.MeanSpotInt1,'String',strcat('<Spot>=',num2str(mean(h.Spots{1,h.ImgNum}))));
        else
            set(h.MeanSpotInt1,'String','<Spot>=?');
            
        end
        if h.Norm(h.ImgNum)~=0
            set(h.MeanBlackInt1,'String',strcat('<Black>=',num2str(h.Norm(h.ImgNum))));
        else
            set(h.MeanBlackInt1,'String',strcat('<Black>=?'));
        end
    end
    function []=PrevImgBtn_Callback(source,eventdata)
        if h.ImgNum <= 1
            return;
        end
        
        h.ImgNum=h.ImgNum-1;
        
        if iscell(h.filename)==1 && h.ImgNum>0
            h.imgMatrix=im2double(imread(fullfile(h.pathname, cell2mat(h.filename(1,h.ImgNum)))));
            imshow(h.imgMatrix, [],'parent',h.Image);
            set(h.showfilename, 'String', h.filename(1,h.ImgNum));
        end
        set(h.ShowImgNum1,'String',['Showing ',num2str(h.ImgNum),' out of ',num2str(size(h.filename,2))])
        if isnan(h.Spots{1,h.ImgNum})==0
            set(h.MeanSpotInt1,'String',strcat('<Spot>=',num2str(mean(h.Spots{1,h.ImgNum}))));
        else
            set(h.MeanSpotInt1,'String','<Spot>=?');
            
        end
        if h.Norm(h.ImgNum)~=0
            set(h.MeanBlackInt1,'String',strcat('<Black>=',num2str(h.Norm(h.ImgNum))));
        else
            set(h.MeanBlackInt1,'String',strcat('<Black>=?'));
        end
        
    end
    function []=SelectSpotBtn_Callback(source,eventdata)
        try
            delete(h.circle);
            delete(h.circleSkin);
        catch
        end
        h.r=str2num(get(h.REdt,'String'));
        SkinKoef=str2num(get(h.SkinKoefEdt,'String'));
        
        axes(h.Image);
        ix=size(h.imgMatrix,1);iy=size(h.imgMatrix,2);
        [cx,cy]=ginput(1);
        h.cx=round(cx);h.cy=round(cy);
        hold(h.Image, 'on');
        h.circle= viscircles([cx,cy],h.r,'EdgeColor','r','LineWidth',0.2);
        hold(h.Image, 'off');
        [x,y]=meshgrid(-(h.cx-1):(iy-h.cx),-(h.cy-1):(ix-h.cy));
        c_mask=((x.^2+y.^2)<=h.r^2);
        h.mask=c_mask.*h.imgMatrix;
        [~,~,v]=find(h.mask);
        h.Spots(h.ImgNum)={v};
        
        %get the info on the skin color -> enlarge initial circle by 60% (?)
        c_maskSkin=((x.^2+y.^2)<=(h.r+SkinKoef*h.r)^2);
        h.maskSkin=(c_maskSkin.*h.imgMatrix)-h.mask;
        [~,~,v]=find(h.maskSkin);
        meanSkin=median(v);
        %imshow(h.maskSkin)
        hold(h.Image, 'on');
        h.circleSkin= viscircles([cx,cy],(h.r+SkinKoef*h.r),'EdgeColor','g','LineWidth',0.2);
        hold(h.Image, 'off');
        %normalize for skin
        h.Spots{1,h.ImgNum}=h.Spots{1,h.ImgNum}-meanSkin;
        set(h.MeanSpotInt1,'String',strcat('<Spot>=',num2str(mean(h.Spots{1,h.ImgNum}))));
    end
    function []=SelectPaperBtn_Callback(source,eventdata)
        try
            delete(h.rect);
        catch
        end
        axes(h.Image);
        h.rect = getrect(h.Image);
        h.x1=round(h.rect(1));
        h.y1=round(h.rect(2));
        h.x2=round(h.rect(1)+h.rect(3));
        h.y2=round(h.rect(4)+h.rect(2));
        hold(h.Image, 'on');
        h.rect=rectangle('Position', [h.x1 h.y1 h.rect(3) h.rect(4)],'parent', h.Image,'EdgeColor','b');
        hold(h.Image, 'off');
        h.Norm(h.ImgNum)=mean(mean(h.imgMatrix(h.y1:h.y2,h.x1:h.x2)));
        set(h.MeanBlackInt1,'String',strcat('<Black>=',num2str(h.Norm(h.ImgNum))) );
    end
%% Data
    function []=FileBtnData_Callback(source,eventdata)
        h.step=0;
        [h.filenamedata, h.pathnamedata]=uigetfile('*.tif', 'Select .TIF file','MultiSelect','on');
        if ~iscell(h.filenamedata)
            h.filenamedata = {h.filenamedata};
        end
        set(h.showfilenameData, 'String', h.filenamedata(1,1));
        h.ImgNumData=1;
        h.imgMatrixData=im2double(imread(fullfile(h.pathnamedata, cell2mat(h.filenamedata(1,h.ImgNumData)))));
        imshow(h.imgMatrixData, [],'parent',h.ImageData);
        h.NormData=zeros(size(h.filenamedata,2),1);
        set(h.ShowImgNum2,'String',['Showing ',num2str(h.ImgNumData),' out of ',num2str(size(h.filenamedata,2))])
        set(h.ShowImgNum2,'Visible','on')
        h.SpotsData=cell(1,size(h.filenamedata,2));
        set(h.MeanSpotInt2,'String','<Spot>=?');
        set(h.MeanBlackInt2,'String',strcat('<Black>=?'));
    end
    function []=NextImgBtnData_Callback(source,eventdata)
        if h.ImgNumData>=size(h.filenamedata,2)
            return;
        end
        
        h.ImgNumData=h.ImgNumData+1;
        
        if iscell(h.filenamedata)==1
            h.imgMatrixData=im2double(imread(fullfile(h.pathnamedata, cell2mat(h.filenamedata(1,h.ImgNumData)))));
            imshow(h.imgMatrixData, [],'parent',h.ImageData);
        end
        set(h.ShowImgNum2,'String',['Showing ',num2str(h.ImgNumData),' out of ',num2str(size(h.filenamedata,2))])
        if isnan(h.SpotsData{1,h.ImgNumData})==0
            set(h.MeanSpotInt2,'String',strcat('<Spot>=',num2str(mean(h.SpotsData{1,h.ImgNumData}))));
        else
            set(h.MeanSpotInt2,'String','<Spot>=?');
            
        end
        if h.NormData(h.ImgNumData)~=0
            set(h.MeanBlackInt2,'String',strcat('<Black>=',num2str(h.NormData(h.ImgNumData))));
        else
            set(h.MeanBlackInt2,'String',strcat('<Black>=?'));
        end
    end
    function []=PrevImgBtnData_Callback(source,eventdata)
        if h.ImgNumData <= 1
            return;
        end
        
        h.ImgNumData=h.ImgNumData-1;
        
        if iscell(h.filenamedata)==1 && h.ImgNumData>0
            h.imgMatrixData=im2double(imread(fullfile(h.pathnamedata, cell2mat(h.filenamedata(1,h.ImgNumData)))));
            imshow(h.imgMatrixData, [],'parent',h.ImageData);
        end
        
        set(h.ShowImgNum2,'String',['Showing ',num2str(h.ImgNumData),' out of ',num2str(size(h.filenamedata,2))])
        if isnan(h.SpotsData{1,h.ImgNumData})==0
            set(h.MeanSpotInt2,'String',strcat('<Spot>=',num2str(mean(h.SpotsData{1,h.ImgNumData}))));
        else
            set(h.MeanSpotInt2,'String','<Spot>=?');
            
        end
        if h.NormData(h.ImgNumData)~=0
            set(h.MeanBlackInt2,'String',strcat('<Black>=',num2str(h.NormData(h.ImgNumData))));
        else
            set(h.MeanBlackInt2,'String',strcat('<Black>=?'));
        end
        
    end
    function []=SelectSpotBtnData_Callback(source,eventdata)
        try
            delete(h.circleData);
            delete(h.circleSkinData);
        catch
        end
        h.rData=str2num(get(h.REdtData,'String'));
        SkinKoef=str2num(get(h.SkinKoefDataEdt,'String'));
        
        axes(h.ImageData);
        ix=size(h.imgMatrixData,1);iy=size(h.imgMatrixData,2);
        [cx,cy]=ginput(1);
        h.cxData=round(cx);h.cyData=round(cy);
        hold(h.ImageData, 'on');
        h.circleData= viscircles([cx,cy],h.rData,'EdgeColor','r','LineWidth',0.2);
        hold(h.ImageData, 'off');
        [x,y]=meshgrid(-(h.cxData-1):(iy-h.cxData),-(h.cyData-1):(ix-h.cyData));
        c_maskData=((x.^2+y.^2)<=h.rData^2);
        h.maskData=c_maskData.*h.imgMatrixData;
        [~,~,v]=find(h.maskData);
        h.SpotsData(h.ImgNumData)={v};
        
        %get the info on the skin color -> enlarge initial circle by 60% (?)
        c_maskSkin=((x.^2+y.^2)<=(h.rData+SkinKoef*h.rData)^2);
        h.maskSkinData=(c_maskSkin.*h.imgMatrixData)-h.maskData;
        [~,~,v]=find(h.maskSkinData);
        meanSkin=median(v);
        %imshow(h.maskSkin)
        hold(h.ImageData, 'on');
        h.circleSkinData= viscircles([cx,cy],(h.rData+SkinKoef*h.rData),'EdgeColor','g','LineWidth',0.2);
        hold(h.Image, 'off');
        %normalize for skin
        h.SpotsData{1,h.ImgNumData}=h.SpotsData{1,h.ImgNumData}-meanSkin;
        h.MeanSpotInt2.String=strcat('<Spot>=',num2str(mean(h.SpotsData{1,h.ImgNumData})));
    end
    function []=SelectPaperBtnData_Callback(source,eventdata)
        try
            delete(h.rectData);
        catch
        end
        axes(h.ImageData);
        rect = getrect(h.ImageData);
        x1=round(rect(1));
        y1=round(rect(2));
        x2=round(rect(1)+rect(3));
        y2=round(rect(4)+rect(2));
        hold(h.ImageData, 'on');
        h.rectData=rectangle('Position', [x1 y1 rect(3) rect(4)],'parent', h.ImageData,'EdgeColor','b');
        hold(h.ImageData, 'off');
        h.NormData(h.ImgNumData)=mean(mean(h.imgMatrixData(y1:y2,x1:x2)));
        set(h.MeanBlackInt2,'String',strcat('<Black>=',num2str(h.NormData(h.ImgNumData))) );
    end

%% calculate all
    function []=CalculateAllBtn_Callback(source,eventdata)        
        % Control
        h.ControldataOut = [];
        FmtSpec='%s mouse %d: %2.8f \n';
        if isfield(h, 'Norm')
            h.MeanNorm=mean(h.Norm);
            
            for i=1:size(h.filename,2)
                Controldata(i)={h.Spots{1,i}'/h.Norm(i)*h.MeanNorm};
               % if kstest(Controldata{i},normscdf(Controldata{i},mean(Controldata{i},std(Controldata{i}))))==0
                    h.ControldataPerMouse(i)=mean(Controldata{i}); %if data distributed normally
                    fprintf(FmtSpec,'Control',i,h.ControldataPerMouse(i))
               % end
            end
            h.ControldataOut=cell2mat(Controldata);
        end
        
        % Data
        h.dataOut = [];
        if isfield(h, 'NormData')
            h.MeanNormData=mean(h.NormData);
            for i=1:size(h.filenamedata,2)
                data(i)={h.SpotsData{1,i}'/h.NormData(i)*h.MeanNormData};
               % if kstest(data{i},normscdf(data{i},mean(data{i},std(data{i}))))==0
                    h.dataPerMouse(i)=mean(data{i}); %if data distributed normally
                    fprintf(FmtSpec,'Data',i,h.dataPerMouse(i))
               % end
            end
            h.dataOut=cell2mat(data);
        end
        %data per mouse
        SEMc = std(h.ControldataPerMouse)/sqrt(length(h.ControldataPerMouse));               % Standard Error
        tsc = tinv([0.025  0.975],length(h.ControldataPerMouse)-1);      % T-Score
        CIControldataPerMouse = mean(h.ControldataPerMouse) + tsc*SEMc;                      % Confidence Intervals
        SEMd = std(h.dataPerMouse)/sqrt(length(h.dataPerMouse));               % Standard Error
        tsd = tinv([0.025  0.975],length(h.dataPerMouse)-1);      % T-Score
        CIdataPerMouse = mean(h.dataPerMouse) + tsd*SEMd;                      % Confidence Intervals
        bar([mean(h.ControldataPerMouse) mean(h.dataPerMouse)],'FaceColor','w','parent',h.BoxAxes);title('Data grouped by mouse')
        hold(h.BoxAxes,'on')
        errorbar([mean(h.ControldataPerMouse) mean(h.dataPerMouse)],[tsc(1)*SEMc tsd(1)*SEMd],'parent',h.BoxAxes)
        %errorbar(mean(h.dataPerMouse),CIdataPerMouse(1),'parent',h.BoxAxes)
        hold(h.BoxAxes,'off')
        set(h.BoxAxes,'xticklabel',{'control','data'})
        %histogram of all datapoints
        axes(h.StatisticAxes); cla;
        hold on;
        HistChc=get(h.HistChc,'Value');
        if ~isempty(h.ControldataOut)
            if HistChc==0
            histogram(h.ControldataOut);
            else
            histogram(h.ControldataPerMouse);
            end
        end
        if ~isempty(h.dataOut)
            if HistChc==0
            histogram(h.dataOut); 
            else
            histogram(h.dataPerMouse);
            end
            title('Histogram of all data points')
        end
        hold off;
        
        if ~isempty(h.ControldataPerMouse) && ~isempty(h.dataPerMouse)
            [res, p, ci,stats] = ttest2(h.ControldataPerMouse, h.dataPerMouse, ...
                'alpha', 0.05, 'tail', 'both', 'VarType', 'unequal');
            outcome = {'false', 'true'};
            resText = {'Statistics', ' ', ['H0 rejected: ', outcome{res + 1}], ...
                ['p value = ', num2str(p)],...
                ['Confidence interval: ', '[', num2str(ci(1)), '; ', num2str(ci(2)), ']'],...
                ['t=',num2str(stats.tstat)]};
            set(h.StatisticResult, 'String', resText);
        end
        resText ={['Mean = ',num2str(mean(h.dataPerMouse))],[', SE = ',num2str(SEMd)],['Confidence interval= [',num2str(CIdataPerMouse(1)),';',num2str(CIdataPerMouse(2)),']']};
        h.ShowGroupData.String=resText;
        resText={['Mean = ',num2str(mean(h.ControldataPerMouse))],[', SE = ',num2str(SEMc)],['Confidence interval= [',num2str(CIControldataPerMouse(1)),';',num2str(CIControldataPerMouse(2)),']']};
        h.ShowGroupControl.String=resText;
        warning off
        legend(h.StatisticAxes,'Control','Data');
        warning on
        
    end
    function []=SaveAllBtn_Callback(source,eventdata)
        outDir = uigetdir(h.pathname, 'Pick a Directory');
        ControldataOut = h.ControldataOut(:);
        dataOut = h.dataOut(:);
        save(fullfile(outDir, 'Controls.mat'),'ControldataOut');
        save(fullfile(outDir, 'Data.mat'), 'dataOut');
        dlmwrite(fullfile(outDir, 'Controls.dat'),ControldataOut);
        dlmwrite(fullfile(outDir, 'Data.dat'),dataOut);
        
    end
end
