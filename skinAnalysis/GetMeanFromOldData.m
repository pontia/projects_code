function GetMeanFromOldData(nMiceC,nMiceD)
data=dlmread('Data.dat');
controls=dlmread('Controls.dat');
FmtSpec='%s mouse %d: %2.8f \n';
PxPerMouseC=length(data)/nMiceC;
PxPerMouseD=length(data)/nMiceD;
for i=1:nMiceC
MeanPerMouse=mean(controls(i:PxPerMouse));
controls(i:i:PxPerMouse)=[];
fprintf(FmtSpec,'Control',i,MeanPerMouse)
end
for i=1:nMiceD
MeanPerMouse=mean(data(i:PxPerMouse));
data(i:i:PxPerMouse)=[];
fprintf(FmtSpec,'Data',i,MeanPerMouse)
end

end
