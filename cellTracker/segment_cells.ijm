// You can override these parameters if the segmentation is not robust enough.
sigma = 7;                    // Approximately half the radius of a cell
segm_algorithm = "Li";        // Other good options are "Otsu" (more conservative), "Triangle" (more enthusiastic).
min_cell_size = 10;           // To remove small debris
max_cell_size = "Infinity";   // Set to a number to limit the max size
batch_mode = false;           // Either true or false; if set to true, no temporary images will be shown. 
                              // Fast but you won't be able to check the results! Recommended to set it to
                              // false and run it on 10 - 20 images to check the quality of the segmentation;
                              // then set it to true to process all images.

// Ask the user to pck the folder to process
dir = getDirectory("Select the source directory");

// Generate a (sorted) list of files in the selected directory
list = getFileList(dir)
Array.sort(list)

// Clear the results table before starting
run("Clear Results");

// To speed up the analysis, we turn on batch bode
setBatchMode(batch_mode);

// Process all files
for (i = 0; i < list.length; i++) {

	// Full name of current file
	filename = dir + list[i];

	// Open the image
	open(filename);

	// Keep track of the name of this image
	curr_image = getTitle();

	// Duplicate the image
	run("Duplicate...", "title=Temp");

	// Perform the segmentation
	run("Gaussian Blur...", "sigma=" + sigma);
	run("32-bit");
	run("Convolve...", "text1=[-1 -1 -1\n-1 8 -1\n-1 -1 -1\n]");
	run("8-bit");
	setAutoThreshold("" + segm_algorithm + " dark");
	setOption("BlackBackground", true);
	run("Convert to Mask");
	run("Watershed");

	// Measure
	run("Set Measurements...", "mean display redirect=[" + curr_image + "] decimal=3");
	run("Analyze Particles...", "size=" + min_cell_size + "-" + max_cell_size + "circularity=0.50-1.00 show=[Overlay Masks] display exclude");

	// Now close the both images and move on to the next file
	close(curr_image);
	close("Temp");
}

// Finally, we disable the batch mode
setBatchMode(false);
