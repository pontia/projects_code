function M=mmad(varargin)
% Median absolute deviation
%
% A widely used estimator for the scale of a sample is the standard 
% deviation, which, however, is not robust. By definition, the median 
% absolute deviation is a robust estimation of the 0.75 quantile of the 
% standard normal distribution. A robust estimate of the scale can be 
% therefore calculated as a function of the median absolute deviation, 
% like this: s=mmad/0.6745.
%
% REMARK
% ------
% The mad returned by this function is s=median|xi-median(xi)|/0.6745, 
% which is the robust alternative to the standard deviation. To obtain the
% estimator of the 0.75 quantile, which is defined as median|xi-median(xi)|, 
% multiply the value returned by this function by the factor 0.6745.
%
% SYNOPSIS      s=mmad(A,mask)
%
% INPUT  A    : (1-3)D array
%        mask : (optional): none, or [y x], or '3d'
%               If no mask is passed, the array is considered to be 1D
%               This can be used to calculate the global mad for an N-D
%               array. If a mask is passed, the mad per mask is calculated
%               and an array with the same size as the input is returned.
%               REMARK: in 3D, mmad uses 26 neighbors (this cannot be
%                       changed, if another mask [y x z] is passed, mmad 
%                       will still calculate the 3D mad but with 26 
%                       neighbors).
% OUTPUT S    : mad of the input array A. It is either a scalar or an N-D
%               array, depending on the input mask.
%
% Aaron Ponti, 2005/06/27


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Check input parameter
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==1
    mask=1;
end

if nargin==2

    % Get mask
    mask=varargin{2};
    
    % Check wheteher the user passed a string -> 3D
    if ischar(mask)
        mask=[1 1 1];
    end

end

% Get input array
A=varargin{1};

% Array dimensions
nArrayDim=numel(size(A));

% Mask dimensions
nMaskDim=numel(mask);

% Check that dimensions match
if nMaskDim>1 && nArrayDim~=nMaskDim
    error(['The number of dimension of the input array A', ...
        ' should match the number of dimentions of the mask.']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 1D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==1

    % Absolute deviatons from the median
    D=abs(A-median(A(:)));

    % Median of the absolute deviations
    M=1.4826*median(D(:));

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 2D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==2
    
    % Absolute deviatons from the median
    d=medfilt2(A,mask);
    D=abs(A-d);

    % Median of the absolute deviations
    M=1.4826*medfilt2(D,mask);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the 3D mad
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)==3

    % Absolute deviatons from the median
    d=ordfilt3D(A,14);
    D=abs(A-d);

    % Median of the absolute deviations
    M=1.4826*ordfilt3D(D,14);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% More than 3D is not supported
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(mask)>3
    error('More than 3D is not supported.');
end
