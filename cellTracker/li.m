function t = Li(img)
% Reimplementation of the ImageJ's Li segmentation algorithm
% From ij.process.AutoThresholder
%
% Implements Li's Minimum Cross Entropy thresholding method
% This implementation is based on the iterative version (Ref. 2) of the algorithm.
% 1) Li C.H. and Lee C.K. (1993) "Minimum Cross Entropy Thresholding"
%    Pattern Recognition, 26(4): 617-625
% 2) Li C.H. and Tam P.K.S. (1998) "An Iterative Algorithm for Minimum
%    Cross Entropy Thresholding"Pattern Recognition Letters, 18(8): 771-776
% 3) Sezgin M. and Sankur B. (2004) "Survey over Image Thresholding
%    Techniques and Quantitative Performance Evaluation" Journal of
%    Electronic Imaging, 13(1): 146-165
%    http://citeseer.ist.psu.edu/sezgin04survey.html
% Ported to ImageJ plugin by G.Landini from E Celebi's fourier_0.8 routines

if ~isa(img, 'uint8')
    error('Currently, only uint8 images are supported.');
end

% Calculate histogram
data = hist(double(img(:)), 0:255);

% Mean gray level
meanL = mean(double(img(:)));

% Tolerance
tolerance=0.5;

% Initial estimate
new_thresh = meanL;
old_thresh = new_thresh + 2 * tolerance;

while abs(new_thresh - old_thresh) > tolerance
    
    old_thresh = new_thresh;
    threshold = round(old_thresh);	% range
    % Calculate the means of background and object pixels
	
    % Background
    sum_back = sum((0 : threshold) .* data(1 : threshold + 1));
    num_back = sum(data(1: threshold + 1));
    if num_back == 0
        mean_back = 0;
    else
        mean_back = (sum_back / double(num_back));
    end
    
    % Foreground
    sum_obj = sum((threshold + 1 : 255) .* data(threshold + 2 : end));
    num_obj = sum(data(threshold + 2: end));
    if num_obj == 0
        mean_obj = 0;
    else
        mean_obj = (sum_obj / double(num_obj));
    end

    % Calculate the new threshold: Equation (7) in Ref. 2
    temp = (mean_back - mean_obj) / (log(mean_back) - log(mean_obj));

	if temp < -eps(double(1))
        new_thresh = floor(temp - 0.5);
    else
        new_thresh = floor(temp + 0.5);
    end
end
t = new_thresh + 1;
