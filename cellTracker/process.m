function [allData, allBW] = process(dirName, fNames, sigma, thresh, maxDispl, maxGap, minTrackLength, analysis)

% Initialize output arguments
allData = {};
allBW = {};

% Initialize waitbar
hWaitbar = waitbar(0, 'Segmenting cells...');

% Perform segmentation 
for i = 1 : numel(fNames)
    
    % Try opening the file
    try
        img = imread(fullfile(dirName, fNames{i}));
    catch
        disp(['Skipping file ', fNames{i}, '...']);
        continue;
    end
       
    % Segment the cells
    [data, BW] = segmentCells(img, sigma, thresh);
    
    % Store the results
    if numel(data) > 0
        allData = cat(2, allData, data);
    else
        allData = cat(2, allData, -1);
    end
    allBW = cat(2, allBW, BW);
    
    % Update waitbar
    waitbar(i/numel(fNames), hWaitbar, ...
        ['Segmenting cells (', num2str(i), ' of ', ...
        num2str(numel(fNames) - 1), ')']);
    
end

% Close waitbar
close(hWaitbar);

% Initialize waitbar
hWaitbar = waitbar(0, 'Tracking cells...');

maxTrackIndex = 0;

foundFirstValidImage = false;
lastValidSource = 0;
% Perform assignment
for i = 1 :  numel(fNames) - 1

    if ~isstruct(allData{i})  
        if ~foundFirstValidImage
            continue;
        else
            % Extract the positions
            pos1 = reshape([allData{lastValidSource}.Centroid], 2, ...
                numel(allData{lastValidSource}))';
            
            % Inform
            fprintf(1, ['Images %d - %d: closing gap. ', ...
                'New tracks started.\n'], i, i + 1);

        end
    else
        
        if ~foundFirstValidImage

            % Found first valid image
            foundFirstValidImage = true;

            % Initialize tracks
            for j = 1 : numel(allData{i})
                allData{i}(j).TrackIndex = j;
                maxTrackIndex = j;
            end

        end
        
        % Extract the positions
        pos1 = reshape([allData{i}.Centroid], 2, numel(allData{i}))';

        % Mark this as last valid source
        lastValidSource = i;

    end
        
    if ~isstruct(allData{i + 1})
        continue;
    end
    
    % Extract the positions
    pos2 = reshape([allData{i + 1}.Centroid], 2, numel(allData{i + 1}))';
    
    % Solve the linear assignment problem
    if i - lastValidSource <= maxGap
        D = pdist2(pos2, pos1);
        D(D > maxDispl) = -1;
        try
            L = lap(D, -1, 0, 1);
            L = L(1 :  size(pos2, 1));
            assignment = find(L <= size(pos1, 1));
            new_tracks = find(L > size(pos1, 1));
        catch
            fprintf(1, ['Images %d - %d: no matches found. ', ...
                'New tracks started.\n'], i, i + 1);
            assignment = [];
            new_tracks = (1 : size(pos2, 1))';
        end

        % Assign the cells to the corresponding track
        for j = 1 : numel(assignment)
            target = L(assignment(j));
            trackIndx = allData{lastValidSource}(target).TrackIndex;
            allData{i + 1}(assignment(j)).TrackIndex = trackIndx;
        end
        
        % Start the new tracks
        for j = 1 : numel(new_tracks)
            maxTrackIndex = maxTrackIndex + 1;
            trackIndx = maxTrackIndex;
            allData{i + 1}(new_tracks(j)).TrackIndex = trackIndx;
        end
    else
        % Initialize new set of tracks
        for j = 1 : numel(allData{i + 1})
            maxTrackIndex = maxTrackIndex + 1;
            allData{i + 1}(j).TrackIndex = maxTrackIndex;
        end
        lastValidSource = i + 1;
    end

    % Update waitbar
    waitbar(i / (numel(fNames) - 1), hWaitbar, ...
        ['Tracking cells (', num2str(i), ' of ', ...
        num2str(numel(fNames) - 1), ')']);

end

% Close the waitbar
close(hWaitbar);

% Filter tracks by min track length
trackLengths = zeros(1, maxTrackIndex);
for i = 1 : numel(allData)
    trackLengths([allData{i}.TrackIndex]) = ... 
        trackLengths([allData{i}.TrackIndex]) + 1;
end

% Find the tracks that are too short
indices = find(trackLengths < minTrackLength);

% Remove the short tracks
newMinTrackIndex = maxTrackIndex + 1;
for i = 1 : numel(allData)
   
    % Tracks to remove
    [toBeDel, iA] = intersect([allData{i}.TrackIndex], indices);
    if isempty(toBeDel)
        continue;
    else
        allData{i}(iA) = [];
    end
end

% Renumber
filteredTrackIndices = zeros(1, sum(cellfun(@numel, allData)));
n = 1;
for i = 1 : numel(allData)
    filteredTrackIndices(n : n + numel([allData{i}.TrackIndex]) - 1) = ...
        [allData{i}.TrackIndex];
    n = n + numel([allData{i}.TrackIndex]);
end
tracksToRenumber = unique(filteredTrackIndices);
newTrackIndices = ... 
    newMinTrackIndex : newMinTrackIndex + numel(tracksToRenumber) - 1;
finalTrackIndices = 1 : numel(newTrackIndices);
tracksMap = cat(2, tracksToRenumber(:), newTrackIndices(:), ...
    finalTrackIndices(:));
for i = 1 : numel(allData)
    for j = 1 : numel(allData{i})
        indx = find(tracksMap(:, 1) == allData{i}(j).TrackIndex);
        allData{i}(j).TrackIndex = tracksMap(indx, 2);
    end
end
for i = 1 : numel(allData)
    for j = 1 : numel(allData{i})
        indx = find(tracksMap(:, 2) == allData{i}(j).TrackIndex);
        allData{i}(j).TrackIndex = tracksMap(indx, 3);
    end
end

% Export the results
exportResults(allData, dirName, analysis, maxTrackIndex);

