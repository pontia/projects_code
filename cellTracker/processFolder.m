function [allData, allBW] = processFolder(dirName, sigma)

% Get directory content
d = dir(dirName);

% Initialize output arguments
allData = {};
allBW = {};

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

% Remove folders
d([d.isdir] == true) = [];

for i = 1 : numel(d)
    
    % Skip dolfers
    if d(i).isdir
        continue;
    end
   
    % Try opening the file
    try
        img = imread(fullfile(dirName, d(i).name));
    catch
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end
       
    % Segment the cells
    [data, BW] = segmentCells(img, sigma);
    
    % Store the results
    if numel(data) > 0
        allData = cat(2, allData, data);
    else
        allData = cat(2, allData, [-1]);
    end
    allBW = cat(2, allBW, BW);
    
    % Update waitbar
    waitbar(i/numel(d), hWaitbar);
    
end

% Close waitbar
close(hWaitbar);
