function varargout = CellTracker(varargin)
% CELLTRACKER MATLAB code for CellTracker.fig
%      CELLTRACKER, by itself, creates a new CELLTRACKER or raises the existing
%      singleton*.
%
%      H = CELLTRACKER returns the handle to a new CELLTRACKER or the handle to
%      the existing singleton*.
%
%      CELLTRACKER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLTRACKER.M with the given input arguments.
%
%      CELLTRACKER('Property','Value',...) creates a new CELLTRACKER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellTracker_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellTracker_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellTracker

% Last Modified by GUIDE v2.5 13-Nov-2017 11:34:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CellTracker_OpeningFcn, ...
    'gui_OutputFcn',  @CellTracker_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellTracker is made visible.
function CellTracker_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellTracker (see VARARGIN)

% Choose default command line output for CellTracker
handles.output = hObject;

if ismac
    hFields = fields(handles);
    for i = 1 : numel(hFields)
        try
            handles.(hFields{i}).FontSize = 11;
        catch
            % Pass
        end
    end
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CellTracker wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CellTracker_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in lbImages.
function lbImages_Callback(hObject, eventdata, handles)
% hObject    handle to lbImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbImages contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbImages
showImageAndResults(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lbImages_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbSelect.
function pbSelect_Callback(hObject, eventdata, handles)
% hObject    handle to pbSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fNames, dirName] = uigetfile( ...
    {'*.tif','TIF files (*.tif)'; ...
    '*.tiff','TIFF files (*.tiff)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Please select all images to be analyzed', ...
    'MultiSelect', 'on');

if isequal(fNames, 0)
    return;
end

if ~isa(fNames, 'cell')
    fNames = {fNames};
end

if length(fNames) < 2
    uiwait(errordlg('You need to pick at least two images!'), 'modal');
    return;
end

% Sort
fNames = sort(fNames);

% Fill the list box
handles.lbImages.String = fNames;

% Activate the file list box, the test segmentation button and the 
% analyse push button
handles.lbImages.Enable = 'on';
handles.pbAnalyze.Enable = 'on';
handles.pbTestSegment.Enable = 'on';

% Store the selection
handles.project.dirName = dirName;
handles.project.fNames = fNames;
guidata(hObject, handles);

% Display the first image
showImageAndResults(hObject, handles);

function showImageAndResults(hObject, handles)

index = handles.lbImages.Value;

dirName = handles.project.dirName;
fName = handles.project.fNames{index};

if ~isfield(handles.project, 'figHandleImage') || ...
        ~ishandle(handles.project.figHandleImage)
    handles.project.figHandleImage = figure('NumberTitle', 'off');
end

% Show image
img = imread(fullfile(dirName, fName));

% Is the image RGB?
if size(img, 3) == 3
    img = rgb2gray(img);
end
 
img = imadjust(img, stretchlim(img), []);
figure(handles.project.figHandleImage);
image = imhandles(handles.project.figHandleImage);
if isempty(image)
    imshow(img);
    handles.project.figHandleImage.NextPlot = 'add';
else
    image.CData = img;
end
handles.project.figHandleImage.Name = fName;

% Show bounding boxes and track indices
if isfield(handles.project, 'allData')
    if isstruct(handles.project.allData{index})
        
        if ~isfield(handles, 'temp')
            handles.temp = struct('lines', [], 'text', []);
        end
            
        if isfield(handles.temp, 'lines')
            delete(handles.temp.lines);
            handles.temp.lines = [];
        end

        if isfield(handles.temp, 'text')
            delete(handles.temp.text);
            handles.temp.text = [];
        end
        
        data = handles.project.allData{index};
        figure(handles.project.figHandleImage);
        handles.project.figHandleImage.NextPlot = 'add';
        hold on;
        handles.temp.lines = [];
        for i = 1 : numel(data)
            x0 = data(i).BoundingBox(1);
            y0 = data(i).BoundingBox(2);
            x = x0 + data(i).BoundingBox(3);
            y = y0 + data(i).BoundingBox(4);
            handles.temp.lines = cat(2, handles.temp.lines, ...
                plot( [ x0 x x x0 x0], [y y y0 y0 y], 'r-'));
            
            if isfield(data(i), 'TrackIndex')
                handles.temp.text = cat(2, handles.temp.text, ...
                    text(x + 3, y0 - 3, num2str(data(i).TrackIndex), ...
                    'Color', 'w', 'BackgroundColor', 'k', ...
                    'FontSize', 9));
            end
        end
    else
        % Delete current plots
        if isfield(handles.temp, 'lines')
            delete(handles.temp.lines);
            handles.temp.lines = [];
        end
        if isfield(handles.temp, 'text')
            delete(handles.temp.text);
            handles.temp.text = [];
        end
    end
end

% Store the selection
guidata(hObject, handles);


% --- Executes on button press in pbAnalyze.
function pbAnalyze_Callback(hObject, eventdata, handles)
% hObject    handle to pbAnalyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the value of the sigma parameter
diameter = str2double(handles.edCellDiameter.String);
sigma = fix(diameter / 2);
if mod(sigma, 2) == 0
    sigma = sigma + 1;
end

% Get the statistical threshold
thresh = str2double(handles.edStatThresh.String);

% Get the value for the max displacement
maxDispl = str2double(handles.edMaxDispl.String);

% Get the max gap
maxGap = str2double(handles.edMaxGap.String);

% Get the min track length
minTrackLength = str2double(handles.edMinTrackLength.String);

% Get the analysis report
if handles.rbMeanIntensity.Value == 1
    analysis = 'mean';
else
    analysis = 'max';
end
[allData, allBW] = process(handles.project.dirName, ...
    handles.project.fNames, ...
    sigma, ...
    thresh, ...
    maxDispl, ...
    maxGap, ...
    minTrackLength, ...
    analysis);
handles.project.allData = allData;
handles.project.allBW = allBW;

% Store the selection
guidata(hObject, handles);

% Display the first image
showImageAndResults(hObject, handles);


function edCellDiameter_Callback(hObject, eventdata, handles)
% hObject    handle to edCellDiameter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edCellDiameter as text
%        str2double(get(hObject,'String')) returns contents of edCellDiameter as a double
value = str2double(handles.edCellDiameter.String);
if isnan(value) || value <= 0
    uiwait(errordlg('Please set a positive value.', 'modal'));
    handles.edCellDiameter.String = '25';
    return;
end

% --- Executes during object creation, after setting all properties.
function edCellDiameter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edCellDiameter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edMaxDispl_Callback(hObject, eventdata, handles)
% hObject    handle to edMaxDispl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edMaxDispl as text
%        str2double(get(hObject,'String')) returns contents of edMaxDispl as a double
value = str2double(handles.edMaxDispl.String);
if isnan(value) || value <= 0
    uiwait(errordlg('Please set a positive value.', 'modal'));
    handles.edMaxDispl.String = '10';
    return;
end

% --- Executes during object creation, after setting all properties.
function edMaxDispl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edMaxDispl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edMaxGap_Callback(hObject, eventdata, handles)
% hObject    handle to edMaxGap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edMaxGap as text
%        str2double(get(hObject,'String')) returns contents of edMaxGap as a double
value = str2double(handles.edMaxGap.String);
if isnan(value) || value < 0
    uiwait(errordlg('Please set a value >= 0.', 'modal'));
    handles.edMaxGap.String = '1';
    return;
end

% --- Executes during object creation, after setting all properties.
function edMaxGap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edMaxGap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edStatThresh_Callback(hObject, eventdata, handles)
% hObject    handle to edStatThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edStatThresh as text
%        str2double(get(hObject,'String')) returns contents of edStatThresh as a double
value = str2double(handles.edStatThresh.String);
if isnan(value) || value < 0
    uiwait(errordlg('Please set a value >= 0.', 'modal'));
    handles.edStatThresh.String = '3';
    return;
end

% --- Executes during object creation, after setting all properties.
function edStatThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edStatThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pbTestSegment.
function pbTestSegment_Callback(hObject, eventdata, handles)
% hObject    handle to pbTestSegment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Initialize output arguments

index = handles.lbImages.Value;
dirName = handles.project.dirName;
fNames = handles.project.fNames;

try
    img = imread(fullfile(dirName, fNames{index}));
catch
    uiwait(errordlg(['Could not open file ', fNames{index}, '...'], ...
        'modal'));
end
       
% Get the value of the sigma parameter
diameter = str2double(handles.edCellDiameter.String);
sigma = fix(diameter / 2);
if mod(sigma, 2) == 0
    sigma = sigma + 1;
end

% Get the statistical threshold
thresh = str2double(handles.edStatThresh.String);

% Segment the cells
[data, BW] = segmentCells(img, sigma, thresh);

% Store the result for display
allData = cell(1, numel(fNames));
allBW = cell(1, numel(fNames));
allData{index} = data;
allBW{index} = BW;
handles.project.allData = allData;
handles.project.allBW = allBW;

% Show result
showImageAndResults(hObject, handles);

% --- Executes on button press in rbMeanIntensity.
function rbMeanIntensity_Callback(hObject, eventdata, handles)
% hObject    handle to rbMeanIntensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbMeanIntensity
handles.rbMeanIntensity.Value = 1;
handles.rbTotIntensity.Value = 0;

% --- Executes on button press in rbTotIntensity.
function rbTotIntensity_Callback(hObject, eventdata, handles)
% hObject    handle to rbTotIntensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbTotIntensity
handles.rbMeanIntensity.Value = 0;
handles.rbTotIntensity.Value = 1;

% --- Executes during object creation, after setting all properties.
function edMinTrackLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edMinTrackLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edMinTrackLength_Callback(hObject, eventdata, handles)
% hObject    handle to edMinTrackLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edMaxDispl as text
%        str2double(get(hObject,'String')) returns contents of edMaxDispl as a double
value = str2double(handles.edMinTrackLength.String);
if isnan(value) || value <= 0
    uiwait(errordlg('Please set a positive value.', 'modal'));
    handles.edMinTrackLength.String = '3';
    return;
end
