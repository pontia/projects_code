function t = triangle(img)
% Reimplementation of the ImageJ's Triangle segmentation algorithm
% From ij.process.AutoThresholder
% 
% Zack, G. W., Rogers, W. E. and Latt, S. A., 1977,
% Automatic Measurement of Sister Chromatid Exchange Frequency,
% Journal of Histochemistry and Cytochemistry 25 (7), pp. 741-753
%
%  modified from Johannes Schindelin plugin
% 
% find min and max

if ~isa(img, 'uint8')
    error('Currently, only uint8 images are supported.');
end

% Calculate histogram
data = hist(double(img(:)), 0:255);

% Find the bins where data is > 0
indices = find(data > 0);
min = indices(1);
min2 = indices(end);

if min > 1
    min = min - 1; % line to the (p==0) point, not to data[min]
end

% The Triangle algorithm cannot tell whether the data is skewed to one side or another.
% This causes a problem as there are 2 possible thresholds between the max and the 2 extremes
% of the histogram.
% Here I propose to find out to which side of the max point the data is furthest, and use that as
% the other extreme.
dmax = max(data);
imax = find(data == dmax, 1);

inverted = false;
if (imax - min) < (min2 - imax)
    % Reverse the histogram
    inverted = true;
    data = data(end : -1 : 1);
    min = 255 - min2 + 1;
    if min < 1
        min = 1;
    end
    imax = 255 - imax + 2;
end

if min == imax
    disp("Triangle:  min == max.");
    t = min;
    return;
end

% Describe line by nx * x + ny * y - d = 0
% nx is just the max frequency as the other point has freq=0
nx = data(imax);
ny = min - imax;
d = sqrt(nx * nx + ny * ny);
nx = nx / d;
ny = ny / d;
d = nx * (min - 1) + ny * data(min);

% Find split point
split = min;
splitDistance = 0;

for i = min + 1 : imax
    newDistance = nx * (i - 1) + ny * data(i) - d;
    if newDistance > splitDistance
        split = i;
        splitDistance = newDistance;
    end
end

if (inverted)
    t = (255 - split + 1);
else
    t = split - 1;
end

