function [data, BW] = segmentCells(img, sigma, thresh, showQC)
if nargin == 3
    showQC = false;
end

% Is the image RGB?
if size(img, 3) == 3
    img = rgb2gray(img);
end

% Keep a copy of the original image
origImg = img;

% Cast the image to single
img = single(img);

% Run an horizontal median filter to reduce the vertical line pattern
img = medfilt2(img, [1 5], 'symmetric');

% Laplacian of Gaussian filter
sz = round([5 * sigma, 5 * sigma]);
if mod(size(sz, 1), 2) == 0
    sz = sz + [1 1];
end
H = -1 .* fspecial('log', sz, sigma); 
I = imfilter(img, H, 'symmetric');

% Find local maxima
sz2 = round([sigma, sigma, 3]);
if mod(size(sz2, 1), 2) == 0
    sz2 = sz2 + [1 1 0];
end
S = zeros([size(I) 3], class(I));
S(:, :, 2) = I;
pos = locmax3d(S, sz2);

% Extract the intensities
locMaxInd = sub2ind(size(I), pos(:, 1), pos(:, 2));
intensities = I(locMaxInd);

% Calculate a threshold
m = median(intensities);
d = mmad(intensities);
t = m + thresh * d;

% Create a black-and-white mask
BW = imbinarize(I, t);

% Remove small objects
BW = bwareaopen(BW, sigma * 3);

% Perform constrained watershed
BW = constrainedWatershed(BW);

% Calculate statistics
cc = bwconncomp(BW, 4);
data = regionprops(cc, origImg, {'Area', 'Centroid', ...
    'MeanIntensity', 'MaxIntensity', 'BoundingBox'});

if showQC == true
    figure;
    subplot(1, 2, 1);
    imshow(imadjust(origImg), []);
    subplot(1, 2, 2);
    imshow(BW, []);
    hold on;
    plot(pos(:, 2), pos(:, 1), 'yo');
    kept_pos = pos(intensities > t, :);
    plot(kept_pos(:, 2), kept_pos(:, 1), 'r*');
end

% =========================================================================

function BW3 = constrainedWatershed(BW)

D = -bwdist(~BW);
Ld = watershed(D);
BW2 = BW;
BW2(Ld == 0) = 0;
mask = imextendedmin(D, 1);
D2 = imimposemin(D, mask);
Ld2 = watershed(D2);
BW3 = BW;
BW3(Ld2 == 0) = 0;

