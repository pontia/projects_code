function exportResults(allData, dirName, analysis, maxTrackIndex)

if nargin == 3
    
    maxTrackIndex = 0;
    for i = 1 : numel(allData)
        
        mx = max([allData{i}.TrackIndex]);
        if mx > maxTrackIndex
            maxTrackIndex = mx;
        end
    end
    
end

nTimepoints = numel(allData);

% Build a result matrix
M = nan(nTimepoints, maxTrackIndex + 1);

% Add the timepoints
M(:, 1) = 1 : nTimepoints;

% Now add the data
for i = 1 : nTimepoints
   
    curr = allData{i};
    if isequal(curr, -1)
        continue;
    end
    
    for j = 1 : numel(curr)
        
        switch analysis
            case 'mean'
                M(i, 1 + curr(j).TrackIndex) = curr(j).MeanIntensity;
            case 'max'
                M(i, 1 + curr(j).TrackIndex) = curr(j).MaxIntensity;
            otherwise
                error('Bad value for ''analysis''.');
        end

    end
    
end

% Save the file
csvFile = fullfile(dirName, ['report_', analysis ,'_intensity.csv']); 
dlmwrite(csvFile, M, 'delimiter', ',');

% Inform
helpdlg(['Results exported to ', csvFile, '.']);
