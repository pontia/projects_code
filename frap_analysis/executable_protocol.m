% Actin
%  
% Excel: 08.xlsx
%  
% VarName8:  CytosolBleached
% VarName9:  CortexBleached
% VarName10: Cytosol
% VarName11: Background
 
CytosolBleached = columnH;
CortexBleached = columnI;
Cytosol = columnJ;
Background = columnK;
 
% Fit background
backgroundLine = fitBackgroundLine(Times, Background);
 
% Fit bleaching curve
[fitresult, gof] = createFit(Times, Cytosol - backgroundLine);
 
% fitresult = 
%      General model:
%      fitresult(x) = a*exp(-b*x)+c
%      Coefficients (with 95% confidence bounds):
%        a =       95.22  (58.07, 132.4)
%        b =    0.009199  (9.27e-05, 0.01831)
%        c =       366.4  (321.9, 410.9)
 
% Correct Cytosol for bleaching
CytosolBleached_Corrected = correctForBleaching(Times, Cytosol - backgroundLine, CytosolBleached - backgroundLine, fitresult.a, fitresult.b, fitresult.c);
 
% Correct CortexBleached for bleaching
CortexBleached_Corrected = correctForBleaching(Times, Cytosol - backgroundLine, CortexBleached - backgroundLine, fitresult.a, fitresult.b, fitresult.c);
 
% Make curves relative to their minimum value
tBleach = find(CortexBleached_Corrected == min(CortexBleached_Corrected));
CortexBleached_Corrected  = CortexBleached_Corrected - CortexBleached_Corrected(tBleach);
%CytosolBleached_Corrected = CytosolBleached_Corrected - CytosolBleached_Corrected(tBleach);
 
% Remove pre-bleaching phase
CortexBleached_Corrected  = [mean(CortexBleached_Corrected(1 : tBleach - 1)); CortexBleached_Corrected(tBleach : end)];
%CytosolBleached_Corrected  = [mean(CytosolBleached_Corrected(1 : tBleach - 1)); CytosolBleached_Corrected(tBleach : end)];
 
% Normalize curves into 0..1 range
CortexBleached_Corrected  = CortexBleached_Corrected ./ CortexBleached_Corrected(1);
%CytosolBleached_Corrected = CytosolBleached_Corrected ./ CortexBleached_Corrected_NoDiff(1); 
 
% Correct the time vector
Times_Corrected = Times - Times(tBleach);
Times_Corrected(1 : tBleach - 2) = [];
 
% Plot the final curve
figure; plot(Times_Corrected, CortexBleached_Corrected);
 
% Subtract diffusion component (Cytosol) from CortexBleached
%CortexBleached_Corrected_NoDiff = CortexBleached_Corrected - CytosolBleached_Corrected;
 
% Fit FRAP curve
 
 

