function Data_Corrected = correctForBleaching(Times, Model_subtracted, Data_subtracted, a, b, c)

% Apply the fited model
Model_predicted = a.*exp(-b.*Times)+c

% Calculate correction factors
corr_factors = Model_predicted ./ Model_predicted(1);

% Apply correction factors
Data_Corrected = Data_subtracted ./ corr_factors;

% Quality check
 figure; 
 subplot(1, 2, 1);
 plot(Times, Model_subtracted, 'k-');
 hold on; 
 plot(Times, Model_predicted, 'r-');
 legend('Model', 'Prediction', 'Location', 'Best');
 subplot(1, 2, 2);
 plot(Times, Data_subtracted, 'k-');
 hold on;
 plot(Times, Data_Corrected, 'r-');
 legend('Data', 'Corrected', 'Location', 'Best');
 
