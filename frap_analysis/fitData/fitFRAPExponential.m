function [fitresult, gof] = fitFRAPExponential(Times, CortexBleached_Corrected_NoDiff, timePointOfBleaching)

% We remove the time points before bleaching and to set time
% starting at 0.
Times = Times(timePointOfBleaching + 1 : end);
Times = Times - Times(1);
CortexBleached_Corrected_NoDiff = CortexBleached_Corrected_NoDiff(timePointOfBleaching + 1 : end);


[xData, yData] = prepareCurveData( Times, CortexBleached_Corrected_NoDiff );

% Set up fittype and options.
ft = fittype( 'A * (1 - exp(-w*x))', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( ft );
opts.Display = 'Off';
opts.Lower = [0 0];
opts.StartPoint = [555 0.2];
opts.Upper = [Inf Inf];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'frapCortexBleached_Corrected_NoDiff vs. frapTimes', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel( 'frapTimes' );
ylabel( 'frapCortexBleached_Corrected_NoDiff' );
grid on