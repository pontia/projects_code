function BackgroundLine = fitBackgroundLine(Times, Background)

X = [ones(numel(Background), 1) Times(:)];

theta = pinv(X'*X)*X'*Background;

BackgroundLine = X * theta;

figure;
plot(Times, Background, 'k-');
hold on;
plot(Times, BackgroundLine, 'r-');
