function extractRimBatch(inDirName, outDirName, channelsForMask, channelsForStats, diameter)
% Extracts statistics of the rim of a cyst.
%
% SYNOPSIS
%
%   extractRimBatch(inDirName, outDirName, channelsForMask, channel, diameter) 
%
% INPUT
%
%   inDirName      : full path of the folder with the images to analyze.
%                    Set to [] to pick in a dialog.             
%   outDirName     : full path of the folder where to store the results
%                    Set to [] to pick in a dialog.
%   channelsForMask: channels to be used to create the mask, e.g. [2 4]
%   channel        : channl for which to calculate the statistics, e.g. 3
%   width          : width of the rim of the cyst (in pixels!)
%
% OUTPUT
%
%   None. All results are stored to disk.
%
% Copyright, Aaron Ponti, 2015

if nargin ~= 5
    error('5 input parameters expected.');
end

if isempty(inDirName)
   inDirName = uigetdir(pwd(), 'Choose directory containing files to analyze.'); 
end
if isequal(inDirName, 0)
    return
end

if isempty(outDirName)
   outDirName = uigetdir(pwd(), 'Choose directory where to store the results.'); 
end
if isequal(outDirName, 0)
    return
end

% Get the content of the input folder
d = dir(inDirName);
nDirEntries = numel(d);

% Create the structuring elements
se_rim = strel('disk', fix(diameter), 0);
se_clean = strel('disk', 3, 0);

% Make sure the output folders exist
if ~exist(outDirName, 'dir')
    mkdir(outDirName);
end
qcDirName = fullfile(outDirName, 'QC');
if ~exist(qcDirName, 'dir')
    mkdir(qcDirName);
end

% Allocate space for the results
stats = cell(1, nDirEntries);

% Number of channels for statistics
nChannelsForStats = numel(channelsForStats);
parfor i = 1 : nDirEntries
   
    if d(i).isdir
        continue;
    end
    
    % Load file
    dataset = loadGeneric(fullfile(inDirName, d(i).name));
    
    if isempty(dataset)
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end
    
    % Create a mask by summing the requested channels
    mask_dataset = dataset(channelsForMask);
    mask = single(mask_dataset{1});
    if numel(mask_dataset) > 1
        for j = 2 : numel(mask_dataset)
            mask = mask + single(mask_dataset{j});
        end
    end
    
    % Coordinates of the center of the image
    center_coords =  0.5 * size(mask);
    
    % Extract the rim
    rim = extractRim(mask, se_rim, se_clean, center_coords);
    
    % Get the pixel intensities in the rim
    meanInts = zeros(1, nChannelsForStats);
    stdInts = zeros(1, nChannelsForStats); 
    for j = 1 : nChannelsForStats
        channel = channelsForStats(j); %#ok<PFBNS>
        img = single(dataset{channel});
        pixel_ints = img(rim);
        
        meanInts(j) = mean(pixel_ints);
        stdInts(j) = std(pixel_ints);
    end
    
    % Store the results
    stats{i} = {d(i).name, meanInts, stdInts};
    
    % Store the quality control pictures
    [~, b] = fileparts(d(i).name);
    rgb_qc = zeros([size(mask), 3], 'uint8');
    rgb_qc(:, :, 1) = nrm(mask, 8);
    rgb_qc(:, :, 2) = 127 * uint8(rim);
    qcFName = fullfile(qcDirName, [b, '_rim.tif']); 
    imwrite(rgb_qc, qcFName);

end

% Remove the empty values from the stats cell array
stats = stats(~cellfun(@isempty, stats));

% Save the statistics to file
statsFName = fullfile(outDirName, 'rim_stats.txt'); 
fid = fopen(statsFName, 'w+');
if fid == -1
    disp('Could not write to disk! Outputting to console.');
end

% Write header
fprintf(fid,  '%64s\t', 'File name');
for i = 1 : nChannelsForStats
    fprintf(fid,  '%16s\t%16s\t', ...
        ['mean(I_', num2str(channelsForStats(i)), ')'], ...
        ['std(I_', num2str(channelsForStats(i)), ')']);
        
end
fprintf(fid,  '\n');

for i = 1 : numel(stats)
    
    fprintf(fid,  '%64s\t', stats{i}{1});
    
    for j = 1 : nChannelsForStats
        fprintf(fid, '%16.4f\t%16.4f\t', stats{i}{2}(j), stats{i}{3}(j)); 
    end

    fprintf(fid, '\n');
    
end

if fid ~= -1
    fclose(fid);
    edit(statsFName);
end

% =========================================================================

function rim = extractRim(mask, se_rim, se_clean, center_coords)

% Segment
t = multithresh(mask, 2);
mask = mask > t(1);

% Fill holes
bw = imfill(mask, 8, 'holes');

% Make sure to pick only the large object in the center
% TODO Check the coordinates
L = bwlabel(bw);
props = regionprops(bw);
areas = [props.Area];
i = find(areas == max(areas), 1);
bw = L == i;

% Clean the object a bit
bw = imdilate(bw, se_clean);
bw = imerode(bw, se_clean);

% Erode
bw_e = imerode(bw, se_rim);

% Extract rim
rim = bw & ~bw_e;



