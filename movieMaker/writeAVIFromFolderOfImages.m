function writeAVIFromFolderOfImages(folder, outFileName, mpg4)
if nargin == 2
    mpg4 = 0;
end

d = dir(folder);
c = 0;
toBeDel = [];
for i = 1 : numel(d)
    if d(i).isdir == 1
        c = c + 1;
        toBeDel(c) = i;
    end
end
d(toBeDel) = [];

if mpg4 == 1
    v = VideoWriter(outFileName, 'MPEG-4');
else
    % AVI
    v = VideoWriter(outFileName);
end
v.FrameRate = 10;
v.Quality = 100;

open(v);

for i = 1 : numel(d)
    
    fprintf(1, 'Adding %s...\n', fullfile(folder, d(i).name));
    img = imread(fullfile(folder, d(i).name));
    if size(img, 3) == 4
        img = img(:, :, 1:3);
    end
    writeVideo(v, img);
end

close(v)