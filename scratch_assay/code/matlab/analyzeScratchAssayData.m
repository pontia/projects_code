function analyzeScratchAssayData(dataFile, resultFolder)

if nargin == 0
    dataFile = 'C:\Users\pontia\Documents\MATLAB\projects_code\scratch_assay\data\processed\data.csv';
    resultFolder = 'C:\Users\pontia\Documents\MATLAB\projects_code\scratch_assay\results\matlab';
elseif nargin == 1
    resultFolder = 'C:\Users\pontia\Documents\projects_code\scratch_assay\results\matlab';
elseif nargin == 2
    % All set
else
    error('Wrong number of input parameters.');
end

% Read in the file
data = importCSVFile(dataFile);
treatment = data(:, 1);
time = cell2mat(data(:, 2));
wound_width = cell2mat(data(:, 3));

% Get all treatments
treatments = unique(treatment);
nTreatments = numel(treatments);

% Allocate space for results
res = zeros(nTreatments, 7);

% Fit a model of the form a * exp(b * t) to all treatments and get the
% coefficients with the 95% confidence intervals
fitResults = cell(1, nTreatments);
for i = 1 : nTreatments
   
    % Get the relevant data
    indices = find(~cellfun(@isempty, strfind(treatment, treatments{i})));
    t = time(indices);
    w = wound_width(indices);
    
    % Fit the exponential model
    [fitresult, gof] = fitModel(t, w);
    
    % Store the results
    cf = confint(fitresult);
    res(i, :) = [ ...
        fitresult.a, cf(1, 1), cf(2, 1), ...
        fitresult.b, cf(1, 2), cf(2, 2), ...
        gof.adjrsquare];
    fitResults{i} = fitresult;
 
    % Plot the results
    hFig = plotDataAndModel(t, w, treatments{i}, fitresult, res(i, :));
    
    % Save the figure to disk
    print(hFig, fullfile(resultFolder, ...
        [strrep(treatments{i}, ' ', '_'), '.png']), '-dpng', '-r300');
    savefig(hFig, fullfile(resultFolder, ...
        [strrep(treatments{i}, ' ', '_'), '.fig']));

    % Close the figure
    close(hFig);

end

% Write the fitting results to file
writeResultsToDisk(res, treatments, fullfile(resultFolder, 'exp_results.txt'));

% Prepare combined plots
hFig = plotCombinedPlots(fitResults, unique(time), treatments);
print(hFig, fullfile(resultFolder, ...
    [strrep(treatments{i}, ' ', '_'), '_combined.png']), '-dpng', '-r300');
savefig(hFig, fullfile(resultFolder, ...
    [strrep(treatments{i}, ' ', '_'), '_combined.fig']));   
close(hFig);

