function hFig = plotDataAndModel(xData, yData, treatment, fitresult, res)

% Open figure
hFig = figure('Name', [treatment, ' - wound fit = a * exp(b * t)']);

% Plot the data and the fit
h = plot(fitresult, xData, yData);

% Change the layout
set(h, 'Marker', '.');
set(h, 'MarkerSize', 15);
set(h, 'LineWidth', 2);
time = unique(xData);
set(gca, 'xtick', time);

% Add a legend
legend(h, 'wound width vs. time', 'fit', 'Location', 'NorthEast');

% Label axes
xlabel('time [h]');
ylabel('wound width (relative)');
% grid on

% Display fit information in the figure title
txtA = ['a = ', num2str(res(1)), ' (', num2str(res(2)), ', ', num2str(res(3)), ')'];
txtB = ['; b = ', num2str(res(4)), ' (', num2str(res(5)), ', ', num2str(res(6)), ')'];
txtC = ['; R^2 (adj) = ', num2str(res(7))];
title([txtA, txtB, txtC]);
