function writeResultsToDisk(res, treatments, fileName)

% Open file
fid = fopen(fileName, 'w');
if (fid == -1)
    fid = 1;
end

% Write header
fprintf( fid, ...
    [ ...
    '%16s\t', ...    % treatment
    '%8s\t', ...     % a
    '%8s\t', ...     % a lower bound (95% confidence)
    '%8s\t', ...     % a upper bound (95% confidence)
    '%8s\t', ...     % b
    '%8s\t', ...     % b lower bound (95% confidence)
    '%8s\t', ...     % b upper bound (95% confidence)
    '%8s\n' ], ...   % adjusted R square
    'treatment', ...
    'a', ...         % treatment
    'aL', ...        % a lower bound (95% confidence)
    'aU', ...        % a upper bound (95% confidence)
    'b', ...         % b 
    'bL', ...        % b lower bound (95% confidence)
    'bU', ...        % b upper bound (95% confidence) 
    'R2adj' );       % adjusted R square  

% Write data
for i = 1 : size(res, 1)
    
fprintf( fid, ...
    [ ...
    '%16s\t', ...      % treatment
    '%8.4f\t', ...      % a
    '%8.4f\t', ...      % a lower bound (95% confidence)
    '%8.4f\t', ...      % a upper bound (95% confidence)
    '%8.4f\t', ...      % b
    '%8.4f\t', ...      % b lower bound (95% confidence)
    '%8.4f\t', ...      % b upper bound (95% confidence)
    '%8.4f\n' ], ...    % adjusted R square
    treatments{i}, ...
    res(i, 1), ...
    res(i, 2), ...
    res(i, 3), ...
    res(i, 4), ...
    res(i, 5), ...
    res(i, 6), ...
    res(i, 7));
    
end

% Close file
if (fid ~= 1)
    fclose(fid);
end
