function hFig = plotCombinedPlots(fitResults, time, treatments)
% modelType: either 'exp' or 'linear'

hFig = figure();
hold on;

colors = {'r', 'g', 'b', 'k', 'm', 'c', 'y'};

plotHandles = zeros(1, numel(treatments));

for i = 1 : numel(fitResults)

    % Get current result
    fitresult = fitResults{i};
    
    % Build the model and the 95% confidence intervals
    a = fitresult.a;
    b = fitresult.b;
    
    y = a .* exp(b * time);

    %yL = aL .* exp(bL * time);
    %yU = aU .* exp(bU * time);

    % Predict 95% confidence interval
    ci = predint(fitresult, time , 0.95, 'functional');
    yL = ci(:, 1);
    yU = ci(:, 2);
    

    % Get the color of current fit
    clr = colors{mod(i - 1, numel(colors)) + 1};
    
    % Add a colored patch to display the confidence interval
    t = [time; time(end: - 1: 1)];
    v = [yL; yU(end: -1: 1)];
    patch(t, v, ones(size(t)), ...
        'FaceColor', clr, 'EdgeColor', clr, 'FaceAlpha', 0.1)

    % Now plot the actual fitted lind
    plotHandles(i) = plot(time, y, 'Color', clr, 'LineWidth', 2, 'LineStyle', '-', ...
        'DisplayName', treatments{i});
    
end

legend(plotHandles, treatments);

    
