  analyzeScratchAssayData <- function(){
    
  # Set the working directory
  rootDir <- "C:/Users/pontia/Documents/MATLAB/projects_code/scratch_assay"
  setwd(rootDir);
  
  # Load the data
  scratch_data <- read.table("./data/processed/data.csv", header = TRUE, sep = ";");
  
  # Write header to results file
  text <- sprintf('%16s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s',
                  'treatment','a','aL','aU','stdE(a)','t(a)','p(a)','b','bL','bU','stdE(b)','t(b)','p(b)');
  write(text, "./results/r/report.txt");
  
  # Extract some data for analysis
  for (treat in unique(scratch_data$treatment)) {
  
    # Extract time and wound_width for current treatment
    time <- scratch_data$time[scratch_data$treatment==treat] 
    wound_width <- scratch_data$wound_width[scratch_data$treatment==treat]
  
    # Prepare image to write to
    fileName = paste("./results/r/", treat, ".png", sep="");
    png(filename=fileName, width=2000, height=1500, res=300)
    
    # Plot the data
    plot.new()
    plot(time, wound_width, pch=19, col="blue", xaxt = "n", xlab="Time [h]", ylab="Wound with [um]", ylim=c(0, 1.3));
    axis(1, unique(scratch_data$time))
  
    # Fit a non-linear model of the form a * exp(b * t)
    m <- nls(wound_width ~ (a*(exp(b*time))), start=list(a = 1, b = 0), trace=F)

    # Get the coefficients and the confidence intervals
    coeffs = coefficients(summary(m));
    confIntervals = confint(m);
    
    # Extract them for easier manipulation
    a = coeffs[1, 1]; aL = confIntervals[1, 1]; aU = confIntervals[1, 2];
    b = coeffs[2, 1]; bL = confIntervals[2, 1]; bU = confIntervals[2, 2];
    stdEA = coeffs[1, 2]; tA = coeffs[1, 3]; pA = coeffs[1, 4];
    stdEB = coeffs[2, 2]; tB = coeffs[2, 3]; pB = coeffs[2, 4];
    
    # Plot the fitted data
    yhat = a * exp(b * unique(time));
    yhatL = aL * exp(bL * unique(time));
    yhatU = aU * exp(bU * unique(time));
    lines(unique(time), yhat, lwd=2, col='red')
    lines(unique(time), yhatL, lwd=1, col='black', lty="dashed")
    lines(unique(time), yhatU, lwd=1, col='black', lty="dashed")

    # Add title (treatment name) and fitted model to the figure
    title(main=paste(treat), sub=paste("",a,"*exp(",b,"*t)", sep=""));
    
    # Save the figure to png
    dev.off()
    
    # Append the summary to the file
    text <- sprintf('%16s\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8E\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8E',
                    treat, a, aL, aU, stdEA, tA, pA, b, bL, bU, stdEB, tB, pB);
    write(text, "./results/r/report.txt", append=TRUE);
  }

  write("", "./results/r/report.txt", append=TRUE);
  write("95% confidence intervals.", "./results/r/report.txt", append=TRUE);
  
}
  
