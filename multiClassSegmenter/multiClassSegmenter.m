function varargout = multiClassSegmenter(varargin)
% MULTICLASSSEGMENTER MATLAB code for multiClassSegmenter.fig
%      MULTICLASSSEGMENTER, by itself, creates a new MULTICLASSSEGMENTER or raises the existing
%      singleton*.
%
%      H = MULTICLASSSEGMENTER returns the handle to a new MULTICLASSSEGMENTER or the handle to
%      the existing singleton*.
%
%      MULTICLASSSEGMENTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTICLASSSEGMENTER.M with the given input arguments.
%
%      MULTICLASSSEGMENTER('Property','Value',...) creates a new MULTICLASSSEGMENTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before multiClassSegmenter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to multiClassSegmenter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help multiClassSegmenter

% Last Modified by GUIDE v2.5 28-Nov-2017 17:46:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @multiClassSegmenter_OpeningFcn, ...
                   'gui_OutputFcn',  @multiClassSegmenter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function multiClassSegmenter_OpeningFcn(hObject, ~, handles, varargin)

% Choose default command line output for multiClassSegmenter
handles.output = hObject;

% Empty the table
handles.tbClasses.Data = {};

% Allocate space to store the ROIs
handles.project.includeROIs = {};
handles.project.excludeROIs = {};
handles.project.dirName = '';
handles.project.fName = '';
handles.project.maskDirName = '';
handles.project.maskFName = '';

% Update handles structure
guidata(hObject, handles);

function varargout = multiClassSegmenter_OutputFcn(~, ~, handles) 
varargout{1} = handles.output;

function figure1_CloseRequestFcn(hObject, ~, handles)
if isfield(handles.project, 'figHandleImage') && ...
        ishandle(handles.project.figHandleImage)
    close(handles.project.figHandleImage);
end
if isfield(handles.project, 'figHandleResultImage') && ...
        ishandle(handles.project.figHandleResultImage)
    close(handles.project.figHandleResultImage);
end
delete(hObject);

% -------------------------------------------------------------------------
%
% CreateFcns
%
% -------------------------------------------------------------------------

function edNumClasses_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edOutput_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edSigma_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pmChannel_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pbMaskReset_Callback(hObject, eventdata, handles)
handles.project.maskDirName = '';
handles.project.maskFName = '';
handles.pbLoadMask.String = 'Load mask (optional)';
guidata(hObject, handles);


function pbLoadMask_Callback(hObject, eventdata, handles)
% Pick file
[maskFName, maskDirName] = uigetfile( ...
    {'*.tif;*.tiff','TIF files (*.tif;*.tiff)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Please select image to be used for masking', ...
    'MultiSelect', 'off', ...
    handles.project.dirName);

if isequal(maskFName, 0)
    return;
end

% Store the file info
handles.project.maskDirName = maskDirName;
handles.project.maskFName = maskFName;
guidata(hObject, handles);

% Update the button
handles.pbLoadMask.String = maskFName;

function pbLoad_Callback(hObject, ~, handles)
% Pick file
[fName, dirName] = uigetfile( ...
    {'*.tif;*.tiff','TIF files (*.tif;*.tiff)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Please select image to be analyzed', ...
    'MultiSelect', 'off', ...
    handles.project.dirName);

if isequal(fName, 0)
    return;
end

% Store the file info
handles.project.dirName = dirName;
handles.project.fName = fName;
guidata(hObject, handles);

% Load the image
loadDataset(dirName, fName, hObject, handles);

function edNumClasses_Callback(~, ~, handles)
value = str2double(handles.edNumClasses.String);
if isnan(value)
    uiwait(errordlg('Invalid number of classes.', 'Error', 'modal'));
    handles.edNumClasses.String = '3';
end
if value < 2
    uiwait(errordlg('You need at least 2 classes.', 'Error', 'modal'));
    handles.edNumClasses.String = '2';
end

function pmChannel_Callback(hObject, ~, handles)
showImage(hObject, handles);

function pbSegment_Callback(hObject, ~, handles)
% Make sure the image is shown
if ~isfield(handles.project, 'figHandleImage') || ...
        ~ishandle(handles.project.figHandleImage)
    % Display the image
    showImage(hObject, handles);
    handles = guidata(hObject);
end

% Get active channel
channel = get_channel(handles);

% Get image
img = handles.project.dataset{1, channel};

% Do we need to filter?
sigma = str2double(handles.edSigma.String);
if isnan(sigma)
    uiwait(errordlg('Invalid value for sigma.', 'Error', 'modal'));
    return;
end

% Gaussian filter the image
if sigma > 0
    img = imgaussfilt(img, sigma, 'Padding', 'replicate');
end

% Get number of classes
classes = str2double(handles.edNumClasses.String);
if isnan(classes)
    uiwait(errordlg('Invalid number of classes.', 'Error', 'modal'));
    return;
end
if classes < 2
    uiwait(errordlg('You need at least 2 classes.', 'Error', 'modal'));
    return;
end

% Sync the ROIs
toBeDel = []; c = 0;
for i = 1 : numel(handles.project.includeROIs)
    if ~(handles.project.includeROIs{i}.isvalid())
        c = c + 1;
        toBeDel(c) = i;
    end
end
if ~isempty(toBeDel)
    handles.project.includeROIs(toBeDel) = [];
end
    
toBeDel = []; c = 0;
for i = 1 : numel(handles.project.excludeROIs)
    if ~(handles.project.excludeROIs{i}.isvalid())
        c = c + 1;
        toBeDel(c) = i;
    end
end
if ~isempty(toBeDel)
    handles.project.excludeROIs(toBeDel) = [];
end

% Get the ROI polygons
includeROIs = handles.project.includeROIs;
if numel(includeROIs) == 0

    % Consider the whole image
    BW = true(size(img));
    
else
    
    % Apply ROI inclusion and exclusion
    BW = false(size(img));
    for i = 1 : numel(includeROIs)
        cBW = createMask(includeROIs{i});
        BW = BW | cBW;
    end
    
    excludeROIs = handles.project.excludeROIs;
    for i = 1 : numel(excludeROIs)
        cBW = createMask(excludeROIs{i});
        cBW = ~cBW;
        BW = BW & cBW;
    end
end

% External mask
if ~isempty(handles.project.maskFName)
    % Load the mask from disk
    mask = imread(fullfile(handles.project.maskDirName, handles.project.maskFName));
    threshold = (min(mask(:)) + max(mask(:))) / 2;
    external_BW = mask > threshold;
else
    external_BW = true(size(img));
end

% Combine the masks
BW = BW & external_BW;

% Segment the image intensities withing the mask
idx = find(BW);
[indices, centroids] = kmeans(single(img(idx)), classes, 'Start', 'Uniform');

% All pixels
all_idx = zeros(numel(BW), 1);
all_idx(idx) = indices;
    
% Now set all_idx to indices for the remainder of the analysis
indices = all_idx;

% Sort the classes from darkest to lightest
[~, p] = sort(centroids, 'ascend');
if any(diff(p) ~= 1)
    for i = 1 : numel(p)
        indices(indices == p(i)) = classes + i;
    end
    
    for i = 1 : numel(p)
        indices(indices == (classes + i)) = i;
    end
end

% Reshape the indices into an image
res = reshape(indices, size(img, 1), size(img, 2));

% Now consider only the pixels in the mask
sel_indices = res(BW == true);

% Display result
if ~isfield(handles.project, 'figHandleResultImage') || ...
        ~ishandle(handles.project.figHandleResultImage)
    handles.project.figHandleResultImage = figure('NumberTitle', 'off');
    handles.project.figHandleResultImage.Name = ['Segmentation result'];
end

% Stretch the classes to make them visible
if classes < 256
    res = nrm(res, 8);
elseif classes < 65535
    res = nrm(res, 16);
else
    res = nrm(res);
end

figure(handles.project.figHandleResultImage);
image = imhandles(handles.project.figHandleResultImage);
if isempty(image)
    imshow(res);
    handles.project.figHandleResultImage.NextPlot = 'add';
else
    image.CData = res;
end

% Calculate areas
totalArea = numel(sel_indices);
num_pixels = zeros(1, classes);
rel_areas = zeros(1, classes);
for i = 1 : classes
    num_pixels(i) = numel(find(sel_indices == i));
    rel_areas(i) =  num_pixels(i) / totalArea;
end

% Write output
handles.tbClasses.Data = cell(classes, 3);
for i = 1 : classes
    handles.tbClasses.Data{i, 1} = i;
    handles.tbClasses.Data{i, 2} = num_pixels(i);
    handles.tbClasses.Data{i, 3} = rel_areas(i);
end

% Update guidata
guidata(hObject, handles);

function edSigma_Callback(~, ~, handles)
value = str2double(handles.edSigma.String);
if isnan(value)
    uiwait(errordlg('Invalid value for sigma.', 'Error', 'modal'));
    handles.edSigma.String = '3';
end
if value < 0
    uiwait(errordlg(['Sigma must be a positive number ', ...
        '(or 0 to disable filtering).'], 'Error', 'modal'));
    handles.edNumClasses.String = '2';
end

function pbIncludeROI_Callback(hObject, ~, handles)
ax = handles.project.figHandleImage.CurrentAxes;
polygon = impoly(ax);
api = iptgetapi(polygon);
api.setColor('red');
if isempty(handles.project.includeROIs )
    handles.project.includeROIs = {polygon};
else
    handles.project.includeROIs = cat(2, ...
        handles.project.includeROIs , {polygon});
end
guidata(hObject, handles);

function pbExcludeROI_Callback(hObject, ~, handles)
ax = handles.project.figHandleImage.CurrentAxes;
polygon = impoly(ax);
api = iptgetapi(polygon);
api.setColor('yellow');
if isempty(handles.project.excludeROIs )
    handles.project.excludeROIs = {polygon};
else
    handles.project.excludeROIs = cat(2, ...
        handles.project.excludeROIs , {polygon});
end
guidata(hObject, handles);

function pbSaveResults_Callback(hObject, eventdata, handles)
[fName, dirName] = uiputfile('*.txt', 'Save results...', 'results.txt');
if isequal(fName, 0)
    return;
end

% Build file names
[~, body, ~] = fileparts(fName);

% Save the statistics
fid = fopen(fullfile(dirName, fName), 'w');
fprintf(fid, '%12s\t%12s\t%12s\n', 'Class', 'Pixels', 'Rel. area');
for i = 1 : str2double(handles.edNumClasses.String)
    fprintf(fid, '%12d\t%12d\t%12.4f\n', ...
        handles.tbClasses.Data{i, 1}, ...
        handles.tbClasses.Data{i, 2}, ...
        handles.tbClasses.Data{i, 3});
end
fclose(fid);

% Save the figures as well
print(handles.project.figHandleImage, ...
    fullfile(dirName, [body, '_rois.png']), '-dpng', '-r150');
print(handles.project.figHandleResultImage, ...
    fullfile(dirName, [body, '_segm.png']), '-dpng', '-r150');

% -------------------------------------------------------------------------
%
% Other functions
%
% -------------------------------------------------------------------------

function channel = get_channel(handles)
if iscell(handles.pmChannel.String)
    channel = str2double(handles.pmChannel.String{handles.pmChannel.Value});
else
    channel = str2double(handles.pmChannel.String);
end

function handles = showImage(hObject, handles)

if ~isfield(handles.project, 'figHandleImage') || ...
        ~ishandle(handles.project.figHandleImage)
    handles.project.figHandleImage = figure('NumberTitle', 'off');
end

% Get active channel
channel = get_channel(handles);

% Update the title
handles.project.figHandleImage.Name = ['Channel ', num2str(channel)];

% Show image
img = handles.project.dataset{1, channel};
img = imadjust(img, stretchlim(img), []);
figure(handles.project.figHandleImage);
image = imhandles(handles.project.figHandleImage);
if isempty(image)
    handles.project.figHandleImage.CurrentAxes = axes();
    imshow(img, 'Parent', handles.project.figHandleImage.CurrentAxes);
    handles.project.figHandleImage.NextPlot = 'add';
else
    image.CData = img;
end

% Store the selection
guidata(hObject, handles);

function loadDataset(dirName, fName, hObject, handles)

% Load the image
dataset = loadGeneric(fullfile(dirName, fName));

% Store the selection
handles.project.dataset = dataset;
guidata(hObject, handles);

% Activate the channels menu
channels = cell(1, size(dataset, 2));
for i = 1 : size(dataset, 2)
   channels{i} = num2str(i);
end
handles.pmChannel.String = channels;

% Enable UI elements
handles = enableUIElements(hObject, handles);

% Display the image
showImage(hObject, handles);

function handles = enableUIElements(hObject, handles)
handles.pmChannel.Enable = 'on';
handles.txChannel.Enable = 'on';
handles.pbSegment.Enable = 'on';
handles.pbIncludeROI.Enable = 'on';
handles.pbExcludeROI.Enable = 'on';
guidata(hObject, handles)
