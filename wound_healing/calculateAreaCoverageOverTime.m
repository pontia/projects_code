function area = calculateAreaCoverageOverTime(inputFolder)

% Do we have an input folder?
if nargin == 0
    inputFolder = uigetdir('', ...
        'Please choose the folder containing the probability maps');
end

% Get the list of files
d = dir(fullfile(inputFolder, '*_Probabilities.tif*'));
if isempty(d)
    disp(['Sorry, no probability maps found in ', inputFolder]);
end

% Allocate space for the result
area = zeros(1, numel(d));

hWaitbar = waitbar(0, 'Please wait...');

% Proocess all files
nImages = numel(d);
for i = 1 : nImages

    % Load image
    img = imread(fullfile(inputFolder, d(i).name));
    
    % Extract cells probability map
    cells = img(:, :, 1);
    
    % Calculate and store area fraction
    area(i) = mean(cells(:));

    % Update waitbar
    waitbar(i / nImages, hWaitbar);

end

% Close waitbar
close(hWaitbar);

% Plot
h = figure; plot(area);
xlabel('Time points');
ylabel('Fraction area covered');
set(get(h, 'CurrentAxes'), 'YLim', [0 1]);

% Save plot as figure and as png
savefig(h, fullfile(inputFolder, 'area_covered.fig'));
print(h, fullfile(inputFolder, 'area_covered.png'), '-dpng', '-painters', '-r150');

% Save to file
dlmwrite(fullfile(inputFolder, 'area_covered.csv'), area(:), ...
    'delimiter', '\t', 'precision', 4);

