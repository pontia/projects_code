function lineageTree = assessTreeQuality(lineageTree, allData)

% Make sure that no node has more than two children
iterator = lineageTree.depthfirstiterator;
toBeRemoved = [];
c = 0;
for i = iterator
    if i == 1
        continue;
    end
    ch = lineageTree.getchildren(i);
    nC = numel(ch) ;
    if nC > 2
        fprintf(1, 'Node %d has %d children! ', i, nC);

        % Get the coordinates of the mother
        [trackM, timepointM, cellM] = decodeValue(lineageTree.get(i));
        motherCoords = allData{timepointM}(cellM).Centroid;
        assert(trackM == allData{timepointM}(cellM).TrackIndex);

        % Get the color of the mother
        motherColor = allData{timepointM}(cellM).RGB;

        % Get the coordinates of the daughter cells
        daughterCoords = [];
        daughterColors = [];
        for j = 1 : nC
            [trackD, timepointD, cellD] = decodeValue(lineageTree.get(ch(j)));
            if isinf(trackD)
                c = c + 1;
                toBeRemoved(c) = j; %#ok<AGROW>
                daughterCoords = cat(1, daughterCoords, [Inf Inf]);
                daughterColors = cat(1, daughterColors, [0.0, 0.0, 0.0]);
            else               
                daughterCoords = cat(1, daughterCoords, allData{timepointD}(cellD).Centroid);
                daughterColors = cat(1, daughterColors, allData{timepointD}(cellD).RGB);
                assert(trackD == allData{timepointD}(cellD).TrackIndex);
                assert(timepointD - 1 == timepointM);
            end
        end

        fprintf(1, 'Timepoints %d -> %d.\n', timepointM, timepointD);

        % Preserve the two closest cells and drop the others
        D = pdist2(motherCoords, daughterCoords);
        Dc = 1 - pdist2(motherColor, daughterColors);
        D = Dc .* D;
        closestCell = find(D == min(D), 1);
        D(closestCell) = Inf;
        secondClosestCell = find(D == min(D), 1);
        D(secondClosestCell) = Inf;
        indices = find(~isinf(D));

        % Append the additional daughter cells to the list of 
        % branches to be relocated to root
        for j = 1 : numel(indices)
           c = c + 1;
           toBeRemoved(c) = ch(indices(j));
        end
    end
end

% Now proceed with the grafting
% Set the parent of the remaining cells to root (1)
toBeRemoved = sort(toBeRemoved, 'descend');
for k = numel(toBeRemoved) : - 1 : 1
    % Move the subtree to the root
    t = lineageTree.subtree(toBeRemoved(k));
    lineageTree = lineageTree.graft(1, t);
    lineageTree = lineageTree.chop(toBeRemoved(k));
end

% Make sure that no two parents have the same child
m = cell2mat(lineageTree.Node);
m(isinf(m)) = [];
assert(numel(m) == numel(unique(m)));
iterator = lineageTree.depthfirstiterator;
allChildren = zeros(1, max(iterator));
for i = iterator
   if i == 1
       continue;
   end
   indices = lineageTree.getchildren(i);
   allChildren(indices) = allChildren(indices) + 1; 
end
assert(all(allChildren < 2));
