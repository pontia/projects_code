classdef Track < handle
   properties (Access = private)
       index = [];
       startTimepoint = [];
       startNode = [];
   end

   methods (Access = public)

       function this = Track(index, startTimepoint, startNode)
           if ~isnumeric(index) && ~isscalar(index)
               error('index must be a scalar.');
           end
           if ~isnumeric(startTimepoint) && ~isscalar(startTimepoint)
               error('startTimepoint must be a scalar.');
           end
           if ~isa(startNode, 'Node')
               error('startNode must be of class Node.');
           end
           this.index = index;
           this.startTimepoint = startTimepoint;
           this.startNode = startNode;

           % TODO: Change this
           assert(this.index == this.startNode.getId());

       end

       function id = getIndex(this)
           id = this.index;
       end

       function t = getStartTimePoint(this)
           t = this.startTimepoint;
       end

       function t = getLastTimePoint(this)
           t = this.startTimepoint + this.getNodeCount() - 1;
       end

       function n = getStartNode(this)
           n = this.startNode;
       end

       function n = getNodeForTimepoint(this, timepoint)
           n = [];
           error('IMPLEMENT ME');
       end

       function nodes = getLeafNodes(this)
           if isempty(this.startNode)
               nodes = [];
           else
               nodes = [];
               nodes = recursiveGetLeafs(this, this.startNode, nodes);
           end
       end

       function count = getNodeCount(this)
           count = 1;
           count = recursiveGetNodeCount(this, this.startNode, count);
       end

       function b = containsTimepoint(this, timepoint)
           b = timepoint >= this.startTimepoint && ...
               timepoint <= this.getLastTimePoint();
       end

       function pos = getNodeCoordinatesForTimepoint(this, timepoint)
           pos = [];
           error('IMPLEMENT ME');
       end

       function disp(this)
           fprintf(1, 'Track id = %d starting at timepoint %d.\n', ...
               this.index, this.startTimepoint);
       end
   end

   methods (Access = private)

       function nodes = recursiveGetLeafs(this, node, nodes)
          c = node.getChildren();
          if isempty(c)
              if isempty(nodes)
                  nodes = node;
              else
                  nodes(end + 1) = c;
              end
          else
              for i = 1 : numel(c)
                  nodes = this.recursiveGetLeafs(c(i), nodes);
              end
          end
       end

       function count = recursiveGetNodeCount(this, node, count)
          c = node.getChildren();
          if isempty(c)
              return;
          else
              count = count + numel(c);
              for i = 1 : numel(c)
                  count = this.recursiveGetNodeCount(c(i), count);
              end
          end
       end

   end

end
