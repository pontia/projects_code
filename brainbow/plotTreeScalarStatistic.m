function statsValues = plotTreeScalarStatistic(tree, allData, statsName, statsTitle, min_track_length)

if nargin == 4
    min_track_length = 0;
end

statsValues = {};
trackIndices = {};
root_children = tree.getchildren(1);
for i = 1 : numel(root_children)
    len = getBranchLength(tree, root_children(i)); 
    if len < min_track_length
        fprintf(1,'plotTreeScalarStatistic: dropped subtree of length %d.\n', len);
        continue;
    end
    [currStatsValues, ~, currTrackIndices] = ...
        extractStatistics(tree, root_children(i), allData, ...
        statsName);
    statsValues = cat(2, statsValues, currStatsValues);
    trackIndices = cat(2, trackIndices, currTrackIndices);
end
for k = 1 : numel(statsValues)
    % Make sure there is something to plot (this should have been taken
    % care by the check above)
    found = 0;
    for i = 1 : size(statsValues{k}, 1)
        if sum(~isnan(statsValues{k}(i, :))) > min_track_length
            found = found + 1;
        end
    end
    if found > 0
        figure('NumberTitle', 'off', ...
            'Name', ['Subtree ', num2str(k),': ', statsTitle]);
        hold on;
        c = 0;
        handles = zeros(1, found);
        for i = 1 : size(statsValues{k}, 1)
            if sum(~isnan(statsValues{k}(i, :))) > min_track_length
                c = c + 1;
                subplot(found, 1, c);
                handles(c) = plot(1 : size(statsValues{k}, 2), ...
                    statsValues{k}(i, :), ...
                    'DisplayName', num2str(trackIndices{k}(i)));
                set(get(handles(c), 'Parent'), 'XLim', ...
                    [1, size(statsValues{k}, 2)]);
                legend('show')
            end
        end
        % Set the same y lim
        ymin = Inf;
        ymax = -Inf;
        for i = 1 : numel(handles)
            lims = get(get(handles(i), 'Parent'), 'YLim');
            if lims(1) < ymin
                ymin = lims(1);
            end
            if lims(2) > ymax
                ymax = lims(2);
            end
        end
        for i = 1 : numel(handles)
            set(get(handles(i), 'Parent'), 'YLim', [ymin, ymax]);
        end
    end
end

function [statsValues, maxBranchNumber, trackIndices] = ...
    extractStatistics(tree, root, allData, ...
    statsName, statsValues, branchNumber, maxBranchNumber, trackIndices)

% Initialize the output
if nargin ~= 4 && nargin ~= 8
    error('Either 4 or 8 input argument expected!');
end

if nargin == 4
    nBranches = 1 + numel(tree.findbranchpoints());
    statsValues = nan(nBranches, numel(allData));
    branchNumber = 1;
    maxBranchNumber = 1;
    trackIndices = nan(size(statsValues, 1), 1);
end

if branchNumber < maxBranchNumber
    branchNumber = maxBranchNumber + 1;
end
maxBranchNumber = branchNumber;

% Get current node coordinates
root_value = tree.get(root);
[originalTrackIndex, timepoint, cellIndex] = decodeValue(root_value);
statsValues(branchNumber, timepoint) = ...
    allData{timepoint}(cellIndex).(statsName);
trackIndices(branchNumber) = originalTrackIndex;

% Get the children
root_children = tree.getchildren(root);

for child = root_children

    value = tree.get(child);
    if isinf(value)
        return
    end
    
    [originalTrackIndex, timepoint, cellIndex] = decodeValue(value);

    % Get the coords
    statsValues(branchNumber, timepoint) = ...
        allData{timepoint}(cellIndex).(statsName);
    trackIndices(branchNumber) = originalTrackIndex;
    
    % Keep going
    nextChildren = tree.getchildren(child);
    while ~isempty(nextChildren)
        if numel(nextChildren) > 1
            for j = 2 : numel(nextChildren)
                newBranch = maxBranchNumber + 1;
                [statsValues, maxBranchNumber, trackIndices] = ...
                    extractStatistics(tree, nextChildren(j), allData, ...
                    statsName, statsValues, newBranch, ...
                    maxBranchNumber, trackIndices);
            end
        end
        next = nextChildren(1);
        nextValue = tree.get(next);
        if isinf(nextValue)
            nextChildren = [];
        else
            [originalTrackIndex, nextTimepoint, nextCellIndex] = decodeValue(nextValue);
            statsValues(branchNumber, nextTimepoint) = ...
                allData{nextTimepoint}(nextCellIndex).(statsName);
            trackIndices(branchNumber) = originalTrackIndex;
            nextChildren = tree.getchildren(next);
        end
    end
end
