function [trackIndex, timepoint, cellIndex] = decodeValue(value)

trackIndex = fix(value / 1e12);
value = value - trackIndex * 1e12;
timepoint = fix(value / 1e8);
value = value - timepoint * 1e8;
cellIndex = fix(value / 1e4);