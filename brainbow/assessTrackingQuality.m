function assessTrackingQuality(allData, maxTrackingStep)

% Build a track matrix
numTracks = 0;
for i = 1 : numel(allData)
    if max([allData{i}.TrackIndex]) > numTracks
        numTracks = max([allData{i}.TrackIndex]);
    end
end

numTimepoints = numel(allData);

coords = cell(numTracks, numTimepoints);
colors = cell(numTracks, numTimepoints);

for i = 1 : numel(allData)
    for j = 1 : numel(allData{i})
        coords{allData{i}(j).TrackIndex, i} = allData{i}(j).Centroid;
        colors{allData{i}(j).TrackIndex, i} = allData{i}(j).RGB;
    end
end

% Now check the tracking
allTracks = cell(1, numTracks);
allTracksColors = cell(1, numTracks);
for track = 1 : size(coords, 1)
    trackStarted = 0;
    current = [];
    currentColor = [];
    for timepoint = 1 : size(coords, 2)
        if ~isempty(coords{track, timepoint})
            current = cat(1, current, coords{track, timepoint});
            currentColor = cat(1, currentColor, colors{track, timepoint});
            trackStarted = 1;
        else
            if trackStarted == 1
                % We close the track here
                allTracks{track} = current;
                allTracksColors{track} = currentColor;
                break;
            end
        end
    end
end

% Check that the tracks do not contain steps larger than maxTrackingStep
for i = 1 : numel(allTracks)

    for j = 1 : size(allTracks{i}, 1) - 1

        D = pdist2(allTracks{i}(j, :), allTracks{i}(j + 1, :));
        Dc = 1 - pdist2(allTracksColors{i}(j, :), allTracksColors{i}(j + 1, :));
        D = Dc .* D;
        
        assert(D <= maxTrackingStep, ...
            'Tracking step larger than allowed!');

    end

end
