function lineageTree = buildLineageTree(allData, searchRadius)

DEBUG = 1;

% Augment the data with information about their timepoint
for i = 1 : numel(allData)
    for j = 1 : numel(allData{i})
        allData{i}(j).Timepoint = i;
    end
end

% We keep a copy of allData for analyzing cell divisions
allDataOriginal = allData;

% Start by creating all tracks that start at timepoint 1 - cell that belong
% to those tracks are removed from the from the allData structure. The
% root of the lineage tree is at a virtual time zero.
lineageTree = tree(encodeValue(0, 0, 0));

% Find the first timepoint with a track starting
cellsPerTimepoint = cellfun(@numel, allData);
firstTimepoint = find(cellsPerTimepoint > 0, 1);

% Tracks starting at time point 'firstTimepoint'
n = 1;
while numel(allData{firstTimepoint}) > 0

    % Get the track index
    trackIndex = allData{firstTimepoint}(1).TrackIndex;

    % Keep track of the index of this cell in the original data
    indexFirstTimepoint = getIndexOfClosest( ...
        allData{firstTimepoint}(1), allDataOriginal{firstTimepoint});

    % Are there possible candidates for a mother cell?
    [index, motherCell] = findMotherCellInPreviousTimepoint(allDataOriginal, ...
        firstTimepoint, allData{firstTimepoint}(1), searchRadius);

    if ~isempty(motherCell)

        % Track index of the mother cell
        motherTrackIndex = motherCell.TrackIndex;

        % Time point of the mother cell
        motherTimepoint = firstTimepoint - 1;

        % Cell index of the mother cell
        motherCellIndex = index;

        % Build the value
        value = encodeValue(motherTrackIndex, ...
            motherTimepoint, motherCellIndex);

        if DEBUG == 1
            fprintf(1, ...
                ['Looking for mother cell for track index %d ', ...
                'at timepoint %d and cell index %d (with code %d).\n'], ...
                motherTrackIndex, motherTimepoint, ...
                motherCellIndex, value);
        end

        % Find the node by value (this overrides current n)
        n = find(lineageTree == value);

        if isempty(n)
            fprintf(1, ['Could not find mother cell vith value %d ', ...
                'in lineage tree! Reverting to root.']);
            n = 1;
        else
            % Make sure that this specific node does not have already more
            % than one child
            if numel(lineageTree.getchildren(n)) > 1
                fprintf(1, ...
                    ['Mother cell already has more than one child. ', ...
                    'Reverting to root.']);
                n = 1;
            end
        end

        if DEBUG == 1 && ~isempty(n)
            fprintf(1, ...
                ['Appending track %d at timepoint %d to parent ', ...
                'track %d (original cell index is %d).\n'], ...
                trackIndex, firstTimepoint, ...
                motherTrackIndex, indexFirstTimepoint);
        end

    else

        if DEBUG == 1
            fprintf(1, ...
                ['Starting track %d at timepoint %d ', ...
                '(original cell index is %d).\n'], ...
                trackIndex, firstTimepoint, indexFirstTimepoint);
        end

    end

    % Add the cell - as data we store: [trackIndex, timepoint, cellIndex]
    [lineageTree, n] = lineageTree.addnode(n, ...
        encodeValue(trackIndex, firstTimepoint, indexFirstTimepoint));

    % Make sure the timepoint difference is only one!
    if DEBUG == 1
        [~, tmpTimepoint] = decodeValue(lineageTree.get(n));
        [~, tmpTimepointP] = decodeValue(...
            lineageTree.get(lineageTree.getparent(n)));
        if tmpTimepointP > 0
            assert(tmpTimepoint - tmpTimepointP == 1, ...
                'Parent cell is more than one time point away!');
        end
    end

    % Delete the extracted cell
    extractedCellPos = allData{firstTimepoint}(1).Centroid;
    extractedCellColor = allData{firstTimepoint}(1).RGB;
    allData{firstTimepoint}(1) = [];

    % Now follow the track across all time points and append the cells to
    % the lineage tree
    found = true;
    t = firstTimepoint + 1;
    while found == true && t <= numel(allData)

        % Can we find the track index in the next time point?
        trackIndexInNextTimepoint = find([allData{t}.TrackIndex] == trackIndex);
        found = ~isempty(trackIndexInNextTimepoint);

        if found == true

            % Calculate the combined spatial and color distance 
            spatialDistFromPrev = pdist2(extractedCellPos, ...
                allData{t}(trackIndexInNextTimepoint).Centroid);
            colorDistFromPrev = 1 - double(...
                pdist2(extractedCellColor, ...
                allData{t}(trackIndexInNextTimepoint).RGB));
            distFromPrev = colorDistFromPrev .* spatialDistFromPrev;

            if distFromPrev > searchRadius
                fprintf(1, 'Error: matching node is at a distance of %f!\n', ...
                    distFromPrev);
            end
        end

        % Make sure we use the original cell index
        originalTrackIndexInNextTimepoint = ...
            find([allDataOriginal{t}.TrackIndex] == trackIndex);

        % Append the cell to the tree - as data we store:
        % [trackIndex, timepoint, cellIndex]
        value = encodeValue(trackIndex, t, originalTrackIndexInNextTimepoint);
        [lineageTree, n] = lineageTree.addnode(n, value);

        % Delete the extracted cell
        if found == true
            extractedCellPos = allData{t}(trackIndexInNextTimepoint).Centroid;
            extractedCellColor = allData{t}(trackIndexInNextTimepoint).RGB;
        end
        allData{t}(trackIndexInNextTimepoint) = [];

        if DEBUG == 1
            fprintf(1, ...
                ['Appending cell with value %d to track %d at ', ...
                'timepoint %d.\n'], ...
                value, trackIndex, t);

            % Assert that the time difference is only one
            if found == true
                [~, tmpTimepoint] = decodeValue(lineageTree.get(n));
                [~, tmpTimepointP] = decodeValue(...
                    lineageTree.get(lineageTree.getparent(n)));
                if tmpTimepointP > 0
                    assert(tmpTimepoint - tmpTimepointP == 1, ...
                        'Parent cell is more than one time point away!');
                end
            end
        end

        % Move on to the next time point
        t = t + 1;

    end

    % Move on to the next 'firstTimePoint'
    cellsPerTimepoint = cellfun(@numel, allData);
    firstTimepoint = find(cellsPerTimepoint > 0, 1);
    n = 1;

    if isempty(firstTimepoint)
        break;
    end

end

% Now refine the tree (some of the branches starting at root may have
% been put there because their mother cells were not in the tree yet).
% We analyze the direct children of the root of the tree, and in particular
% those who have a starting timepoint larger than one.
children = lineageTree.getchildren(1);
for i = 1 : numel(children)

    child = children(i);
    value = lineageTree.get(child);
    if isempty(value)
        continue;
    end
    [~, timepoint, cellIndex] = decodeValue(value);

    if timepoint == 1
        continue;
    end

    % Get the cell
    currentCell = allDataOriginal{timepoint}(cellIndex);

    % Does the cell have a mother in the tree?
    [index, motherCell] = ...
        findMotherCellInPreviousTimepoint(allDataOriginal, timepoint, ...
        currentCell, searchRadius);

    if isempty(motherCell)
        continue;
    end

    % Track index of the mother cell
    motherTrackIndex = motherCell.TrackIndex;

    % Time point of the mother cell
    motherTimepoint = timepoint - 1;

    % Cell index of the mother cell
    motherCellIndex = index;

    % Build the value
    value = encodeValue(motherTrackIndex, ...
        motherTimepoint, motherCellIndex);

    % Find the mother cell in the tree
    n = find(lineageTree == value);
    if isempty(n)
        fprintf(1, ['Failed to find mother cell for track %d at ', ...
            'timepoint %d and cell index %d. This is a bug!\n'], ...
            motherTrackIndex, motherTimepoint, motherCellIndex);
        continue;
    end

    % Change the parent of current branch to value
    branch = lineageTree.subtree(child);
    lineageTree = lineageTree.graft(n, branch);
    lineageTree = lineageTree.chop(child);

end

% =========================================================================

function [index, motherCell] = ...
    findMotherCellInPreviousTimepoint(allDataOriginal, timepoint, ...
    currentCell, searchRadius)

index = [];
motherCell = [];

previousTimepoint = timepoint - 1;
if previousTimepoint < 1
    return;
end

if isempty(allDataOriginal{previousTimepoint})
    return;
end

% Get the closest cell within searchRadius
D = pdist2(currentCell.Centroid, ...
    reshape([allDataOriginal{previousTimepoint}.Centroid], ...
    size(currentCell.Centroid, 2), ...
    numel(allDataOriginal{previousTimepoint}))');
if min(D) > searchRadius
    return;
end

index = find(D == min(D), 1);
motherCell = allDataOriginal{previousTimepoint}(index);

function index = getIndexOfClosest(currentCell, allCells)

if size(currentCell.Centroid, 1) > 1
    error('currentCell must be just one!');
end

D = pdist2(currentCell.Centroid, ...
    reshape([allCells.Centroid], ...
    size(currentCell.Centroid, 2), ...
    numel(allCells))');

index = find(D == min(D), 1);

