function buildCytoplasmImages(image_folder, basename, numTimeDigits, firstTimePoint, lastTimePoint, ch_labeled_all_nuclei, suffix_labeled_all_nuclei, dilation_rounds)
% buildCytoplasmImages Expands cells from the labeled nuclei

if nargin == 0
    image_folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\may\cp_output';
    basename = 'export_';
    numTimeDigits = 3;
    firstTimePoint = 0;
    lastTimePoint = 137;
    ch_labeled_all_nuclei = 0;    % All nuclei channel (labeled)
    suffix_labeled_all_nuclei = '_Z000_Probabilities_All_Nuclei.tif'; % All nuclei channel (labeled)
    suffix_cytoplasm_image = '_Z000_Probabilities_Cytoplasm.tif'; % Cytoplasm image
    dilation_rounds = 3;
end

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

% Format string
strgT = sprintf('%%.%dd', numTimeDigits);

% Structuring element
se = strel('disk', 1, 8);

nTimepoints = lastTimePoint - firstTimePoint + 1;
currentTimepoint = 0;
for timepoint = firstTimePoint : lastTimePoint

    % Image name
    common_image_name = fullfile(image_folder, [basename, ...
        'T', sprintf(strgT, timepoint), '_', ...
        'C%d%s']);
    common_image_name = strrep(common_image_name, '\', '/');
    
    % Load the label image
    L = imread(sprintf(common_image_name, ...
        ch_labeled_all_nuclei, suffix_labeled_all_nuclei));
    
    % Make sure we have enough gray-value range
    L = uint16(L);

    % We cannot simply dilate the labels to find the boundaries,
    % since higher labels eat into the lower ones.
    % We only compare labels that are within overlapping range
    % of each other.
    
    % Get all objects
    S = regionprops(L, 'Centroid', 'Area', 'PixelList', ...
        'PixelIdxList');
    
    % Add the label value
    for i = 1 : numel(S)
        S(i).Label = L(S(i).PixelIdxList(1));
    end
    
    % Sort the objects by their value
    [~, p] = sort([S.Label]);
    S = S(p);
    assert(all(diff([S.Label]) > 0));
    
    % Search distance is 2 times the dilation rounds (since the structuring
    % element radius is 1)
    d = 2 * dilation_rounds;
    
    % To compare
    matches = [];
    for i = 1 : numel(S)
        for j = i + 1 : numel(S)
            
            pixDist = pdist2(S(i).PixelList, S(j).PixelList);
            [y, x] = find(pixDist == min(pixDist(:)), 1);
            if pixDist(y, x) < d
                matches = cat(1, matches, [i, j]);
            end
        end
    end
    
    % If there are no potential overlaps, we just dilate the
    % image
    if isempty(matches)
        D = L;
        for d = 1 : dilation_rounds
            D = imdilate(D, se);
        end
    else
        
        % Process all nuclei that do not overlap
        S_far = S; S_far(unique(matches)) = [];
        
        D = zeros(size(L), class(L));
        for i = 1 : numel(S_far)
            D(S_far(i).PixelIdxList) = L(S_far(i).PixelIdxList);
        end
        
        % Dilate the subset
        for d = 1 : dilation_rounds
            D = imdilate(D, se);
        end
        
        % Process all couple of nuclei that are close to each other
        for i = 1 : size(matches, 1)
            
            % Process the pair
            pairD = processNuclei(L, S, matches(i, 1), matches(i, 2), ...
                dilation_rounds, se);
            
            % Append the cells
            D = D + pairD;
        end

    end

    % Subtract the nuclei to get the cytoplasm
    D = cast(single(D) - single(L), class(L));
    
    % Save the cytoplasm image
    outFileName = sprintf(common_image_name, ...
        ch_labeled_all_nuclei, suffix_cytoplasm_image);
    imwrite(D, outFileName);
    
    % Inform
    fprintf(1, 'Written file %s.\n', outFileName);

    % Update waitbar
    currentTimepoint = currentTimepoint + 1;
    waitbar(currentTimepoint / nTimepoints, hWaitbar);

end

% Close the waitbar
close(hWaitbar);

function D = processNuclei(L, S, i, j, dilation_rounds, se)

assert(all(diff([S.Label]) > 0))

Di = zeros(size(L), class(L));
Dj = zeros(size(L), class(L));
Di(S(i).PixelIdxList) = 1;
Dj(S(j).PixelIdxList) = 1;

for k = 1 : dilation_rounds
    
    % Dilate the nuclei individually
    Di = imdilate(Di, se);
    Dj = imdilate(Dj, se);
    
    % Is there some overlap?
    Dt = Di + Dj;
    ind = find(Dt == 2);
    if ~isempty(ind)
        % Remove the overlapping pixels
        Di(ind) = 0;
        Dj(ind) = 0;
    end
    
end

% Reassign the correct label
Di(Di > 0) = S(i).Label;
Dj(Dj > 0) = S(j).Label;

% Combine the images
D = Di + Dj;