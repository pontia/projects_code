function lineageTree = pruneLineageTree(lineageTree, allData, S_target_nucleus, radius)

DEBUG = 1;

% Get all children
root_children = lineageTree.getchildren(1);

% In a first round, prune all branches that do not start at firstTimepoint
toBeDel = [];
c = 0;
for i = 1 : numel(root_children)

    % Decode each child
    [~, timepoint] = decodeValue(lineageTree.get(root_children(i)));
    
    % If the child does not originate at the first time point, mark
    % the whole branch for deletion
    if timepoint ~= 1
        c = c + 1;
        toBeDel(c) = root_children(i);
    end
end

assert(isempty(setdiff(toBeDel, root_children)));

% Now delete the branches
for i = numel(toBeDel) : -1 : 1
    lineageTree = lineageTree.chop(toBeDel(i));
end

if DEBUG == 1
    % Plot the target nuclei positions
    figure; hold on;
    for i = 1 : numel(S_target_nucleus)
        coords = S_target_nucleus(i).Centroid;
        plot(coords(1), coords(2), 'ro', 'MarkerSize', 8);
    end
    axis ij
end

% Update the list of all root children
root_children = lineageTree.getchildren(1);

% Then we discard all branches that do not originate from a target nucleus
toBeDel = [];
c = 0;
for i = 1 : numel(root_children)
   
    % Decode each child
    [~, timepoint, cellIndex] = ...
        decodeValue(lineageTree.get(root_children(i)));
    
    % Get the cell coordinates
    coords = allData{timepoint}(cellIndex).Centroid;

    % Find the closest target cell
    D = pdist2(coords, ...
        reshape([S_target_nucleus.Centroid], 2, ...
        numel(S_target_nucleus))');
    if min(D) > radius
        % There is no target cell close to this; we mark the branch for
        % deletion
        c = c + 1;
        toBeDel(c) = root_children(i);
    end
    
    if DEBUG == 1
        % Plot current nucleus positions
        marker = '.';
        if min(D) > radius
            marker = 'x';
        end
        plot(coords(1), coords(2), 'k', 'Marker', marker, 'MarkerSize', 6);
    end
end

assert(numel(setdiff(root_children, toBeDel)) == numel(S_target_nucleus));

% Now delete the branches
for i = numel(toBeDel) : -1 : 1
    lineageTree = lineageTree.chop(toBeDel(i));
end
