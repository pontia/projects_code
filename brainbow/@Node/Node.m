classdef Node < handle

    properties (Access = private)
       id = [];
       children = [];
       props = struct();
   end

   methods (Access = public)

       function this = Node(id, props)
           if ~isnumeric(id) && ~isscalar(id)
               error('id must be a scalar.');
           end
           if ~isstruct(props) || ~isscalar(props)
               error('props must be a scalar structure object.');
           end
           this.id = id;
           this.props = props;
           this.children = [];
       end

       function id = getId(this)
           id = this.id;
       end

       function props = getProperty(this, propertyName)
           if isfield(this.props, propertyName)
               props = this.props.(propertyName);
           else
               error(['No property name ', propertyName, ' defined!']);
           end
       end

       function b = hasId(this, id)
           b = id == this.id;
       end

       function this = addChild(this, node)
           if ~isa(node, 'Node')
               error('node must be of class Node');
           end
           if this == node
               error('You cannot add a Node to itself!');
           end
           if isempty(this.children)
               this.children = node;
           else
               this.children(end + 1) = node;
           end

           % Do not add the same child more than once
           this.children = unique(this.children);
       end

       function c = getChildren(this)
           c = this.children;
       end

       function disp(this)
           fprintf(1, 'Node with %d child(ren).\n', numel(this.children));
       end
   end
end
