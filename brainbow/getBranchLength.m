function len = getBranchLength(tree, startingNode)

lengths = allCumulativeBranchLengths(tree, startingNode);
len = max(lengths);

function lengths = allCumulativeBranchLengths(tree, root, currentBranch, lengths)
if nargin == 2
    lengths = [];
    currentBranch = 1;
    lengths(1) = 1;
end

nextChildren = tree.getchildren(root);
while ~isempty(nextChildren)
    if numel(nextChildren) > 1
        for i = 2 : numel(nextChildren)
            lengths(numel(lengths) + 1) = lengths(currentBranch) + 1;
            lengths = allCumulativeBranchLengths(tree, nextChildren(i), numel(lengths), lengths);
        end
    end
    lengths(currentBranch) = lengths(currentBranch) + 1;
    nextChildren = tree.getchildren(nextChildren(1));
end
