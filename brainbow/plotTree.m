function plotTree(tree, allData, clr, min_track_length, maxTrackingStep, imgSize)
figure; hold on;
root_children = tree.getchildren(1);
for i = 1 : numel(root_children)
    len = getBranchLength(tree, root_children(i)); 
    if len >= min_track_length
        plotBranch(tree, root_children(i), allData, [], clr, min_track_length, maxTrackingStep, 0)
    else
        fprintf(1,'plotTree: dropped subtree of length %d.\n', len);
    end
end
title('Tracks');
legend('show')
axis ij
axis equal
hold off;
axis([1, imgSize(1) 1, imgSize(2)])

function plotBranch(tree, nodeId, allData, parentNodeId, clr, min_track_length, maxTrackingStep, trackIndexForWholeTree)

if nargin == 7
    trackIndexForWholeTree = -1;
end

if isempty(parentNodeId)
    
    % This is the beginning of a tree
    parentNodeCoords = [];
    
else
    
    % This is a branch
    assert(tree.getparent(nodeId) == parentNodeId);
    parentNode = tree.get(parentNodeId);
    [~, parentNodeTimepoint, parentNodeCellIndex] = decodeValue(parentNode);
    parentNodeCoords = allData{parentNodeTimepoint}(parentNodeCellIndex).Centroid;
end

% Get current node coordinates
nodeValue = tree.get(nodeId);
[trackIndexForNodeBranch, timepoint, cellIndex] = decodeValue(nodeValue);
nodeCoords = allData{timepoint}(cellIndex).Centroid;

if isempty(parentNodeId) || trackIndexForWholeTree == -1
    trackIndexForWholeTree = trackIndexForNodeBranch;
    fprintf(1, '* * * Starting tree for track ID %d and node %d (no parent).\n', ...
        trackIndexForWholeTree, nodeId);
    marker = 's';
else    
    fprintf(1, '    > Building branch for track ID %d (branch track ID %d) and node %d attached to coords (%f, %f).\n', ...
        trackIndexForWholeTree, trackIndexForNodeBranch, nodeId, parentNodeCoords(1), parentNodeCoords(2));
    marker = 'o';
end

% Append coordinates
coords = cat(1, parentNodeCoords, nodeCoords);

% Assert
if size(coords, 1) > 1
    if pdist2(coords(end, :), coords(end - 1, :)) >= maxTrackingStep
        fprintf(['Distance between parent node ID %d and node ID %d ', ...
            'is larger than max tracking step %f.\n'], ...
            parentNodeId, nodeId, maxTrackingStep);
    end
end

% Get the children
children = tree.getchildren(nodeId);

for childId = children
    
    % Get current node coordinates
    childNodeValue = tree.get(childId);
    if isinf(childNodeValue)
        continue;
    end
    [~, childNodeTimepoint, childNodeCellIndex] = ...
        decodeValue(childNodeValue);
    childNodeCoords = allData{childNodeTimepoint}(childNodeCellIndex).Centroid;

    % Append them
    coords = cat(1, coords, childNodeCoords);

    % Keep going
    nextChildren = tree.getchildren(childId);
    while ~isempty(nextChildren)
        
        if numel(nextChildren) > 1
            % In case of more than one direct child, we process
            % children 2 and above recursively
            for j = 2 : numel(nextChildren)
                
                % Call recursively
                plotBranch(tree, nextChildren(j), allData, ...
                    tree.getparent(nextChildren(j)), clr, ...
                    min_track_length, maxTrackingStep, ...
                    trackIndexForWholeTree);
            end
        end
        nextNodeId = nextChildren(1);
        nextNodeValue = tree.get(nextNodeId);
        if isinf(nextNodeValue)
            nextChildren = [];
        else
            [~, nextTimepoint, nextCellIndex] = decodeValue(nextNodeValue);
            coords = cat(1, coords, allData{nextTimepoint}(nextCellIndex).Centroid);
            nextChildren = tree.getchildren(nextNodeId);
        end
    end
    
    if isempty(parentNodeId)
        fprintf(1, '    o Plotting root.\n');
    else
        fprintf(1, '    o Plotting branch for node %d.\n', nodeId);
    end
    plot(coords(:, 2), coords(:, 1), '.-', ...
        'MarkerSize', 8, 'Color', clr(trackIndexForWholeTree, :), ...
        'DisplayName', num2str(trackIndexForNodeBranch));
    plot(coords(1, 2), coords(1, 1), marker, 'Color', clr(trackIndexForWholeTree, :), ...
        'MarkerSize', 10, 'DisplayName', num2str(trackIndexForNodeBranch));
end
