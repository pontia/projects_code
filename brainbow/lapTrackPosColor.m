function allData = lapTrackPosColor(allData, maxDispl, maxGap)
% Tracks objects
%
% SYNOPSIS
%
%    allData = lapTrack(allData, maxDispl, maxGap)
%
% INPUT
%
%    allData:  cell array of structures. Each structure must contain a
%              'Centroid' field with the coordinates of the object (as
%              returned by regionprops.
%    maxDispl: max allowed displacement between matched objects
%    maxGap  : max number of time points for a track to be closed.
%              Please mind: in this early version, this is only considered
%              if there are no objects at all for current timepoint!
%
% OUTPUT
%
%    allData:  augmented cell array of structures where each object is
%              assigned a .TrackIndex index.
%
% Aaron Ponti, 2017/12/21

% Initialize waitbar
hWaitbar = waitbar(0, 'Tracking...');

maxTrackIndex = 0;

foundFirstValidImage = false;
lastValidSource = 0;

for i = 1 :  numel(allData) - 1

    if ~isstruct(allData{i})
        if ~foundFirstValidImage
            continue;
        else

            % Extract the positions
            pos1 = reshape([allData{lastValidSource}.Centroid], 2, ...
                numel(allData{lastValidSource}))';

            % Inform
            fprintf(1, ['Images %d - %d: closing gap. ', ...
                'New tracks started.\n'], i, i + 1);

        end
    else

        if ~foundFirstValidImage

            % Found first valid image
            foundFirstValidImage = true;

            % Initialize tracks
            for j = 1 : numel(allData{i})
                allData{i}(j).TrackIndex = j;
                maxTrackIndex = j;
            end

        end

        % Extract the positions
        pos1 = reshape([allData{i}.Centroid], 2, numel(allData{i}))';

        % Extract the colors
        col1 = reshape([allData{i}.RGB], 3, numel(allData{i}))';

        % Mark this as last valid source
        lastValidSource = i;

    end

    if ~isstruct(allData{i + 1})
        continue;
    end

    % Extract the positions
    pos2 = reshape([allData{i + 1}.Centroid], 2, numel(allData{i + 1}))';

    % Extract the colors
    col2 = reshape([allData{i + 1}.RGB], 3, numel(allData{i + 1}))';

    % Solve the linear assignment problem
    if i - lastValidSource <= maxGap

        % Calculate distances
        D = pdist2(pos2, pos1);

        % Weight by the color similarity
        Dc = 1 - double(pdist2(col2, col1));
        assert(max(Dc(:)) <= 1.0);
        D = Dc .* D;

        D(D > maxDispl) = -1;
        try
            L = lap(D, -1, 0, 1);
            L = L(1 :  size(pos2, 1));
            assignment = find(L <= size(pos1, 1));
            new_tracks = find(L > size(pos1, 1));
        catch
            fprintf(1, ['Images %d - %d: no matches found. ', ...
                'New tracks started.\n'], i, i + 1);
            assignment = [];
            new_tracks = (1 : size(pos2, 1))';
        end

        % Assign the cells to the corresponding track
        for j = 1 : numel(assignment)
            target = L(assignment(j));
            trackIndx = allData{lastValidSource}(target).TrackIndex;
            allData{i + 1}(assignment(j)).TrackIndex = trackIndx;
        end

        % Start the new tracks
        for j = 1 : numel(new_tracks)
            maxTrackIndex = maxTrackIndex + 1;
            trackIndx = maxTrackIndex;
            allData{i + 1}(new_tracks(j)).TrackIndex = trackIndx;
        end
    else
        % Initialize new set of tracks
        for j = 1 : numel(allData{i + 1})
            maxTrackIndex = maxTrackIndex + 1;
            allData{i + 1}(j).TrackIndex = maxTrackIndex;
        end
        lastValidSource = i + 1;
    end

    % Update waitbar
    waitbar(i / (numel(allData) - 1), hWaitbar, ...
        ['Tracking (', num2str(i), ' of ', ...
        num2str(numel(allData) - 1), ')']);

end

% Close waitbar
close(hWaitbar);
