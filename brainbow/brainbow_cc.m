function [allData, lineageTree, statsValuesTotalCytoplasmIntensity, ...
    statsValuesTotalTargetedNucleusIntensity, ...
    statsValuesMeanTargetedNucleusIntensity, ...
    allCytoplasmIntensities] = brainbow_cc

minTotCytoplamIntensity = 0;
image_folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\may\tif'; % 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\april\tif';
cp_folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\may\cp_output'; % 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\april\cp_output';
basename = 'export_';
results_folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\may\matlab_output'; % 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\april\matlab_output';
numTimeDigits = 3;              % Number of digits in the time index
min_area = 100;                  % Filter out small objects
ch0 = 2;                  % First channel of the three brainbow components
ch1 = 3;                  % Second channel of the three brainbow components
ch2 = 4;                  % Third channel of the three brainbow components
ch_all_nucleus = 0;        % All nuclei channel
ch_target_nucleus = 2;    % Target nuclei channel
ch_prob = 0; %1;          % Cytoplasm channel (probabilities from ilastik)
suffix = '_Z000.tif';     % File ending for the original channels (exported from Imaris)
suffix_prob = '_Z000_Probabilities_Cytoplasm.tif';     % Cytoplasm channel
suffix_result = '_Z000_BRAINBOW_RGB.tif';    % Rainbow RGB result images
suffix_target_nucleus = '_Z000_Probabilities_Targets.tif'; % '_Z000_Probabilities_Targets.tif'; % Target nuclei channel
suffix_all_nucleus = '_Z000_Probabilities_All_Nuclei.tif'; % '_Z000_Probabilities_Targets.tif'; % Target nuclei channel
firstTimePoint = 0;
lastTimePoint = 137; % 123;
use_median = true;              % Use median intensity or total intensity
only_membrane = false;          % Only use cell membrane or whole cytoplasm
min_track_length = 10;          % Discard short-lived tracks
maxTrackingStep = 20;           % Maximum allowed displacement between timepoints
% / End of parameters

if ~exist(results_folder, 'dir')
    mkdir(results_folder);
end
    
% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

% Initialize array to store object information for tracking
allData = cell(1, numel(firstTimePoint : lastTimePoint));

% Keep track of all cytoplasm intensities for some statistical analysis
allCytoplasmIntensities = zeros(1, 10000);
currentCytoplasmIntensityNum = 0;

strgT = sprintf('%%.%dd', numTimeDigits);
n = 0;
for timepoint = firstTimePoint : lastTimePoint

    % Centroids
    C = struct('Centroid', [], 'Area', [], 'PixelList', [], ...
        'PixelIdxList', [], 'TotalCytoplasmIntensity', [], ...
        'TotalTargetedNucleusIntensity', [], ...
        'MeanTargetedNucleusIntensity', [], ...
        'RGB', []);

    % Common image channel name
    common_image_name = fullfile(image_folder, [basename, ... 
        'T', sprintf(strgT, timepoint), '_', ...
        'C%d%s']);
    common_image_name = strrep(common_image_name, '\', '/');

    % Common cell profiler channel name
    common_cp_name = fullfile(cp_folder, [basename, ... 
        'T', sprintf(strgT, timepoint), '_', ...
        'C%d%s']);
    common_cp_name = strrep(common_cp_name, '\', '/');

    % Load ch0
    img_ch0 = imread(sprintf(common_image_name, ch0, suffix));

    % Load ch1
    img_ch1 = imread(sprintf(common_image_name, ch1, suffix));

    % Load ch2
    img_ch2 = imread(sprintf(common_image_name, ch2, suffix));

    % Load image (all nuclei)
    img_all_nuclei = imread(sprintf(common_image_name, ...
        ch_all_nucleus, suffix));

    % Load segmented, connected components image (all nuclei)
    segm_all_nuclei = imread(sprintf(common_cp_name, ...
        ch_all_nucleus, suffix_all_nucleus));
    cc_all_nuclei = bwconncomp(segm_all_nuclei > 0);
    S_all_nuclei = regionprops(cc_all_nuclei, ...
        img_all_nuclei, 'Centroid', 'PixelValues');

    % For the first timepoint, we also extract the position of the target
    % nuclei
    if timepoint == firstTimePoint
    
        % Load image (all nuclei)
        img_target_nuclei = imread(sprintf(common_image_name, ...
            ch_target_nucleus, suffix));
        
        % Load segmented, connected components image (target nuclei)
        segm_target_nuclei = imread(sprintf(common_cp_name, ...
            ch_target_nucleus, suffix_target_nucleus));
        cc_target_nucleus = bwconncomp(segm_target_nuclei > 0);
        S_target_nucleus = regionprops(cc_target_nucleus, ...
            img_target_nuclei, 'Centroid', 'PixelValues');
        
    end
    
    % Load segmented, connected components image (cytoplasm)
    segm_cytoplasm = imread(sprintf(common_cp_name, ch_prob, suffix_prob));
    num_cc = unique(segm_cytoplasm(:));
    num_cc(num_cc == 0) = [];

    % Create RGB image
    [y, x] = size(img_ch0);

    rgb_final = zeros([y, x, 3], 'uint8');
    r_final = zeros([y, x], 'uint8');
    g_final = zeros([y, x], 'uint8');
    b_final = zeros([y, x], 'uint8');

    for c = 1 : numel(num_cc)

        BW = false(size(segm_cytoplasm));
        BW(segm_cytoplasm == num_cc(c)) = true;

        % Find connected components
        cc = bwconncomp(BW);
        S = regionprops(cc, 'Centroid', 'Area', 'PixelList', ...
            'PixelIdxList');
        if numel(S) > 1
            T = struct('Centroid', [], 'Area', [], 'PixelList', [], ...
                'PixelIdxList', []);
            T.Area = sum([S.Area]);
            PixelList = [];
            PixelIdxList = [];
            for k = 1 : numel(S)
                PixelList = cat(1, PixelList, S(k).PixelList);
                PixelIdxList = cat(1, PixelIdxList, S(k).PixelIdxList);
            end
            T.Centroid = mean(PixelList, 1);
            T.PixelList = PixelList;
            T.PixelIdxList = PixelIdxList;
            S = T;
        end
        clear('cc');

        if S.Area < min_area
            continue;
        end

        % Find the closest target nucleus and extract the intensity
        D_target_nucleus = pdist2(S.Centroid, ...
            reshape([S_all_nuclei.Centroid], 2, ...
            numel(S_all_nuclei))');   
        indx = find(D_target_nucleus == min(D_target_nucleus), 1);
        S.TotalTargetedNucleusIntensity = sum(S_all_nuclei(indx).PixelValues);
        S.MeanTargetedNucleusIntensity = mean(S_all_nuclei(indx).PixelValues);

        if only_membrane == true

            % Extract the cell boundary
            BW = getMembrane(cc.ImageSize, S);
            indices = find(BW);

            % Extract the intensities from the three channels for
            % current label
            r = img_ch0(indices);
            g = img_ch1(indices);
            b = img_ch2(indices);

        else

            % Extract the intensities from the three channels for
            % current label
            r = img_ch0(S.PixelIdxList);
            g = img_ch1(S.PixelIdxList);
            b = img_ch2(S.PixelIdxList);

            indices = S.PixelIdxList;
        end

        if use_median == true
            % Median intensities
            mR = median(single(r(:)));
            mG = median(single(g(:)));
            mB = median(single(b(:)));

            % Total intensity
            tot = mR + mG + mB;

            if tot < minTotCytoplamIntensity
                tot = 0;
                % Ratios
                rR = 0;
                rG = 0;
                rB = 0;
            else
                % Ratios
                rR = mR ./ tot;
                rG = mG ./ tot;
                rB = mB ./ tot;
            end
            
        else
            % Total intensity
            tot = single(sum(r(:))) + single(sum(g(:))) + single(sum(b(:)));
            
            if tot < minTotCytoplamIntensity
                tot = 0;
                % Ratios
                rR = 0;
                rG = 0;
                rB = 0;
            else
                % Ratios
                rR = sum(single(r(:))) ./ tot;
                rG = sum(single(g(:))) ./ tot;
                rB = sum(single(b(:))) ./ tot;
            end

        end
        
        % Store the total cytoplasm intensity for durther statistical
        % analysis
        currentCytoplasmIntensityNum = currentCytoplasmIntensityNum + 1;
        allCytoplasmIntensities(currentCytoplasmIntensityNum) = tot;
 
        % Store the total intensity
        S.TotalCytoplasmIntensity = tot / ...
            numel(S_all_nuclei(indx).PixelValues);

        % Store the relative color components (RGB)
        S.RGB = double([rR, rG, rB]);

        % Append current S
        C = cat(2, C, S);

        % Stretch to 255 and put back
        r_final(indices) = uint8(255 * rR);
        g_final(indices) = uint8(255 * rG);
        b_final(indices) = uint8(255 * rB);

    end

    C(1) = [];
    allData{n + 1} = C;

    % Add the nuclei
    values = unique(segm_all_nuclei);
    newValues = linspace(0, 255, numel(values));
    values(values == 0) = [];
    newValues(newValues == 0) = [];
    for k = 1 : numel(values)
        currIndex = segm_all_nuclei == values(k);
        r_final(currIndex) = newValues(k);
        g_final(currIndex) = newValues(k);
        b_final(currIndex) = newValues(k);
    end
    
    % Merge
    rgb_final(:, :, 1) = r_final;
    rgb_final(:, :, 2) = g_final;
    rgb_final(:, :, 3) = b_final;

    % Output file name
    output_name = fullfile(results_folder, [basename, ...
        'T', sprintf(strgT, timepoint), suffix_result]);
    output_name = strrep(output_name, '\', '/');

    % Save
    imwrite(rgb_final, output_name);

    % Update waitbar
    n = n + 1;
    waitbar(n / (lastTimePoint - firstTimePoint + 1), hWaitbar);

end

% Keep only the intensities that were actually stored
allCytoplasmIntensities = ...
    allCytoplasmIntensities(1:currentCytoplasmIntensityNum);

% Close waitbar
close(hWaitbar);

% Track cells
allData = lapTrackPosColor(allData, maxTrackingStep, 0);

% Assess tracking quality
assessTrackingQuality(allData, maxTrackingStep);

% Build a tree with the tracks
lineageTree = buildLineageTree(allData, maxTrackingStep);

% Prune tree from all branches that do not start from a target nucleus
lineageTree = pruneLineageTree(lineageTree, allData, S_target_nucleus, 5);

% TEMP = Replace empty values with Inf for sorting (plot)
iterator = lineageTree.depthfirstiterator;
for i = iterator
    n = lineageTree.get(i);
    if isempty(n)
        lineageTree = lineageTree.set(i, Inf);
    end
end

% Assess tree quality
lineageTree = assessTreeQuality(lineageTree, allData);

figure;plot(lineageTree);

% Count tracks
maxTrackIndex = -1;
for i = 1 : numel(allData)
   S = allData{i};
   m = max([S.TrackIndex]);
   if m > maxTrackIndex
       maxTrackIndex = m;
   end
end

% Create colors for tracks
clr = -1 .* ones(maxTrackIndex, 3);
for i = 1 : maxTrackIndex
    clr(i, :) = rand(1, 3);
end

% Plot tree
imgSize = size(rgb_final); imgSize = imgSize(1:2);
plotTree(lineageTree, allData, clr, min_track_length, maxTrackingStep, imgSize);

% Plot the intensity measurements
statsValuesTotalCytoplasmIntensity = ...
    plotTreeScalarStatistic(lineageTree, allData, 'TotalCytoplasmIntensity', ...
    'Total Cytoplasm Intensity', min_track_length);

statsValuesTotalTargetedNucleusIntensity = ...
    plotTreeScalarStatistic(lineageTree, allData, 'TotalTargetedNucleusIntensity', ...
    'Total Targeted Nucleus Intensity', min_track_length);

statsValuesMeanTargetedNucleusIntensity = ...
    plotTreeScalarStatistic(lineageTree, allData, 'MeanTargetedNucleusIntensity', ...
    'Mean Targeted Nucleus Intensity', min_track_length);

% =========================================================================

function BW = getMembrane(ImageSize, S)

BW = false(ImageSize);
BW(S.PixelIdxList) = true;
BW = imfill(BW, 'holes');
erodedBW = imerode(BW, strel('disk', 3));
BW = BW - erodedBW;
