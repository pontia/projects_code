classdef LineageTree < handle
   properties (Access = private)
       tracks = containers.Map('KeyType', 'uint64', 'ValueType', 'any');
   end
   
   methods (Access = public)
       
       function this = LineageTree()
           this.tracks = containers.Map('KeyType', 'uint64', ...
               'ValueType', 'any');
       end

       function addTrack(this, track)
           this.tracks(track.getIndex()) = track;
       end

       function track = getTrack(this, index)
           track = this.tracks(index);
       end
       
       function b = hasTrack(this, index)
           b = this.tracks.isKey(index);
       end

       function n = getNumberOfTracks(this)
           n = numel(this.tracks.keys());
       end
       
       function indices = getTrackIndices(this)
           indices = cell2mat(this.tracks.keys());
       end

       function numTimePoints = totalNumberOfTimepoints(this)
           numTimePoints =0;
           indices = this.getTrackIndices();
           for i = 1 : numel(indices)
               if this.getTrack(indices(i)).getLastTimePoint() > numTimePoints
                   numTimePoints = this.getTrack(indices(i)).getLastTimePoint();
               end
           end
       end
       
       function disp(this)
           fprintf(1, 'LineageTree with %d tracks.\n', numel(this.tracks.keys()));
       end
       
       function findCellDivisions(this, searchRadius)
           error('IMPLEMENT ME');
           for i = 1 : this.totalNumberOfTimepoints() - 1
               pos0 = this.getAllPositionsAtTimePoint(i);
               pos1 = this.getAllPositionsAtTimePoint(i + 1);
           end
       end
       
       function pos = getAllPositionsAtTimePoint(this, timepoint)
           indices = this.getTracksIndicesContainingTimepoint(timepoint);
           error('IMPLEMENT ME');
       end
       
       function indices = getTracksIndicesContainingTimepoint(this, timepoint)
           indices = [];
           keys = this.tracks.keys();
           for i = 1 : numel(keys)
               track = this.tracks(keys{i});
               assert(keys{i} == track.getIndex())
               if track.containsTimepoint(timepoint)
                   indices = cat(1, indices, keys{i});
               end
           end
       end
           
   end
   
end
