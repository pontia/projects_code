function brainbow

% RGB = imread('RGB_Z000.tif');
% labels = imread('2016-10-29 09-25 N8-1-40X(1) 36h after recombination_C5_Z000_LABEL.tiff');

% Parameters
folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\20171122\inset_2\'; % 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\201711 dataset\tiff';
result_folder = 'F:\Data\Projects\hanyu\Brainbow_Hanyu_Qin\20171122\inset_2\result_new';
basename = 'inset_2_'; % 'B_cropped_sum_projected_'; % 'cropped_projected_';
ch0 = 2;
ch1 = 3;
ch2 = 4;
ch_prob = 5;
suffix = '_Z000.tif';
suffix_prob = '_Z000_Probabilities_.tiff';
suffix_result = '_Z000_BRAINBOW_RGB.tiff';
firstTimePoint = 0;
lastTimePoint = 43;
min_area = 800; % 1100;
use_median = true;
only_membrane = true;
% / End of parameters

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

strgT = sprintf('%%.%dd', length(num2str(lastTimePoint)));
allDone = false;
n = 0;
for timepoint = firstTimePoint : lastTimePoint

    % Common channel name
    common_name = fullfile(folder, [basename, ... 
        'T', sprintf(strgT, timepoint), '_', ...
        'C%d%s']);
    common_name = strrep(common_name, '\', '/');
    
    % Load ch0
    img_ch0 = imread(sprintf(common_name, ch0, suffix));
    
    % Load ch1
    img_ch1 = imread(sprintf(common_name, ch1, suffix));
    
    % Load ch2
    img_ch2 = imread(sprintf(common_name, ch2, suffix));

    % Load probabilities
    p = imread(sprintf(common_name, ch_prob, suffix_prob));
    BW = p > 127;

    % Create RGB image
    [y, x] = size(img_ch0);

    rgb_final = zeros([y, x, 3], 'uint8');
    r_final = zeros([y, x], 'uint8');
    g_final = zeros([y, x], 'uint8');
    b_final = zeros([y, x], 'uint8');

    % Find connected components
    cc = bwconncomp(BW);

    for i = 1 : cc.NumObjects
    
        if numel(cc.PixelIdxList{i}) < min_area
            continue;
        end
    
        if only_membrane == true

            % Extract the cell boundary
            BW = getMembrane(cc, i);
            indices = find(BW);
            
            % Extract the intensities from the three channels for
            % current label
            r = img_ch0(indices);
            g = img_ch1(indices);
            b = img_ch2(indices);
            
        else
            
            % Extract the intensities from the three channels for
            % current label
            r = img_ch0(cc.PixelIdxList{i});
            g = img_ch1(cc.PixelIdxList{i});
            b = img_ch2(cc.PixelIdxList{i});

            indices = cc.PixelIdxList{i};
        end
        
        if use_median == true
            % Median intensities
            mR = median(single(r(:)));
            mG = median(single(g(:)));
            mB = median(single(b(:)));
            
            % Total intensity
            tot = mR + mG + mB;

            % Ratios
            rR = mR ./ tot;
            rG = mG ./ tot;
            rB = mB ./ tot;
        else
            % Total intensity
            tot = single(sum(r(:))) + single(sum(g(:))) + single(sum(b(:)));
            
            % Ratios
            rR = sum(single(r(:))) ./ tot;
            rG = sum(single(g(:))) ./ tot;
            rB = sum(single(b(:))) ./ tot;
        end

        % Stretch to 255 and put back
        r_final(indices) = uint8(255 * rR);
        g_final(indices) = uint8(255 * rG);
        b_final(indices) = uint8(255 * rB);
    
    end
    
    % Merge
    rgb_final(:, :, 1) = r_final;
    rgb_final(:, :, 2) = g_final;
    rgb_final(:, :, 3) = b_final;

    % Output file name
    output_name = fullfile(folder, [basename, ...
        'T', sprintf(strgT, timepoint), suffix_result]);
    output_name = strrep(output_name, '\', '/');
    
    % Save
    imwrite(rgb_final, output_name);

    % Update waitbar
    n = n + 1;
    waitbar(n / (lastTimePoint - firstTimePoint + 1), hWaitbar);
    
end

% Close waitbar
close(hWaitbar);

function BW = getMembrane(cc, i)

BW = false(cc.ImageSize);
BW(cc.PixelIdxList{i}) = true;
BW = imfill(BW, 'holes');
erodedBW = imerode(BW, strel('disk', 3));
BW = BW - erodedBW;

