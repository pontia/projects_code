function plotImarisStats(allIntensityMean, allTime, allTrackID, allFiles, ...
    numTracks, fileNum, randomTracks, pointCloud, yLim, openCFTool)
% Plot Imaris statistics processed by processImarisStats().
%
% INPUT 
%
%   allIntensityMean: vector of all intensities as returned by processImarisStats()
%   allTime         : vector of all time points as returned by processImarisStats()
%   allTrackID      : vector of all track IDs as returned by processImarisStats()
%   allFiles        : vector of all file names as returned by processImarisStats()
%   numTracks       : number of tracks to be plotted from one of the files
%   fileNum         : number of the files for which tracks are plotted; set
%                     to 0 to pick one file at random. Set to Inf to plot
%                     them all.
%   randomTracks    : whether to pick the track IDs to be plotted are
%                     random or not.
%   pointCloud      : whether to plot only the data points without lines (1),
%                     or not (0).
%   yLim            : limits for the y axis (set to [] to adjust automatcally)
%   openCFTool      : whether to open the extracted data in the cftool

% Markers
markers = {'o', '+', '*', '.', 'x', 's', 'd', '^', 'v', '>', '<', 'p', 'h'};

% Pick a file at random
uFiles = unique(allFiles);
nFiles = numel(uFiles);
if fileNum == 0
    n = randi(numel(uFiles), 1);
else
    n = fileNum;
end
if n > nFiles
    disp(['Sorry, there are only ', num2str(nFiles), ' files.']);
    return;
end

% Extract the measurements for the (randomly) selected file
finder = @(x) strcmp(x, uFiles{n});
fIndices = cellfun(finder, allFiles);

% Total number of tracks for this file
trackIDs = allTrackID(fIndices);
uTrackID = unique(trackIDs);
totNumTracks = numel(uTrackID);
if numTracks == 0
    numTracks = totNumTracks; 
end

% Pick numTracks 
if numTracks > totNumTracks
    numTracks = totNumTracks;
end

% If the tracks should be picked at random, sample numTracks tracks
if randomTracks == 1
    indices = randperm(totNumTracks, numTracks);
else
    indices = 1 : numTracks;
end

% Extract the time points and the intensities for the chosen file
time = allTime(fIndices);
meanInt = allIntensityMean(fIndices);

% Should we draw the line?
if pointCloud == 1
    lineStyle = 'none';
else
    lineStyle = '-';
end

% Prepare space to store data for the fit
xData = [];
yData = [];

% Plot
figure; hold on;
for i = 1 : numTracks
    trackID = uTrackID(indices(i));
    indx = trackIDs == trackID;
    plot(time(indx), meanInt(indx), ...
        'Marker', markers{1 + mod(i, numel(markers))}, ...
        'LineStyle', lineStyle, ...
        'DisplayName', num2str(trackID));
    
    % Store data for the fit
    xData = cat(1, xData, time(indx));
    yData = cat(1, yData, meanInt(indx));
end
if ~isempty(yLim)
    set(gca, 'YLim', yLim);
end

fileName = unique(allFiles(fIndices));
title([fileName{1}, ' (', num2str(n), ')'], 'Interpreter', 'none');
if randomTracks == 1
    randomStr = ' (at random)';
else
    randomStr = '';
end
xlabel(['Total number of tracks in file: ', ...
    num2str(totNumTracks), '; plotting ', ...
    num2str(numTracks), randomStr]);
% legend();

% Fit an exponential

% Set up fittype and options
ft = fittype('exp1');
opts = fitoptions('Method', 'NonlinearLeastSquares');

% Fit model to data
fitresult = fit(xData, yData, ft, opts);

% Apply the model to get the predicted values
x = unique(xData);
yhat = feval(fitresult, x);

hold on
plot(x, yhat, 'r-', 'LineWidth', 3, ...
    'DisplayName', ['y = ', num2str(fitresult.a), ' * ', ...
    'exp(', num2str(fitresult.b), ' * x)']);
legend('Location', 'bestoutside');

if openCFTool == 1
    cftool(xData, yData);
end

