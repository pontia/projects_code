function extractMeanVesicleShape(rootDirName, firstTimePoint)

if nargin ~= 2
    error('2 input parameters expected.');
end

% Working directories
dirName = fullfile(rootDirName, 'gray');
outDirName = dirName;
if ~exist(outDirName, 'dir')
    mkdir(outDirName);
end

d = dir(dirName);

% Quality control dir
qcDirName = fullfile(outDirName, 'QC');
if ~exist(qcDirName, 'dir')
    mkdir(qcDirName);
end

expression = '^(?<body>.+)_(?<suffix>\d+)\.tif$';

% =========================================================================
%
% Extract the vesicle
%
% =========================================================================

hWaitbar = waitbar(0, 'Please wait while extracting average vesicle...');

% Accumulator
A = [];

% Store properties
regionProperties = cell(1, numel(d));

n = 0;
for i = 1 : numel(d)

    if d(i).isdir
        waitbar(i/numel(d), hWaitbar);
        continue
    end

    % Check the name
    tokens = regexp(d(i).name, expression, 'tokens');
    if isempty(tokens) || numel(tokens) ~= 1 && numel(tokens{1}) ~= 2
        continue;
    end
    
    % Extract time index
    timeIndex = str2double(tokens{1}{2});
    if isnan(timeIndex)
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end
    
    % If the time index is lower than firstTimepoint we skip it
    if timeIndex < firstTimePoint
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end

    % Read the image
    img = imread(fullfile(dirName, d(i).name));

    % Initialize the accumulator
    if isempty(A)
        A = zeros(size(img));
    end

    % Estimate background
    %     [~, t] = iterativeThreshold(img, 127);
    %    t = multithresh(img, 2);
    t = 255 .* graythresh(img);
    I = img >= t;

    % Measure features
    r = regionprops(logical(I), img, ...
        {'Area', 'ConvexImage', 'BoundingBox', 'MaxIntensity', ...
        'MajorAxisLength', 'MinorAxisLength', 'EquivDiameter', ...
        'Eccentricity', 'Centroid'});
    r = r([r.Area] == max([r.Area]));
    
    % Get the whole vesicle
    L = false(size(img));
    x0 = 1 + fix(r.BoundingBox(1));
    x  = x0 + r.BoundingBox(3) - 1;
    y0 = 1 + fix(r.BoundingBox(2));
    y  = y0 + r.BoundingBox(4) - 1;
    L(y0:y, x0:x) = r.ConvexImage;
    r.FullConvexImage = L;
    
    % Store them
    n = n + 1;
    regionProperties{n} = r;

    % Update waitbar
    waitbar(i/numel(d), hWaitbar);

end

% Close the waitbar
close(hWaitbar);

if n == 0
    disp([dirName, ': No vesicles found!']);
    return
end

regionProperties = regionProperties(1:n);

% =========================================================================
%
% Get the 'ideal' vesicle mask
%
% =========================================================================

criteria = zeros(1, numel(regionProperties));
for i = 1 : numel(regionProperties)
   
    bbox = sort([regionProperties{i}.BoundingBox(3), ...
        regionProperties{i}.BoundingBox(4)]);
    ratio = bbox(1) / bbox(2);

    criteria(i) = ratio;

end

% The 'best' vesicle is the intersection of all the ones with at least 90%
% of the maximum criterium
[~, sortedIndices] = sort(criteria, 'descend');
A = double(regionProperties{sortedIndices(1)}.FullConvexImage);
for i = 2 : numel(find(criteria >= 1.00 * max(criteria)))
    A = A + double(regionProperties{sortedIndices(i)}.FullConvexImage);
end

% Save accumulator
accFName = fullfile(outDirName, 'Accumulator.mat');
save(accFName, 'A');

% Save vesicle image quality control
qcOutFName = fullfile(qcDirName, 'Accumulator.tif');
imwrite(nrm(A, 8), qcOutFName);

