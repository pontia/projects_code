function prepareImagesForVesicleExtractionBatch()

rootDirNames = { ...
    'F:\Data\Projects\petra_dittrich\new\control-no-flow', ...
    'F:\Data\Projects\petra_dittrich\new\Typ3-22deg', ...
    'F:\Data\Projects\petra_dittrich\new\Typ1-22deg', ...
    'F:\Data\Projects\petra_dittrich\new\typ1-t-elevated'};

for n = 1 : numel(rootDirNames)
    
    rootDirName = rootDirNames{n};
    
    prepareImagesForVesicleExtraction(rootDirName, 6, 3);
    
end
