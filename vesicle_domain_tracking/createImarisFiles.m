function createImarisFiles(conn, dirName)

% Top folders
datasetTopDir = fullfile(dirName, 'dataset', 'top');
if ~exist(datasetTopDir, 'dir')
    disp('Could not find top dataset files! Skipping.');
    return;
end
outputTopFileName = fullfile(dirName, 'top.ims');

% Bottom folders
datasetBotDir = fullfile(dirName, 'dataset', 'bottom');
outputBotFileName = fullfile(dirName, 'bottom.ims');
if ~exist(datasetBotDir, 'dir')
    disp('Could not find bottom dataset files! Skipping.');
    return;
end

% Read metadata
try
    S = load(fullfile(dirName, 'metadata.mat'), 'metadata');
catch
    disp('Could not find metadata.mat file! Skipping.');
    return
end

% Create top file
create(conn, datasetTopDir, outputTopFileName, S.metadata);

% Create bottom file
create(conn, datasetBotDir, outputBotFileName, S.metadata);


% =========================================================================

function create(conn, datasetDir, outputFileName, metadata)

% Try picking the first TIF file
d = dir(datasetDir);
firstFileName = '';
found = 0;
i = 1;
while ~found
    if ~d(i).isdir
        [~, ~, e] = fileparts(d(i).name);
        if strcmpi(e, '.tif')
            firstFileName = fullfile(datasetDir, d(i).name);
            found = 1;
        end
    end
    i = i + 1;
end

% Try opening the file in Imaris
conn.mImarisApplication.FileOpen(firstFileName, 'reader="TiffSeries"');

% Get dataset
aDataSet = conn.mImarisApplication.GetDataSet();

% Set colors
try
    aDataSet.SetChannelColorRGBA(0, conn.mapRgbaVectorToScalar([1 0 0 0]));
    aDataSet.SetChannelColorRGBA(1, conn.mapRgbaVectorToScalar([0 1 0 0]));
    aDataSet.SetChannelColorRGBA(2, conn.mapRgbaVectorToScalar([0 0 1 0]));
    aDataSet.SetChannelColorRGBA(3, conn.mapRgbaVectorToScalar([0 1 1 0]));
catch
    disp('Could not set all channel colors.');
end

% Set the voxel size (Z is set to be the same as X since it is a 2D image)
conn.setVoxelSizes([metadata.voxelX, metadata.voxelY, metadata.voxelX]);

% Since the time stamps are absolute times (from some given starting 
% point), we build the time stamps on today's date starting at 0:00:00
today = datevec(now());
today(4:6) = [0 0 0];
timeSteps = [0 diff(metadata.timestamps)];

% Build and set the timestamps. Since we might have dropped some of the
% initial timepoints, we only keep writing time stamps until we run out of
% timepoints in the dataset
for i = 1 : min(metadata.nTimepoints, aDataSet.GetSizeT())
    
    % Set current timestamp
    try
        aDataSet.SetTimePoint(i - 1, ...
            mapTimeToTimeString(timeSteps(i), today));
    catch
        % Skip
    end
end

% Save the file
conn.mImarisApplication.FileSave(outputFileName, 'writer="Imaris5"');

% -------------------------------------------------------------------------

function timeString = mapTimeToTimeString(time, today)

today(6) = today(6) + time;
timeString = datestr(today, 'yyyy-mm-dd HH:MM:SS.FFF');

