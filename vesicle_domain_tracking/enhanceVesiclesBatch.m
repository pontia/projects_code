function enhanceVesiclesBatch(conn)

rootDirNames = { ...
    'F:\Data\Projects\petra_dittrich\new\control-no-flow', ...
    'F:\Data\Projects\petra_dittrich\new\Typ3-22deg', ...
    'F:\Data\Projects\petra_dittrich\new\Typ1-22deg', ...
    'F:\Data\Projects\petra_dittrich\new\typ1-t-elevated'};

useAccumulator = 1;
forceAccumulator = 1;
firstTimepoint = 6;

for n = 1 : numel(rootDirNames)
    
    rootDirName = rootDirNames{n};
    
    d = dir(rootDirName);
    
    for i = 1 : numel(d)
        
        if d(i).isdir
            
            if strcmp(d(i).name, '.') || strcmp(d(i).name, '..')
                continue;
            end
            
            currDir = fullfile(rootDirName, d(i).name);
            
            try
                extractMeanVesicleShape(currDir, firstTimepoint);
                
                enhanceVesiclesAlternativeVesicleDetector(currDir, ...
                    useAccumulator, forceAccumulator, firstTimepoint);

                assembleDataset(currDir)
                
                createImarisFiles(conn, currDir);
                
             catch
                
                disp(['Skipping ', fullfile(rootDirName, d(i).name), '...']);
                % Skip
                
            end
            
        end
        
        
    end
end
