function prepareImagesForVesicleExtraction(dirName, firstTimepoint, subtrFactor)

if nargin == 0
    %dirName = 'F:\Data\Projects\petra_dittrich\new\Typ3-22deg\';
    %dirName = 'F:\Data\Projects\petra_dittrich\new\Typ1-22deg\';
    dirName = 'F:\Data\Projects\petra_dittrich\new\typ1-t-elevated\';
    firstTimepoint = 6;
    subtrFactor = 3;
end

d = dir(dirName);

strg = sprintf('%%.%dd', 4);

H = fspecial('gaussian', [25 25], 5);
Hsmall = fspecial('gaussian', [5 5], 1);

for i = 1 : numel(d)

    if d(i).isdir
        continue;
    end
        
    [~, b, e] = fileparts(d(i).name);
    
    if ~strcmpi(e, '.lsm')
        continue;
    end
        
    fileName = fullfile(dirName, d(i).name);

    [dataset, metadata] = loadGeneric(fileName); %#ok<ASGLU>
    if isempty(dataset)
        continue
    end

    % Dataset folder name
    baseDataDir = fullfile(dirName, strrep(b, ' ', '_'));
    
    % Store the metadata information
    save(fullfile(baseDataDir, 'metadata.mat'), 'metadata');
    
    nTimepoints = size(dataset, 1);
    
    % Create output dirs
    allDirName = fullfile(baseDataDir, 'gray');
    rgbDirName = fullfile(baseDataDir, 'rgb');
    grayTopDirName = fullfile(allDirName, 'top');
    grayBotDirName = fullfile(allDirName, 'bottom');
    rgbTopDirName = fullfile(rgbDirName, 'top');
    rgbBotDirName = fullfile(rgbDirName, 'bottom');
    
    if ~exist(allDirName, 'dir')
        mkdir(allDirName);
    end
    if ~exist(grayTopDirName, 'dir')
        mkdir(grayTopDirName);
    end
    if ~exist(grayBotDirName, 'dir')
        mkdir(grayBotDirName);
    end
    if ~exist(rgbDirName, 'dir')
        mkdir(rgbDirName);
    end
    if ~exist(rgbTopDirName, 'dir')
        mkdir(rgbTopDirName);
    end
    if ~exist(rgbBotDirName, 'dir')
        mkdir(rgbBotDirName);
    end
   
    hWaitbar = waitbar(0, 'Processing...');
    
    for timepoint = 1 : nTimepoints
        
        if timepoint < firstTimepoint
            continue;
        end

        % Filter channels
        stackOne = dataset{timepoint, 1};
        stackTwo = dataset{timepoint, 2};
        outStackOne = zeros(size(stackOne), 'like', stackOne);
        outStackTwo = zeros(size(stackOne), 'like', stackOne);
        outStack = zeros(size(stackOne), 'like', stackOne);
        for z = 1 : size(stackOne, 3)
            
            % Channel 1
            outStackOne(:, :, z) = medfilt2(stackOne(:, :, z), [3 3], 'symmetric');
            
            % Channel 2
            outStackTwo(:, :, z) = medfilt2(stackTwo(:, :, z), [3 3], 'symmetric');
            
            % Combined: this will be enhanced in enhanceVesicle.m
            tmp =  single(outStackOne(:, :, z)) - ...
                subtrFactor * single(outStackTwo(:, :, z));
            tmp = imfilter(tmp, Hsmall, 'symmetric');
            outStack(:, :, z) = cast(tmp, 'like', stackOne);
        end
        
        % All together
        proj = max(outStack, [], 3);
        
        t = multithresh(proj, 3);
        M = proj > t(1);
        
        V = imfilter(proj, H, 'symmetric');
        V = uint8(M.* single(V));
        
        outFileName = fullfile(allDirName, ['frame_', sprintf(strg, timepoint), '.tif']);
        imwrite(V, outFileName);
        
        % Bottom
        halfZ = fix(size(outStack, 3) / 2);
        proj = max(outStack(: ,:, 1 : halfZ), [], 3);
        t = multithresh(proj, 3);
        M = proj > t(1);
        V = imfilter(proj, Hsmall, 'symmetric');
        Vtop = uint8(M.* single(V));
        
        outFileName = fullfile(grayBotDirName, ['frame_', sprintf(strg, timepoint), '.tif']);
        imwrite(Vtop, outFileName);
        
        % Top
        proj = max(outStack(: ,:, halfZ + 1 : end), [], 3);
        t = multithresh(proj, 3);
        M = proj > t(1);       
        V = imfilter(proj, Hsmall, 'symmetric');
        Vbottom = uint8(M.* single(V));
        
        outFileName = fullfile(grayTopDirName, ['frame_', sprintf(strg, timepoint), '.tif']);
        imwrite(Vbottom, outFileName);
        
        % Save RGBs
        
        % Bottom
        outRGBTopFileName = fullfile(rgbBotDirName, ['frame_', sprintf(strg, timepoint), '.tif']);
        RGB = zeros([size(V), 3], 'like', stackOne);
        RGB(:, :, 1) = max(outStackOne(: ,:, 1 : halfZ), [], 3);
        RGB(:, :, 2) = max(outStackTwo(: ,:, 1 : halfZ), [], 3);
        imwrite(RGB, outRGBTopFileName);

        % Top
        outRGBBotFileName = fullfile(rgbTopDirName, ['frame_', sprintf(strg, timepoint), '.tif']);
        RGB = zeros([size(V), 3], 'like', stackOne);
        RGB(:, :, 1) = max(outStackOne(: ,:, halfZ + 1 : end), [], 3);
        RGB(:, :, 2) = max(outStackTwo(: ,:, halfZ + 1 : end), [], 3);
        imwrite(RGB, outRGBBotFileName);
        
        waitbar(timepoint/nTimepoints, hWaitbar);
    
    end
    
    close(hWaitbar);
end
