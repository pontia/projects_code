function enhanceVesicles(dirName, outDirName, useAccumulator, forceAccumulator, firstTimePoint)

if nargin ~=5
    error('5 input parameters expected.');
end

if ~exist(outDirName, 'dir')
    mkdir(outDirName);
end

d = dir(dirName);

% Quality control dir
qcDirName = fullfile(outDirName, 'QC');
if ~exist(qcDirName, 'dir')
    mkdir(qcDirName);
end

expression = '^(?<body>.+)_(?<suffix>\d+)\.tif$';

% =========================================================================
%
% Extract the vesicle
%
% =========================================================================

hWaitbar = waitbar(0, 'Please wait while extracting average vesicle...');

% Accumulator
A = [];

for i = 1 : numel(d)

    if d(i).isdir
        waitbar(i/numel(d), hWaitbar);
        continue
    end

    % Check the name
    tokens = regexp(d(i).name, expression, 'tokens');
    if numel(tokens) ~= 1 && numel(tokens{1}) ~= 2
        error('Unexpected file name!');
    end
    
    % Extract time index
    timeIndex = str2double(tokens{1}{2});
    if isnan(timeIndex)
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end
    
    % If the time index is lower than firstTimepoint we skip it
    if timeIndex < firstTimePoint
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end

    % Read the image
    img = imread(fullfile(dirName, d(i).name));

    % Initialize the accumulator
    if isempty(A)
        A = zeros(size(img));
    end

    % Estimate background
    %     [~, t] = iterativeThreshold(img, 127);
    %    t = multithresh(img, 2);
    t = 255 .* graythresh(img);
    I = img >= t;

    % Get the largest object
    r = regionprops(logical(I), img, ...
        {'Area', 'ConvexImage', 'BoundingBox', 'MaxIntensity'});
    c = find([r.Area] == max([r.Area]));
    if isempty(c)
        continue;
    end

    % Get the whole vesicle
    L = false(size(img));
    x0 = 1 + fix(r(c).BoundingBox(1));
    x  = x0 + r(c).BoundingBox(3) - 1;
    y0 = 1 + fix(r(c).BoundingBox(2));
    y  = y0 + r(c).BoundingBox(4) - 1;
    L(y0:y, x0:x) = r(c).ConvexImage;

    % Accumulate it
    A = A + double(L);

    % Update waitbar
    waitbar(i/numel(d), hWaitbar);

end

% Save accumulator quality control
qcOutFName = fullfile(qcDirName, 'Accumulator.tif');
imwrite(nrm(A, 8), qcOutFName);

% Close the waitbar
close(hWaitbar);

% =========================================================================
%
% Process all vesicles
%
% =========================================================================

hWaitbar = waitbar(0, 'Please wait while enhancing signal...');

% We consider a sort of median area of accumulation
if useAccumulator == 1
    if forceAccumulator == 1
       W = A > 0; 
    else
        levels = unique(A(:));
        levels(levels == 0) = [];
        nLevels = numel(levels);
        area = zeros(1, nLevels);
        for i = nLevels : - 1 : 1
            area(i) = numel(find(A >= levels(i)));
        end
        diffArea = abs(area - median(area));
        level = levels(find(diffArea == min(diffArea), 1));
        W = A >= level;
    end
else
    W = true(size(A));
end

% We also create a version of the accumulator that suppresses the border
% se = strel('disk', 7);
% S = imerode(A, se);
S = W;  % Currently not used.

for i = 1 : numel(d)

    if d(i).isdir
        waitbar(i/numel(d), hWaitbar);
        continue
    end

    % Check the name
    tokens = regexp(d(i).name, expression, 'tokens');
    if numel(tokens) ~= 1 && numel(tokens{1}) ~= 2
        error('Unexpected file name!');
    end
    
    % Extract time index
    timeIndex = str2double(tokens{1}{2});
    if isnan(timeIndex)
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end
    
    % If the time index is lower than firstTimepoint we skip it
    if timeIndex < firstTimePoint
        disp(['Skipping file ', d(i).name, '...']);
        continue;
    end

    % Read the image
    img = imread(fullfile(dirName, d(i).name));

    % Working copy
    fImg = double(img);

    % Internal "background"
    bkg = multithresh(fImg(W), 1);

    % Now suppress the foreground and invert the image
    fImg = bkg - fImg;
    fImg(fImg < 0) = 0;

    % Now we apply the eroded accumulator
    fImg = S.* fImg;

    % Change back to 8 bit
    fImg = uint8(fImg);

    % Output file name
    [~, b] = fileparts(d(i).name);
    outFName = fullfile(outDirName, [b, '_enhanced.tif']);
    imwrite(fImg, outFName);

    % Quality control
    [sy, sx] = size(W);
    O = zeros([sy, 2*sx], 'uint8');
    O(:, 1 : sx) = img;
    O(:, sx + 1 : 2 * sx) = fImg;

    qcOutFName = fullfile(qcDirName, [b, '_comparison.tif']);
    imwrite(O, qcOutFName);

    waitbar(i/numel(d), hWaitbar);

end

% Close the waitbar
close(hWaitbar);
