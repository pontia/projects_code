function assembleDataset(dirName)

% Top folders
rgbTopDir = fullfile(dirName, 'rgb', 'top');
allTopEnhDir = fullfile(dirName, 'gray', 'top', 'enhanced');
outputTopDirName = fullfile(dirName, 'dataset', 'top');
assemble(rgbTopDir, allTopEnhDir, outputTopDirName);

% Bottom folders
rgbBotDir = fullfile(dirName, 'rgb', 'bottom');
allBotEnhDir = fullfile(dirName, 'gray', 'bottom', 'enhanced');
outputBotDirName = fullfile(dirName, 'dataset', 'bottom');
assemble(rgbBotDir, allBotEnhDir, outputBotDirName)

% =========================================================================

function assemble(rgbDirName, enhGrayDirName, outputDirName)

% Output folder
if ~exist(outputDirName, 'dir')
    mkdir(outputDirName);
end

dRgb = dir(rgbDirName);
dEgray = dir(enhGrayDirName); 

hWaitbar = waitbar(0, 'Please wait while add channels 1-3...');

expression = '^(?<body>.+)_(?<suffix>\d+)\.tif$';

% Process RGB files
for i = 1 : numel(dRgb)
    
    if dRgb(i).isdir
        waitbar(i/numel(dRgb), hWaitbar);
        continue
    end

    % Read the image
    img = imread(fullfile(rgbDirName, dRgb(i).name));

    % Prepare the file names
    tokens = regexp(dRgb(i).name, expression, 'tokens');
    if numel(tokens) ~= 1 && numel(tokens{1}) ~= 2
        error('Unexpected file name!');
    end

    % Compose the file names
    RName = fullfile(outputDirName, ['frame_Z1_C1_T', tokens{1}{2}, '.tif']);
    GName = fullfile(outputDirName, ['frame_Z1_C2_T', tokens{1}{2}, '.tif']);
    BName = fullfile(outputDirName, ['frame_Z1_C3_T', tokens{1}{2}, '.tif']);
    
    % Save the split channels
    imwrite(img(:, :, 1), RName);
    imwrite(img(:, :, 2), GName);
    imwrite(img(:, :, 3), BName);

    % Update waitbar
    waitbar(i/numel(dRgb), hWaitbar);

end

% Recycle waitbar
waitbar(0, hWaitbar, 'Please wait while adding enhanced channel 4...');

expression = '^(?<body>.+)_(?<suffix>\d+)_enhanced\.tif$';

% Process enhanced gray files
for i = 1 : numel(dEgray)
    
    if dEgray(i).isdir
        waitbar(i/numel(dEgray), hWaitbar);
        continue
    end

    % Read the image
    img = imread(fullfile(enhGrayDirName, dEgray(i).name));

    % Prepare the file names
    tokens = regexp(dEgray(i).name, expression, 'tokens');
    if numel(tokens) ~= 1 && numel(tokens{1}) ~= 2
        error('Unexpected file name!');
    end

    % Compose the file name
    EGName = fullfile(outputDirName, ['frame_Z1_C4_T', tokens{1}{2}, '.tif']);

    % Save the split channels
    imwrite(img, EGName);

    % Update waitbar
    waitbar(i/numel(dEgray), hWaitbar);
    
end

% Close waitbar
close(hWaitbar);
