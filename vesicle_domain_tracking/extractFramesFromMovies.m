function extractFramesFromMovies()
% This function extracts all frames in selected movies both as RGB images
% and as the luminance channel of the LAB transform of the original RGB 
% image. 

% Get all files
[filename, pathname] = uigetfile( ...
    {'*.avi','AVI files (*.avi)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Pick a file', ...
    'MultiSelect', 'on');

if isequal(filename, 0)
    return
end

if ~iscell(filename)
    filename = {filename};
end

% Format string
strg = sprintf('%%.%dd', 4);

hWaitbar = waitbar(0, 'Processing file...');

for i = 1 : numel(filename)

    % Prepare output folders
    [~, b] = fileparts(filename{i});
    outFolder = fullfile(pathname, b);        
    if ~exist(outFolder, 'dir')
        mkdir(outFolder);
    end

    grayOutFolder = fullfile(outFolder, 'gray');        
    if ~exist(grayOutFolder, 'dir')
        mkdir(grayOutFolder);
    end

    rgbOutFolder = fullfile(outFolder, 'rgb');        
    if ~exist(rgbOutFolder, 'dir')
        mkdir(rgbOutFolder);
    end
    
    waitbar((i - 1) / numel(filename), hWaitbar, ...
        ['Processing file ', filename{i}, '...']);

    % Try opening the file
    v = VideoReader(fullfile(pathname, filename{i}));

    % Export
    c = 0;
    while hasFrame(v)

        c = c + 1;

        % Extract frame to be used
        img = v.readFrame();

        % Numeric suffix
        frameStr = sprintf(strg, c);

        % Save the RGB image
        outGRBFName = fullfile(rgbOutFolder, ...
            ['frame_', frameStr, '.tif']);
        
        % Write frame
        imwrite(img, outGRBFName);
        
        % Convert to LAB and take luminance channel
        lab = rgb2lab(img);
        frame = uint8(lab(:, :, 1));

        % Output file name
        grayOutFName = fullfile(grayOutFolder, ...
            ['frame_', frameStr, '.tif']);

        % Write frame
        imwrite(frame, grayOutFName);

    end

    % Update waitbar
    waitbar(i / numel(filename), hWaitbar);

end

% Close waitbar
close(hWaitbar);
