using HDF5
using JLD

function run_PappaBear()

	# Change to the working directory
	cd("E:/aaron/SHGcalc2");

	# Load the SHGCalc script
	reload("SHGcalc2_64.jl");

	# Run the simulation
	tic();
	shg = PappaBear();
	toc();

	# Save result to a JLD (HDF5) file
	file = jldopen("E:/aaron/shg_pappabear.jld", "w")
	write(file, "shg", shg)
	close(file)

end
