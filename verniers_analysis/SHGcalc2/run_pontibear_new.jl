function run_PontiBear_New()

	# Change to the working directory
	cd("E:/aaron/SHGcalc2");

	# Load the SHGCalc script
	reload("SHGcalc2_64.jl");

	# Set some parameters
	ehat = [1.0,0.0,0.0]
	
	# Run the simulation
	tic();
	#shg = PontiBear(0.1,0.05,myfloat(ehat);downsamp=10,sarcowidth=5.5)
	shg = PontiBear(0.125,0.05,myfloat(ehat);downsamp=24,sarcowidth=5.5,downsampz=4)
	size(shg) #write down these dimensions
	outfile = open("E:/aaron/shg_pontibear_downsample.bin","w")
	write(outfile,shg)
	close(outfile)
	toc();

end