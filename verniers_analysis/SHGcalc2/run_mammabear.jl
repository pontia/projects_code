using HDF5
using JLD

function run_MammaBear()

	# Change to the working directory
	cd("E:/aaron/SHGcalc2");

	# Load the SHGCalc script
	reload("SHGcalc2_64.jl");

	# Run the simulation
	tic();
	shg = MammaBear();
	toc();

	# Save result to a JLD (HDF5) file
	file = jldopen("E:/aaron/shg_mammabear.jld", "w")
	write(file, "shg", shg)
	close(file)
	
end
