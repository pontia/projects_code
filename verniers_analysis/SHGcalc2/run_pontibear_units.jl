function run_PontiBear_Units()

	# Change to the working directory
	cd("E:/aaron/SHGcalc2");

	# Load the SHGCalc script
	reload("SHGcalc2_64.jl");

	# Set some parameters
    ehat = [1.0,1.0,0.2]

	# Run the simulation
	tic();
	shg = PontiBear(0.125,0.05,myfloat(ehat);downsamp=3,sarcowidth=0.6,downsampz=2)
	size(shg) #write down these dimensions
	outfile = open("E:/aaron/shg_pontibear_units.raw","w")
	write(outfile,shg)
	close(outfile)
	toc();

end
