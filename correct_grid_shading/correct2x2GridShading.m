function [cImg, factorsCol, factorsRow] = correct2x2GridShading(img, ...
    polynomialDegree, trendPolynomialDegree, plotQCFig)

if nargin == 1
    polynomialDegree = 21;
    trendPolynomialDegree = 3;
    plotQCFig = 0;
elseif nargin == 2
    trendPolynomialDegree = 3;
    plotQCFig = 0;
elseif nargin == 3
    plotQCFig = 0;
elseif nargin == 4
    % All set
else
    error('Wrong number of input arguments.');
end

% Some handy constants
nCols = size(img, 2);
nRows = size(img, 1);
stepX = fix(nCols / 2);
stepY = fix(nRows / 2);
support = fix(polynomialDegree / 2) + 1;

% Store a copy ot the original image (if needed)
if plotQCFig == 1
    originalImg = img;
end

% Store the mean intensity and data type of the original image. We will
% need them to offset the corrected image and restore its data type.
meanOriginalImg = mean(img(:));
meanOriginalStd = std(double(img(:)));
classOriginalImg = class(img);

% Normalize and make double precision
img = im2double(img);

% =========================================================================
%
% Calculate correction factors for the columns
%
% =========================================================================

% Get the row average
colI = mean(img, 1);

% Median filter
colIFilt = medfilt1(colI, support);

% Fit 2 polynomials for the columns
profileColI1 = colIFilt(1 : stepX);
profileColI2 = colIFilt(stepX + 1 : end);
fittedProfileColI1 = fitProfile(profileColI1, polynomialDegree);
fittedProfileColI2 = fitProfile(profileColI2, polynomialDegree);

% Make sure there are no zeros in the fitted polynomials
fittedProfileCol = [fittedProfileColI1, fittedProfileColI2];
fittedProfileCol(fittedProfileCol == 0) = 1;

% Fit trend polynomial
fittedTrendColI = fitProfile(colIFilt, trendPolynomialDegree);

% Calculate correction factors
factorsCol = fittedTrendColI ./ fittedProfileCol;

% Median filter factors to prevent discontinuities
factorsCol = medfilt1(factorsCol, 3);

% =========================================================================
%
% Correct the columns
%
% =========================================================================

for i = 1 : nCols
    img(:, i) = factorsCol(i) .* img(:, i);
end

% =========================================================================
%
% Calculate correction factors for the rows
%
% =========================================================================

% Get the row average
rowI = mean(img, 2);
rowI = rowI(:)';

% Median filter
rowIFilt = medfilt1(rowI, support);

% Fit 2 polynomials for the rows
profileRowI1 = rowIFilt(1 : stepY);
profileRowI2 = rowIFilt(stepY + 1 : end);
fittedProfileRowI1 = fitProfile(profileRowI1, polynomialDegree);
fittedProfileRowI2 = fitProfile(profileRowI2, polynomialDegree);

% Make sure there are no zeros in the fitted polynomials
fittedProfileRow = [fittedProfileRowI1, fittedProfileRowI2];
fittedProfileRow(fittedProfileRow == 0) = 1;

% Fit trend polynomial
fittedTrendRowI = fitProfile(rowIFilt, trendPolynomialDegree);

% Calculate correction factors
factorsRow = fittedTrendRowI ./ fittedProfileRow;

% Median filter factors to prevent discontinuities
factorsRow = medfilt1(factorsRow, 3);

% =========================================================================
%
% Correct the rows
%
% =========================================================================

for i = 1 : nRows
    img(i, :) = factorsRow(i) .* img(i, :);
end

% =========================================================================
%
% Bring the intensity average in the corrected image back to where it was
% and cast to the original data type
%
% =========================================================================

cImg = img ./ std(img(:)) .* meanOriginalStd + meanOriginalImg;
cImg = cast(cImg, classOriginalImg);

% =========================================================================
%
% Plot quality control figures?
%
% =========================================================================

% Plot
if plotQCFig == 1
    plotQualityControlFigure();
    plotCorrectionComparison();
end

% =========================================================================
% ===  Internal functions =================================================
% =========================================================================

    function fittedProfile = fitProfile(profile, n)
        
        % warning off
        
        % Pad profile
        padded = padarray(profile(:), n, 'symmetric', 'both')';
        
        % x values
        x = 1 : numel(padded);
        
        % Fit intensity trends in channel with nth degree polynom
        [p, s, mu] = polyfit(x, padded, n);
        
        % Fitted polynom
        fittedPaddedProfile = polyval(p, x, s, mu);
        
        % Remove pad
        fittedProfile = fittedPaddedProfile(n + 1: end - n);
        
        % warning on
        
    end

    function plotQualityControlFigure()
        
        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Quality Control Figure 1');
        
        % Plot average column intensities
        subplot(2, 2, 1);
        plot(colI, 'k-');
        xlabel('Average column intensities');
        yLimCol = get(gca, 'YLim');
        hold on
        
        % Plot fitted lines
        plot(1 : stepX, fittedProfileColI1, 'r-');
        plot(stepX + 1 : nCols, fittedProfileColI2, 'm-');
        
        % Plot trend (for correction)
        plot(1 : numel(fittedTrendColI), fittedTrendColI, 'b.-');
        
        % Plot average row intensities
        subplot(2, 2, 2);
        plot(rowI, 'k-');
        xlabel('Average row intensities');
        yLimRow = get(gca, 'YLim');
        hold on
        
        % Plot fitted lines
        plot(1 : stepY, fittedProfileRowI1, 'r-');
        plot(stepY + 1 : nRows, fittedProfileRowI2, 'm-');
        
        % Plot trend (for correction)
        plot(1 : numel(fittedTrendRowI), fittedTrendRowI, 'b.-');
        
        % Plot corrected column intensities
        subplot(2, 2, 3);
        plot(mean(img, 1), 'k-');
        xlabel('Corrected, average column intensities');
        set(gca, 'YLim', yLimCol);
        
        % Plot corrected row intensities
        subplot(2, 2, 4);
        plot(mean(img, 2), 'k-');
        xlabel('Corrected, average row intensities');
        set(gca, 'YLim', yLimRow);
        
    end

% =========================================================================

    function plotCorrectionComparison()
        
        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Quality Control Figure 2');
        
        % Dislay images before and after correction
        subplot(1, 2, 1);
        imshow(originalImg, []);
        xlabel('Original image');
        subplot(1, 2, 2);
        imshow(img, []);
        xlabel('Corrected image');

    end

end

