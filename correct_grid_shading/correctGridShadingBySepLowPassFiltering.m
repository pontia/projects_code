function [img, imgY, imgX] = correctGridShadingBySepLowPassFiltering(img, ...
    kernelSize, plotQCFig)

if nargin == 1
    kernelSize = 51;
    plotQCFig = 0;
elseif nargin == 2
    plotQCFig = 0;
elseif nargin == 3
    % All set
else
    error('Wrong number of input arguments.');
end

% =========================================================================
%
% Cast to double
%
% =========================================================================

% Keep a copy of the original image
if plotQCFig == 1
    originalImg = img;
end

% Keep track of the original image class
originalClass = class(img);

% Cast to double
img = double(img);

% =========================================================================
%
% Filter in vertical direction
%
% =========================================================================

% Sigma
kSigma = fix(kernelSize / 5);
if mod(kSigma, 2) == 0
    kSigma = kSigma + 1;
end

% Kernel
Ky = fspecial('gaussian', [kernelSize 1], kSigma);

% Filter
imgY = imfilter(img, Ky, 'symmetric', 'same');

% Offset
offsetY = median(imgY(:));

% Subtract the filtered version and add back the offset
img = img - imgY + offsetY;

% =========================================================================
%
% Filter in horizontal direction
%
% =========================================================================

% Kernel
Kx = Ky';

% Filter
imgX = imfilter(img, Kx, 'symmetric', 'same');

% Offset
offsetX = median(imgX(:));

% Subtract the filtered version and add back the offset
img = img - imgX + offsetX;

% =========================================================================
%
% Cast back
%
% =========================================================================

img = cast(img, originalClass);

% =========================================================================
%
% Plot quality control figures?
%
% =========================================================================

% Plot
if plotQCFig == 1
    plotCorrectionComparison();
end

% =========================================================================
% ===  Internal functions =================================================
% =========================================================================

    function plotCorrectionComparison()

        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Background estimations');
        
        % Y direction
        subplot(1, 2, 1);
        imshow(imgY, []);
        xlabel('Y direction');
        subplot(1, 2, 2);
        imshow(imgX, []);
        xlabel('X direction');
        
        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Quality Control Figure');
        
        % Dislay images before and after correction
        subplot(1, 2, 1);
        imshow(originalImg, []);
        xlabel('Original image');
        subplot(1, 2, 2);
        imshow(img, []);
        xlabel('Corrected image');

    end

end

