/* Image max and median filters by Fabio Bellavia (fbellavia@unipa.it),
 * refer to: F. Bellavia, D. Tegolo, C. Valenti,
 * "Improving Harris corner selection strategy",
 * IET Computer Vision 5(2), 2011.
 * Only for academic or other non-commercial purposes.
 *
 * Circular max and median filters for non-integer 2D array,
 * they offers a good compromise between speed and memory occupation.
 *
 * The median filter implementation is based on:
 * W. Hardle, W. Steiger,
 * "Algorithm AS 296: Optimal Median Smoothing"
 * Journal of the Royal Statistical Society,
 * Series C (Applied Statistics), pp. 258-264, 
 * before it was implemented through the Quickselect algorithm.
 * Though faster algorithms exist, they require much more memory.
 */

#include "mex.h"
#include <stdio.h>
#include <math.h>

#define OK 1
																							
#define SILENT 	0
#define VERBOSE 1

#define FILT_MIN -1
#define FILT_MAX +1

#define R(s,o) ((ceil(3*(s))>1?ceil(3*(s)):1)+(o))
#define R2D(r) (2*(r)+1)
#define D2R(d) (((d)-1)/2)
#define ISODD(n) (((int)n/2)*2<(int)n)
						 
typedef struct dvect {
	int n;
	double *el;
} dvect;

typedef struct ivect {
	int n;
	int *el;
} ivect;

typedef struct xy {
	int x;
	int y;
} xy;	

typedef struct iptvect {
	int n;
	xy *el;
} iptvect;

typedef struct pt {
	double x;
	double y;
} pt;

typedef struct bmp_img {
	double *val;
	int row;
	int col;
} bmp_img;

typedef struct heapy {
	double *key; 
    int *index;
    int nc;
    int h;
} heapy;

#ifdef _MSC_VER
double trunc(double x);
double log2(double x);
#endif

int lr_mapper(bmp_img mask, iptvect *tocancel, iptvect *toadd);
int ud_mapper(bmp_img mask, iptvect *tocancel, iptvect *toadd);
bmp_img maxfilter(bmp_img img, bmp_img mask, int mm, double t);
bmp_img medianfilter(bmp_img img, bmp_img disk, int md, double t);
double median_smooth(double v, heapy hm, int md, double t);
double max_smooth(double v, heapy hm, int md, double t);

heapy heapclear(heapy in);
heapy heapadd(double val, int idx, heapy in);
int heapmodify(double val, int idx, heapy in);
int heapmodify_left(double val, int idx, heapy in,int s);
int heapmodify_right(double val, int idx, heapy in, int s);
int heapmodify_left_right(double val, int idx, heapy in, int s);
int exchange(int i, int j, heapy in);
int ksort(heapy in);
heapy heap_allocate(int h);
int heap_deallocate(heapy in);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
    const mxArray *prhs[]);
