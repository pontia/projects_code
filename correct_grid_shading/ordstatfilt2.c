/* Image max and median filters by Fabio Bellavia (fbellavia@unipa.it),
 * refer to: F. Bellavia, D. Tegolo, C. Valenti,
 * "Improving Harris corner selection strategy",
 * IET Computer Vision 5(2), 2011.
 * Only for academic or other non-commercial purposes.
 *
 * Circular max and median filters for non-integer 2D array,
 * they offers a good compromise between speed and memory occupation.
 *
 * The median filter implementation is based on:
 * W. Hardle, W. Steiger,
 * "Algorithm AS 296: Optimal Median Smoothing"
 * Journal of the Royal Statistical Society,
 * Series C (Applied Statistics), pp. 258-264, 
 * before it was implemented through the Quickselect algorithm.
 * Though faster algorithms exist, they require much more memory.
 */

/*
 * to compile: mex ordstatfilt2.c
 */

#include "ordstatfilt2.h"

#ifdef _MSC_VER
double clog2;

double log2(double x) {  
    return log(x)/clog2;  
}

double trunc(double x) {  
    return (x>0)?floor(x):ceil(x);  
}
#endif

int lr_mapper(bmp_img mask, iptvect *tocancel, iptvect *toadd) {
	int i, j, n, m, w, z;
	double *aux;
	
	aux=(double *)mxCalloc(mask.col*(mask.row+1),sizeof(double));
	for (i=0;i<mask.col;i++)
		for (j=0;j<mask.row;j++) {
				aux[i*(mask.row+1)+j]+=(mask.val[i*mask.row+j])?1:0;
				aux[i*(mask.row+1)+j+1]+=(mask.val[i*mask.row+j])?2:0;
			}
	for (i=m=n=0;i<mask.col*(mask.row+1);i++) {
		if (aux[i]==1) n++;
		if (aux[i]==2) m++;
	}
	tocancel->n=n;
	toadd->n=m;
	tocancel->el=(xy *)mxMalloc(sizeof(xy)*tocancel->n);
	toadd->el=(xy *)mxMalloc(sizeof(xy)*toadd->n);
	w=z=0;
	for (i=0;i<mask.col;i++)
		for (j=0;j<=mask.row;j++) {
			if (aux[i*(mask.row+1)+j]==1) {
				tocancel->el[w].x=i;
				tocancel->el[w++].y=j;
			}
			if (aux[i*(mask.row+1)+j]==2) {
				toadd->el[z].x=i;
				toadd->el[z++].y=j;
			}
		}
	mxFree(aux);	
	return 1;
}

int ud_mapper(bmp_img mask, iptvect *tocancel, iptvect *toadd) {
	int i, j, n, m, w, z;
	double *aux;
	
	aux=(double *)mxCalloc((mask.col+1)*mask.row,sizeof(double));
	for (i=0;i<mask.col;i++)
		for (j=0;j<mask.row;j++) {
				aux[i*(mask.row)+j]+=(mask.val[i*mask.row+j])?1:0;
				aux[(i+1)*mask.row+j]+=(mask.val[i*mask.row+j])?2:0;
			}
	for (i=m=n=0;i<(mask.col+1)*mask.row;i++) {
		if (aux[i]==1) n++;
		if (aux[i]==2) m++;
	}
	tocancel->n=n;
	toadd->n=m;
	tocancel->el=(xy *)mxMalloc(sizeof(xy)*tocancel->n);
	toadd->el=(xy *)mxMalloc(sizeof(xy)*toadd->n);
	w=z=0;
	for (i=0;i<=mask.col;i++)
		for (j=0;j<mask.row;j++) {
			if (aux[i*mask.row+j]==1) {
				tocancel->el[w].x=i;
				tocancel->el[w++].y=j;
			}
			if (aux[i*mask.row+j]==2) {
				toadd->el[z].x=i;
				toadd->el[z++].y=j;
			}
		}
	mxFree(aux);
	return 1;
}

heapy heapclear(heapy in) {
	int i;
	
	for (i=0;i<in.h;in.key[i]=0,in.index[i]=i,in.index[in.h+i]=0,i++);
	in.nc=0;
	return in;
}

heapy heapadd(double val, int idx, heapy in) {
	int k;

	in.index[idx]=in.nc;
	in.key[in.nc]=val;
	in.index[in.h+in.nc]=idx;
	k=++in.nc;
	for (;(k>1) && (in.key[k/2-1]<in.key[k-1]);k=k/2)
		exchange(k-1,k/2-1,in);
	return in;
}

int heapmodify(double val, int idx, heapy in) {
	int k,w;

	in.key[in.index[idx]]=val;
	k=in.index[idx]+1;
	for (;(k>1) && (in.key[k/2-1]<in.key[k-1]);k=k/2)
		exchange(k-1,k/2-1,in);	
	k=in.index[idx]+1;
	while (2*k<=in.nc) {
		w=2*k;
		if ((w<in.nc) && (in.key[w-1]<in.key[w]))
			w++;
		if (in.key[k-1]>=in.key[w-1])
			break;
		exchange(k-1,w-1,in);			
		k=w;
	}
	return 1;
}

int exchange(int i, int j, heapy in) {
	double aux_key;
	int aux_index;
		
	aux_index=in.index[in.index[in.h+i]];
	in.index[in.index[in.h+i]]=in.index[in.index[in.h+j]];
	in.index[in.index[in.h+j]]=aux_index;
	aux_index=in.index[in.h+i];
	in.index[in.h+i]=in.index[in.h+j];
	in.index[in.h+j]=aux_index;
	aux_key=in.key[i];
	in.key[i]=in.key[j];
	in.key[j]=aux_key;
	return 1;
}

int heapmodify_left_right(double val, int idx, heapy in, int s) {
		
	if (in.index[idx]>=s) {
		heapmodify_right(val,idx,in,s);
		heapmodify_left(in.key[s],in.index[in.h+s],in,s);
	} 
	else {
		heapmodify_left(val,idx,in,s);
		heapmodify_right(in.key[s],in.index[in.h+s],in,s);	
	}
	return 1;
}

int heapmodify_left(double val, int idx, heapy in, int s) {
	int k,w;

	if (!s)
		return 1;
	in.key[in.index[idx]]=val;
	k=s-in.index[idx]+1;
	for (;(k>1) && (in.key[s-k/2+1]<in.key[s-k+1]);k=k/2)
		exchange(s-k+1,s-k/2+1,in);	
	k=s-in.index[idx]+1;
	while (2*k<=s+1) {
		w=2*k;
		if ((w<s+1) && (in.key[s-w+1]<in.key[s-w]))
			w++;
		if (in.key[s-k+1]>=in.key[s-w+1])
			break;
		exchange(s-k+1,s-w+1,in);		
		k=w;
	}
	return 1;
}

int heapmodify_right(double val, int idx, heapy in, int s) {
	int k,w;

	if (s==in.h-1)
		return 1;
	in.key[in.index[idx]]=val;
	k=in.index[idx]-s+1;
	for (;(k>1) && (in.key[s+k/2-1]>in.key[s+k-1]);k=k/2)
		exchange(s+k-1,s+k/2-1,in);	
	k=in.index[idx]-s+1;
	while (2*k<=in.h-s) {
		w=2*k;
		if ((w<in.h-s) && (in.key[s+w-1]>in.key[s+w]))
			w++;
		if (in.key[s+k-1]<=in.key[s+w-1])
			break;
		exchange(s+k-1,s+w-1,in);		
		k=w;
	}
	return 1;	
}

heapy heap_allocate(int h) {
	heapy in;
	
	in.h=h;
	in.nc=0;
	in.index=(int *)mxMalloc(sizeof(int)*in.h*2);
	in.key=(double *)mxMalloc(sizeof(double)*in.h);
	return in;
}

int heap_deallocate(heapy in) {
	mxFree(in.key);
	mxFree(in.index);
	return 1;
}

bmp_img maxfilter(bmp_img img, bmp_img disk, int mm, double t) {
	int i, j, m, n, irad, jrad, io, jo;
	bmp_img res, mx;
	iptvect lr[2], ud[2];
	heapy hm;
	double vm=0;

	if (mm==FILT_MAX)
		t=0;    
	io=!ISODD(disk.col);
	jo=!ISODD(disk.row);	
	irad=D2R(disk.col)+io;
	jrad=D2R(disk.row)+jo;
	res.row=img.row;
	res.col=img.col;
	res.val=(double *)mxMalloc(sizeof(double)*res.row*res.col);
	mx.row=img.row+disk.row-1;
	mx.col=img.col+disk.col-1;
	mx.val=(double *)mxMalloc(mx.row*mx.col*sizeof(double));
	for (i=0;i<mx.row*mx.col;i++)
		mx.val[i]=0;
	for (i=irad;i<img.col+irad;i++)
		for (j=jrad;j<img.row+jrad;j++) {
			mx.val[i*mx.row+j]=img.val[(i-irad)*img.row+j-jrad]*mm;
			vm=mx.val[i*mx.row+j]<vm?mx.val[i*mx.row+j]:vm;
		}
	hm=heap_allocate(disk.row*disk.col);
	lr_mapper(disk,&(lr[0]),&(lr[1]));
	ud_mapper(disk,&(ud[0]),&(ud[1]));
	/*block 0 (set)*/
	i=irad-1;
	j=jrad;
	hm=heapclear(hm);
	for (m=0;m<disk.col;m++)
		for (n=0;n<disk.row;n++)
			hm=heapadd((disk.val[m*disk.row+n])?
				mx.val[m*mx.row+n]:vm-1,m*disk.row+n,hm);	
	res.val[0]=max_smooth(hm.key[0],hm,0,t)*mm;	
	while (OK) {
		/*block 1 (lr)*/
		for (i++;j<res.row+jrad-1;j++) {
			for (m=0;m<lr[0].n;m++)	
				heapmodify(-1,((lr[0].el[m].x+i-irad)%disk.col)*
					disk.row+(lr[0].el[m].y+j-jrad)%disk.row,hm);				
			for (n=0;n<lr[1].n;n++)
				heapmodify(mx.val[(lr[1].el[n].x+i-irad)*mx.row+
					(lr[1].el[n].y+j-jrad)],
					((lr[1].el[n].x+i-irad)%disk.col)*disk.row+
					(lr[1].el[n].y+j-jrad)%disk.row,hm);		
			res.val[(i-irad)*res.row+j-jrad+1]=
                max_smooth(hm.key[0],hm,0,t)*mm;
		}
		/*block 2 (ud right)*/	
		if (i>=res.col+irad-1)
			break;		
		for (m=0;m<ud[0].n;m++)	
			heapmodify(-1,((ud[0].el[m].x+i-irad)%disk.col)*disk.row+
				(ud[0].el[m].y+res.row-1)%disk.row,hm);				
		for (n=0;n<ud[1].n;n++)
			heapmodify(mx.val[(ud[1].el[n].x+i-irad)*mx.row+
				(ud[1].el[n].y+res.row-1)],
				((ud[1].el[n].x+i-irad)%disk.col)*disk.row+
				(ud[1].el[n].y+res.row-1)%disk.row,hm);
		res.val[(i-irad+1)*res.row+res.row-1]=
            max_smooth(hm.key[0],hm,0,t)*mm;
		/*block 3 (rl)*/
		for (i++;j>jrad;j--) {
			for (m=0;m<lr[1].n;m++)	
				heapmodify(-1,((lr[1].el[m].x+i-irad)%disk.col)*
					disk.row+(lr[1].el[m].y+j-1-jrad)%disk.row,hm);				
			for (n=0;n<lr[0].n;n++)
				heapmodify(mx.val[(lr[0].el[n].x+i-irad)*mx.row+
					(lr[0].el[n].y+j-1-jrad)],
					((lr[0].el[n].x+i-irad)%disk.col)*disk.row+
					(lr[0].el[n].y+j-1-jrad)%disk.row,hm);		
			res.val[(i-irad)*res.row+j-jrad-1]=
                max_smooth(hm.key[0],hm,0,t)*mm;
		}
		/*block 4 (ud right)*/
		if (i>=res.col+irad-1)
			break;		
		for (m=0;m<ud[0].n;m++)	
			heapmodify(-1,((ud[0].el[m].x+i-irad)%disk.col)*disk.row+
				ud[0].el[m].y,hm);				
		for (n=0;n<ud[1].n;n++)
			heapmodify(mx.val[(ud[1].el[n].x+i-irad)*mx.row+
				(ud[1].el[n].y)],
				((ud[1].el[n].x+i-irad)%disk.col)*disk.row+
				ud[1].el[n].y,hm);
		res.val[(i-irad+1)*res.row]=
            max_smooth(hm.key[0],hm,0,t)*mm;
	}
	mxFree(mx.val);
	heap_deallocate(hm);
	mxFree(lr[0].el);
	mxFree(lr[1].el);
	mxFree(ud[0].el);
	mxFree(ud[1].el);
	return res;	
}

bmp_img medianfilter(bmp_img img, bmp_img disk, int md, double t) {
	int i, j, m, n, irad, jrad, io , jo;
	iptvect lr[2], ud[2];
	heapy hm;
	bmp_img mx, res;
	double vm=0;

	io=!ISODD(disk.col);
	jo=!ISODD(disk.row);	
	irad=D2R(disk.col)+io;
	jrad=D2R(disk.row)+jo;
	mx.row=img.row+disk.row-1;
	mx.col=img.col+disk.col-1;
	mx.val=(double *)mxMalloc(mx.row*mx.col*sizeof(double));
	for (i=0;i<mx.row*mx.col;i++)
		mx.val[i]=0;
	res.row=img.row;
	res.col=img.col;
	res.val=(double *)mxMalloc(res.row*res.col*sizeof(double));	
	for (i=irad;i<img.col+irad;i++)
		for (j=jrad;j<img.row+jrad;j++) {
			mx.val[i*mx.row+j]=img.val[(i-irad)*img.row+j-jrad];
			vm=mx.val[i*mx.row+j]<vm?mx.val[i*mx.row+j]:vm;
		}
	hm=heap_allocate(disk.row*disk.col);
	lr_mapper(disk,&(lr[0]),&(lr[1]));
	ud_mapper(disk,&(ud[0]),&(ud[1]));	
	/*block 0 (set)*/
	i=irad-1;
	j=jrad;
	hm=heapclear(hm);
	for (m=0;m<disk.col;m++)
		for (n=0;n<disk.row;n++) {
				hm.key[m*disk.row+n]=(disk.val[m*disk.row+n])?
					mx.val[m*mx.row+n]:vm-1;
				hm.index[m*disk.row+n]=m*disk.row+n;
				hm.index[hm.h+m*disk.row+n]=m*disk.row+n;
			}		
	ksort(hm);
	res.val[0]=median_smooth(hm.key[md],hm,md,t);
	while (OK) {
		/*block 1 (lr)*/
		for (i++;j<res.row+jrad-1;j++) {
			for (m=0;m<lr[0].n;m++)	
				heapmodify_left_right(
					vm-1,((lr[0].el[m].x+i-irad)%disk.col)*disk.row+
					(lr[0].el[m].y+j-jrad)%disk.row,hm,md);				
			for (n=0;n<lr[1].n;n++)
				heapmodify_left_right(
					mx.val[(lr[1].el[n].x+i-irad)*mx.row+
					(lr[1].el[n].y+j-jrad)],
					((lr[1].el[n].x+i-irad)%disk.col)*disk.row+
					(lr[1].el[n].y+j-jrad)%disk.row,hm,md);		
			res.val[(i-irad)*res.row+j-jrad+1]=
				median_smooth(hm.key[md],hm,md,t);	
		}
		/*block 2 (ud right)*/	
		if (i>=res.col+irad-1)
			break;		
		for (m=0;m<ud[0].n;m++)	
			heapmodify_left_right(
				vm-1,((ud[0].el[m].x+i-irad)%disk.col)*disk.row+
				(ud[0].el[m].y+res.row-1)%disk.row,hm,md);				
		for (n=0;n<ud[1].n;n++)
			heapmodify_left_right(
				mx.val[(ud[1].el[n].x+i-irad)*mx.row+
				(ud[1].el[n].y+res.row-1)],
				((ud[1].el[n].x+i-irad)%disk.col)*disk.row+
				(ud[1].el[n].y+res.row-1)%disk.row,hm,md);
		res.val[(i-irad+1)*res.row+res.row-1]=
			median_smooth(hm.key[md],hm,md,t);	
		/*block 3 (rl)*/
		for (i++;j>jrad;j--) {
			for (m=0;m<lr[1].n;m++)	
				heapmodify_left_right(
				vm-1,((lr[1].el[m].x+i-irad)%disk.col)*disk.row+
					(lr[1].el[m].y+j-1-jrad)%disk.row,hm,md);				
			for (n=0;n<lr[0].n;n++)
				heapmodify_left_right(
					mx.val[(lr[0].el[n].x+i-irad)*mx.row+
					(lr[0].el[n].y+j-1-jrad)],
					((lr[0].el[n].x+i-irad)%disk.col)*disk.row+
					(lr[0].el[n].y+j-1-jrad)%disk.row,hm,md);		
			res.val[(i-irad)*res.row+j-jrad-1]=
				median_smooth(hm.key[md],hm,md,t);		
		}
		/*block 4 (ud right)*/	
		if (i>=res.col+irad-1)
			break;		
		for (m=0;m<ud[0].n;m++)	
			heapmodify_left_right(
				vm-1,((ud[0].el[m].x+i-irad)%disk.col)*disk.row+
				ud[0].el[m].y,hm,md);				
		for (n=0;n<ud[1].n;n++)
			heapmodify_left_right(
				mx.val[(ud[1].el[n].x+i-irad)*mx.row+
				(ud[1].el[n].y)],
				((ud[1].el[n].x+i-irad)%disk.col)*disk.row+
				ud[1].el[n].y,hm,md);
		res.val[(i-irad+1)*res.row]=
			median_smooth(hm.key[md],hm,md,t);	
	}
	mxFree(mx.val);
	heap_deallocate(hm);
	mxFree(lr[0].el);
	mxFree(lr[1].el);
	mxFree(ud[0].el);
	mxFree(ud[1].el);
	return res;	
}

double median_smooth(double v, heapy hm, int md, double t) {
	int tt;
	double vs;
	
	if (t>0) {
		tt=0;
		if (md+1<hm.h) {
			vs=hm.key[md+1];
			tt=1;
		}
		if (md+2<hm.h) {
			tt=1;
			vs=vs<hm.key[md+2]?vs:hm.key[md+2];
		}
		if (tt)
			v=(v*(1-t)+vs*t);	
	}
	return v;
}

double max_smooth(double v, heapy hm, int md, double t) {
	int tt;
	double vs;
	
	if (t>0) {
		tt=0;
		if (md+1<hm.h) {
			vs=hm.key[md+1];
			tt=1;
		}
		if (md+2<hm.h) {
			tt=1;
			vs=vs>hm.key[md+2]?vs:hm.key[md+2];
		}
		if (tt)
			v=(v*(1-t)+vs*t);	
	}
	return v;
}

int ksort(heapy in) {
	int i, j, k, l, r, z, m, n, c;
	double  v;
	iptvect stack;
	
	stack.n=ceil(log2(in.h))+1;
	stack.el=(xy *)mxMalloc(stack.n*sizeof(xy));	
	stack.el[0].x=0;
	stack.el[0].y=in.h-2;
	c=1;		
	while (c--) {	
		l=stack.el[c].x;
		r=stack.el[c].y;
		k=l+(r-l)/2;			
		if (in.key[l]>in.key[l+1])
			exchange(l,l+1,in);	
		i=m=l;
		j=n=r;
		if (in.key[l]>in.key[k])
			exchange(l,k,in);
		if (in.key[k]<in.key[r+1])
			exchange(k,r+1,in);			
		v=in.key[r+1];		
		while (i<=j) {				
			for (;(in.key[i]<v) && (i<=j);i++);		
			for (;(j>=l) && (in.key[j]>v) && (i<=j);j--);
			if (j<l) break;
			if ((in.key[i]==v) && (i<=j))
				exchange(i++,m++,in);						
			if ((in.key[j]==v) && (i<=j))
				exchange(j--,n--,in);						
			if (i<j)
				exchange(i,j,in);			
		}		
		exchange(i,r+1,in);
		for (z=0;z<m-l;z++)
			exchange(l+z,i-z-1,in);		
		for (z=0;z<r-n;z++)
			exchange(n+z+1,i+z+1,in);			
		if (i-m-2>n-i-1) {
			if (i-m-2>=0) {
				stack.el[c].x=l;
				stack.el[c].y=l+i-m-2;		
				c++;
			}
			if (n-i-1>=0) { 
				stack.el[c].x=i+r-n+1;
				stack.el[c].y=r;			
				c++;
			}			
		}
		else {
			if (n-i-1>=0) { 
				stack.el[c].x=i+r-n+1;
				stack.el[c].y=r;			
				c++;
			}			
			if (i-m-2>=0) {
				stack.el[c].x=l;
				stack.el[c].y=l+i-m-2;
				c++;
			}
		}
	}
	mxFree(stack.el);	
	return 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
    const mxArray *prhs[]) {
	int i, s=0;
	bmp_img img, ker, res;
    double *what_, what, smooth;
    double *rr;
 
#ifdef _MSC_VER
	clog2=log(2);
#endif    
   
    if (nrhs!=3)
        mexErrMsgTxt("Wrong number of input arguments!");
   
    if (nlhs!=1)
        mexErrMsgTxt("Wrong number of output arguments!");
  
    for (i=0;i<3;i++)
        if (!mxIsDouble(prhs[i])) {
            mexErrMsgTxt("Input arguments should be double!");
        }
    
    if (mxGetM(prhs[2])*mxGetN(prhs[2])>1)
        mexErrMsgTxt("Third argument must by a double!");  
    what_=(double *)mxGetPr(prhs[2]);
    what=*what_;

    if (mxGetM(prhs[0])*mxGetN(prhs[0])<2)
        mexErrMsgTxt("First argument must by a double matrix!");        
    img.val=(double *)mxGetPr(prhs[0]);
    img.row=mxGetM(prhs[0]);
    img.col=mxGetN(prhs[0]);
       
    if (mxGetM(prhs[1])*mxGetN(prhs[1])<2)
        mexErrMsgTxt("Second argument must by a double matrix!");    
    ker.val=(double *)mxGetPr(prhs[1]);
    ker.row=mxGetM(prhs[1]);
    ker.col=mxGetN(prhs[1]);
    for (i=0;i<ker.col*ker.row;i++) {
        if ((ker.val[i]==0) || (ker.val[i]==1))
            s+=(ker.val[i]>0);
        else
            mexErrMsgTxt("Kernel mask must be binary!");    
    }
       
    what--;
    smooth=what-trunc(what);
    what=trunc(what);
    if (what==0)
        res=maxfilter(img,ker,FILT_MIN,smooth); 
    else if (what==s-1) {
        if (smooth>0)
            mexErrMsgTxt("Order statistic out of range!");   
        res=maxfilter(img,ker,FILT_MAX,0);
    }
    else if ((what>0) && (what<s-1)) {
        what=(ker.row*ker.col)-s+what;
        res=medianfilter(img,ker,what,smooth);    
    }
    else
        mexErrMsgTxt("Order statistic out of range!");          
        
    plhs[0]=mxCreateDoubleMatrix(0,0,mxREAL);
    mxSetPr(plhs[0],res.val);
    mxSetM(plhs[0],res.row);    
    mxSetN(plhs[0],res.col);
}
