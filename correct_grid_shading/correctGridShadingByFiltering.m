function [img, imgY, imgX] = correctGridShadingByFiltering(img, ...
    kernelSize, useMedFilt, plotQCFig)

if nargin == 1
    kernelSize = 51;
    useMedFilt = 0;
    plotQCFig = 0;
elseif nargin == 2
    useMedFilt = 0;
    plotQCFig = 0;
elseif nargin == 3
    plotQCFig = 0;
elseif nargin == 4
    % All set
else
    error('Wrong number of input arguments.');
end

% =========================================================================
%
% Cast to double
%
% =========================================================================

% Keep a copy of the original image
if plotQCFig == 1
    originalImg = img;
end

% Keep track of the original image class
originalClass = class(img);

% Cast to double
img = double(img);

% =========================================================================
%
% Filter
%
% =========================================================================

if useMedFilt == 1
    
    % Kernel
    K = ones(kernelSize, kernelSize);
    idx = sum(K(:)) / 2 + 0.5;
    

    % Filter
    % This is the equivalent to: imgF = medfilt2(img, [kernelSize kernelSize]);  % 0-padding
    imgF = kordstatfilt2(img, K, idx);
    

else
    
    % Sigma
    kSigma = fix(kernelSize / 5);
    if mod(kSigma, 2) == 0
        kSigma = kSigma + 1;
    end
    
    % Kernel
    K = fspecial('gaussian', kernelSize, kSigma);
    
    % Filter
    imgF = imfilter(img, K, 'symmetric', 'same');
    
end

% Offset
offset = median(imgF(:));

% Subtract the filtered version and add back the offset
img = img - imgF + offset;

% =========================================================================
%
% Cast back
%
% =========================================================================

img = cast(img, originalClass);

% =========================================================================
%
% Plot quality control figures?
%
% =========================================================================

% Plot
if plotQCFig == 1
    plotCorrectionComparison();
end

% =========================================================================
% ===  Internal functions =================================================
% =========================================================================

    function plotCorrectionComparison()

        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Background estimations');
        
        % Background
        imshow(imgF, []);
        xlabel('background');
        
        % Open figure
        figure('NumberTitle', 'off', 'Name', 'Quality Control Figure');
        
        % Dislay images before and after correction
        subplot(1, 2, 1);
        imshow(originalImg, []);
        xlabel('Original image');
        subplot(1, 2, 2);
        imshow(img, []);
        xlabel('Corrected image');

    end

end

