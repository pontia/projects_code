function processAllTimepoints(hQu, type)

% type = 1: corect by polynomial fitting
% type = 2: corect by separate low-pass filterig
% type = 3: corect by median filtering


aDataset = quGetDataset(hQu);

nTimepoints = getNTimepoints(aDataset);
nChannels = getNChannels(aDataset);

for t = 1 : nTimepoints

    for c = 1 : nChannels
        
        img = getImage(aDataset, 'specified', t, -1, c);
        
        if type == 1
            img = correct2x2GridShading(img, 21, 3, 0);
        elseif type == 2
            % The speed up does not compensate for the bad result!
            img = correctGridShadingBySepLowPassFiltering(img, 151, 1);
        else
            % VERY slow if the median is used -- but best results
            tic; img = correctGridShadingByFiltering(img, 151, 1, 1); toc;
        end
        
        aDataset = setImage(aDataset, img, t, -1, c);
        
    end
    
end

aDataset = resetMinMaxValues(aDataset);

quSetDataset(hQu, aDataset);

quRedraw(hQu);
